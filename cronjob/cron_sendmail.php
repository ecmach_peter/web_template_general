<?php
	include_once 'include/config_sys.php';
	if(isset($_GET['auth']) && $_GET['auth'] == MAIL_CRON_AUTH){
		require_once 'include/dblink.php';
		include_once 'include/class.crud.basic.php';
		include_once 'include/sendemail.php';
		include_once 'include/mail_content.php';
		
		header('Content-Type: text/plain');
		$crud = new crud_basic($DB_con);

		$_MAIL_LINK = MAIL_LINK;

		$contact_emailus = $crud->select('email_send_temp', array('send_hold' => 0));
		if(count($contact_emailus) > 0){
			$mail_content = new mail_content($crud);
			foreach($contact_emailus as $data){
				$content = unserialize($data['send_data']);
				$_MAIL_SENDTO = MAIL_SENDTO;

				//密件副本
				$arr_mail_bcc = array(
								);
				$arr_images = array('logo'=>'images/mail_logo.png', 'footer'=>'images/mail_footer.png');
				switch ($data['send_type']) {
					//聯絡我們
					case 'contact':
						$arr_content = $mail_content->mail_contact($content);
						$arr_mail_to = array(
											$_MAIL_SENDTO => MAIL_FROMNAME
										);
						break;
					//忘記密碼
					case 'forget':
						$arr_content = $mail_content->mail_forgot($content);
						$arr_mail_to = array(
											$content['email_sendto'] => MAIL_FROMNAME
										);
						break;
				}

				$_MAIL_TITLE = $arr_content['title'];
				$txtMessage = $arr_content['txt'];
				$htmlMessage = $arr_content['html'];

	 			$emailUs = sendEmail(
 								$mail, 
								SITENAME.'-'.$_MAIL_TITLE,	// subject
								$htmlMessage,				// HTML message
								$txtMessage,				// plain text message
								$arr_mail_to,				// recipients
								array(),					// CC
								$arr_mail_bcc,				// BCC
								array(),					// Reply to
								$arr_images					// Embedded images
							); //管理者
				
				if($emailUs){
					// $crud->update('contact', array('contact_emailus'=>1), array('contact_id'=>$contact['contact_id']));
					$crud->delete('email_send_temp', array('send_id' => $data['send_id']));
					if(DEMO_MAIL)echo "contact_emailus - OK\n\n";
				}
				else{
					if(DEMO_MAIL)echo 'contact_emailus - FAILED - ID: '.$data['send_id']."\n";
				}
			}
			// sleep(1);
		}
		if(DEMO_MAIL)echo "\nfinished!";
	}
	exit;
?>