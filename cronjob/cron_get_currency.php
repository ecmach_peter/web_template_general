<?php
	$is_cron = false;
	if(isset($_GET['auth']) && $_GET['auth'] == 'a2CWXGhnkl4KQDa'){
		header('Content-Type: text/json; charset=utf-8');
		include_once './include/dblink.php';
		include_once './include/class.crud.basic.php';
		include_once './include/function.php';
		$crud = new crud_basic($DB_con);
		$is_cron = true;
	}
	else if($_GET['auth'] <> 'QmrMMlYXG181JbR' || !isset($crud)){
		header('HTTP/1.0 403 Forbidden');
		exit;
	}

	//取得需要抓取的幣別
	$arr_chk_query = $crud->sql("SELECT `currency_name`, CONCAT(`currency_name`, 'USD') AS `currency_name_USD`
								FROM `currency`");
	$arr_all_currency = array();
	foreach ($arr_chk_query as $value) {
		$arr_query[] = $value['currency_name'].'USD';
		$arr_all_currency[] = $value['currency_name'];
	}
	$str_query = join(',', $arr_query);

	//若為cron job才抓取
	if($is_cron){
		/* 使用YAHOO服務
			Using the Public API (without authentication), you are limited to 2,000 requests per hour per IP (or up to a total of 48,000 requests a day).
			Using the Private API (with OAuth authentication using an API Key), you are limited to 20,000 requests per hour per IP and you are limited to 100,000 requests per day per API Key.
		*/

		$arr_in_currency_database = array_column($arr_chk_query, 'currency_name_USD');
		// print_r($arr_in_currency_database);exit;
		$query_url = 'https://query.yahooapis.com/v1/public/yql?q=select%20*%20from%20yahoo.finance.xchange%20where%20pair%20in%20(%22'.$str_query.'%22)&format=json&env=store://datatables.org/alltableswithkeys&callback=';

		$str_now_time = date('Y-m-d H:i:s');
		$get_JSON = json_decode(getcurl($query_url));
		if($get_JSON->query->results){
			foreach ($get_JSON->query->results->rate as $value) {
				$arr_currency_data[$value->id] = $value->Rate;

				if(count($arr_in_currency_database) > 0 && array_search($value->id, $arr_in_currency_database) !== false){
					$arr_data = array(
									'currency_time' => $str_now_time,
									'currency_rate' => $value->Rate,
									'currency_date' => date("Y-m-d", strtotime($value->Date)),
								);
					$crud->update('currency', $arr_data, array('currency_name' => substr($value->id, 0, 3)));
				}
				else{
					$arr_data = array(
									'currency_time' => $str_now_time,
									'currency_name' => $value->id,
									'currency_rate' => $value->Rate,
									'currency_date' => date("Y-m-d", strtotime($value->Date)),
								);
					$crud->create('currency', $arr_data);
				}
			}
		}
		// print_r($arr_currency_data);exit;
	}

	//從資料庫取資出匯率
	$arr_currency = $crud->sql("SELECT `currency_rate`, CONCAT(`currency_name`, 'USD') AS `currency_name_USD`
								FROM `currency`");
	$arr_currency_data = array();
	foreach ($arr_currency as $value) {
		$arr_currency_data[$value['currency_name_USD']] = $value['currency_rate'];
	}
	// print_r($arr_currency_data);exit;


	//產品價格處理
	//將已在資料庫的資料存起來，避免SQL不斷重複查詢
	$arr_product_price_currency = $crud->select('product_price_currency', array());
	if(count($arr_product_price_currency) > 0){
		foreach ($arr_product_price_currency as $key => $value) {
			$arr_in_database[$value['product_id']][$value['product_currency']] = array(
																					'default' => $value['product_default_price'], 
																					'start' => $value['product_start_price'], 
																				);
		}
	}
	// print_r($arr_in_database);exit;

	$arr_product = $crud->sql("SELECT *
							FROM `product`");
	if(count($arr_product) > 0){
		foreach ($arr_product as $key => $value) {
			//將所有幣別轉為美金
			$_product_default_price_USD = $value['product_default_price'] * $arr_currency_data[$value['product_currency'].'USD'];
			$_product_start_price_USD = $value['product_start_price'] * $arr_currency_data[$value['product_currency'].'USD'];
			$arr_product_price_currency = array(
												'product_id' => $value['product_id'],
												// 'product_currency' => $value['product_currency'],
											);

			//將產品幣別由美金轉換為各幣別
			foreach ($arr_all_currency as $currency) {
				$_product_default_price = round($_product_default_price_USD / $arr_currency_data[$currency.'USD']);
				$_product_start_price = round($_product_start_price_USD / $arr_currency_data[$currency.'USD']);

				//資料處理
				$arr_databace = (isset($arr_in_database[$value['product_id']]))? $arr_in_database[$value['product_id']]:array();

				//確認資料庫是否已有此幣別
				if(isset($arr_in_database[$value['product_id']][$currency])){
					//確認新舊資料是否相同，若相同則不修改
					if($arr_in_database[$value['product_id']][$currency]['default'] <> $_product_default_price || $arr_in_database[$value['product_id']][$currency]['start'] <> $_product_start_price){
						$arr_data = array(
										'product_default_price' => $_product_default_price,
										'product_start_price' => $_product_start_price,
									);
						$crud->update('product_price_currency', $arr_data, array('product_id' => $value['product_id'], 'product_currency' => $currency));
					}
				}
				else{
					$arr_data = array(
									'product_id' => $value['product_id'],
									'product_currency' => $currency,
									'product_default_price' => $_product_default_price,
									'product_start_price' => $_product_start_price,
								);
					$crud->create('product_price_currency', $arr_data);
				}

				// print_r($arr_data);
			}
		}
	}

	//會員價格處理
	//將已在資料庫的資料存起來，避免SQL不斷重複查詢
	$arr_product_price_currency = $crud->select('product_selling_price_currency', array());
	$arr_in_database = array();
	if(count($arr_product_price_currency) > 0){
		foreach ($arr_product_price_currency as $key => $value) {
			$arr_in_database[$value['selling_id']][$value['product_currency']] = $value['selling_price'];
		}
	}
	// print_r($arr_in_database);exit;

	$arr_product_selling = $crud->sql("SELECT `selling_id`, `selling_price`, `product_currency`
										FROM `product_selling_price`
										INNER JOIN `product` ON `product_selling_price`.`product_id` = `product`.`product_id`");
	//--------------------------------------------------------------------------------------
	if(count($arr_product_selling) > 0){
		foreach ($arr_product_selling as $key => $value) {
			//將所有幣別轉為美金
			$_selling_price_USD = $value['selling_price'] * $arr_currency_data[$value['product_currency'].'USD'];
			$arr_product_price_currency = array(
												'selling_id' => $value['selling_id'],
												// 'product_currency' => $value['product_currency'],
											);

			//將產品幣別由美金轉換為各幣別
			foreach ($arr_all_currency as $currency) {
				$_selling_price = round($_selling_price_USD / $arr_currency_data[$currency.'USD']);

				//資料處理
				$arr_databace = (isset($arr_in_database[$value['selling_id']]))? $arr_in_database[$value['selling_id']]:array();

				//確認資料庫是否已有此幣別
				if(isset($arr_in_database[$value['selling_id']][$currency])){
					//確認新舊資料是否相同，若相同則不修改
					if($arr_in_database[$value['selling_id']][$currency] <> $_selling_price){
						$arr_data = array(
										'selling_price' => $_selling_price,
									);
						$crud->update('product_selling_price_currency', $arr_data, array('selling_id' => $value['selling_id'], 'product_currency' => $currency));
					}
				}
				else{
					$arr_data = array(
									'selling_id' => $value['selling_id'],
									'product_currency' => $currency,
									'selling_price' => $_selling_price,
								);
					$crud->create('product_selling_price_currency', $arr_data);
				}

				// print_r($arr_data);
			}
		}
	}
?>