<?php
	function getcurl($xurl){
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_URL,$xurl);
		curl_setopt($ch, CURLOPT_USERAGENT, "Mozilla/4.0 (compatible;)");
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION,true); //true ,false
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false); /* SSL prevent */
		//curl_setopt($ch, CURLOPT_USERAGENT, "Google Bot");
		$ffp = curl_exec($ch);
		curl_close($ch);
		return $ffp;
	}
	
	function error($pub, $pvt = '')
	{
		$msg = $pub;
		if(DEBUG && $pvt !== ''){
			$msg .= ": $pvt";
			exit("An error occurred ($msg).");
		}
	}

	function iizi_random_bytes($length = 32){
		if (function_exists('random_bytes')){ // only php 7
			return bin2hex(random_bytes($length));
		}
		return bin2hex(openssl_random_pseudo_bytes($length));
	}
	
	function get_post_var($var) // remove magic_quotes if necessary
	{
		if(isset($_POST[$var])){
			$val = $_POST[$var];
			if(get_magic_quotes_gpc())
				$val = stripslashes($val);
			return $val;
		}else
			return false;
	}

	function start_session($expire = 0){
		if ($expire == 0) {
			$expire = ini_get('session.gc_maxlifetime');
		} else {
			ini_set('session.gc_maxlifetime', $expire);
		}

		if (empty($_COOKIE['PHPSESSID'])) {
			session_set_cookie_params($expire);
			session_start();
		} else {
			session_start();
			setcookie('PHPSESSID', session_id(), time() + $expire);
		}
	}

	function custom_number_format($n, $precision = 1) {
		if($n > 1000000000000) return round(($n / 1000000000000), $precision).'T';
        else if($n > 1000000000) return round(($n / 1000000000), $precision).'B';
        else if($n > 1000000) return round(($n / 1000000), $precision).'M';
        else if($n > 1000) return round(($n / 1000), $precision).'K';

		return $n;
	}
	
	function encrypt($data, $key) {
		$key = md5($key);
		$x = 0;
		$len = strlen($data);
		$l = strlen($key);
		for ($i = 0; $i < $len; $i++){
			if ($x == $l){
				$x = 0;
			}
			$char .= $key{$x};
			$x++;
		}
		for ($i = 0; $i < $len; $i++){
			$str .= chr(ord($data{$i}) + (ord($char{$i})) % 256);
		}
		return base64_encode($str);
	}

	function decrypt($data, $key) { 
		$key = md5($key);
		$x = 0;
		$data = base64_decode($data);
		$len = strlen($data);
		$l = strlen($key);
		for ($i = 0; $i < $len; $i++){
			if ($x == $l){
				$x = 0;
			}
			$char .= substr($key, $x, 1);
			$x++;
		}
		for ($i = 0; $i < $len; $i++){
			if (ord(substr($data, $i, 1)) < ord(substr($char, $i, 1))){
				$str .= chr((ord(substr($data, $i, 1)) + 256) - ord(substr($char, $i, 1)));
			}
			else {
				$str .= chr(ord(substr($data, $i, 1)) - ord(substr($char, $i, 1)));
			}
		}
		return $str;
	}
	

	function get_serial_number($key, $type, $bits = 4){
		global $crud;
		$arr_num = $crud->getid('number_control', array('number_type' => $type, 'number_key' => $key));
		if(empty($arr_num['number_id'])){
			$new_num = 1;
			$result = $crud->create('number_control', array('number_type' => $type, 'number_cout' => 1, 'number_key' => $key));
		}
		else{
			$new_num = $arr_num['number_cout'] + 1;
			$result = $crud->update('number_control', array('number_cout' => $new_num), array('number_id' => $arr_num['number_id']));
		}
		$serial_number = str_pad($new_num, $bits, '0', STR_PAD_LEFT);   //下注編號

		return $serial_number;
	}
	//sprintf 使用array key方式，較好閱讀
	function vksprintf($str, $args) { 
		if (is_object($args)) { 
			$args = get_object_vars($args); 
		} 
		$map = array_flip(array_keys($args)); 
		$new_str = preg_replace_callback('/(^|[^%])%([a-zA-Z0-9_-]+)\$/', 
				function($m) use ($map) { return $m[1].'%'.($map[$m[2]] + 1).'$'; }, 
				$str); 
		return vsprintf($new_str, $args); 
	} 

	//執行非同步處理
	function async_connection($host, $url){
		$fp = @stream_socket_client("ssl://{$host}:443", $errno, $errstr, 30);
		if (!$fp) {
			// echo "$errstr ($errno)<br />\n";
		}
		else {
			stream_set_blocking($fp, 0);
			$out = "GET /{$url} / HTTP/1.1\r\n";
			$out .= "Host: {$host}\r\n";
			$out .= "Connection: Close\r\n\r\n";
		 
			fwrite($fp, $out);
			/*忽略執行結果*/
			// while (!feof($fp)) {
			// 	fgets($fp, 128);
			// }
			fclose($fp);
		}
	}

	function getYoutubeID($ytURL) {
		preg_match("/^(?:http(?:s)?:\/\/)?(?:www\.)?(?:m\.)?(?:youtu\.be\/|youtube\.com\/(?:(?:watch)?\?(?:.*&)?v(?:i)?=|(?:embed|v|vi|user)\/))([^\?&\"'>]+)/", $ytURL, $matches);
		return $matches[1];
	}
?>