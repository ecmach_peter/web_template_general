<?php
	ini_set('error_reporting', E_ALL ^ E_NOTICE);
	// DEBUG is meant to be disabled on a 'production install' to avoid leaking server setup details
	define('DEBUG', false); // Debugging
	define('BASE', '/');	// Install folder on server if using subfolder
	define('PROTOCOL', (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == "on")? "https":"http");

	/** 時區設定 **/
	date_default_timezone_set('Asia/Taipei');
	$now_date = date('Y-m-d');
	$now_date_time = date('Y-m-d H:i:s');

	/** SESSION */
	define('SESSION_TIMEOUT', 3600); //1hour
	define('SESSION_NAME', 'LETAO_ALEX');	//SESSION命名用

	// ** 網站參數設定 ** //
	define('RECACHE_NUM', '001');
	define('FAVICON', 'frontend/img/sys/favicon.png?'.RECACHE_NUM);
	define('LANGUAGE', false);
	define('DESCRIPTION', '');
	define('KEYWORDS', '');
	define('OG_IMAGE', PROTOCOL."://{$_SERVER['HTTP_HOST']}".BASE.'frontend/img/sys/logo.png');


	// ** MySQL 設定 ** //
	/** 的資料庫名稱，請更改 'database_name_here' */
	define('DB_NAME', 'ecmachco_alex');

	/** MySQL 資料庫使用者名稱，請更改 'username_here' */
	define('DB_USER', 'ecmachco_alex');

	/** MySQL 資料庫密碼，請更改 'password_here' */
	define('DB_PASSWORD', 'N38ET=82mIv?');

	/** MySQL 主機位址 */
	define('DB_HOST', 'localhost');

	/** 建立資料表時預設的文字編碼 */
	define('DB_CHARSET', 'utf8');


	/**** MAIL SETTINGS ****/
	define('DEMO_MAIL', false);  //測試

	/* 服務參數 */
	define('MAIL_LINK', 'https://alexengconsulting.com'.BASE);
	define('MAIL_CRON_AUTH', 'RNp5C8iDlvDT0QGpaw5U');
	if(DEMO_MAIL){
		define('MAIL_HOST', 'alexengconsulting.com');
		define('MAIL_USERNAME', 'no_reply@alexengconsulting.com');
		define('MAIL_PASSWORD', 'H66vHT8ME^nC');
		define('MAIL_FROMNAME', SITENAME);
		define('MAIL_SENDTO', 'info@alexengconsulting.com');
	}else{  //客戶實際參數
		define('MAIL_HOST', 'alexengconsulting.com');
		define('MAIL_USERNAME', 'no_reply@alexengconsulting.com');
		define('MAIL_PASSWORD', 'H66vHT8ME^nC');
		define('MAIL_FROMNAME', SITENAME);
		define('MAIL_SENDTO', 'info@alexengconsulting.com');
	}

	// ** 多語系網址處理 ** //
	define('LANGUAGE_DEFAULT_DOMAIN', 'alexengconsulting.com');
	define('LANGUAGE_EN_DOMAIN', 'en.alexengconsulting.com');

	// ** Piwik ** //
	define('PIWIKLINK', false);
	define('PIWIKIDSITE', '32');		//網站ID
	define('PIWIKTOKEN', '2d74948f0b70c8613c4e2d1c30a2fc3a');	//Piwik Token

	/* ALLPAY 參數 */
	define('ALLPAY_DEBUG', true);
	//綠界限制金額
	define('ALLPAY_CREDIT_LIMIT', 500000);
	if(ALLPAY_DEBUG){
		//金流
		define('ALLPAY_AIOCHECKOUT', 'https://payment-stage.ecpay.com.tw/Cashier/AioCheckOut/V5');
		define('ALLPAY_SERVICEURL', 'https://payment-stage.ecpay.com.tw/Cashier/AioCheckOut/V5');
		define('ALLPAY_HASHKEY', '5294y06JbISpM5x9');
		define('ALLPAY_HASHIV', 'v77hoKGq4kWxNNIS');
		define('ALLPAY_MERCHANTID', '2000132');
	}
	else{
		define('ALLPAY_AIOCHECKOUT', 'https://payment.ecpay.com.tw/Cashier/AioCheckOut/V5');
		define('ALLPAY_SERVICEURL', 'https://payment.ecpay.com.tw/Cashier/AioCheckOut/V5');
		define('ALLPAY_HASHKEY', '');
		define('ALLPAY_HASHIV', '');
		define('ALLPAY_MERCHANTID', '');
	}
		
	//返回網址(注意HTTP協定)
	define('ALLPAY_RETURNURL', PROTOCOL."://{$_SERVER['HTTP_HOST']}".BASE.'allpay_return');
	define('ALLPAY_RETURNURL_FOR_FULLMOON', PROTOCOL."://{$_SERVER['HTTP_HOST']}".BASE.'allpay_return_fullmoon');
	define('ALLPAY_ORDERRESULTURL', PROTOCOL."://{$_SERVER['HTTP_HOST']}".BASE.'paid');
	define('ALLPAY_PAYMENTINFOURL', PROTOCOL."://{$_SERVER['HTTP_HOST']}".BASE.'allpay_getcode');	//取號完成通知頁面(server)
	define('ALLPAY_CLIENTREDIRECTURL', PROTOCOL."://{$_SERVER['HTTP_HOST']}".BASE.'payment_info');			//取號完成說明頁面

	/* Google recaptcha */
	if(DEBUG){
		define('RECAPTCHA_SITEKEY', '6LeIxAcTAAAAAJcZVRqyHh71UMIEGNQ_MXjiZKhI');	//6LdNfAkUAAAAAE9pjs5Pp0_HiSMOTuGcVO52Qx_Y
		define('RECAPTCHA_SECRETKEY', '6LeIxAcTAAAAAGG-vFI1TnRWxMZNFuojJ4WifJWe');	//6LdNfAkUAAAAAEyaJLUAa73VTJsyPU_rD28ajW-l
	}
	else{
		define('RECAPTCHA_SITEKEY', '6LdmpDUUAAAAAMnGsm8loIXg3VpIzCOaNU5KG9Jy');	//6LdNfAkUAAAAAE9pjs5Pp0_HiSMOTuGcVO52Qx_Y
		define('RECAPTCHA_SECRETKEY', '6LdmpDUUAAAAAGXfTMXJZBsRx8LdW5Y-hcQ2qZfi');	//6LdNfAkUAAAAAEyaJLUAa73VTJsyPU_rD28ajW-l
	}

	/** CDN for JAVA SCRIPT and STYLESHEETS */
	define('CONFIG_USE_CDN', false);

	/*
	Subresource Integrity (SRI) - https://www.w3.org/TR/SRI/
	About SRI https://hacks.mozilla.org/2015/09/subresource-integrity-in-firefox-43/
	Generate SRI hashes at https://report-uri.io/home/sri_hash

	Example:
	<script src='<?php if(CONFIG_USE_CDN) echo CONFIG_CDN_JQUERY_MIN_JS; else echo CONFIG_LOCAL_JQUERY_MIN_JS; ?>' type='text/javascript' integrity='<?php echo CONFIG_INTEGRITY_JQUERY_MIN_JS;?>' crossorigin='anonymous'></script>
	*/

	define('CONFIG_CDN_BOOTSTRAP_MIN_CSS', 'https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css');
	define('CONFIG_LOCAL_BOOTSTRAP_MIN_CSS', 'css/bootstrap.min.css');
	define('CONFIG_INTEGRITY_BOOTSTRAP_MIN_CSS', 'sha256-7s5uDGW3AHqw6xtJmNNtr+OBRJUlgkNJEo78P4b0yRw= sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7 sha512-nNo+yCHEyn0smMxSswnf/OnX6/KwJuZTlNZBjauKhTK0c+zT+q5JOCx0UFhXQ6rJR9jg6Es8gPuD2uZcYDLqSw==');

	define('CONFIG_CDN_FONT_AWESOME_MIN_CSS', 'https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css');
	define('CONFIG_LOCAL_FONT_AWESOME_MIN_CSS', 'css/font-awesome.min.css');
	define('CONFIG_INTEGRITY_FONT_AWESOME_MIN_CSS', 'sha256-AIodEDkC8V/bHBkfyxzolUMw57jeQ9CauwhVW6YJ9CA= sha384-T8Gy5hrqNKT+hzMclPo118YTQO6cYprQmhrYwIiQ/3axmI1hQomh7Ud2hPOy8SP1 sha512-4uGZHpbDliNxiAv/QzZNo/yb2FtAX+qiDb7ypBWiEdJQX8Pugp8M6il5SRkN8jQrDLWsh3rrPDSXRf3DwFYM6g==');

	define('CONFIG_CDN_JQUERY_UI_CSS', 'https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/themes/smoothness/jquery-ui.css');
	define('CONFIG_LOCAL_JQUERY_UI_CSS', 'css/jquery-ui.css');
	define('CONFIG_INTEGRITY_JQUERY_UI_CSS', 'sha256-f45CCkrD6n9v0IHOByNBAUFNJ98mCm1UdmP44MDvuvQ= sha384-rHEtTzUO+F6AxgVJs86ZaDoE9sm0tDz/xaSt1ohRCJoeu4gkMIUvcA4cnm60Em/k sha512-EgzRsIhImqd4KENZvewCbE/darokbIO23LzPI0PvUEdCtOTuKiIN50Kenx2REZpkMcSw4YFYwcudUAZ3eLuFxw==');

	define('CONFIG_CDN_BOOTSTRAP_THEME_MIN_CSS', 'https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap-theme.min.css');
	define('CONFIG_LOCAL_BOOTSTRAP_THEME_MIN_CSS', 'css/bootstrap-theme.min.css');
	define('CONFIG_INTEGRITY_BOOTSTRAP_THEME_MIN_CSS', 'sha256-o8bM0Z5cFvrvvvQp0EJFi4LICvBA9FCx7iCNuojVsN8= sha384-fLW2N01lMqjakBkx3l/M9EahuwpSfeNvV63J5ezn3uZzapT0u7EYsXMjQV+0En5r sha512-kgrXVPaJi1gUqEbb3lVdpJ3tWjfCfEUGD4tKm5GpUW1Gy6PIq3n89kk0zEIWxgBlfpaZD7lcmTFdHL8u5eSzug==');

	define('CONFIG_CDN_BOOTSTRAP_MIN_JS', 'https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js');
	define('CONFIG_LOCAL_BOOTSTRAP_MIN_JS', 'js/bootstrap.min.js');
	define('CONFIG_INTEGRITY_BOOTSTRAP_MIN_JS', 'sha256-KXn5puMvxCw+dAYznun+drMdG1IFl3agK0p/pqT9KAo= sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS sha512-2e8qq0ETcfWRI4HJBzQiA3UoyFk6tbNyG+qSaIBZLyW9Xf3sWZHN/lxe9fTh1U45DpPf07yj94KsUHHWe4Yk1A==');

	define('CONFIG_CDN_JQUERY_MIN_JS', 'https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js');
	define('CONFIG_LOCAL_JQUERY_MIN_JS', 'js/jquery.min.js');
	define('CONFIG_INTEGRITY_JQUERY_MIN_JS', 'sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44= sha384-rY/jv8mMhqDabXSo+UCggqKtdmBfd3qC2/KvyTDNQ6PcUJXaxK1tMepoQda4g5vB sha512-DUC8yqWf7ez3JD1jszxCWSVB0DMP78eOyBpMa5aJki1bIRARykviOuImIczkxlj1KhVSyS16w2FSQetkD4UU2w==');

	define('CONFIG_CDN_JQUERY_UI_MIN_JS', 'https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js');
	define('CONFIG_LOCAL_JQUERY_UI_MIN_JS', 'js/jquery-ui.min.js');
	define('CONFIG_INTEGRITY_JQUERY_UI_MIN_JS', 'sha256-xNjb53/rY+WmG+4L6tTl9m6PpqknWZvRt0rO1SRnJzw= sha384-YWP9O4NjmcGo4oEJFXvvYSEzuHIvey+LbXkBNJ1Kd0yfugEZN9NCQNpRYBVC1RvA sha512-BHDCWLtdp0XpAFccP2NifCbJfYoYhsRSZOUM3KnAxy2b/Ay3Bn91frud+3A95brA4wDWV3yEOZrJqgV8aZRXUQ==');
?>