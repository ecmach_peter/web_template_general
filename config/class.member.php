<?php
class member{
	private $crud;

	function __construct($crud){
		$this->crud = $crud;
	}

	//註冊會員
	public function register($account, $password, $email, $type = 1){
		$uniqid = base64_encode(iizi_random_bytes(3)).uniqid();
		$hashed_password = password_hash($password, PASSWORD_DEFAULT, array('cost'=>12));
		$arr_data = array(
						'member_uniqid' => $uniqid,
						'member_calling_code' => $account[0],
						'member_account' => $account[1],
						'member_password' => $hashed_password,
						'member_name' => get_post_var('name'),
						'member_email' => $email,
						'member_online' => 1,
						'member_type' => $type,
						'member_register' => date('Y-m-d H:i:s'),
						'member_lastlogin' => date('Y-m-d H:i:s'),
						'member_mobile1' => '+'.join('', $account)
					);
		$this->crud->create('member', $arr_data);

		//-----信箱驗證暫時關閉
		// $code = substr(base64_encode(iizi_random_bytes(2)), 0, 6);
		// $email_verify_id = $this->crud->create('email_verify', array('user_uniqid'=>$uniqid, 'email_verify_code_hash'=>sha1($code), 'email_verify_created_on'=>date('Y-m-d H:i:s'), 'email_verify_registration'=>1));
			
		// $send_data = serialize(array('email_verify_code'=>$code, 'email_verify_id'=>$email_verify_id, 'email_sendto'=>$account, 'email_register_datetime'=>date('Y-m-d H:i:s')));
		// $return = $this->crud->create('email_send_temp', array('send_type'=>1, 'send_data'=>$send_data));
		
		// if($email_verify_id !== false){
		// 	$send_data = serialize(array('email_verify_code'=>$code, 'email_verify_id'=>$email_verify_id, 'email_sendto'=>$account, 'email_register_datetime'=>date('Y-m-d H:i:s')));
		// 	$return = $this->crud->create('email_send_temp', array('send_type'=>1, 'send_data'=>$send_data));
		// }

		$_SESSION[SESSION_NAME.'_user_uniqid'] = $uniqid;
		$_SESSION[SESSION_NAME.'_user_login_time'] = date('Y-m-d H:i:s');
	}

	//會員登入
	public function login($account, $password){
		if($account[1] !== false || $password !== false){
			$data = $this->crud->getid('member', array('member_calling_code' => $account[0], 'member_account' => $account[1], 'member_online' => 1));
			if(empty($data)){
				password_verify($password , '$2y$12$ADmKm8nj1NRWcrE8Xoc4ouBkZnjTnwqU3cozL300QEZGYIUpG83pG'); // delay redirect with random password check to prevent leaking valid login emails
				$arr['sts'] = false;
				$arr['msg'] = '帳號或密碼錯誤!';
			}
			else if($data['member_type'] == 2){
				if($password === true){
					$this->do_session($data['member_uniqid'], $data['member_account']);
					$arr['sts'] = true;
				}
				else{
					$arr['sts'] = false;
					$arr['msg'] = '您為Facebook會員，請使用Facebook登入!';
				}
			}
			else{
				if(!password_verify($password , $data['member_password'])){
					$arr['sts'] = false;
					$arr['msg'] = '帳號或密碼錯誤!';
				}
				else{
					$this->do_session($data['member_uniqid'], $data['member_account']);

					$arr['sts'] = true;
					
					if(get_post_var('rememberme') !== false){ // handling of the remember me cookie
						require_once 'include/dblink.userdata.php';
						$pdo_user = new db_pdo($DB_user);
						
						$Auth_selector = base64_encode(iizi_random_bytes(9)).uniqid();
						$Auth_token = base64_encode(iizi_random_bytes(21));
						$Auth_expires = date('Y-m-d H:i:s', strtotime('+2 weeks +4 hours'));
						$Auth_array = array('user_uniqid' => $data['member_uniqid'], 'selector' => $Auth_selector, 'token_hash' => password_hash($Auth_token, PASSWORD_DEFAULT, array('cost' => 10)), 'expires' => $Auth_expires);

						if($pdo_user->create('user_remember_me_auth', $Auth_array) !== false){
							setcookie(
								'HODOR',
								 $Auth_selector.':'.$Auth_token,
								 strtotime($Auth_expires),
								 '/',
								 $_SERVER['HTTP_HOST'],
								 false, // TLS-only
								 true  // http-only
							);
							header('refresh: 1; url='.FULL_BASE_URL.'panel');
							echo 'Processing, please wait...';
							exit;
						}
					}
					// header(sprintf('Location: %s', FULL_BASE_URL.'panel')); exit;
				}
			}
		}
		else{
			$arr['sts'] = false;
			$arr['msg'] = '帳號或密碼錯誤!';
		}

		return $arr;
	}

	private function do_session($uniqid, $account){
		$_SESSION[SESSION_NAME.'_user_uniqid'] =  $uniqid;
		$_SESSION[SESSION_NAME.'_user_login_time'] =  date('Y-m-d H:i:s');

		$this->crud->update('member', array('member_lastlogin' => date('Y-m-d H:i:s')), array('member_uniqid' => $uniqid));
	}
}