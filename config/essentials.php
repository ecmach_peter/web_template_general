<?php
session_start();

/* echo '<pre>';
var_dump($_SESSION);
echo '</pre>';
echo '<pre>';
var_dump(session_id());
echo '</pre>'; */

// reauthenticate if browser changes (session hijacking prevention)
if(isset($_SESSION['S_user_agent']) && strcmp($_SESSION['S_user_agent'], isset($_SERVER['HTTP_USER_AGENT'])?$_SERVER['HTTP_USER_AGENT']:'')!==0){
	session_unset();
	session_regenerate_id(true);	
	session_destroy();
	header(sprintf('Location: %s', FULL_BASE_URL.'login?e=2')); exit; // need to login again S_user_agent changed
}

function get_ip_address_session(){
	if(array_key_exists('addr', $_GET) )
		$ip = $_GET['addr'];
	else
		$ip = $_SERVER['REMOTE_ADDR'];

	$v4 = preg_match("/^([0-9]{1,3}\.){3}[0-9]{1,3}$/", $ip);
	
	//ipv6 not supported by server
/* 	$v6 = preg_match("/^[0-9a-f]{1,4}:([0-9a-f]{0,4}:){1,6}[0-9a-f]{1,4}$/", $ip);
	if($v6!=0){
		return $ip;
	} */

	if($v4!=0){ //allow IPs from same subnet.
		$pos = strrpos($ip, '.');
		if($pos !== false){
			$ip = substr($ip, 0, $pos+1);
		}
		return $ip.'0/24';
	}
}

// reauthenticate if ip range of client changes (session hijacking prevention)
 if(isset($_SESSION['S_ip_address_range']) && strcmp($_SESSION['S_ip_address_range'], get_ip_address_session())!==0){
	session_unset();
	session_destroy();
	header(sprintf('Location: %s', FULL_BASE_URL.'login?e=3')); exit; // need to login again S_ip_address_range changed
}
 
 // on new session
 if(!isset($_SESSION['S_created'])){
	 $_SESSION['S_created'] = time();
	 ini_set('session.gc_maxlifetime', SESSION_TIMEOUT);
	 $_SESSION['S_user_agent'] = (isset($_SERVER['HTTP_USER_AGENT']))?$_SERVER['HTTP_USER_AGENT']:'';
	 $_SESSION['S_ip_address_range_range'] = get_ip_address_session();
 }
 
 // change sesson id every 30 minutes (prevents session id fixation)
if(isset($_SESSION['S_created']) && (time() - $_SESSION['S_created'] > 1800)){
	session_regenerate_id(true);			// change session ID for the current session and invalidate old session ID
	$_SESSION['S_created'] = time();	// update creation time
}

// session times out
if(isset($_SESSION['S_last_activity']) && (time() - $_SESSION['S_last_activity'] > SESSION_TIMEOUT)){
    session_unset();     // unset $_SESSION variable for the run-time 
    session_destroy();   // destroy session data in storage
	header(sprintf('Location: %s', FULL_BASE_URL.'login?e=4')); exit; // need to login again Session timed out
}
 $_SESSION['S_last_activity'] = time(); // update last activity time stamp

// load language strings
// if(isset($_SESSION['lang']) && strlen($_SESSION['lang'])<3)
// {
// 	$supported_languages = array('tw', 'en');
	
// 	if(in_array($_SESSION['lang'], $supported_languages)){
// 		require_once 'lang/'.$_SESSION['lang'].'.lang.php';
// 	}
// 	else
// 		require_once 'lang/'.DEFAULT_LANGUAGE.'.lang.php';
// }else
// 	require_once 'lang/'.DEFAULT_LANGUAGE.'.lang.php';

function getLangStr($key){
	global $lang_str;
	return isset($lang_str[$key])?$lang_str[$key]:'>>>'.$key.'<<<';
}

// use this function to catch and report errors
// DEBUG is meant to be disabled on a 'production install' to avoid leaking server setup details.
// $pub non sensitive data
// $pvt sensitive data not to be leaked in error messages, but can be seen in DEBUG mode
function error($pub, $pvt = '')
{
	$msg = $pub;
	if(DEBUG && $pvt !== ''){
		$msg .= ": $pvt";
		exit("An error occurred ($msg).");
	}
}

// debug function
if(DEBUG){
	function d($v){
		echo '<pre>';
		var_dump($v);
		echo '</pre>';
	}
}

function get_post_var($var) // remove magic_quotes if necessary
{
	if(isset($_POST[$var])){
		$val = $_POST[$var];
		if(get_magic_quotes_gpc())
			$val = stripslashes($val);
		return $val;
	}else
		return false;
}

// creates random bytes
function iizi_random_bytes($length){
    if(function_exists('random_bytes')){ // only php 7
        return bin2hex(random_bytes($length));
    }
    return bin2hex(openssl_random_pseudo_bytes($length));
}

// creates random strings
function iizi_random_str($length, $space = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ')
{
    $str = '';
    $max = mb_strlen($space, '8bit') - 1;
    for ($i = 0; $i < $length; ++$i) {
		if(function_exists('random_int')){ // only php 7
			$str .= $space[random_int(0, $max)];
		}else{
			$str .= $space[mt_rand(0, $max)];
		}
    }
    return $str;
}
?>
