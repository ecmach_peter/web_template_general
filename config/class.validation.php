<?php
	require_once(dirname(__FILE__).'/sanitize.php'); 
	class Validation{
		public function success($data = '', $filter = false){
			$arr = array('sts' => true,);
			if(!empty($data)){
				$arr['data'] = $data;
			}
			echo (!$filter)? json_encode($arr):preg_replace('/\\\\[rnt]/', '', json_encode($arr));
		}

		public function system_error(){
			throw new Exception('系統異常，請您重新整理後再試，或與客服人員聯絡');
		}

		//驗證必填項目
		#'required' => true,
		#'required' => '請輸入XXXX',
		public function check_required_input($name, $verify_info){
			if(empty(get_post_var($name))){
				$message = ($verify_info === true)? '':$verify_info;
				$this->error_item = $name;
				throw new Exception($message);
			}
		}

		//驗證正規表達
		#'preg' => array("/^(09[0-9]{8,8}+)$/", '手機號碼格式錯誤， e.g. 0912345678'),
		public function check_preg_input($name, $verify_info){
			if(!preg_match($verify_info[0], get_post_var($name))){
				$this->error_item = $name;
				throw new Exception($verify_info[1]);
			}
		}

		//驗證過濾器
		#'filter_var' => array(FILTER_VALIDATE_EMAIL, '電子信箱格式錯誤， e.g: demo@yourmail.com'),
		public function check_filter_var_input($name, $verify_info){
			if(!filter_var(get_post_var($name), $verify_info[0])){
				$this->error_item = $name;
				throw new Exception($verify_info[1]);
			}
		}

		//驗證長度
		#'max_length' => array(80, '您輸入的地址太長了，最多80個字'),
		public function check_max_length_input($name, $verify_info){
			if(!sanitize_input_srt(get_post_var($name), $verify_info[0])){
				$this->error_item = $name;
				throw new Exception($verify_info[1]);
			}
		}

		//驗證密碼
		#'password' => array('8,30', '密碼格式錯誤，請輸入8~30字的英數組合'),
		public function check_password_input($name, $verify_info){
			if(!preg_match("/^(?=.*\d)(?=.*[a-zA-Z]).{{$verify_info[0]}}$/", get_post_var($name)) || preg_match("/[\x7f-\xff]/", get_post_var($name))){
				$this->error_item = $name;
				throw new Exception($verify_info[1]);
			}
		}

		//驗證日期
		/*
			接受格式
			2017-10-13
			2017/10/13
			2017.10.13
			20171013

			$verify_info[0]為預留欄位
		*/
		#'date' => array('', '日期格式錯誤'),
		public function check_date_input($name, $verify_info, $value = ''){
			$value = (empty($value))? get_post_var($name):$value;
			$date = str_replace(array('-', '/', '.'), '', $value);
			$format = 'Ymd';

			$date_format = DateTime::createFromFormat($format, $date);
			if(!($date_format && $date_format->format($format) == $date)){
				$this->error_item = $name;
				throw new Exception($verify_info[1]);
			}
		}

		//驗證日期區間
		#'date_range' => array(' ~ ', '出租期間日期格式錯誤'),
		public function check_date_range_input($name, $verify_info){
			$date = explode($verify_info[0], get_post_var($name));
			foreach($date as $value){
				$this->check_date_input($name, array('', $verify_info[1]), $value);
			}

			if($date[0] > $date[1]){
				throw new Exception('起始日不得大於截止日');
			}
		}

		//驗證過濾器
		#'filter_var' => array(FILTER_VALIDATE_EMAIL, '電子信箱格式錯誤， e.g: demo@yourmail.com'),
		public function check_int_input($name, $verify_info){
			$int_options = array('options' => array('min_range' => $verify_info[0], 'max_range' => $verify_info[1]));
			if(!filter_var(get_post_var($name), FILTER_VALIDATE_INT)){
				$this->error_item = $name;
				throw new Exception($verify_info[2]);
			}
		}

		//驗證檔案必填項目
		#'file_required' => true,
		#'file_required' => '請輸入XXXX',
		public function check_file_required_input($name, $verify_info){
			if(!isset($_FILES[$name])){
				$message = ($verify_info['file_required'] === true)? '':$verify_info['file_required'];
				$this->error_item = $name;
				throw new Exception($message);
			}
		}

		//驗證檔案格式
		#'file_type' => array('jpeg', 'jpg', 'png'),
		public function check_file_type_input($name, $verify_info){
			if(isset($_FILES[$name])){
				if(is_array($_FILES[$name]['name'])){
					$not_allow_extension = array();
					foreach($_FILES[$name]['name'] as $value){
						$files_info = pathinfo($value);
						if(!$this->chk_extension($files_info['extension'], $verify_info)){
							$not_allow_extension[] = $value;
						}
					}

					if(count($not_allow_extension) > 0){
						$message = '請上傳副檔名為 *.'.join(', *.', $verify_info).' 的檔案【'.join('】,【', $not_allow_extension).'】';
						$this->error_item = $name;
						throw new Exception($message);
					}
				}
				else{
					$files_info = pathinfo($_FILES[$name]['name']);
					if(!$this->chk_extension($files_info['extension'], $verify_info)){
						$message = '請上傳副檔名為 *.'.join(', *.', $verify_info).' 的檔案';
						$this->error_item = $name;
						throw new Exception($message);
					}
				}
			}
		}

		//驗證檔案大小
		#'file_size' => 5242880,
		public function check_file_size_input($name, $verify_info){
			if(isset($_FILES[$name])){
				if(is_array($_FILES[$name]['name'])){
					$not_allow_size = array();
					foreach($_FILES[$name]['size'] as $key => $value){
						if($value > $verify_info){
							$not_allow_size[] = $_FILES[$name]['name'][$key];
						}
					}

					if(count($not_allow_size) > 0){
						$message = '檔案【'.join('】,【', $not_allow_size).'】超過檔案大小限制'.$this->filesize_formatted($verify_info);
						$this->error_item = $name;
						throw new Exception($message);
					}
				}
				else{
					if($_FILES[$name]['size'] > $verify_info){
						$message = "檔案【{$_FILES[$name]['name']}】超過檔案大小限制".$this->filesize_formatted($verify_info);
						$this->error_item = $name;
						throw new Exception($message);
					}
				}
			}
		}

		//驗證副檔名
		private function chk_extension($extension, $verify_info){
			return (array_search($extension, $verify_info) === false)? false:true;
		}

		//轉換檔案大小
		private function filesize_formatted($size){
			$units = array('B', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB');
			$power = ($size > 0)? floor(log($size, 1024)):0;
			return number_format($size / pow(1024, $power), 2, '.', ',').' '.$units[$power];
		}
	}
?>