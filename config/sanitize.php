<?php
function sanitize_input_srt($string='', $max_length=10, $encoding='UTF-8'){
	$string = strip_tags($string);
	if(mb_strlen($string, $encoding)>0 && mb_strlen($string, $encoding)<=$max_length)
		return true;
	return false;
}

function sanitize_input_int($int=0, $min=1, $max=10){
	if(is_numeric($int) && $int>=$min && $int<=$max)
		return true;
	return false;
}
?>
