<?php
	class mail_content{
		function __construct($crud){
			$this->crud = $crud;
			$this->_MAIL_LINK = MAIL_LINK;
		}

		//客服中心
		public function mail_contact($arr_content){
			$_content = nl2br($arr_content['contact_message']);
			$arr = array(
						'title' => '聯絡我們',
						'txt' => "姓名： ".$arr_content['contact_name']."\n".
								"填寫時間： ".$arr_content['contact_time']."\n".
								"連絡電話： ".$arr_content['contact_phone']."\n".
								"電子信箱： ".$arr_content['contact_email']."\n".
								"公司名稱： ".$arr_content['contact_company']."\n".
								"國　　家： ".$arr_content['contact_country']."\n".
								"問題內容： \n".$arr_content['contact_message']."\n",
						'html' => <<<HTML
									<!doctype html>
									<html lang="zh-Hant-TW">
									<body>
										<table cellpadding="0" cellspacing="0" border="0" style="width: 550px; margin: 25px auto; text-align: center; background: rgba(255,255,255,.55); padding: 40px; border-radius:20px; border:1px dashed #a59083;">
											<tr>
												<td>
														<p style="margin-bottom: 30px;">
															<a href="{$this->_MAIL_LINK}"><img style="border: 0; text-align: center;" src="images/mail_logo.png" alt="Logo"></a>
														</p>
														<p style="margin:20px 50px;padding:10px 0;text-align: left;border-bottom:1px dashed #999;">姓名：{$arr_content['contact_name']}</p>
														<p style="margin:20px 50px;padding:10px 0;text-align: left;border-bottom:1px dashed #999;">填寫時間：{$arr_content['contact_time']}</p>
														<p style="margin:20px 50px;padding:10px 0;text-align: left;border-bottom:1px dashed #999;">連絡電話：{$arr_content['contact_phone']}</p>
														<p style="margin:20px 50px;padding:10px 0;text-align: left;border-bottom:1px dashed #999;">電子信箱：{$arr_content['contact_email']}</p>
														<p style="margin:20px 50px;padding:10px 0;text-align: left;border-bottom:1px dashed #999;">公司名稱：{$arr_content['contact_company']}</p>
														<p style="margin:20px 50px;padding:10px 0;text-align: left;border-bottom:1px dashed #999;">國　　家：{$arr_content['contact_country']}</p>
														<p style="margin:20px 50px;padding:10px 0;text-align: left;border-bottom:1px dashed #999;border-bottom:none;">問題內容：<br>{$_content}</p>
												</td>
											</tr>
										</table>
									</body>
									</html>
HTML
					);

			return $arr;
		}

		//驗證信件
		public function mail_verify($arr_content){
			$url = "{$this->_MAIL_LINK}mail_verify/{$arr_content['email_verify_id']}/{$arr_content['email_verify_code']}";
			$ex_time = date("Y-m-d H:i:s", strtotime($arr_content['email_verify_created_on']."+20 minute"));
			$arr = array(
						'title' => '驗證信件',
						'txt' => "帳號： {$arr_content['email_sendto']}\n".
								"驗證連結： {$url}\n".
								"驗證期限： {$ex_time}",
						'html' => <<<HTML
									<!doctype html>
									<html lang="zh-Hant-TW">
									<body>
										<table cellpadding="0" cellspacing="0" border="0" style="width: 550px; margin: 25px auto; text-align: center; background: rgba(255,255,255,.55); padding: 40px; border-radius:20px; border:1px dashed #a59083;">
											<tr>
												<td>
													<p style="margin-bottom: 30px;">
														<a href="{$this->_MAIL_LINK}"><img style="border: 0;" src="images/mail_logo.png" alt="Logo"></a>
													</p>
													<p style="margin:20px 50px;padding:10px 0;text-align: left;border-bottom:1px dashed #999;">帳號：{$arr_content['email_sendto']}</p>
													<p style="margin:20px 50px;padding:10px 0;text-align: left;border-bottom:1px dashed #999;"><a href="{$url}">點這裡進行驗證</a></p>
													<p style="margin:20px 50px;padding:10px 0;text-align: left;border-bottom:1px dashed #999;">※請於{$ex_time}之前進行驗證</p>
												</td>
											</tr>
										</table>
									</body>
									</html>
HTML
					);

			return $arr;
		}

		//忘記密碼
		public function mail_forgot($arr_content){
			$url = "{$this->_MAIL_LINK}pwdchange/{$arr_content['email_verify_id']}/{$arr_content['email_verify_code']}";
			$ex_time = date("Y-m-d H:i:s", strtotime($arr_content['email_verify_created_on']."+20 minute"));
			$arr = array(
						'title' => '忘記密碼資訊',
						'txt' => "帳號： {$arr_content['email_account']}\n".
								"修改連結： {$url}\n".
								"修改期限： {$ex_time}",
						'html' => <<<HTML
									<!doctype html>
									<html lang="zh-Hant-TW">
									<body>
									<div>
										<table cellpadding="0" cellspacing="0" border="0" style="width: 550px; margin: 25px auto; text-align: center; background: rgba(255,255,255,.55); padding: 40px; border-radius:20px; border:1px dashed #a59083;">
											<tr>
												<td>
													<p style="margin-bottom: 30px;">
														<a href="{$this->_MAIL_LINK}"><img style="border: 0;" src="images/mail_logo.png" alt="Logo"></a>
													</p>
													<p style="margin:20px 50px;padding:10px 0;text-align: left;border-bottom:1px dashed #999;">帳號：{$arr_content['email_account']}</p>
													<p style="margin:20px 50px;padding:10px 0;text-align: left;border-bottom:1px dashed #999;"><a href="{$url}">點這裡進行修改</a></p>
													<p style="margin:20px 50px;padding:10px 0;text-align: left;border-bottom:1px dashed #999;">※請於{$ex_time}之前進行修改</p>
												</td>
											</tr>
										</table>
									</div>
									</body>
									</html>
HTML
					);

			return $arr;
		}
	}
?>