<?php
define("DEBUG_SENDMAIL", false);
set_time_limit(120); 											//如果email是有問題，可能會超時

require_once "config_sys.php";
require_once "class.phpmailer.php";
require_once "class.smtp.php";

$mail = new PHPMailer;

if(DEBUG_SENDMAIL)
$mail->SMTPDebug = 3;									// Enable verbose debug output

$mail->SMTPOptions = array(								// immotionhost uses self signed certificates
    "ssl" => array(
        "verify_peer" => false,
        "verify_peer_name" => false,
        "allow_self_signed" => true
    )
);

$mail->isSMTP();													// Set mailer to use SMTP
$mail->Host = MAIL_HOST; 								// Specify main and backup SMTP servers
$mail->SMTPAuth = true;									// Enable SMTP authentication
$mail->Username = MAIL_USERNAME;				// SMTP username
$mail->Password = MAIL_PASSWORD;				// SMTP password  
$mail->isHTML(true);											// Set email format to HTML 
$mail->CharSet = "UTF-8";

function sendEmail($mail, $subject, $messageHTML, $altMessage, $recipients=array(), $cc=array(), $bcc=array(), $replyTo=array(), $embedImg=array()){
	$mail->clearAllRecipients( ); // clear Recipients
	$mail->clearReplyTos();
	$mail->clearAttachments();

	if(!empty($replyTo)){
		foreach($replyTo as $email => $name){
			$mail->AddReplyTo($email, $name);
		}
	}
	
	$mail->setFrom(MAIL_USERNAME, MAIL_FROMNAME);

	if(!empty($recipients)){
		foreach($recipients as $email => $name){
			$mail->addAddress($email, $name);
		}
	}

	if(!empty($cc)){
		foreach($cc as $email => $name){
			$mail->addCC($email, $name);
		}
	}

	if(!empty($bcc)){
		foreach($bcc as $email => $name){
			$mail->addBCC($email, $name);
		}
	}
	
	if(!empty($embedImg)){
		foreach($embedImg as $name => $src){
			$mail->AddEmbeddedImage($src, $name);
			$messageHTML = str_replace($src, "cid:".$name, $messageHTML);
		}
	}
	
	$mail->Subject = $subject;
	$mail->Body = $messageHTML;
	$mail->AltBody = $altMessage;
	
	return $mail->Send();
}
?>
