<?php
    define('USER_NAME', '洪鐵重機械股份有限公司');
    define('USER_SHORTNAME', '洪鐵');
    define('USER_EMAIL', 'hc.ht@msa.hinet.net');
    define('USER_FACEBOOK', 'https://www.facebook.com/www.hungtieh.com.tw/');
    define('USER_TEL', '+88634792537');
    define('USER_TEL_SHOW', '03-479-2537');
    define('USER_COPYRIGHT', '2018 © 洪鐵重機械股份有限公司');
    define('SITENAME', USER_SHORTNAME);
    define('SITETITLE', USER_NAME);    //網站標題後敘述

	define('DESIGNER_NAME', '');

    //========================      Path
    define('PATH_BASE', '/');
    define('PATH_LIB_BASE', 'lib/');
    define('PATH_LOG_BASE', 'log/');
    define('PATH_CONFIG_BASE', 'config/');
    define('PATH_FRONTEND_BASE', 'frontend/');
    define('PATH_FRONTEND_SRC', 'frontend/src/');
    define('PATH_FRONTEND_INCLUDE', 'frontend/src/include/');
    define('PATH_FRONTEND_CSS_SRC', '/frontend/css/');
    define('PATH_FRONTEND_JS_SRC', '/frontend/js/');
    define('PATH_FRONTEND_IMG_SYS_SRC', 'frontend/img/sys/');
    define('PATH_FRONTEND_IMG_USR_SRC', 'frontend/img/usr/');
    define('PATH_FRONTEND_HELPER_SRC', 'frontend/helper/');
    define('PATH_DB_HELPER_SRC', 'frontend/db/');
    define('PATH_FRONTEND_LANG_SRC', 'frontend/lang/');


    //========================      Config
    define('CONF_DEBUG_LEVEL', '4');
    define('CONF_DEBUG_EN', 'Y');

    define('CONF_BILINGUAL', true);
    define('BRAND_OWNER', true);
    define('CONF_HAS_MEMBER', true);
    define('CONF_CASE', true);
    define('CONF_NEWS_CATEGORY', true);

?>