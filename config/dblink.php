<?php
require_once dirname(__FILE__)."/config_sys.php";

try{
	$dsn = "mysql:dbname=".DB_NAME.";host=".DB_HOST.";charset=".DB_CHARSET;
	$DB_con = new PDO($dsn, DB_USER, DB_PASSWORD);
	$DB_con->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	$DB_con->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
	$DB_con->exec("set names utf8");
}catch(PDOException $e){
	error("database connection", $e->getMessage());
}
?>
