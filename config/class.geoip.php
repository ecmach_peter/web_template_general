<?php
	class GetGeo{
		function __construct(){
		}

		public function GetInformation(){
			$_IP = $this->GetIP();
			// $_IP = '114.33.29.66';
			// $_IP = '119.29.232.113';
			$result = $this->GetGeoIP($_IP);

			return $result;
		}

		public function GetGeoIP($IP){
			if($this->CheckIP($IP)){
				//get geoip
				$_dir = dirname(__FILE__);
				require_once($_dir."/geoip/geoip.inc");
				require_once($_dir."/geoip/geoipcity.inc");
				require_once($_dir."/geoip/geoipregionvars.php");
				$gi = geoip_open("include/geoip/GeoLiteCity.dat", GEOIP_STANDARD);
				$resultGeo = geoip_record_by_addr($gi, $IP);

				//get agent info
				$host = ($this->CheckIP($IP))? preg_replace("/^[^.]+./", "", gethostbyaddr($IP)):'';
			}
			$result = array(
							'IP' => $IP,
							'country' => $resultGeo->country_name,
							'city' => $resultGeo->city,
							'host' => $host,
							'referer' => $_SERVER['HTTP_REFERER'],
							'agent' => $_SERVER['HTTP_USER_AGENT']
						);
			return $result;
		}

		public function GetIP(){
			if (getenv("HTTP_CLIENT_IP") && strcasecmp(getenv("HTTP_CLIENT_IP"), "unknown"))
				$ip = getenv("HTTP_CLIENT_IP");
			else if (getenv("HTTP_X_FORWARDED_FOR") && strcasecmp(getenv("HTTP_X_FORWARDED_FOR"), "unknown"))
				$ip = getenv("HTTP_X_FORWARDED_FOR");
			else if (getenv("REMOTE_ADDR") && strcasecmp(getenv("REMOTE_ADDR"), "unknown"))
				$ip = getenv("REMOTE_ADDR");
			else if (isset($_SERVER['REMOTE_ADDR']) && $_SERVER['REMOTE_ADDR'] && strcasecmp($_SERVER['REMOTE_ADDR'], "unknown"))
				$ip = $_SERVER['REMOTE_ADDR'];
			else
				$ip = "unknown";
			return $ip;
		}

		public function CheckIP($IP){
			return (filter_var($IP, FILTER_VALIDATE_IP))? true:false;
		}
	}
?>