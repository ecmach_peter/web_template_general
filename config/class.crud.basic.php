<?php
class crud_basic{
	private $db;

	function __construct($DB_con){
		$this->db = $DB_con;
	}

	public function getLastID()
	{
		try{return $this->db->lastInsertId();}
		catch(PDOException $e){error('crud_basic->getLastID', $e->getMessage()); return false;}
	}

	public function sql($query){ //自訂sql
		try{return $this->db->query($query)->fetchAll(PDO::FETCH_ASSOC);}
		catch(PDOException $e){error('crud_basic->sql', $e->getMessage()); return false;}
	}

	public function sql_prepare($query, $a){ //自訂sql
		try{
			$this->db->setAttribute(PDO::ATTR_EMULATE_PREPARES, true);
			$stmt = $this->db->prepare($query);
			$stmt->execute($a);
			return $stmt->fetchAll(PDO::FETCH_ASSOC);
		}catch(PDOException $e){error('crud_basic->select', $e->getMessage()); return false;}
	}

	public function multi_sql($query){ //執行多筆sql
		try{return $this->db->exec($query);}
		catch(PDOException $e){error('crud_basic->sql', $e->getMessage()); return false;}
	}

	public function select($table, $where, $orderby = array(), $column = array()){ //多筆
		try{
			$a = array();
			$c = '*';
			$w = $o = '';

			//WHERE 處理
			foreach ($where as $key => $value) {
				//SQL 語句
				$w .= ' AND `'.$key.'` = :'.$key;
				//注入array處理
				$a[':'.$key] = $value;
			}

			//排序參數
			if(count($orderby) > 0){
				$o = 'ORDER BY ';
				foreach ($orderby as $key => $value) {
					$o .= "`{$key}` {$value},";
				}

				$o = substr($o, 0, strlen($o) - 1);
			}

			//欄位
			if(count($column) > 0){
				$c = '';
				foreach ($column as $key => $value) {
					$as = (is_numeric($key))? '':" AS `{$key}`";
					$c .= "`{$value}`{$as},";
				}
				$c = substr($c, 0, strlen($c) - 1);
			}

			$stmt = $this->db->prepare("SELECT {$c} FROM `{$table}` WHERE 1 = 1 {$w} {$o}");
			
			$stmt->execute($a);
			return $stmt->fetchAll(PDO::FETCH_ASSOC);
		}catch(PDOException $e){error('crud_basic->select', $e->getMessage()); return false;}
	}
	
	public function getID($table, $where, $orderby = array(), $column = array()){ //多筆
		try{
			$result = self::select($table, $where, $orderby, $column);
			if($result!=false && count($result)>0)
				return $result[0];
			else
				return false;
		}catch(PDOException $e){error('crud_basic->getID', $e->getMessage()); return false;}
	}

	public function create($table, $columnsArray){
		try{
			$a = array();
			$c = '';
			$v = '';
			foreach ($columnsArray as $key => $value) {
				$c .= '`'.$key.'`, ';
				$v .= ':'.$key.', ';
				$a[':'.$key] = $value;
			}
			$c = rtrim($c,', ');
			$v = rtrim($v,', '); 
			$stmt = $this->db->prepare('INSERT INTO `'.$table.'`('.$c.') VALUES('.$v.')');
			$stmt->execute($a);
			return self::getLastID();
		}catch(PDOException $e){error('crud_basic->create', $e->getMessage()); return false;}
	}

	public function update($table, $columnsArray, $where){
		try{
			$a = array();
			$w = '';
			$c = '';
			foreach ($where as $key => $value) {
				$w.= ' AND `'.$key. '` = :'.$key;
				$a[':'.$key] = $value;
			}
			foreach ($columnsArray as $key => $value) {
				$c .= '`'.$key.'` = :'.$key.', ';
				$a[':'.$key] = $value;
			}
			$c = rtrim($c,', ');
			$stmt = $this->db->prepare('UPDATE `'.$table.'` SET '.$c.' WHERE 1=1 '.$w);
			$stmt->execute($a);
			//$affected_rows = $stmt->rowCount();
			return true;
		}catch(PDOException $e){error('crud_basic->update', $e->getMessage()); return false;}
	}

	public function delete($table, $where){
		try{
			$a = array();
			$w = '';
			foreach ($where as $key => $value) {
				$w .= ' AND `' .$key. '` = :'.$key;
				$a[':'.$key] = $value;
			}
			$stmt = $this->db->prepare('DELETE FROM `'.$table.'` WHERE 1=1 '.$w);
			$stmt->execute($a);
			//$affected_rows = $stmt->rowCount();
			return true;
		}catch(PDOException $e){error('crud_basic->delete', $e->getMessage()); return false;}
	}

	//暫停送出指令
	public function beginTransaction(){
		try{
			$this->db->beginTransaction();
			return true;
		}catch(PDOException $e){error('crud_basic->beginTransaction', $e->getMessage()); return false;}
	}

	//送出指令
	public function commit(){
		try{
			$this->db->commit();
			return true;
		}catch(PDOException $e){error('crud_basic->commit', $e->getMessage()); return false;}
	}

	//返回所有指令
	public function rollBack(){
		try{
			$this->db->rollBack();
			return true;
		}catch(PDOException $e){error('crud_basic->rollBack', $e->getMessage()); return false;}
	}
}