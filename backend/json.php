<?php
    header('Content-Type: text/json; charset=utf-8');
?>
{
    "button": {
        "add": true,
        "del": true
    },
    "set": {
        "del": {
            "file": {
                "column": [
                    "course_photo"
                ]
            }
        }
    },
    "list": [
        {
            "column": "course_title",
            "type": "text",
            "title": "課程名稱"
        },
        {
            "column": "course_date",
            "type": "text",
            "title": "日期",
            "class": "hidden-xs"
        },
        {
            "column": "course_type",
            "type": "text",
            "title": "課程類型",
            "alias": "get_course_type"
        },
        {
            "column": "course_online",
            "type": "switch",
            "title": "上架狀態"
        },
        {
            "column": "course_date",
            "type": "edit",
            "title": "操作"
        }
    ],
    "form": [
        {
            "column": "course_title",
            "type": "text",
            "title": "課程名稱",
            "size": 5,
            "required": true
        },
        {
            "column": "course_online",
            "title": "上架狀態",
            "type": "radio",
            "data": [
                {
                    "title": "上架",
                    "value": "1",
                    "checked": true
                },
                {
                    "title": "下架",
                    "value": "0"
                }
            ],
            "size": 4
        },
        {
            "column": "course_type",
            "title": "課程類型",
            "type": "radio",
            "data": [
                {
                    "title": "體驗課程",
                    "value": "1",
                    "checked": true
                },
                {
                    "title": "實戰課程",
                    "value": "2"
                }
            ],
            "size": 4
        },
        {
            "column": "course_hours",
            "type": "text",
            "title": "課程總時數",
            "size": 2
        },
        {
            "column": "course_price",
            "type": "text",
            "title": "課程費用",
            "size": 2,
            "required": true
        },
        {
            "column": "course_date",
            "type": "text",
            "title": "上課時間",
            "size": 5
        },
        {
            "column": "course_photo",
            "type": "photo",
            "title": "課程圖片",
            "size": 5,
            "required": true
        },
        {
            "column": "course_intro",
            "type": "wysiwyg",
            "title": "課程介紹",
            "size": 9,
            "required": true
        },
        {
            "column": "course_outline",
            "type": "wysiwyg",
            "title": "課程大綱",
            "size": 9
        },
        {
            "column": "course_teacher",
            "type": "wysiwyg",
            "title": "講師介紹",
            "size": 9
        },
        {
            "column": "course_target",
            "type": "wysiwyg",
            "title": "適合對象",
            "size": 9
        },
        {
            "column": "course_lecture",
            "type": "wysiwyg",
            "title": "課程講義",
            "size": 9
        },
        {
            "column": "course_persons",
            "type": "wysiwyg",
            "title": "人數限制",
            "size": 9
        },
        {
            "column": "course_time",
            "type": "wysiwyg",
            "title": "上課時間",
            "size": 9
        },
        {
            "column": "course_charge",
            "type": "wysiwyg",
            "title": "收費標準",
            "size": 9
        },
        {
            "column": "course_precautions",
            "type": "wysiwyg",
            "title": "注意事項",
            "size": 9
        }
    ]
}