<?php 
require_once "my_session_start.php";
require_once "right_check.php";

//2015/10/29 最底下含rights_check
date_default_timezone_set("Asia/Taipei");
my_session_start(3600);


$MM_authorizedUsers = "";  //20150825 無用
$MM_donotCheckaccess = "true";  //20150825 無用 

// *** Restrict Access To Page: Grant or deny access to this page
function isAuthorized($strUsers, $strGroups, $UserName, $UserGroup) { //20150825 isAuthorized一定為true，真正是MM_Username是否有設值 
  // For security, start by assuming the visitor is NOT authorized. 
  $isValid = False; 

  // When a visitor has logged into this site, the Session variable MM_Username set equal to their username. 
  // Therefore, we know that a user is NOT logged in if that Session variable is blank. 
  if (!empty($UserName)) { 
    // Besides being logged in, you may restrict access to only certain users based on an ID established when they login. 
    // Parse the strings into arrays. 
    $arrUsers = Explode(",", $strUsers); 
    $arrGroups = Explode(",", $strGroups); 
    if (in_array($UserName, $arrUsers)) { //201508025 判斷是否有在 授權使用者列表
      $isValid = true; 
    } 
    // Or, you may restrict access to only certain users based on their username. 
    if (in_array($UserGroup, $arrGroups)) {  //201508025 判斷是否有在 授權群組列表
      $isValid = true; 
    } 
    if (($strUsers == "") && true) { //201508025 無言，true應該是為false才對 
      $isValid = true; 
    } 
  } 
  return $isValid; 
}

$MM_restrictGoTo = "./";

if (!((isset($_SESSION["MM_Username"])) && (isAuthorized("",$MM_authorizedUsers, $_SESSION["MM_Username"], $_SESSION["MM_UserGroup"])))) { //20150825 isAuthorized一定為true，真正是MM_Username是否有設值
  //20150825 驗證不通過，轉回 login.php
  $MM_qsChar = "?";
  $MM_referrer = htmlentities($_SERVER["PHP_SELF"]);
  
  if (strpos($MM_restrictGoTo, "?")) $MM_qsChar = "&";
  if (isset($_SERVER["QUERY_STRING"]) && strlen($_SERVER["QUERY_STRING"]) > 0) //20150825 是否有帶參數
    $MM_referrer .= "?" . $_SERVER["QUERY_STRING"];
    
  $MM_restrictGoTo = $MM_restrictGoTo;//. $MM_qsChar . "accesscheck=" . urlencode($MM_referrer);
  
  header("Location: ". $MM_restrictGoTo); 
  exit;
}
?>