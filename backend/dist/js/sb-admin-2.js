/*!
 * Start Bootstrap - SB Admin 2 v3.3.7+1 (http://startbootstrap.com/template-overviews/sb-admin-2)
 * Copyright 2013-2016 Start Bootstrap
 * Licensed under MIT (https://github.com/BlackrockDigital/startbootstrap/blob/gh-pages/LICENSE)
 */
$(function() {
	$('#side-menu').metisMenu();
});

//Loads the correct sidebar on window load,
//collapses the sidebar on window resize.
// Sets the min-height of #page-wrapper to window size
$(function() {
	$(window).bind("load resize", function() {
		var topOffset = 50;
		var width = (this.window.innerWidth > 0) ? this.window.innerWidth : this.screen.width;
		if (width < 768) {
			$('div.navbar-collapse').addClass('collapse');
			topOffset = 100; // 2-row-menu
		} else {
			$('div.navbar-collapse').removeClass('collapse');
		}

		var height = ((this.window.innerHeight > 0) ? this.window.innerHeight : this.screen.height) - 1;
		height = height - topOffset;
		if (height < 1) height = 1;
		if (height > topOffset) {
			$("#page-wrapper").css("min-height", (height) + "px");
		}
	});

	var url = window.location;
	// var element = $('ul.nav a').filter(function() {
	//     return this.href == url;
	// }).addClass('active').parent().parent().addClass('in').parent();
	var element = $('ul.nav a').filter(function() {
		return this.href == url;
	}).addClass('active').parent();

	while (true) {
		if (element.is('li')) {
			element = element.parent().addClass('in').parent();
		} else {
			break;
		}
	}
});

var scripts = document.getElementsByTagName('script'),
	src = scripts[scripts.length - 1].src,
	arg = src.indexOf('?') !== -1 ? src.split('?').pop() : '',
	settings = {},
	post_str = '';
arg.replace(/(\w+)(?:=([^&]*))?/g, function(a, key, value) {
	settings[key] = value;
	post_str += '&'+key+'='+value;
});

function doajax(ajax){
	this.async = true;
	this.type = 'text';
	this.debug = false;
	this.reurl = false;
	this.files = null;

	var url = (ajax.reurl)? ajax.url:'./'+settings['a']+'-'+ajax.url,
		arr_data = (ajax.data+'&'+post_str).split('&'),
		formData = new FormData();

	$.each(arr_data, function(index, value) {
		var arr_value = value.split('=');
		if(arr_value[0]){
			formData.append(decodeURIComponent(arr_value[0]), arr_value[1]);
		}
	});

	if(ajax.files != null){
		ajax.files.each(function(){
			var $this = $(this),
				_length = $this[0].files.length;

			for (var i = 0; i < _length; i++) {
				if($this[0].files[i]){
					formData.append($this[0].name, $this[0].files[i]); 
				}
			}
			formData.append(
								$this.attr('id'),
								JSON.stringify($this.data())
							);
			// if($this[0].files[0]){
			//  formData.append($this[0].name, $this[0].files[0]);
			//  formData.append(
			//                      $this.attr('id'),
			//                      JSON.stringify($this.data())
			//                  );
			// }
		});
	}

	$.ajax({
		url: url,
		type: 'post',
		data: formData,
		dataType: ajax.type,
		async: ajax.async,
		processData: false,
		contentType: false,
		beforeSend: function(xhr, opts) {
			if(ajax.beforeSend) ajax.beforeSend(xhr, opts);
		},
		success: function(msg) {
			var _MM_logout = false;
			if(ajax.type != 'json' && msg.search('MM_logout') != -1){
				var _msg = JSON.parse(msg);
				if(_msg.MM_logout) _MM_logout = true;
			}
			else if(msg.MM_logout){
				_MM_logout = true;
			}

			if(_MM_logout){
				location.href = './login';
				return false;
			}
			
			if(ajax.callback) ajax.callback(msg);
			if(ajax.debug) console.log(msg)
		},
		error:function(xhr, ajaxOptions, thrownError){
			console.error('err');
			console.error('sts:'+xhr.status);
			console.error('error:'+thrownError);
			console.error('url:'+url);
		}
	});
}


function doajax_file(ajax){
	this.debug = false;
	this.reurl = false;
	var files = ajax.file,
		formData = new FormData(),
		url = (ajax.reurl)? ajax.url:'./'+settings['a']+'-'+ajax.url;
	for (var i = 0; i < files.length; i++) {
		var file = files[i];
		
		// Check the file type.
		/*if (!file.type.match('image.*')) {
			continue;
		}*/
		
		// Add the file to the request.
		formData.append('photos[]', file, file.name);
	}
	
	$.extend(ajax.data,ajax.data,settings);
	for(var key in ajax.data)
		formData.append(key,ajax.data[key]);
	var xhr = new XMLHttpRequest();
	xhr.open('POST', url, true);
	xhr.onload = function () {
		if (xhr.status === 200) {
			var msg = xhr.responseText;
			if(ajax.type == 'json') msg = JSON.parse(msg);
			if(ajax.callback) ajax.callback(msg);
			if(ajax.debug) console.log(msg)
		}
		else {
			console.error('An error occurred!');
		}
	};
	xhr.send(formData);
}

var do_notify = {
	success : function(msg){
		this.message = (msg)? msg:'<i class="fa fa-check fa-lg"></i> 資料修改完成';
		this.type = 'success';
		this.show();
	},
	error : function(msg){
		this.message = (msg)? msg:'<i class="fa fa-exclamation-triangle fa-lg"></i> 資料修改失敗，請您稍後再試或與管理員聯絡';
		this.type = 'danger';
		this.show();
	},
	show: function(){
		$.notifyClose();
		$.notify({
			message: '<strong>'+this.message+'</strong>' 
		},{
			type: this.type,
			delay: 3000,
			placement: {
				align: "center"
			},
			z_index: 1001,
		});
	}
}

function parseParams(str) {
	return str.split('&').reduce(function (params, param) {
		var paramSplit = param.split('=').map(function (value) {
			return decodeURIComponent(value.replace('+', ' '));
		});
		params[paramSplit[0]] = paramSplit[1];
		return params;
	}, {});
}