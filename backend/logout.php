<?php
session_start();
session_destroy();

$home_url = './';
header('Location: ' . $home_url);
?>