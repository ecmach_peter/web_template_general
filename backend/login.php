<?php
	require_once('../config/dblink.php'); 
	include_once('../config/class.crud.basic.php');
	include_once('./include/log_bruteforce.php');

	// *** Validate request to login to this site.
	if (!isset($_SESSION)){session_start();}

	$loginFormAction = htmlentities($_SERVER['PHP_SELF']);
	if (isset($_GET['accesscheck'])) { //登入使用者，能回到上一頁，而不是固定頁面
		$_SESSION['PrevUrl'] = $_GET['accesscheck'];
	}
	// BruteForce Attack Protection
	$bf_logs = new log_bruteforce($DB_con);
	$block = $bf_logs->bruteforcecheck();
	
	if(!$block){
		//登入錯誤次數已達上限
		$_form = <<<HTML
					<div class="panel-body">
						<h3 class="text-center">IP已暫時封鎖</h3>
						<h4 class="text-center text-white">由於您所屬的IP，嘗試登入錯誤次數已達上限，系統已暫時封鎖您的IP，請您30分鐘後再進行登入</h4>
					</div>
HTML;
	}
	else{
		if (isset($_POST['name'])) {
			$loginUsername = $_POST['name'];
			$password = $_POST['password'];
			$MM_fldUserAuthorization = "";
			$MM_redirectLoginSuccess = "back_project";
			$MM_redirectLoginFailed = "login.php?nomsg=1";
			$MM_redirecttoReferrer = false;
			//mysql_select_db($database_bear, $bear);
			
			$LoginRS__query = "SELECT count(*) FROM user WHERE user_online = 1 AND username = ? AND password = ?; "; 
			
			try {
				$crud = new crud_basic($DB_con);
				$loginFoundUser = 0;
				$arr_user = $crud->getid('user', array('username' => $loginUsername));

				if(!empty($arr_user['u_id'])){
					if(password_verify($password , $arr_user['password'])){
						$loginFoundUser = 1;
					}
				}


			}catch (PDOException $e){
				echo $e->getMessage().$sql;
				die();
			}
				
			// *** Login Logs ***
			$bf_logs->logIP($loginUsername, $loginFoundUser);
			
			//$LoginRS = mysql_query($LoginRS__query, $bear) or die(mysql_error());
			//$loginFoundUser = mysql_num_rows($LoginRS);
			if($loginFoundUser) {
				$loginStrGroup = "";
				
				if (PHP_VERSION >= 5.1) {session_regenerate_id(true);} else {session_regenerate_id();}
				
				//declare two session variables and assign them
				$_SESSION['MM_Username'] = $loginUsername;
				$_SESSION['MM_UserGroup'] = $loginStrGroup;
				
				//得到更多使用者資訊user_id rights等
				$sql_user_info = "select user.u_id from user WHERE user_online = 1 AND username = ?; ";
				$sth = $DB_con->prepare($sql_user_info);
				$sth->execute(array($loginUsername));
				$user_id = $sth->fetchAll(PDO::FETCH_ASSOC)[0]['u_id'];
				$_SESSION['user_id'] = $user_id;
				
				if (isset($_SESSION['PrevUrl']) && false) {
					$MM_redirectLoginSuccess = $_SESSION['PrevUrl'];	
				}	 
				
				header("Location: " . $MM_redirectLoginSuccess );
				exit;
			}
			else {	 
				header("Location: ". $MM_redirectLoginFailed );
				exit;
			}
		}

		if($_GET['nomsg'] == 1){
			$_alert = <<<HTML
						<div class="alert alert-danger fade in" role="alert" id="alert_error">
							<div id="alert_content"><i class="fa fa-exclamation-triangle fa-lg"></i> 帳號或密碼錯誤！</div>
						</div>
HTML;
		}

		$_form = <<<HTML
					<div class="panel-body">
						<h2 class="text-center">登入頁面&nbsp;/&nbsp;Login Page</h2>
						<form role="form" ACTION="{$loginFormAction}" id="form1" name="form1" method="POST">
							<div class="form-group">
								<label for="adminID">登入帳號&nbsp;/&nbsp;Username：</label>
								<input type="text" class="form-control" name="name" id="adminID" placeholder="請輸入您的管理者帳號" required autofocus="autofocus">
							</div>
							<div class="form-group">
								<label for="adminPSD">登入密碼&nbsp;/&nbsp;Password：</label>
								<input type="password" class="form-control" name="password" id="adminPSD" placeholder="請輸入您的登入密碼" required>
							</div>
							{$_alert}
							<div class="col-md-12 text-center">
								<button type="submit" class="btn btn-default">登入後台&nbsp;/&nbsp;Login</button>
							</div>
						</form>
					</div>
HTML;
	}
?>
<!DOCTYPE html>
<html lang="zh-TW">
<?php include_once 'back_head.php'; ?>
<link href="./dist/css/login.css?<?=RECACHE_NUM;?>" rel="stylesheet">
</head>
	<body>
		<div class="container login">
			<div class="row">
				<div class="panel panel-default panel-custom">
					<div class="panel-heading text-center">
						<h1>
							<?=SITENAME;?>&nbsp;<br><small>後台管理系統</small>
						</h1>
					</div>
					<?=$_form;?>
					<div class="panel-footer">
						<p class="text-center">
							如遇使用上問題，請電洽：<a href="tel:+88642707-3097">(04)2707-3097</a>，我們將儘快為您服務。
						</p>
						<p class="text-center">
							服務時間：週一至週五&nbsp;&nbsp;09:00-18:00
							<br>Let go
							<a href="https://google.com.tw" target="_blank">google.com</a>
						</p>
					</div>
				</div>
			</div>
		</div>
<?php include_once 'back_footer.php'; ?>
</body>
</html>