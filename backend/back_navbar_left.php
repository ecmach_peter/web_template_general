<?php
	$arr_user = $crud->getid('user', array('u_id' => $_SESSION['user_id']));
?>
			<div class="sidebar-nav navbar-collapse">
				<div class="info">
					<a href="./">
						<img src="images/logo.png" alt="" class="img-responsive center-block">
					</a>
					<p class="">
						管理員
						<span class="large"><?=$arr_user['alias'];?></span>
					</p>
				</div>
				<ul class="nav" id="side-menu">
					<!-- <li class="sidebar-search">
						<div class="input-group custom-search-form">
							<input type="text" class="form-control" placeholder="Search...">
							<span class="input-group-btn">
								<button class="btn btn-default" type="button">
									<i class="fa fa-search"></i>
								</button>
							</span>
						</div>
					</li> -->
					<?php
						//取得群組單元，child為該群組下有幾項單元
						$arr_unit = $crud->sql("SELECT `system_unit_id`, `system_unit_code`, `system_unit_name`, `system_unit_icon`,
													(SELECT COUNT(*) 
														FROM `system_name` 
														WHERE `system_switch` = 1 
															AND `system_name`.`system_unit` = `system_unit`.`system_unit_code`
															AND `system_code` IN (SELECT `system_code` 
																					FROM `rights` 
																					WHERE `user_id` = '{$_SESSION['user_id']}')
													) AS `child` 
												FROM `system_unit`
												WHERE `system_unit_par_id` = 0 AND `system_unit_switch` = 1
												ORDER BY `system_unit_sort`");
						foreach ($arr_unit as $key => $value) {
							//取得子單元
							$arr_navbar_c = $crud->sql("SELECT `system_unit_id`, `system_unit_code`, `system_unit_name`, `system_unit_icon`,
															(SELECT COUNT(*) 
																FROM `system_name` 
																WHERE `system_switch` = 1 
																	AND `system_name`.`system_unit` = `system_unit`.`system_unit_code`
																	AND `system_code` IN (SELECT `system_code` 
																							FROM `rights` 
																							WHERE `user_id` = '{$_SESSION['user_id']}')
															) AS `child` 
														FROM `system_unit`
														WHERE `system_unit_par_id` = '{$value['system_unit_id']}' AND `system_unit_switch` = 1
														ORDER BY `system_unit_sort`");
							$str_third = '';
							if(count($arr_navbar_c) > 0){
								foreach ($arr_navbar_c as $c_key => $c_value) {
									//取得單元
									$arr_navbar = $crud->sql("SELECT `system_code`, `system_name` 
															FROM `system_name` 
															WHERE `system_switch` = 1 
																	AND `system_unit` = '{$c_value['system_unit_code']}' 
																	AND `system_code` IN (SELECT `system_code` 
																							FROM `rights` 
																							WHERE `user_id` = '{$_SESSION['user_id']}')
															ORDER BY `system_sort`");
									$str_child = $in = '';
									if(count($arr_navbar) > 0){	//超過一項的單元
										foreach ($arr_navbar as $_key => $_value) {
											$active = ($act == $_value['system_code'])? 'class="active"':'';
											$str_child .= <<<HTML
															<li>
																<a {$active} href="./{$_value['system_code']}">{$_value['system_name']}</a>
															</li>
HTML;
											if($act == $_value['system_code'] && $in == ''){
												$_navbar_icon = $c_value['system_unit_icon'];
												$_navbar_unit = $c_value['system_unit_name'];
												$_navbar_name = $_value['system_name'];
												$in = 'in';
											}
										}
										$str_third = <<<HTML
												<li>
													<a href="javascript:;">{$c_value['system_unit_name']}<span class="fa arrow"></span></a>
													<ul class="nav nav-third-level">
														{$str_child}
													</ul>
												</li>
HTML;

									}
									else{	//群組單元下沒有任何單元直接略過
										continue;
									}
								}
							}

							//取得單元
							$arr_navbar = $crud->sql("SELECT `system_code`, `system_name` 
													FROM `system_name` 
													WHERE `system_switch` = 1 
															AND `system_unit` = '{$value['system_unit_code']}' 
															AND `system_code` IN (SELECT `system_code` 
																					FROM `rights` 
																					WHERE `user_id` = '{$_SESSION['user_id']}')
													ORDER BY `system_sort`");
							$str_child = $in = '';
							if(($value['child'] + count($arr_navbar_c)) > 1){	//超過一項的單元
								foreach ($arr_navbar as $_key => $_value) {
									$active = ($act == $_value['system_code'])? 'class="active"':'';
									$str_child .= <<<HTML
													<li>
														<a {$active} href="./{$_value['system_code']}">{$_value['system_name']}</a>
													</li>
HTML;
									if($act == $_value['system_code'] && $in == ''){
										$_navbar_icon = $value['system_unit_icon'];
										$_navbar_unit = $value['system_unit_name'];
										$_navbar_name = $_value['system_name'];
										$in = 'in';
									}
								}
								echo <<<HTML
										<li>
											<a href="javascript:;"><i class="fa fa-{$value['system_unit_icon']}"></i>&nbsp;{$value['system_unit_name']}<span class="fa arrow"></span></a>
											<ul class="nav nav-second-level">
												{$str_child}{$str_third}
											</ul>
										</li>
HTML;

							}
							else if($value['child'] > 0){	//只有一項的單元不需群組選項
								$active = ($act == $arr_navbar[0]['system_code'])? 'class="active"':'';
								echo <<<HTML
										<li>
											<a {$active} href="./{$arr_navbar[0]['system_code']}"><i class="fa fa-fw fa-{$value['system_unit_icon']}"></i>&nbsp;{$arr_navbar[0]['system_name']}</a>
										</li>
HTML;
								if($act == $arr_navbar[0]['system_code'] && $in == ''){
									$_navbar_icon = $value['system_unit_icon'];
									$_navbar_unit = $value['system_unit_name'];
									$_navbar_name = $arr_navbar[0]['system_name'];
								}
							}
							else{	//群組單元下沒有任何單元直接略過
								continue;
							}
						}

					?>
				</ul>
			</div>