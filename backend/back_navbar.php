<div class="navbar-header">
	<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
		<span class="sr-only">Toggle navigation</span>
		<span class="icon-bar"></span>
		<span class="icon-bar"></span>
		<span class="icon-bar"></span>
	</button>
	<a class="navbar-brand" href="./"><?=SITENAME;?>後台管理系統</a>
</div>
<!-- /.navbar-header -->

<ul class="nav navbar-top-links navbar-right">
	<li>
		<a href="../" target="_blank"><i class="fa fa-fw fa-globe"></i> 前台</a>
	</li>
	<li class="dropdown">
		<a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i> <?php echo isset($_SESSION['MM_Username'])?$_SESSION['MM_Username']:"使用者帳號" ?> <b class="caret"></b></a>
		<ul class="dropdown-menu">
			<li>
				<a href="logout"><i class="fa fa-fw fa-power-off"></i> 登出後台</a>
			</li>
		</ul>
	</li>
</ul>
<!-- /.navbar-top-links -->