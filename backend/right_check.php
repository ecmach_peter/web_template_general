<?php
include_once './include/function.php';

function right_check($system_code="", $user_id="", $pdo_db, $crud_act=""){
	$pdo_crud = new crud_basic($pdo_db);
	$rights_checked = false;  //無權限
	$ar_rights_checked = array(); //單一權限判斷
	
	$sql = "select system_name.system_code, system_name.system_name, user.username  from system_name, rights, user where user.user_online = 1 and rights.user_id = user.u_id and rights.system_code = system_name.system_code ";
	//結果 單筆 且 crud為空，代表 可以存取system_code全部功能
	//     單筆 且 crud有值，代表 只能存取system_code單一功能
	//     多筆 且 crud皆不為空，代表 只能存取system_code部皆功能
	//     多筆 且 crud部份為空或全為空，代表 系統錯亂
	
	if($crud_act==""){  //傳入crud為空，代表是以 確認 存取system_code全部功能
		$where = "and rights.system_code = '$system_code' and rights.user_id = '$user_id' and rights.crud_act = '$crud_act'; ";
		$sql .= $where;

		$rows = $pdo_crud->sql($sql);
		$row_count = count($rows);

		if($row_count == 1){
			$rights_checked = true;
		}elseif($row_count>1){ //系統錯誤 只能有一筆，連絡系統資訊人員，有多筆代表，同時間有全部功能，又有部份功能，不符合邏輯，系統管控有錯
			header("Content-Type:text/html; charset=utf-8");
			echo "此為系統錯誤訊息，請系統資訊人員確認！ 錯誤代碼：err1001！ ";
			exit;
		}else{ //$row_count < 1 沒權限
			$rights_checked = false;
		}
	
	}else{ //查 system_code 單一功能權限
		$ar_crud = str_split($crud_act);
		foreach($ar_crud as $ar_t){
			$where = " where rights.system_code = '$system_code' and rights.user_id = '$user_id' and rights.crud_act like '%$ar_t%' ";
			$sql .= $where;
			
			$rows = $pdo_crud->sql($sql);
			$row_count = count($rows);
			
			if($row_count == 1){ //有權限
				$ar_rights_checked = array_push_after($ar_rights_checked, array("$ar_t"=>true), 0);
			}elseif($row_count>1){ //系統錯誤 只能有一筆，連絡系統資訊人員，有多筆代表，同時間有部份功能，相同權限多筆，不符合邏輯，系統管控有錯
				header("Content-Type:text/html; charset=utf-8");
				echo "此為系統錯誤訊息，請系統資訊人員確認！ 錯誤代碼：err1002！ ";
				exit;
			}else{ //$row_count < 1 沒權限
				$ar_rights_checked = array_push_after($ar_rights_checked, array("$ar_t"=>false), 0);
			}
			 
		}
		
	}
	
	//while($row=$stmt->fetch(PDO::FETCH_ASSOC)){ }
	
	$MM_restrictGoTo = "./"; //轉到無權階畫面

	if(!$rights_checked) { //無權限，轉到 無權限顯示頁面
		//20150825 驗證不通過，轉回 login.php
		$MM_qsChar = "?";
		$MM_referrer = htmlentities($_SERVER['PHP_SELF']);
		
		if (strpos($MM_restrictGoTo, "?")) $MM_qsChar = "&";
		if (isset($_SERVER['QUERY_STRING']) && strlen($_SERVER['QUERY_STRING']) > 0) //20150825 是否有帶參數
		  $MM_referrer .= "?" . $_SERVER['QUERY_STRING'];
		  
		// $MM_restrictGoTo = $MM_restrictGoTo. $MM_qsChar . "accesscheck=" . urlencode($MM_referrer);
		header("Location: ". $MM_restrictGoTo); 
		exit;
	}
	
	$key=array_search(false, $ar_rights_checked);
	
	if($key!=false){ //代表有些權限是沒有的
		header("Content-Type:text/html; charset=utf-8");
		echo "請求權限為$crud_act ，其中 $key 為權限";
		exit;
	}
}    
?>