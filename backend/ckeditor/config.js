/**
 * @license Copyright (c) 2003-2015, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.md or http://ckeditor.com/license
 */

CKEDITOR.editorConfig = function( config ) {
	// Define changes to default configuration here. For example:
	// config.language = 'fr';
	// config.uiColor = '#AADC6E';
  
    //工具列設定
	config.toolbar = 'TadToolbar';
    config.toolbar_TadToolbar =
    [
        ['Source','-','Templates','-','Cut','Copy','Paste'],
        ['Undo','Redo','-','Find','Replace','-','SelectAll'],
        ['Link','Unlink','Anchor'],
        ['Image','Youtube','Table','HorizontalRule','Smiley','SpecialChar','PageBreak'],
		['RemoveFormat','Maximize'],
        '/',
        ['Bold','Italic','Underline','Strike','-','Subscript','Superscript'],
        ['NumberedList','BulletedList','-','Outdent','Indent','Blockquote'],
        ['JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock'],
        ['Format','FontSize','-','TextColor','BGColor']
    ];
	
	var roxyFileman = 'fileman/index.html';
	config.filebrowserBrowseUrl = roxyFileman;
	config.filebrowserImageBrowseUrl = roxyFileman+'?type=image';
	config.removeDialogTabs = 'link:upload;image:upload';
	//http://jimmysu.logdown.com/posts/289076-setting-ckeditor
	config.allowedContent = true; //避免 CKEditor 自作主張轉換你的原始碼
	config.fontSize_sizes = '10pt/10pt;13/13px;16/16px;18/18px;20/20px;22/22px;24/24px;36/36px;48/48px;'; //調整字型大小
	config.font_names = 'Arial;Arial Black;Comic Sans MS;Courier New;Tahoma;Times New Roman;Verdana;新細明體;細明體;標楷體;微軟正黑體;Meiryo;Microsoft YaHei'; //調整字型
	config.undoStackSize = 50; //還原次數
	config.extraPlugins = 'youtube';
	
};

CKEDITOR.plugins.add('fileUpload',
{
    init: function (editor) {
        editor.addCommand( 'OpenDialog',new CKEDITOR.dialogCommand( 'OpenDialog' ) );
        editor.ui.addButton('FileUpload',
            {
                label: 'Upload images',
                command: 'OpenDialog',
                icon: CKEDITOR.plugins.getPath('fileUpload') + '8.png'
            });
        editor.contextMenu.addListener( function( element ){
            return { 'My Dialog' : CKEDITOR.TRISTATE_OFF };
        });
        CKEDITOR.dialog.add( 'OpenDialog', function( api ){
            var dialogDefinition =
            {
                title : 'Gestisci immagini',
                minWidth : 700,
                minHeight : 500,
                contents : [
                        {
                            expand : true,
                            padding : 0,
                            elements :
                            [
                                {

                                    type : 'html',
                                    html : ' <iframe src="jQuery-File-Upload/basic-plus.html" style="width:100%;height:490px" />'
                                }
                            ]
                        }
                ],
                buttons : []
            };
            return dialogDefinition;
        } );

    }
});