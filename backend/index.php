<?php
	require_once('../config/dblink.php'); 
	require_once('./include/essentials.php');
	include_once('../config/class.crud.basic.php');
	require_once('./include/function.php');

	$act = $_GET['act'];	//目前執行項目
	define('B_VAR_1', (!empty($_GET['var1']))? $_GET['var1']:'');		//URL參數

	//語言參數
	$_LANGUAGE = array(
					'' => '中',
					'_en' => '英'
				);

	$crud = new crud_basic($DB_con);

	//權限檢查
	$arr_right_ignore = array('logout', 'back_project');	//不須判斷權限的頁面
	if(array_search($act, $arr_right_ignore) === false && !empty($act)){
		require_once('./login_check.php');
		right_check($act, $_SESSION['user_id'], $DB_con);

		$arr_unit_rights = $crud->getid('rights', array('system_code' => $act, 'user_id' => $_SESSION['user_id']));
		$B_ARR_UNIT_RIGHTS = array(
								'add' => ($arr_unit_rights['right_add'] == 1)? true:false,
								'edit' => ($arr_unit_rights['right_edit'] == 1)? true:false,
								'del' => ($arr_unit_rights['right_del'] == 1)? true:false,
								'view' => ($arr_unit_rights['right_view'] == 1)? true:false
							);
	}

	if(empty($_SESSION['MM_Username'])){	//未登入
		//若為AJAX則回傳登入需求
		if($_SERVER['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest'){
			$arr['MM_logout'] = true;
			echo json_encode($arr);
			exit;
		}

		$_file = 'login';
	}
	else if(empty($act)){	//首頁
		$_file = 'back_project';
	}
	else{
		$arr_system = $crud->getID('system_name', array('system_code' => $act));

		if($arr_system && count($arr_system) > 0){		//確認資料庫有沒有該單元
			$_dir = ($arr_system['system_module'] == 1)? 'module':$arr_system['system_path'];
			$_file = ($arr_system['system_module'] == 1)? 'module':$arr_system['system_file'];
			$_table = (!empty($arr_system['system_table']))? $arr_system['system_table']:$act;
			define('SYSTEM_JSON', $arr_system['system_json']);

			//管理模組處理
			if(!empty(B_VAR_1)){
				$arr_ctrl = array('page', 'form', 'control', 'categories', 'sorts');	//允許的管理檔案
				if(array_search(B_VAR_1 ,$arr_ctrl ) !== false){
					$_file = "module_".B_VAR_1;
				}
			}
		}
		else{
			$_file = $act;
		}
	}
	$_path = (empty($_dir))? "./{$_file}.php":"./{$_dir}/{$_file}.php";

	if(file_exists($_path)){
		//AJAX路徑使用參數
		define('INCLUDE_DIR', $_dir);
		define('INCLUDE_ACT', $act);
		define('INCLUDE_TABLE', $_table);

		include($_path);
	}
	else{
		header("Location: ./"); 
	}
?>