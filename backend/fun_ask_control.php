<?php
	require_once('../include/dblink.php'); 
	require_once('./include/essentials.php');
	include_once('../include/class.crud.basic.php');

	$crud = new crud_basic($DB_con);
	switch (get_post_var('type')) {
		case 'ask':
			$arr['sts'] = true;
			$result = $crud->update('member_ask', array('member_ask_reply' => get_post_var('msg'), 'member_ask_reply_time' => date('Y-m-d H:i:s')), array('member_ask_id' => get_post_var('id')));
			if(!$result){
				$arr['sts'] = false;
				$arr['msg'] = '系統異常，請稍後在試';
			}
			else{
				$arr['msg'] = '訊息回覆完成';

				//寄發諮詢信件給管理者
				$oid = get_post_var('oid');
				$arr_order = $crud->sql("SELECT *
										FROM `member_order`
										WHERE `order_id` = {$oid}")[0];
				$arr_data = array('order_number' => $arr_order['order_number'], 'email_sendto' => $arr_order['order_email'], 'ask_id' => get_post_var('id'));
				$crud->create('email_send_temp', array('send_type' => 11, 'send_data' => serialize($arr_data)));
			}

			echo json_encode($arr);
			break;
	}
?>