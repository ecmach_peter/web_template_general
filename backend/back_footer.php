<!-- jQuery -->
<script src="./vendor/jquery/jquery.min.js?<?=RECACHE_NUM;?>"></script>

<!-- Bootstrap Core JavaScript -->
<script src="./vendor/bootstrap/js/bootstrap.min.js?<?=RECACHE_NUM;?>"></script>

<!-- Metis Menu Plugin JavaScript -->
<script src="./vendor/metisMenu/metisMenu.min.js?<?=RECACHE_NUM;?>"></script>

<!-- Morris Charts JavaScript -->
<script src="./vendor/raphael/raphael.min.js?<?=RECACHE_NUM;?>"></script>
<script src="./vendor/morrisjs/morris.min.js?<?=RECACHE_NUM;?>"></script>
<!-- <script src="./data/morris-data.js"></script> -->

<!-- Custom Theme JavaScript -->
<script src="./dist/js/sb-admin-2.js?d=<?=INCLUDE_DIR;?>&a=<?=INCLUDE_ACT;?>&<?=RECACHE_NUM;?>"></script>

<script src="./ckeditor/ckeditor.js?<?=RECACHE_NUM;?>"></script>
<script src="./vendor/blockUI/jquery.blockUI.js?<?=RECACHE_NUM;?>"></script>
<script src="./vendor/bootstrap-notify/bootstrap-notify.js?<?=RECACHE_NUM;?>"></script>
<script src="./vendor/bootbox/bootbox.min.js?<?=RECACHE_NUM;?>"></script>
<script src="./vendor/select2/select2.min.js?<?=RECACHE_NUM;?>"></script>
<script src="./vendor/bootstrap-select/bootstrap-select.min.js?<?=RECACHE_NUM;?>"></script>
<script src="./vendor/footable/footable.min.js?<?=RECACHE_NUM;?>"></script>
<script src="./vendor/nestable/jquery.nestable.js?<?=RECACHE_NUM;?>"></script>
<script src="./vendor/slimscroll/jquery.slimscroll.min.js?<?=RECACHE_NUM;?>"></script>
<script src="./vendor/aj-address/aj-address.js?<?=RECACHE_NUM;?>"></script>
<script src="./vendor/daterangepicker/moment.min.js?<?=RECACHE_NUM;?>"></script>
<script src="./vendor/daterangepicker/daterangepicker.js?<?=RECACHE_NUM;?>"></script>
<script src="./vendor/sweetalert2/es6-promise.auto.js?<?=RECACHE_NUM;?>"></script>
<script src="./vendor/sweetalert2/sweetalert2.min.js?<?=RECACHE_NUM;?>"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.10.2/jquery-ui.min.js?<?=RECACHE_NUM;?>"></script>
<script src="./vendor/jquery/jquery.ui.touch-punch.min.js?<?=RECACHE_NUM;?>"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.5.0/Chart.bundle.min.js?<?=RECACHE_NUM;?>"></script>
<script src="./vendor/pignose.calendar/pignose.calendar.min.js"></script>