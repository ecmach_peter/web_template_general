<?php
	function plugins($plugin_value){
		global $_LANGUAGE,$crud,$edit_id,$arr_data,$_lock;
		$_type = $plugin_value->_type;
		$_column = $plugin_value->column;
		$_class = $plugin_value->class;
		$_isrequired = ($plugin_value->required)? true:false;
		$_language = ($plugin_value->language)? true:false;
		$_required = ($_isrequired)? 'required="required"':'';
		$_unique = ($plugin_value->unique)? 'data-unique="true"':'';
		// $_lock = ($arr_data["{$table}_lock"] == 1)? 'disabled':'';
		switch($_type){
			//純程式處理，若已審核完畢則不可再做任何變更
			case 'model_brand':
				$arr_brand = $crud->select('brand', array());
				$str_brand = '';
				if(count($arr_brand) > 0){
					foreach ($arr_brand as $key => $value) {
						$_selected = ($value['brand_id'] == $arr_data[$_column])? 'selected':'';
						$str_brand .= <<<HTML
							<option value="{$value['brand_id']}" {$_selected}>{$value['brand_title']} - {$value['brand_title_en']}</option>
HTML;
					}
				}
				$append = <<<HTML
							<select class="js-example-tags" name="{$_column}" id="{$_column}" {$_lock} style="width: 100%">
								{$str_brand}
							</select>
HTML;
				break;
		}

		return $append;
	}
	$plugins_JS = <<<JAVASCRIPT
		$(".js-example-tags").select2({
			tags: true
		});

		function chk_plugins(){
			var _perr = false,
				obj = {
					type: 'plugins',
					_type: 'unique',
					id : $('#edit_id').val(),
					value: $('#txt_model_number').val()+$('#txt_model_version').val()
				};

			doajax({
				url: 'control',
				data: jQuery.param(obj),
				type: 'json',
				async: false,
				callback: function(msg){
					_perr = (msg.unique)? false:true;
				}
			});

			if(_perr){
				do_notify.error('<i class="fa fa-exclamation-triangle fa-lg"></i> 此 型號已經使用過了');
			}
			return _perr;
		}
JAVASCRIPT;
?>