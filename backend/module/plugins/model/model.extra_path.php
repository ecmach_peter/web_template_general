<?php
	function get_extra_path($_path_type){
		global $crud, $table, $edit_id, $arr_data, $del_id;
		switch($_path_type){
			//編輯時
			case 'edit':
				//無主資料夾時新增
				$_pdir = "../uploadimages/{$table}/";
				if (!is_dir($_pdir)){
					mkdir($_pdir,0777);
				}

				//無主資料夾時新增
				$_pdir .= "Maker/";
				if (!is_dir($_pdir)){
					mkdir($_pdir,0777);
				}

				//無ID資料夾時新增
				$_post_brand = urldecode(get_post_var('txt_model_brand'));
				$arr_brand = $crud->getid('brand', array('brand_id' => $_post_brand));
				if(empty($arr_brand['brand_id'])){
					$arr_add_brand = explode('#-#', $_post_brand);
					if(!empty($arr_add_brand[0]) && !empty($arr_add_brand[1])){
						$dir = "{$_pdir}{$arr_add_brand[1]}/";
					}
				}
				else{
					$dir = "{$_pdir}{$arr_brand['brand_title_en']}/";
				}

				if (!is_dir($dir)){
					mkdir($dir,0777);
				}
				break;

			//讀取
			case 'read':
				$arr_now_model = $crud->getid('brand', array('brand_id' => $arr_data['model_brand']));
				$dir = "../uploadimages/{$table}/Maker/{$arr_now_model['brand_title_en']}/";
				break;

			//刪除
			case 'del':
				$arr_now_model = $crud->getid('model', array('model_id' => $del_id));
				$arr_now_brand = $crud->getid('brand', array('brand_id' => $arr_now_model['model_brand']));
				$dir = "../uploadimages/{$table}/Maker/{$arr_now_brand['brand_title_en']}/{$arr_now_model['model_datasheet']}";

				if(!empty($dir) && file_exists($dir)){
					unlink($dir);
				}
				break;
		}

		return $dir;
	}
?>