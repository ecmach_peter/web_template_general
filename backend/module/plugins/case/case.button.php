<?php
	function plugins_button($_type = ''){
		global $_LANGUAGE,$crud,$edit_id,$table;

		if(empty($_type)) $_type = get_post_var('_type');

		switch($_type){
			//簽到
			case 'filter_label':
				$arr_works_type = $crud->select('works_type', array(), array('works_type_sort' => 'ASC'), array('works_type_id', 'works_type_title'));
				$str_works_type = '';
				if(count($arr_works_type) > 0){
					foreach ($arr_works_type as $value) {
						$str_works_type .= <<<HTML
									<option value="{$value['works_type_id']}">{$value['works_type_title']}</option>
HTML;
					}
				}
				$append = <<<HTML
							<select id="filter_view" name="filter_view[case`.`works_type_id]" data-live-search="true" class="selectpicker" data-width="fit" style="display:none;">
								<option value="">全部類型</option>
								<option data-divider="true"></option>
								{$str_works_type}
							</select>
HTML;
				break;
		}

		$_JS = <<<HTML
HTML;

		$arr = array(
					'append' => $append,
					'js' => $_JS
				);
		return $arr;
	}
?>