<?php
	function plugins($plugin_value){
		global $_LANGUAGE,$crud,$edit_id,$arr_data, $_lock;
		$_type = $plugin_value->_type;
		$_column = $plugin_value->column;
		$_class = $plugin_value->class;
		$_isrequired = ($plugin_value->required)? true:false;
		$_language = ($plugin_value->language)? true:false;
		$_required = ($_isrequired)? 'required="required"':'';
		$_unique = ($plugin_value->unique)? 'data-unique="true"':'';
		// $_lock = ($arr_data["{$table}_lock"] == 1 || (!$B_ARR_UNIT_RIGHTS['deit'] && $edit_id <> 'add'))? 'disabled':'';
		switch($_type){
			//賣家處理
			case 'member':
				$arr_member = $crud->select('member', array(), array('member_name' => 'ASC'));
				$option = '';
				$has_selected = false;
				if(count($arr_member) > 0){
					foreach ($arr_member as $key => $value) {
						$selected = ($arr_data[$_column] == $value['member_uniqid'])? 'selected':'';
						$option .= <<<HTML
							<option value="{$value['member_uniqid']}" {$selected} data-subtext="{$value['member_name']}">+{$arr_member['member_calling_code']}{$value['member_account']}</option>
HTML;
					}
				}

				$append = <<<HTML
							<select id="txt_{$_column}" name="txt_{$_column}" class="selectpicker" data-live-search="true" {$_lock} data-size="10" data-selected-text-format="count" data-title="請選擇" data-style="btn-default nomt nomb" {$_multiple} {$_required} style="display:none;">
								{$option}
							</select>
HTML;
				break;
		}

		return $append;
	}
	$plugins_JS = <<<JAVASCRIPT
		$('#has_edit').parents('.form-group').hide();
		$('#txt_product_model').on('change', function(){
			$('.cate_code').text($(this).find(':selected').data('code'));
			$('.cate_ver').text($(this).find(':selected').data('ver'));
		});
		$('#product_selling_price select').selectpicker();

		$('#product_selling_price').on('click', '.btn_del_detail_vice', function(){
			var _this = $(this),
				that = _this.parents('table').find('#product_selling_price');
			_this.parents('tr').remove();
			update_num(that);

			if($('#product_selling_price tr:visible').length < 1){
				$('.data_empty').fadeIn();
			}
		});
		$( '#product_selling_price' ).sortable({
			handle: "i",
			items: "tr"
		});

		$('.btn_add_product_price').on('click', function(){
			var _this = $(this),
				_random_num = random_num(),
				_append = replaceAll($('#append_html').html(), 'newViceID', 'new_' + _random_num),
				that = _this.parents('table').find('#product_selling_price');
			that.append(_append).find(':disabled').attr('disabled', false);
			that.find('.product_price_target:last').selectpicker();
			update_num(that);

			if($('#product_selling_price tr:visible').length > 1){
				$('.data_empty').hide();
			}
		});

		$('#txt_product_country').on('change', function(){
			var _this = $(this),
				that = $('.div_checkbox').parents('.form-group');
			that.css('display', (_this.val() == 'HKG')? '':'none').find('input').attr('disabled', (_this.val() == 'HKG')? false:true);
		}).change();

		$('#module_form form').find('[id*="txt_product_name"], #txt_product_serial, #txt_product_engine_serial, #txt_product_year, #txt_product_hours, #fxt_product_report, #txt_product_country, #fxt_product_photo, #txt_product_currency, [id="txt_product_epd_nrmm_label"], #txt_product_start_price, #txt_product_default_price, [name*="product_price"]:enabled').on('change', function(){
			$('#has_edit').val('Y');
		});

		function update_num(target){
			target.find('tr:not(.data_empty)').each(function(e){
				$(this).find('td').eq(1).text(e + 1);
			});
		}

		function random_num(){
			return Math.floor(Math.random() * (100000 - 0 + 1)) + 0;
		}

		function replaceAll(txt, replace, with_this) {
			return txt.replace(new RegExp(replace, 'g'),with_this);
		}

		function chk_plugins(){
			var that = $('#product_selling_price'),
				_perr = false;

			that.find('input:not(:checkbox, [aria-label="Search"]), select').filter(':enabled').each(function(){
				var _this = $(this);
				if(_this.val() == '' || _this.val() == null){
					var _label = $('#product_selling_price').parent().find('thead th').eq(_this.parents('td').index()).text();
					do_notify.error('<i class="fa fa-exclamation-triangle fa-lg"></i> 請確認特殊價格-'+_label);
					_perr = true;
					return false;
				}
			});
			return _perr;
		}
JAVASCRIPT;
?>