<?php
	function plugins_button($_type = ''){
		global $_LANGUAGE,$crud,$edit_id,$table;

		if(empty($_type)) $_type = get_post_var('_type');

		switch($_type){
			//簽到
			case 'filter_label':
				$arr_works_cate = $crud->select('works_cate', array(), array('works_cate_sort' => 'ASC'), array('works_cate_id', 'works_cate_title'));
				$str_works_cate = '';
				if(count($arr_works_cate) > 0){
					foreach ($arr_works_cate as $value) {
						$str_works_cate .= <<<HTML
									<option value="{$value['works_cate_id']}">{$value['works_cate_title']}</option>
HTML;
					}
				}
				$append = <<<HTML
							<select id="filter_view" name="filter_view[works`.`works_cate_id]" data-live-search="true" class="selectpicker" data-width="fit" style="display:none;">
								<option value="">全部標籤</option>
								<option data-divider="true"></option>
								{$str_works_cate}
							</select>
HTML;
				break;
		}

		$_JS = <<<HTML
HTML;

		$arr = array(
					'append' => $append,
					'js' => $_JS
				);
		return $arr;
	}
?>