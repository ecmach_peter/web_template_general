<?php
	function plugins($plugin_value){
		global $_LANGUAGE,$crud,$edit_id,$arr_data, $_lock;
		$_type = $plugin_value->_type;
		$_column = $plugin_value->column;
		$_class = $plugin_value->class;
		$_isrequired = ($plugin_value->required)? true:false;
		$_language = ($plugin_value->language)? true:false;
		$_required = ($_isrequired)? 'required="required"':'';
		$_unique = ($plugin_value->unique)? 'data-unique="true"':'';
		// $_lock = ($arr_data["{$table}_lock"] == 1 || (!$B_ARR_UNIT_RIGHTS['deit'] && $edit_id <> 'add'))? 'disabled':'';
		switch($_type){
			//賣家處理
			case 'member':
				$arr_member = $crud->select('member', array(), array('member_name' => 'ASC'));
				$option = '';
				if(count($arr_member) > 0){
					$arr_value = json_decode($arr_data[$_column], true);
					foreach ($arr_member as $key => $value) {
						if($edit_id <> 'add') $selected = (array_search($value['member_uniqid'], $arr_value) !== false)? 'selected':'';
						$option .= <<<HTML
							<option value="{$value['member_uniqid']}" {$selected} data-subtext="{$value['member_name']}">+{$arr_member['member_calling_code']}{$value['member_account']}</option>
HTML;
					}
				}

				$append = <<<HTML
							<select id="jxt_{$_column}" name="jxt_{$_column}[]" class="selectpicker" data-live-search="true" {$_lock} data-size="10" data-selected-text-format="count" data-title="不限制" data-style="btn-default nomt nomb" multiple {$_required} style="display:none;">
								{$option}
							</select>
HTML;
				break;
			//賣家處理
			case 'model':
				$arr_model = $crud->sql("SELECT `model_id`, `model_number`, `model_version`
										FROM `model`
										INNER JOIN `brand` ON `model_brand` = `brand_id`
										ORDER BY `model_number`, `model_version`");
				$option = '';
				if(count($arr_model) > 0){
					foreach ($arr_model as $key => $value) {
						$selected = ($arr_data[$_column] == $value['model_id'])? 'selected':'';
						$option .= <<<HTML
							<option value="{$value['model_id']}" {$selected}>{$value['model_number']}-{$value['model_version']}</option>
HTML;
					}
				}

				$append = <<<HTML
							<select id="txt_{$_column}" name="txt_{$_column}" class="selectpicker" data-live-search="true" {$_lock} data-size="10" data-selected-text-format="count" data-title="請選擇" data-style="btn-default nomt nomb" {$_multiple} {$_required} style="display:none;">
								{$option}
							</select>
HTML;
				break;
		}

		return $append;
	}
	$plugins_JS = <<<JAVASCRIPT
JAVASCRIPT;
?>