<?php
	function plugins($plugin_value){
		global $_LANGUAGE,$crud,$edit_id,$arr_data, $_lock;
		$_type = $plugin_value->_type;
		$_column = $plugin_value->column;
		$_class = $plugin_value->class;
		$_isrequired = ($plugin_value->required)? true:false;
		$_language = ($plugin_value->language)? true:false;
		$_required = ($_isrequired)? 'required="required"':'';
		$_unique = ($plugin_value->unique)? 'data-unique="true"':'';
		// $_lock = ($arr_data["{$table}_lock"] == 1 || (!$B_ARR_UNIT_RIGHTS['deit'] && $edit_id <> 'add'))? 'disabled':'';
		switch($_type){
			//賣家處理
			case 'member':
				$arr_member = $crud->select('member', array(), array('member_name' => 'ASC'));
				$option = '';
				if(count($arr_member) > 0){
					foreach ($arr_member as $key => $value) {
						$selected = ($arr_data[$_column] == $value['member_uniqid'])? 'selected':'';
						$option .= <<<HTML
							<option value="{$value['member_uniqid']}" {$selected} data-subtext="{$value['member_name']}">+{$arr_member['member_calling_code']}{$value['member_account']}</option>
HTML;
					}
				}

				$append = <<<HTML
							<select id="txt_{$_column}" name="txt_{$_column}" class="selectpicker" data-live-search="true" {$_lock} data-size="10" data-selected-text-format="count" data-title="請選擇" data-style="btn-default nomt nomb" {$_multiple} {$_required} style="display:none;">
								{$option}
							</select>
HTML;
				break;
			//賣家處理
			case 'model':
				$arr_model = $crud->sql("SELECT `model_id`, `model_number`, `model_version`
										FROM `model`
										INNER JOIN `brand` ON `model_brand` = `brand_id`
										ORDER BY `model_number`, `model_version`");
				$option = '';
				if(count($arr_model) > 0){
					foreach ($arr_model as $key => $value) {
						$selected = ($arr_data[$_column] == $value['model_id'])? 'selected':'';
						$option .= <<<HTML
							<option value="{$value['model_id']}" {$selected}>{$value['model_number']}-{$value['model_version']}</option>
HTML;
					}
				}

				$append = <<<HTML
							<select id="txt_{$_column}" name="txt_{$_column}" class="selectpicker" data-live-search="true" {$_lock} data-size="10" data-selected-text-format="count" data-title="請選擇" data-style="btn-default nomt nomb" {$_multiple} {$_required} style="display:none;">
								{$option}
							</select>
HTML;
				break;
		}

		return $append;
	}
	$plugins_JS = <<<JAVASCRIPT
		$('[name="txt_label_mode"]').on('change', function(){
			var is_NRMM = ($(this).val() == 'NRMM')? true:false;
			var NRMM = $('#txt_label_NRMM_code, #txt_label_NRMM_label_code, #txt_label_NRMM_charges, #fxt_label_NRMM_file, #fxt_label_NRMM_photo');
			var QPME = $('#txt_label_QPME_label_code, #txt_label_QPME_expires, #txt_label_QPME_charges, #fxt_label_QPME_file, #fxt_label_QPME_photo');

			NRMM.attr('disabled', (is_NRMM)? false:true).parents('.form-group').css('display', (is_NRMM)? '':'none');
			QPME.attr('disabled', (is_NRMM)? true:false).parents('.form-group').css('display', (is_NRMM)? 'none':'');
		}).filter(':checked').change();

		if(!$('#txt_label_mode:checked').get(0)){
			$('#txt_label_NRMM_code, #txt_label_NRMM_label_code, #txt_label_NRMM_charges, #fxt_label_NRMM_file, #fxt_label_NRMM_photo, #txt_label_QPME_label_code, #txt_label_QPME_expires, #txt_label_QPME_charges, #fxt_label_QPME_file, #fxt_label_QPME_photo').attr('disabled', true).parents('.form-group').css('display', 'none');
		}

		/*$('[name="del_label_QPME_file"], [name="del_label_NRMM_file"], [name="del_label_QPME_photo"], [name="del_label_NRMM_photo"]').parents('.input-group-addon').remove();*/
		function chk_plugins(){
			var _perr = false;

			if(!$('#txt_label_mode:checked').get(0)){
				do_notify.error('<i class="fa fa-exclamation-triangle fa-lg"></i> 請選擇申請類型');
				_perr = true;
			}
			else if($('#txt_label_mode:checked').val() == 'NRMM'){
				$('#txt_label_NRMM_code, #txt_label_NRMM_label_code, #txt_label_NRMM_charges, #fxt_label_NRMM_file, #fxt_label_NRMM_photo').each(function(){
					var _this = $(this);
					if(_this.is(':file')){
					}
					else if(_this.val() == '' || _this.val() == null){
						do_notify.error('<i class="fa fa-exclamation-triangle fa-lg"></i> 請確認NRMM資訊');
						_perr = true;
						return false;
					}
				});
			}
			else if($('#txt_label_mode:checked').val() == 'QPME'){
				$('#txt_label_QPME_label_code, #txt_label_QPME_expires, #txt_label_QPME_charges, #fxt_label_QPME_file, #fxt_label_QPME_photo').each(function(){
					var _this = $(this);
					if(_this.is(':file')){
					}
					else if(_this.val() == '' || _this.val() == null){
						do_notify.error('<i class="fa fa-exclamation-triangle fa-lg"></i> 請確認QPME資訊');
						_perr = true;
						return false;
					}
				});
			}
			return _perr;
		}
JAVASCRIPT;
?>