<?php
	function plugins($plugin_value){
		global $_LANGUAGE,$crud,$edit_id,$arr_data, $_lock;
		$_type = $plugin_value->_type;
		$_column = $plugin_value->column;
		$_class = $plugin_value->class;
		$_isrequired = ($plugin_value->required)? true:false;
		$_language = ($plugin_value->language)? true:false;
		$_required = ($_isrequired)? 'required="required"':'';
		$_unique = ($plugin_value->unique)? 'data-unique="true"':'';
		// $_lock = ($arr_data["{$table}_lock"] == 1 || (!$B_ARR_UNIT_RIGHTS['deit'] && $edit_id <> 'add'))? 'disabled':'';
		switch($_type){
			//型號
			case 'video':
				$str_title = '';
				if($_language){
					foreach ($_LANGUAGE as $l_key => $l_value) {
						$mt = (empty($str_title))? '':'mt5';
						$str_title .= <<<HTML
									<div class="input-group {$mt}">
										<span class="input-group-addon" id="sizing-addon2">{$l_value}</span>
										<input type="text" class="form-control" name="video[%video_id\$s][title{$l_key}]" value="%video_title{$l_key}\$s" {$_lock}>
									</div>
HTML;
					}
				}
				$str_tr = <<<HTML
							<tr>
								<td class="col-small"><i class="fa fa-bars fa-2x mt5 cursor_move"></i></td>
								<td class="col-xlarge">
									{$str_title}
								</td>
								<td>
									<input type="text" class="form-control" name="video[%video_id\$s][url]" value="%video_url\$s" {$_lock}>
								</td>
								<td class="col-smedium center">
									<input id="btn_price_online-%video_id\$s" name="video[%video_id\$s][online]" class="cmn-toggle cmn-toggle-round" type="checkbox" %video_online_checked\$s {$_lock} {$disabled}>
									<label for="btn_price_online-%video_id\$s"></label>
								</td>
								<td class="col-smedium center">
									<button type="button" class="btn btn-danger btn_del_detail_vice" {$_lock}><i class="fa fa-trash-o fa-lg"></i></button>
								</td>
							</tr>
HTML;

				$arr_video = $crud->select(
										'brand_video',
										array('host_id' => $edit_id),
										array('video_sort' => 'ASC'),
										array(
											'video_id',
											'video_title',
											'video_title_en',
											'video_url',
											'video_online',
										)
									);
				$str_video = <<<HTML
								<tr class="data_empty">
									<td colspan="5" >還沒有任何影片</td>
								</tr>
HTML;
				if(count($arr_video) > 0){
					$str_video = '';
					foreach($arr_video as $value){
						$arr_video_data = array(
										'video_id' => $value['video_id'],
										'video_title' => $value['video_title'],
										'video_title_en' => $value['video_title_en'],
										'video_url' => $value['video_url'],
										'video_online_checked' => ($value['video_online'] == 1)? 'checked':'',
									);
						$str_video .= vksprintf($str_tr, $arr_video_data);
					}
				}

				$arr_video_data = array(
								'video_id' => 'newVideoID',
								'video_title' => '',
								'video_title_en' => '',
								'video_url' => '',
								'video_online_checked' => 'checked',
							);
				$str_init_tr = vksprintf($str_tr, $arr_video_data);
				$append = <<<HTML
							<div class="panel panel-default">
								<div class="panel-body">
									<div class="table-responsive">
										<table class="tc-table table">
											<thead>
												<tr>
													<th class="col-small">#</th>
													<th class="col-xlarge">標題</th>
													<th>連結</th>
													<th class="col-smedium center">開關</th>
													<th class="col-smedium center">刪除</th>
												</tr>
											</thead>
											<tbody id="brand_video">
												{$str_video}
											</tbody>
											<tfoot>
												<tr>
													<td colspan="6">
														<button type="button" class="btn btn-primary btn_add_brand_video" {$_lock}><i class="fa fa-plus fa-lg"></i> 新增</button>
													</td>
												</tr>
											</tfoot>
										</table>
										<table style="display:none;">
											<tbody id="append_html">
												{$str_init_tr}
											</tbody>
										</table>
									</div>
								</div>
							</div>
HTML;
				break;
			//顯示分類選擇
			case 'view_cate':
				//取得分類
				$arr_cate = $crud->sql("SELECT `c1`.`cate_id`, `c1`.`cate_title`, `c2`.`cate_id` AS `par_id`, `c2`.`cate_title` AS `par_title`
										FROM `product`
										INNER JOIN `model` ON `model_id` = `product_model`
										INNER JOIN `model_cate` AS `c1` ON `model_cate` = `c1`.`cate_id`
										INNER JOIN `model_cate` AS `c2` ON `c1`.`cate_par_id` = `c2`.`cate_id`
										WHERE `model_brand` = '{$edit_id}' AND `c2`.`cate_online` = 1 AND `c1`.`cate_online` AND `model_online` = 1 AND `product_online` = 1
										GROUP BY `c1`.`cate_id`
										ORDER BY `c2`.`cate_sort`, `c1`.`cate_sort`");
				$str_cate = $str_cate_view = '';
				if(count($arr_cate) > 0){
					$_tmp_id = '';
					$arr_value = explode(',', $arr_data[$_column]);
					foreach($arr_cate as $key => $value){
						if($_tmp_id <> $value['par_id']){
							if(!empty($str_cate)){
								$str_cate .= '</optgroup>';
							}
							$str_cate .= <<<HTML
									<optgroup label="{$value['par_title']}">
HTML;
							$_tmp_id = $value['par_id'];
						}

						$selected = (array_search($value['cate_id'], $arr_value) !== false)? 'selected':'';
						$str_cate .= <<<HTML
								<option value="{$value['cate_id']}" {$selected}>{$value['cate_title']}</option>
HTML;
					}
					$str_cate .= '</optgroup>';

					$append = <<<HTML
								<select id="cxt_{$_column}" name="cxt_{$_column}[]" class="selectpicker" data-size="10" data-selected-text-format="count" data-live-search="true" data-title="請選擇" data-style="btn-default nomt nomb" {$_required} {$_disabled} {$_lock} data-max-options="2" multiple style="display:none;">
									{$str_cate}
								</select>
HTML;

					$str_cate_view = join(',', array_column($arr_cate, 'cate_id'));
				}
				else{
					$append = <<<HTML
						<p class="form-control-static" id="txt_{$_column}">尚未有此品牌的商品</p>
HTML;
				}
				break;
		}

		return $append;
	}
	$plugins_JS = <<<JAVASCRIPT
		$('#brand_video').on('click', '.btn_del_detail_vice', function(){
			var _this = $(this),
				that = _this.parents('table').find('#brand_video');
			_this.parents('tr').remove();

			if($('#brand_video tr:visible').length < 1){
				$('.data_empty').fadeIn();
			}
		});
		$('#brand_video').sortable({
			handle: "i",
			items: "tr"
		});

		$('#append_html').find('input').attr('disabled', true);

		$('.btn_add_brand_video').on('click', function(){
			var _this = $(this),
				_random_num = new Date().getTime(),
				_append = replaceAll($('#append_html').html(), 'newVideoID', 'new_' + _random_num),
				that = $('#brand_video');
			that.append(_append).find(':disabled').attr('disabled', false);

			if($('#brand_video tr:visible').length > 1){
				$('.data_empty').hide();
			}
		});

		function replaceAll(txt, replace, with_this) {
			return txt.replace(new RegExp(replace, 'g'),with_this);
		}

		function chk_plugins(){
			var cxt_brand_view_cate = $('#cxt_brand_view_cate');
			if(!cxt_brand_view_cate.val()){
				cxt_brand_view_cate.after($('<input>').attr({type: 'hidden', name: 'cxt_brand_view_cate'}).val(''));
			}
		}
JAVASCRIPT;
?>