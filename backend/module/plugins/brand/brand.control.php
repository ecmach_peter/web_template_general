<?php
	function plugins($_type = ''){
		global $_LANGUAGE, $crud, $edit_id, $table, $primary_key, $_is_add;

		if(empty($_type)) $_type = get_post_var('_type');

		switch($_type){
			//修改資料(額外資料)
			case 'edit':
				$has_edit = (get_post_var('has_edit') == 'Y')? true:false;

				//價格處理
				$arr_vice = $_POST['video'];
				// print_r($_POST);
				// exit;
				$arr_vice_sqlin = array();
				if(count($arr_vice) > 0){
					$vice_sort = 0;
					foreach ($arr_vice as $key => $value) {
						$vice_key = substr($key, 0, 3);

						$arr_data = array(
										'host_id' => $edit_id,
										'video_title' => $value['title'],
										'video_title_en' => $value['title_en'],
										'video_url' => $value['url'],
										'video_online' => (empty($value['online']))? 0:1,
										'video_sort' => $vice_sort
									);
						switch($vice_key){
							//新增影片
							case 'new':
								$new_key = $crud->create('brand_video', $arr_data);
								array_push($arr_vice_sqlin, $new_key);
								$has_edit = true;
								break;

							//修改影片
							default:
								$crud->update('brand_video', $arr_data, array('video_id' => $key));
								array_push($arr_vice_sqlin, $key);
								break;
						}
						$vice_sort++;
					}
				}

				if(count($arr_vice_sqlin) > 0){
					$str_vice_sqlin = join(',', $arr_vice_sqlin);
					$crud->sql("DELETE 
								FROM `brand_video` 
								WHERE `host_id` = {$edit_id} AND `video_id` NOT IN ({$str_vice_sqlin})");
				}
				break;
		}

		return $append;
	}
?>