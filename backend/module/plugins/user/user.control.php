<?php
	function plugins($_type = ''){
		global $_LANGUAGE, $crud, $edit_id, $table, $primary_key, $_is_add;

		if(empty($_type)) $_type = get_post_var('_type');

		switch($_type){
			//修改資料(額外資料)
			case 'edit':
				$crud->delete('rights', array('user_id' => $edit_id));

				$arr_right = $_POST['rights'];
				foreach ($arr_right as $key => $value) {
					$arr_data = array(
									'system_code' => $key,
									'user_id' => $edit_id,
									'right_add' => (empty($value['add']))? 0:1,
									'right_del' => (empty($value['del']))? 0:1,
									'right_edit' => (empty($value['edit']))? 0:1,
									'right_view' => (empty($value['view']))? 0:1
								);
					$crud->create('rights', $arr_data);
				}
				break;
		}

		return $append;
	}
?>