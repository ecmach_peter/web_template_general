<?php
	function plugins($plugin_value){
		global $_LANGUAGE,$crud,$edit_id,$arr_data,$_lock;
		$_type = $plugin_value->_type;
		$_column = $plugin_value->column;
		$_class = $plugin_value->class;
		$_isrequired = ($plugin_value->required)? true:false;
		$_language = ($plugin_value->language)? true:false;
		$_required = ($_isrequired)? 'required="required"':'';
		$_unique = ($plugin_value->unique)? 'data-unique="true"':'';
		// $_lock = ($arr_data["{$table}_lock"] == 1 || (!$B_ARR_UNIT_RIGHTS['deit'] && $edit_id <> 'add'))? 'disabled':'';
		if($_SESSION['user_id'] == 1) $_lock = '';
		switch($_type){
			//權限設定
			case 'right':
				if($edit_id <> 'add'){
					$arr_have_right = $crud->select('rights', array('user_id' => $edit_id));
					$_arr_have_right = array();
					foreach ($arr_have_right as $_key => $_value) {
						$_arr_have_right[$_value['system_code']] = array(
																		'add' => ($_value['right_add'] == 1)? 'checked':'',
																		'del' => ($_value['right_del'] == 1)? 'checked':'',
																		'edit' => ($_value['right_edit'] == 1)? 'checked':'',
																		'view' => ($_value['right_view'] == 1)? 'checked':'',
																	);
					}
				}

				$arr_system_unit = $crud->select('system_unit', array('system_unit_switch' => 1));
				if(count($arr_system_unit) > 0){
					foreach ($arr_system_unit as $key => $value) {
						$arr_system_name = $crud->select('system_name', array('system_unit' => $value['system_unit_code'],'system_switch' => 1));
						
						$tmp_right = '';
						if(count($arr_system_name) > 0){
							foreach ($arr_system_name as $_key => $_value) {
								$_tmp = $_arr_have_right[$_value['system_code']];
								$tmp_right .= <<<HTML
												<tr data-id="{$value['system_unit_id']}">
													<td class="col-medium center">{$_value['system_name']}</td>
													<td class="col-smedium center">
														<button type="button" class="btn btn-primary btn_all_right" {$_lock}><i class="fa fa-check-square-o fa-lg"></i></button>
													</td>
													<td class="col-smedium center">
														<input id="cmn-toggle-{$_value['system_name_id']}-add" name="rights[{$_value['system_code']}][add]" class="cmn-toggle cmn-toggle-round" type="checkbox" {$_tmp['add']} {$_lock}>
														<label for="cmn-toggle-{$_value['system_name_id']}-add"></label>
													</td>
													<td class="col-smedium center">
														<input id="cmn-toggle-{$_value['system_name_id']}-edit" name="rights[{$_value['system_code']}][edit]" class="cmn-toggle cmn-toggle-round" type="checkbox" {$_tmp['edit']} {$_lock}>
														<label for="cmn-toggle-{$_value['system_name_id']}-edit"></label>
													</td>
													<td class="col-smedium center">
														<input id="cmn-toggle-{$_value['system_name_id']}-del" name="rights[{$_value['system_code']}][del]" class="cmn-toggle cmn-toggle-round" type="checkbox" {$_tmp['del']} {$_lock}>
														<label for="cmn-toggle-{$_value['system_name_id']}-del"></label>
													</td>
													<td class="col-smedium center">
														<input id="cmn-toggle-{$_value['system_name_id']}-view" name="rights[{$_value['system_code']}][view]" class="cmn-toggle cmn-toggle-round" type="checkbox" {$_tmp['view']} {$_lock}>
														<label for="cmn-toggle-{$_value['system_name_id']}-view"></label>
													</td>
												</tr>
HTML;
							}
						}
						else{
							continue;
						}


						$append .= <<<HTML
									<div class="panel panel-default">
										<!-- /.panel-heading -->
										<div class="panel-body">
											<div class="table-responsive">
												<table class="tc-table table">
													<thead>
														<tr>
															<th colspan="6" class="center">{$value['system_unit_name']}</th>
														</tr>
														<tr>
															<th class="col-medium center">單元</th>
															<th class="col-smedium center">全選</th>
															<th class="col-smedium center">新增</th>
															<th class="col-smedium center">修改</th>
															<th class="col-smedium center">刪除</th>
															<th class="col-smedium center">讀取</th>
														</tr>
													</thead>
													<tbody class="rights_form">
														{$tmp_right}
													</tbody>
												</table>
											</div>
										</div>
									</div>
HTML;
					}
				}
				break;
		}

		return $append;
	}
	$plugins_JS = <<<JAVASCRIPT
			$(".btn_all_right").on('click', function(){
				$(this).parents('tr').find(':checkbox').prop('checked', true);
			});

			$('[name*="rights"]:NOT([name*="][view]"])').on('change', function(){
				var _this = $(this);
				if(_this.is(':checked')) _this.parents('tr').find(':checkbox:last').prop('checked', true);
			});

			function chk_plugins(){
				var _perr = false;

				if(!$('.rights_form :checkbox:checked').get(0)){
					_perr = true;
					do_notify.error('<i class="fa fa-exclamation-triangle fa-lg"></i> 至少要設定一個權限');
				}

				return _perr;
			}
JAVASCRIPT;
?>