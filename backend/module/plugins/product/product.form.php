<?php
	function plugins($plugin_value){
		global $_LANGUAGE,$crud,$edit_id,$arr_data, $_lock;
		$_type = $plugin_value->_type;
		$_column = $plugin_value->column;
		$_class = $plugin_value->class;
		$_isrequired = ($plugin_value->required)? true:false;
		$_language = ($plugin_value->language)? true:false;
		$_required = ($_isrequired)? 'required="required"':'';
		$_unique = ($plugin_value->unique)? 'data-unique="true"':'';
		// $_lock = ($arr_data["{$table}_lock"] == 1 || (!$B_ARR_UNIT_RIGHTS['deit'] && $edit_id <> 'add'))? 'disabled':'';
		switch($_type){
			//商品建置年限
			case 'product_create_year':
				$_year = ($edit_id == 'add')? date('Y'):$arr_data[$_column];
				$append = <<<HTML
						<input type="hidden" name="txt_{$_column}" id="txt_{$_column}" value="{$_year}" data-column="{$_column}" {$_required} {$_unique} {$_lock} {$_number} {$_readonly} />
HTML;
				break;

			//規格名稱
			case 'product_name':
				$_readonly = ($edit_id == 'add')? 'readonly':'';
				$append = <<<HTML
							<input type="text" class="form-control" name="txt_{$_column}" id="txt_{$_column}" value="{$arr_data[$_column]}" data-column="{$_column}" {$_required} {$_unique} {$_lock} {$_number} {$_readonly} />
HTML;
				break;

			//規格名稱
			case 'product_code':
				$_readonly = ($edit_id == 'add')? 'readonly':'';
				if($edit_id == 'add'){
					$append = <<<HTML
								<p class="form-control-static" id="txt_{$_column}">待確認新增後系統將自動產生</p>
HTML;
				}
				else{
					$_alert = ($arr_data['product_ver_code'] <> 'Z')? '':'<span class="input-group-addon cate_code"><i class="fa fa-exclamation-triangle text-danger" title="版本號已用盡"></i></span>';
					$_readonly = ($arr_data['product_ver_code'] <> 'Z')? 'readonly':'';
					$append = <<<HTML
								<div class="input-group">
									<span class="input-group-addon cate_code">{$arr_data[$_column]}</span>
									<input type="text" class="form-control" name="txt_product_ver_code" id="txt_product_ver_code" value="{$arr_data['product_ver_code']}" data-column="{$_column}" {$_required} {$_unique} {$_lock} {$_number} {$_readonly} />
									{$_alert}
								</div>
HTML;
				}
				break;
			//型號
			case 'product_model':
				if($edit_id == 'add'){
					$arr_model = $crud->sql("SELECT *
											FROM `model`
											INNER JOIN `model_cate` ON `model_cate` = `cate_id`
											WHERE `model_online` = 1");
					if(count($arr_model) > 0){
						foreach ($arr_model as $key => $value) {
							$selected = ($arr_data[$_column] == $value['model_id'])? 'selected="selected"':'';
							$option .= <<<HTML
										<option value="{$value['model_id']}" data-code="{$value['cate_code']}" data-ver="{$value['model_version']}" {$selected}>{$value['model_number']}</option>
HTML;
						}
					}
					$append = <<<HTML
								<select id="txt_{$_column}" name="txt_{$_column}" class="selectpicker" data-live-search="true" data-size="10" data-selected-text-format="count" data-title="請選擇" data-style="btn-default nomt nomb" {$_required} {$_lock} style="display:none;">
									{$option}
								</select>
HTML;
				}
				else{
					$arr_model = $crud->getid('model', array('model_id' => $arr_data[$_column]));
					$append = <<<HTML
							<p class="form-control-static" id="txt_{$_column}">{$arr_model['model_number']}</p>
HTML;
				}
				break;

			//型號
			case 'product_price':
				include_once(dirname(__FILE__).'/class.product_price.php');
				$product_price = new product_price($crud);

				//動態新增用的表單
				$arr_data = array(
								'num' => 1,
								'row_data' => array(
												'product_id' => 'newID',
												'selling_id' => 'newViceID',
												'disabled' => true
											)
							);
				$append_html = $product_price->product_price_append($arr_data);

				$arr_product_price = $crud->select('product_selling_price', array('product_id' => $edit_id), array('selling_sort' => 'ASC'));
				$str_vice = <<<HTML
								<tr class="data_empty">
									<td colspan="6" >目前沒有特殊價格，將以預設價格顯示</td>
								</tr>
HTML;
				if(count($arr_product_price) > 0){
					$str_vice = '';
					foreach ($arr_product_price as $key => $value) {
						$arr_data = array(
										'num' => ($key + 1),
										'row_data' => $value
									);
						$str_vice .= $product_price->product_price_append($arr_data);
					}
				}

				$append = <<<HTML
							<div class="panel panel-default">
								<!-- /.panel-heading -->
								<div class="panel-body">
									<div class="table-responsive">
										<table class="tc-table table">
											<thead>
												<tr>
													<th class="col-small">序</th>
													<th class="col-small">#</th>
													<th class="col-large">價格</th>
													<th>對象</th>
													<th class="col-smedium center">開關</th>
													<th class="col-smedium center">刪除</th>
												</tr>
											</thead>
											<tbody id="product_selling_price">
												{$str_vice}
											</tbody>
											<tfoot>
												<tr>
													<td colspan="6">
														<button type="button" class="btn btn-primary btn_add_product_price" {$_lock}><i class="fa fa-plus fa-lg"></i> 新增</button>
													</td>
												</tr>
											</tfoot>
										</table>
										<table style="display:none;">
											<tbody id="append_html">
												{$append_html}
											</tbody>
										</table>
									</div>
								</div>
								<!-- /.panel-body -->
							</div>
HTML;
				break;

			//商品地區
			case 'product_country':
				$arr_country_code = $crud->sql("SELECT *
												FROM `country_code`
												ORDER BY FIELD(`country_letter3_code`, 'TWN', 'HKG') DESC, `country_name`");
				foreach ($arr_country_code as $key => $value) {
					$selected = ($arr_data[$_column] == $value['country_letter3_code'])? 'selected="selected"':'';
					$_name = (empty($value['country_name_ch']))? $value['country_name']:$value['country_name_ch'];
					$option .= <<<HTML
								<option value="{$value['country_letter3_code']}" {$selected}>{$_name}</option>
HTML;
				}
				$append = <<<HTML
							<select id="txt_{$_column}" name="txt_{$_column}" class="selectpicker" data-live-search="true" data-size="10" data-selected-text-format="count" data-title="請選擇" data-style="btn-default nomt nomb" {$_required} {$_lock} style="display:none;">
								{$option}
							</select>
HTML;
				break;

			case 'has_edit':
				$append = <<<HTML
							<input type="hidden" id="has_edit" name="has_edit" value="">
HTML;
				break;

			//賣家處理
			case 'member':
// 				$arr_member = $crud->select('member', array(), array('member_name' => 'ASC'));
// 				$option = '';
// 				$has_selected = false;
// 				if(count($arr_member) > 0){
// 					foreach ($arr_member as $key => $value) {
// 						$member_name = (empty($arr_member['member_nickname']))? $value['member_name'].'(無暱稱)':$arr_member['member_nickname'];
// 						$arr_store = $crud->select('store', array('member_uniqid' => $value['member_uniqid']));
// 						if(count($arr_store) > 0){
// 							$tmp_option = '';
// 							foreach ($arr_store as $_key => $_value) {
// 								$selected = ($arr_data['product_store'] == $_value['store_id'])? 'selected':'';
// 								if($arr_data['product_store'] == $_value['store_id']) $has_selected = true;
// 								$tmp_option .= <<<HTML
// 									<option value="{$value['member_uniqid']}" data-store="{$_value['store_id']}" {$selected}>[店]{$_value['store_name']}</option>
// HTML;
// 							}

// 							if(!$has_selected) $selected = ($arr_data[$_column] == $value['member_uniqid'])? 'selected':'';
// 							$option .= <<<HTML
// 								<optgroup label="{$value['member_name']}">
// 									<option value="{$value['member_uniqid']}" {$selected}>[個]{$member_name}</option>
// 									{$tmp_option}
// 								</optgroup>
// HTML;
// 						}
// 						else{
// 							$selected = ($arr_data[$_column] == $value['member_uniqid'])? 'selected':'';
// 							$option .= <<<HTML
// 								<option value="{$value['member_uniqid']}" {$selected}>[個]{$member_name}</option>
// HTML;
// 						}
// 					}
// 				}

// 				$append = <<<HTML
// 							<select id="txt_{$_column}" name="txt_{$_column}" class="selectpicker" data-live-search="true" {$_lock} data-size="10" data-selected-text-format="count" data-title="請選擇" data-style="btn-default nomt nomb" {$_multiple} {$_required} style="display:none;">
// 								{$option}
// 							</select>
// 							<input id="txt_product_store" name="txt_product_store" type="hidden" value="{$arr_data['product_store']}">
// 							<script>
// 								$('#txt_product_owner').on('change', function(){
// 									var _this = $(this);
// 									$('#txt_product_store').val(_this.find(':selected').data('store'));
// 								});
// 							</script>
// HTML;
				break;
		}

		return $append;
	}
	$plugins_JS = <<<JAVASCRIPT
		$('#has_edit').parents('.form-group').hide();
		$('#txt_product_model').on('change', function(){
			$('.cate_code').text($(this).find(':selected').data('code'));
			$('.cate_ver').text($(this).find(':selected').data('ver'));
		});
		$('#product_selling_price select').selectpicker();

		$('#product_selling_price').on('click', '.btn_del_detail_vice', function(){
			var _this = $(this),
				that = _this.parents('table').find('#product_selling_price');
			_this.parents('tr').remove();
			update_num(that);

			if($('#product_selling_price tr:visible').length < 1){
				$('.data_empty').fadeIn();
			}
		});
		$( '#product_selling_price' ).sortable({
			handle: "i",
			items: "tr"
		});

		$('.btn_add_product_price').on('click', function(){
			var _this = $(this),
				_random_num = random_num(),
				_append = replaceAll($('#append_html').html(), 'newViceID', 'new_' + _random_num),
				that = _this.parents('table').find('#product_selling_price');
			that.append(_append).find(':disabled').attr('disabled', false);
			that.find('.product_price_target:last').selectpicker();
			update_num(that);

			if($('#product_selling_price tr:visible').length > 1){
				$('.data_empty').hide();
			}
		});

		$('#txt_product_country').on('change', function(){
			var _this = $(this),
				that = $('.div_checkbox').parents('.form-group');
			that.css('display', (_this.val() == 'HKG')? '':'none').find('input').attr('disabled', (_this.val() == 'HKG')? false:true);
		}).change();

		$('#module_form form').find('[id*="txt_product_name"], #txt_product_serial, #txt_product_engine_serial, #txt_product_year, #txt_product_hours, #fxt_product_report, #txt_product_country, #fxt_product_photo, #txt_product_currency, [id="txt_product_epd_nrmm_label"], #txt_product_start_price, #txt_product_default_price, [name*="product_price"]:enabled').on('change', function(){
			$('#has_edit').val('Y');
		});

		function update_num(target){
			target.find('tr:not(.data_empty)').each(function(e){
				$(this).find('td').eq(1).text(e + 1);
			});
		}

		function random_num(){
			return Math.floor(Math.random() * (100000 - 0 + 1)) + 0;
		}

		function replaceAll(txt, replace, with_this) {
			return txt.replace(new RegExp(replace, 'g'),with_this);
		}

		function chk_plugins(){
			var that = $('#product_selling_price'),
				_perr = false;

			that.find('input:not(:checkbox, [aria-label="Search"]), select').filter(':enabled').each(function(){
				var _this = $(this);
				if(_this.val() == '' || _this.val() == null){
					var _label = $('#product_selling_price').parent().find('thead th').eq(_this.parents('td').index()).text();
					do_notify.error('<i class="fa fa-exclamation-triangle fa-lg"></i> 請確認特殊價格-'+_label);
					_perr = true;
					return false;
				}
			});
			return _perr;
		}
JAVASCRIPT;
?>