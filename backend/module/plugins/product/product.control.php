<?php
	function plugins($_type = ''){
		global $_LANGUAGE, $crud, $edit_id, $table, $primary_key, $_is_add;

		if(empty($_type)) $_type = get_post_var('_type');

		switch($_type){
			//修改資料(額外資料)
			case 'edit':
				$has_edit = (get_post_var('has_edit') == 'Y')? true:false;

				//價格處理
				$arr_vice = $_POST['product_price'];
				// print_r($_POST);
				// exit;
				$arr_vice_sqlin = array();
				if(count($arr_vice) > 0){
					$vice_sort = 0;
					foreach ($arr_vice as $key => $value) {
						$vice_key = substr($key, 0, 3);

						$arr_data = array(
										'product_id' => $edit_id,
										'selling_price' => urldecode($value['price']),
										'selling_target' => urldecode(join(',', $value['target'])),
										'selling_online' => (empty($value['online']))? 0:1,
										'selling_sort' => $vice_sort
									);
						switch($vice_key){
							//新增價格
							case 'new':
								$new_key = $crud->create('product_selling_price', $arr_data);
								array_push($arr_vice_sqlin, $new_key);
								$has_edit = true;
								break;

							//修改價格
							default:
								$crud->update('product_selling_price', $arr_data, array('selling_id' => $key));
								array_push($arr_vice_sqlin, $key);
								break;
						}
						$vice_sort++;
					}
				}

				if(count($arr_vice_sqlin) > 0){
					$str_vice_sqlin = join(',', $arr_vice_sqlin);
					$crud->sql("DELETE 
								FROM `product_selling_price` 
								WHERE `product_id` = {$edit_id} AND `selling_id` NOT IN ({$str_vice_sqlin})");
					$has_edit = true;
				}

				if($_is_add){
					$model = get_post_var('txt_product_model');
					$arr_model = $crud->sql("SELECT *
											FROM `model`
											INNER JOIN `model_cate` ON `model_cate` = `cate_id`
											WHERE `model_id` = {$model}")[0];
					$cate_code = $arr_model['cate_code'];

					$ser_key = $cate_code.substr(date('y'), 1).date('m');
					$product_code = $ser_key.get_serial_number($ser_key, 'M');

					$arr_update_data = array(
									'product_code' => $product_code,
									'product_ver_code' => 0,
									'product_create_date' => date('Y-m-d')
								);
					$crud->update($table, $arr_update_data, array($primary_key => $edit_id));
				}
				else if($has_edit){
					$ver_code = get_post_var('txt_product_ver_code');
					$arr_use_code = array();
					for ($i = 0; $i < 10; $i++) { 
						$arr_use_code[] = (string)$i;
					}

					$arr_use_code = array_merge($arr_use_code, range('A', 'Z'));


					if($ver_code <> 'Z'){
						$next_index = array_search($ver_code, $arr_use_code) + 1;
						$next_ver = $arr_use_code[$next_index];
					}
					else{
						$next_ver = 'Z';
					}

					$arr_update_data = array(
											'product_ver_code' => $next_ver
										);
					$crud->update($table, $arr_update_data, array($primary_key => $edit_id));
				}

				$_GET['auth'] = 'QmrMMlYXG181JbR';
				include_once '../cron_get_currency.php';
				break;

			//刪除資料
			case 'del':
				$del_id = get_post_var('del_id');
				$result = $crud->delete('product_selling_price', array('product_id' => $del_id));
				$result = $crud->delete('product_album', array('host_id' => $del_id));
				$result = $crud->delete('product_album_basic', array('host_id' => $del_id));
				break;
		}

		return $append;
	}
?>