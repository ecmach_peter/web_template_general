<?php
	function get_extra_path($_path_type){
		global $crud, $table, $edit_id, $arr_data, $del_id;
		switch($_path_type){
			//編輯時d
			case 'edit':
				//無主資料夾時新增
				$_pdir = "../uploadimages/{$table}/";
				if (!is_dir($_pdir)){
					mkdir($_pdir,0777);
				}

				//無ID資料夾時新增
				$_year = get_post_var('txt_product_create_year');
				$dir = "{$_pdir}{$_year}/";
				if (!is_dir($dir)){
					mkdir($dir,0777);
				}

				//無ID資料夾時新增
				$arr_now_product = $crud->getid('product', array('product_id' => $edit_id));
				$_product_code = $arr_now_product['product_code'];
				$dir = "{$dir}{$_product_code}/";
				if (!is_dir($dir)){
					mkdir($dir,0777);
				}
				break;

			//讀取
			case 'read':
				$dir = "../uploadimages/{$table}/{$arr_data['product_create_year']}/{$arr_data['product_code']}/";
				break;

			//刪除
			case 'del':
				$arr_now_product = $crud->getid('product', array('product_id' => $del_id));
				$dir = "../uploadimages/{$table}/{$arr_now_product['product_create_year']}/{$arr_now_product['product_code']}/";
				break;
		}

		return $dir;
	}
?>