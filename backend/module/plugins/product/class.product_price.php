<?php
	class product_price{
		private $_option, $row_data, $crud;

		function __construct($crud){
			$arr_member = $crud->select('member', array());
			$member_list = '';
			if(count($arr_member) > 0){
				foreach ($arr_member as $key => $value) {
					$member_list .= <<<HTML
									<option value="{$value['member_id']}">{$value['member_name']}</option>
HTML;
				}
			}
			$this->member_list = $member_list;
		}

		//功能表單
		public function project_detail_form($arr_data){
			return $append;
		}

		//功能說明表單
		public function product_price_append($arr_data){
			global $_lock;
			$row_data = $arr_data['row_data'];
			$num = $arr_data['num'];
			$input_name = "product_price[{$row_data['selling_id']}]";
			$disabled = ($row_data['disabled'])? 'disabled':'';
			$checked = ($row_data['selling_online'] == 1 || $row_data['disabled'] == true)? 'checked':'';
			$option = $this->member_list;

			if(!empty($row_data['selling_target'])){
				$arr_target = explode(',', $row_data['selling_target']);
				if(count($arr_target) > 0){
					foreach ($arr_target as $key => $value) {
						$option = str_replace('value="'.$value.'"', 'value="'.$value.'" selected ', $option);
					}
				}
			}

			$append = <<<HTML
						<tr>
							<td><a href="javascript:;"><i class="fa fa-bars fa-lg cursor_move"></i></a></td>
							<td>{$num}</td>
							<td><input type="text" class="form-control" name="{$input_name}[price]" value="{$row_data['selling_price']}" {$disabled} {$_lock}></td>
							<td>
								<select name="{$input_name}[target][]" name="{$input_name}[target][]" class="product_price_target" data-live-search="true" {$_lock} data-size="10" data-selected-text-format="count" data-title="請選擇" data-style="btn-default nomt nomb" multiple data-actions-box="true"  data-container="body" {$disabled} {$_lock}>
									{$option}
								</select>
							</td>
							<td>
								<input id="btn_price_online-{$row_data['selling_id']}" name="{$input_name}[online]" class="cmn-toggle cmn-toggle-round" type="checkbox" {$checked} {$_lock} {$disabled}>
								<label for="btn_price_online-{$row_data['selling_id']}"></label>
							</td>
							<td><button type="button" class="btn btn-danger btn_del_detail_vice" {$_lock}><i class="fa fa-trash-o fa-lg"></i></button></td>
						</tr>
HTML;

			return $append;
		}
	}
?>