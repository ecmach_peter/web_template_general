<?php
	function plugins($plugin_value, $_type){
		global $_LANGUAGE,$crud,$primary_key;
		$str_append = '';
		$primary_id = $plugin_value[$primary_key];
		$_lock = ($plugin_value["{$table}_lock"] == 1)? 'disabled':'';
		switch($_type){
			//賣家處理
			case 'owner':
				if(empty($plugin_value['product_store'])){
					$_owner_member = $crud->getid('member', array('member_uniqid' => $plugin_value['product_owner']));
					$_owner_name = (empty($_owner_member['member_nickname']))? $_owner_member['member_name']:$_owner_member['member_nickname'];
					$url = './member-'.$_owner_member['member_id'];
				}
				else{
					$_owner_store = $crud->getid('store', array('store_id' => $plugin_value['product_store']));
					$_owner_name = $_owner_store['store_name'];
					$url = './store-'.$_owner_store['store_id'];
				}
				$append = <<<HTML
							<a href="{$url}" target="_blank"><i class="fa fa-link"></i></a> {$_owner_name}
HTML;
				break;
		}

		return $append;
	}

	$plugins_JS = <<<HTML
HTML;
?>