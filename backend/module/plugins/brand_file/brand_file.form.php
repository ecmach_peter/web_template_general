<?php
	function plugins($plugin_value){
		global $_LANGUAGE,$crud,$edit_id,$arr_data, $_lock;
		$_type = $plugin_value->_type;
		$_column = $plugin_value->column;
		$_class = $plugin_value->class;
		$_isrequired = ($plugin_value->required)? true:false;
		$_language = ($plugin_value->language)? true:false;
		$_required = ($_isrequired)? 'required="required"':'';
		$_unique = ($plugin_value->unique)? 'data-unique="true"':'';
		// $_lock = ($arr_data["{$table}_lock"] == 1 || (!$B_ARR_UNIT_RIGHTS['deit'] && $edit_id <> 'add'))? 'disabled':'';
		switch($_type){
			//賣家處理
			default:
				break;
		}

		return $append;
	}
	$plugins_JS = <<<JAVASCRIPT
		function chk_plugins(){
			var is_add = ($('#edit_id').val() == 'add')? true:false;
			var _perr = false;
			if((is_add && $('#fxt_file_file').val() == '' && $('#fxt_file_file_en').val() == '') || ($('.btn_del_file').length > 0 && $('.btn_del_file:checked').length == $('.btn_del_file').length)){
				do_notify.error('<i class="fa fa-exclamation-triangle fa-lg"></i> 請至少上傳一個檔案');
				_perr = true;
			}
			return _perr;
		}
JAVASCRIPT;
?>