<?php
	function plugins_button($_type = ''){
		global $_LANGUAGE,$crud,$edit_id,$table;

		if(empty($_type)) $_type = get_post_var('_type');

		switch($_type){
			//簽到
			case 'filter_brand':
				$arr_brand = $crud->select('brand', array(), array('brand_sort' => 'ASC'), array('brand_id', 'brand_title'));
				$str_brand = '';
				if(count($arr_brand) > 0){
					foreach ($arr_brand as $value) {
						$str_brand .= <<<HTML
									<option value="{$value['brand_id']}">{$value['brand_title']}</option>
HTML;
					}
				}
				$append = <<<HTML
							<select id="filter_view" name="filter_view[brand`.`brand_id]" data-live-search="true" class="selectpicker" data-width="fit" style="display:none;">
								<option value="">全部品牌</option>
								<option data-divider="true"></option>
								{$str_brand}
							</select>
HTML;
				break;
		}

		$_JS = <<<HTML
HTML;

		$arr = array(
					'append' => $append,
					'js' => $_JS
				);
		return $arr;
	}
?>