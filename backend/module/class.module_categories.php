<?php
	class module_categories{

		function __construct($crud){
			$this->crud = $crud;
		}

		public function cate_data(){
			$cate_data = <<<HTML
						<div class="dd">
							{$this->cate_loop(0)}
						</div>
						<div id="cate_init" style="display:none;">
							{$this->cate_init()}
						</div>
HTML;
			return $cate_data;
		}

		//分類階層處理
		private function cate_loop($_par_id){
			GLOBAL $_JSON_categories;
			$_INCLUDE_TABLE = INCLUDE_TABLE;
			$_main_cate = $this->crud->sql("SELECT *
											FROM `{$_INCLUDE_TABLE}_cate`
											WHERE `cate_par_id` = {$_par_id}
											ORDER BY `cate_sort`");
			$cate_data = '';

			if(count($_main_cate) > 0){
				$disabled = ($_par_id == 0 && $_JSON_categories->lock_primary)? 'disabled':'';
				foreach ($_main_cate as $key => $value) {
					//會持續取得下層分類直到無下層
					$_checked = ($value['cate_online'] == 1)? 'checked':'';
					$tmp_data .= <<<HTML
								<li class="dd-item" {$noDragToRoot} data-id="{$value['cate_id']}" data-par-id="{$value['cate_par_id']}">
									<div class="dd-handle dd2-handle">
										<i class="normal-icon fa fa-bars bigger-125"></i>
										<i class="drag-icon fa fa-arrows bigger-125"></i>
									</div>
									<div class="dd2-content btn-white">
										<span id="cate_title">{$value['cate_title']}</span>
										<div class="pull-right" style="padding-left: 10px">
												<button type="button" class="btn btn-primary btn-xs" id="btn_cate_edit"><i class="fa fa-pencil bigger-170"></i></button>
												<button type="button" class="btn btn-danger btn-xs btn_del_cate" {$disabled}><i class="fa fa-trash-o bigger-170"></i></button>
										</div>
										<div class="switch pull-right">
											<input id="btn_cate-{$value['cate_id']}" name="txt_cate_online" data-table="{$_INCLUDE_TABLE}_cate" data-primary_key="cate_id" data-id="{$value['cate_id']}" class="cmn-toggle cmn-toggle-round btn_switch" type="checkbox" {$_checked} {$_lock} {$disabled}>
											<label class="switch_label" for="btn_cate-{$value['cate_id']}"></label>
										</div>
									</div>
									
									{$this->cate_loop($value['cate_id'])}
								</li>
HTML;
				}
				$cate_data = <<<HTML
							<ol class="dd-list">
								{$tmp_data}
							</ol>
HTML;
			}
			return $cate_data;
		}

		private function cate_init(){
			$_INCLUDE_TABLE = INCLUDE_TABLE;
			$str_data = <<<HTML
							<li class="dd-item" data-id="varCateID">
								<div class="dd-handle dd2-handle">
									<i class="normal-icon fa fa-bars bigger-125"></i>
									<i class="drag-icon fa fa-arrows bigger-125"></i>
								</div>
								<div class="dd2-content btn-white">
									<span id="cate_title">varCateTitle</span>
									<div class="pull-right action-buttons">
										<div class="switch">
											<input id="btn_cate-varCateID" name="txt_cate_online" data-table="{$_INCLUDE_TABLE}_cate" data-primary_key="cate_id" data-id="varCateID" class="cmn-toggle cmn-toggle-round btn_switch" type="checkbox" varcatechecked {$_lock}>
											<label class="switch_label" for="btn_cate-varCateID"></label>
										</div>
										<a href="javascript:;">
											<i id="btn_cate_edit" class="fa fa-pencil bigger-170 text-primary"></i>
										</a>
										<a href="javascript:;">
											<i class="fa fa-trash-o bigger-170 text-danger btn_del_cate"></i>
										</a>
									</div>
								</div>
							</li>
HTML;
			return $str_data;
		}
	}
?>