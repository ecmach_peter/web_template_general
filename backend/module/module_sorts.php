<?php
	$_JSON = json_decode(SYSTEM_JSON);

	$_table = INCLUDE_TABLE;

	//取得資料標題(以第二欄位為主)，用於排序
	$_DB_NAME = DB_NAME;
	if($_JSON->set->sort->column){
		$second_column = $_JSON->set->sort->column;
	}
	else{
		$second_column = $crud->sql("SELECT `COLUMN_NAME`
									FROM `INFORMATION_SCHEMA`.`COLUMNS`
									WHERE `TABLE_SCHEMA` = '{$_DB_NAME}' AND `TABLE_NAME`='{$_table}'
									LIMIT 1,1")[0]['COLUMN_NAME'];
	}

	//取PRIMARY欄位名稱，用來計算筆數、操作用ID
	$primary_key = $crud->sql("SHOW KEYS FROM `{$_table}` WHERE Key_name = 'PRIMARY'")[0]['Column_name'];

	if($_JSON->button->categories){
		$arr_cate = $crud->select(INCLUDE_TABLE.'_cate', array());
		foreach ($arr_cate as $_key => $_value) {
			$arr_sort = $crud->sql("SELECT *
									FROM {$_table}
									WHERE `{$_table}_cate` = '{$_value['cate_id']}'
									ORDER BY `{$_table}_sort`");
			$tmp_data = '';
			foreach ($arr_sort as $key => $value) {
				if($_JSON->set->sort->photo){
					$tmp_data .= <<<HTML
									<li class="btn btn-default img" data-id="{$value[$primary_key]}"><i><img width="100%" src="../uploadimages/{$_table}/{$value[$primary_key]}/{$value[$second_column]}"></i></li>
HTML;
				}
				else{
					$tmp_data .= <<<HTML
									<li class="btn btn-default module" data-id="{$value[$primary_key]}"><i class="fa fa-bars fa-lg"></i>{$value[$second_column]}</i></li>
HTML;
				}
			}
			$arr['data'] .= <<<HTML
							<div class="mt50">
								<h3 class="nomt">{$_value['cate_title']}</h3>
								<ul class="sortable">
									{$tmp_data}
								</ul>
								<button type="button" class="btn btn-success btn_sort_save" data-id="{$_value['cate_id']}" ><i class="fa fa-save fa-lg"></i> 確認</button>
							</div>
HTML;
		}

		$JAVASCRIPT = <<<JAVASCRIPT
							$('.btn_sort_save').on('click', function(){
								var sort_data = $(this).prev().sortable("toArray", { attribute: "data-id" });
								doajax({
									url:'control',
									data:'type=sort&sort='+sort_data,
									callback:function(){
										update_list();
										do_notify.success();
									}
								});
							});
JAVASCRIPT;
	}
	else{
		//----- 額外欄位
		$extra_column = $_JSON->set->extra_column;
		if(count($extra_column) > 0){
			foreach ($extra_column as $key => $value) {
				$_column .= ", ({$value}) AS `{$key}`";
			}
		}

		$arr_sort = $crud->sql("SELECT *{$_column}
								FROM {$_table}
								ORDER BY `{$_table}_sort`");
		$tmp_data = '';
		foreach ($arr_sort as $key => $value) {
			if($_JSON->set->sort->photo){
				$tmp_data .= <<<HTML
								<li class="btn btn-default img" data-id="{$value[$primary_key]}"><i><img width="100%" src="../uploadimages/{$_table}/{$value[$primary_key]}/{$value[$second_column]}"></i></li>
HTML;
			}
			else{
				$tmp_data .= <<<HTML
								<li class="btn btn-default module" data-id="{$value[$primary_key]}"><i class="fa fa-bars fa-lg"></i>{$value[$second_column]}</i></li>
HTML;
			}
		}
		$arr['data'] .= <<<HTML
							<ul class="sortable">
								{$tmp_data}
							</ul>
HTML;

		$JAVASCRIPT = <<<JAVASCRIPT
							$('#btn_sort_save').off().on('click', function(){
								var sort_data = $('.sortable').sortable("toArray", { attribute: "data-id" });
								doajax({
									url:'control',
									data:'type=sort&sort='+sort_data,
									callback:function(){
										update_list();
										do_notify.success();
									}
								});
							});
JAVASCRIPT;
	}

	$arr['data'] .= <<<HTML
						<script>
							$( ".sortable" ).sortable({
								handle: "i",
								items: "> li",
								update: function( event, ui ) {
								}
							});
							{$JAVASCRIPT}
						</script>
HTML;

	echo json_encode($arr);
?>