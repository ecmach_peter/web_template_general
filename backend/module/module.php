<?php
	include_once('./module/class.module_control.php');
	$_JSON = json_decode(SYSTEM_JSON);
	$_module = new module_control($_JSON);
	$default_pagesize = (empty($_JSON->set->pagesize))? 10:$_JSON->set->pagesize;

	//是否為純檢視模式
	$is_view = ($_JSON->set->is_view)? true:false;

	if(!empty(B_VAR_1)) $default_form = B_VAR_1;

	//頁籤處理
	$str_tabs = '';
	if($_JSON->set->tabs && count($_JSON->set->tabs) > 0){
		$default_tab = false;
		foreach ($_JSON->set->tabs as $key => $value) {
			if($default_tab === false || $value->active){
				$default_tab = $key;
			}
			$active = ($value->active)? 'active':'';
			$label = ($value->label)? $value->label:'default';
			$str_tabs .= <<<HTML
					<li role="presentation" class="module_tabs {$active}" data-tab="{$key}"><a href="#home" aria-controls="home" role="tab" data-toggle="tab">{$value->text} <span class="label label-{$label}"></span></a></li>
HTML;
		}

		$str_tabs = <<<HTML
						<ul class="nav nav-tabs" role="tablist">
							{$str_tabs}
						</ul>
						<input type="hidden" id="now_tab" value="{$default_tab}">
HTML;
	}
?>
<!DOCTYPE html>
<html lang="en">

<head>
	<?php include_once 'back_head.php'; ?>
</head>

<body>

	<div id="wrapper">

		<!-- Navigation -->
		<nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
			<?php include_once 'back_navbar.php'; ?>

			<div class="navbar-default sidebar" role="navigation">
				<?php include_once 'back_navbar_left.php'; ?>
			</div>
			<!-- /.navbar-static-side -->
		</nav>

		<!-- Page Content -->
		<div id="page-wrapper">
			<div class="container-fluid">
				<div class="row">
					<div class="col-lg-12">
					</div>
				</div>
				<div class="row">
					<div id="module_list">
						<div class="col-md-12">
							<h2 class="page-header">
								<i class="fa fa-<?=$_navbar_icon;?>"></i>&nbsp;<?=$_navbar_name;?>&nbsp;<small>列表</small>
							</h2>
						</div>
						<div class="col-md-12 text-right">
							<form class="form-inline">
								<?=$_module->control_button();?>
								<select id="page_view" class="selectpicker" data-width="fit" style="display:none;">
									<option value="10" <?=($default_pagesize == 10)? 'selected':'';?>>顯示10筆</option>
									<option value="30" <?=($default_pagesize == 30)? 'selected':'';?>>顯示30筆</option>
									<option value="50" <?=($default_pagesize == 50)? 'selected':'';?>>顯示50筆</option>
								</select>
							</form>
						</div>
						<div class="col-md-12">
							<?=$str_tabs;?>
							<table class="table table-bordered table-striped table-hover tc-table table-primary footable mt10" data-page-size="2000">
								<thead>
									<tr>
										<?=$_module->table_header();?>
									</tr>
								</thead>
								<tbody id="list_tbody">
								</tbody>
							</table>
							<div class="col-md-12 text-center page_info">
							</div>
						</div>
						<!-- /.row -->
						<div class="row">
							<nav class="text-center">
								<ul class="pagination">
								</ul>
							</nav>
						</div>
					</div>

					<div id="module_form" style="display:none;">
						<div class="col-md-12">
							<h2 class="page-header text-success">
								<i class="fa fa-<?=$_navbar_icon;?>"></i>&nbsp;<?=$_navbar_name;?>&nbsp;<small><span class="act_title"></span></small>
							</h2>
						</div>
						<div class="col-md-12">
							<form class="form-horizontal">
								<div id="form_data"></div>
								<hr>
								<div class="row">
									<div class="col-md-8 col-md-offset-4">
							<?php
								if($is_view){
									echo <<<HTML
										<button type="button" class="btn btn-danger" id="form_cancel" ><i class="fa fa-reply fa-lg"></i> 返回</button>
HTML;
								}
								else{
									echo <<<HTML
										<button type="submit" class="btn btn-success" ><i class="fa fa-save fa-lg"></i> 確認</button>
										<button type="button" class="btn btn-danger" id="form_cancel" ><i class="fa fa-times fa-lg"></i> 取消</button>
HTML;
								}
							?>
									</div>
								</div>
								<input id="edit_id" name="edit_id" type="hidden">
							</form>
						</div>
					</div>
					<div id="module_sort" style="display:none;">
						<div class="col-md-12">
							<h2 class="page-header text-success">
								<i class="fa fa-<?=$_navbar_icon;?>"></i>&nbsp;<?=$_navbar_name;?>&nbsp;<small><span class="act_title"></span></small>
							</h2>
						</div>
						<div class="col-md-12">
							<div id="form_sort">
							</div>
							<div class="row">
								<div class="col-md-8 col-md-offset-4">
								<?php
									if(!$_JSON->button->categories){
										echo <<<HTML
											<button type="button" id="btn_sort_save" class="btn btn-success" ><i class="fa fa-save fa-lg"></i> 確認</button>
HTML;
									}
								?>
									<button type="button" class="btn btn-danger" id="form_cancel" ><i class="fa fa-reply"></i> 返回</button>
								</div>
							</div>
						</div>
					</div>
					<div id="module_categorie" style="display:none;">
						<div class="col-md-12">
							<h2 class="page-header text-success">
								<i class="fa fa-<?=$_navbar_icon;?>"></i>&nbsp;<?=$_navbar_name;?>&nbsp;<small><span class="act_title"></span></small>
							</h2>
						</div>
						<div class="col-sm-12">
							<div class="row">
								<div class="col-md-9 col-xs-12 col-lg-offset-4 col-md-offset-">
									<button type="button" class="btn btn-danger" id="form_cancel" ><i class="fa fa-reply"></i> 返回</button>
									<button type="button" class="btn btn-primary" id="btn_add_cate" ><i class="fa fa-plus-square fa-lg"></i> 新增分類</button>
									<button type="button" class="btn btn-success" id="btn_save_cate" ><i class="fa fa-save fa-lg"></i> 儲存階層排序</button>
								</div>
								<div id="form_categorie" class="col-md-9 col-xs-12 col-lg-offset-4 col-md-offset-3">
								</div>
								<div class="col-md-9 col-xs-12 col-lg-offset-4 col-md-offset-">
									<button type="button" class="btn btn-danger" id="form_cancel" ><i class="fa fa-reply"></i> 返回</button>
									<button type="button" class="btn btn-primary" id="btn_add_cate" ><i class="fa fa-plus-square fa-lg"></i> 新增分類</button>
									<button type="button" class="btn btn-success" id="btn_save_cate" ><i class="fa fa-save fa-lg"></i> 儲存階層排序</button>
								</div>
							</div>
						</div>
						<div id="div_form_categorie" style="display:none;">
							<form class="form-horizontal form_categorie">
								<div id="form_categorie_data"></div>
								<input id="edit_categorie_id" name="edit_id" type="hidden">
								<input id="external" name="external" type="hidden" value="<?=INCLUDE_ACT.'_cate';?>">
							</form>
						</div>
					</div>
				</div>
				<!-- /.row -->
			</div>
			<!-- /.container-fluid -->
		</div>
		<!-- /#page-wrapper -->

	</div>
	
	<div class="modal fade" id="image_view" tabindex="-1" role="dialog">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title">圖片預覽</h4>
				</div>
				<div class="modal-body">
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-primary" data-dismiss="modal">關閉</button>
				</div>
			</div><!-- /.modal-content -->
		</div><!-- /.modal-dialog -->
	</div><!-- /.modal -->
	<input id="hx_act" type="hidden" value="<?=$act;?>" />
	<!-- /#wrapper -->
	<?php include_once 'back_footer.php'; ?>
	<script type="text/javascript">
		$(function(){

			var _act = $('#hx_act').val();
			$(window).bind("popstate", function () {
				var state = event.state;
				if(state){
					if(state.eid){
						var eid = state.eid;
						history.replaceState({eid:eid},'',"./"+_act+"-"+eid);   //新增紀錄
						update_form(eid);
					}
					else if(state.act){
						var act = state.act,
							btn = state.btn;
						history.replaceState(null,'',"./"+_act+"-"+act);   //新增紀錄
						$('#'+btn).click();
					}
				}
				else{
					display_area('list');
					history.replaceState(null,'',"./"+_act);   //新增紀錄
				}
			});

			$('.select2').select2({
				width: 100,
				minimumResultsForSearch: Infinity
			});

			$('#page_view').on('change', function(){
				update_list();
			});

			$('#module_list').on('click', '#btn_edit', function(){
				var eid = $(this).data('id');
				history.pushState({eid:eid},'',"./"+_act+"-"+eid);   //新增紀錄
				update_form(eid);
			});

			$('#btn_add').on('click', function(){
				var eid = 'add';
				history.pushState({eid:eid},'',"./"+_act+"-"+eid);   //新增紀錄
				update_form(eid);
			});

			$('#btn_sort').on('click', function(){
				var $this = $(this);
				history.pushState({act:$this.data('act'),btn:$this.attr('id')},'',"./"+_act+"-"+$this.data('act'));

				doajax({
					url: 'sorts',
					type: 'json',
					callback:function(msg){
						$('#form_sort').html(msg.data);
						$('.act_title').text('排序');
						display_area('sort');
					}
				});
			});

			$('#btn_categorie').on('click', function(){
				var $this = $(this);
				history.pushState({act:$this.data('act'),btn:$this.attr('id')},'',"./"+_act+"-"+$this.data('act'));
				doajax({
					url: 'categories',
					type: 'json',
					callback:function(msg){
						$('#form_categorie').html(msg.data);
						$('.act_title').text('分類');
						display_area('categorie');
					}
				});
			});

			$('#module_list').on('click', '#btn_del', function(){
				var del_id = $(this).data('id');
				bootbox.dialog({
					message: "<h4><strong>確定要刪除這項資料嗎?</strong></h4>",
					size: 'small',
					buttons: {
						success: {
							label: "確認",
							className: "btn-danger nomargin",
							callback: function() {
								doajax({
									url: 'control',
									type: 'json',
									data: 'type=del&del_id='+del_id,
									beforeSend: function(xhr, opts) {
										$.blockUI();
									},
									callback:function(msg){
										update_list();
										$.unblockUI();
										(msg.sts)? do_notify.success('<i class="fa fa-check fa-lg"></i> 資料刪除完成'):do_notify.error('<i class="fa fa-exclamation-triangle fa-lg"></i> '+msg.msg);
									}
								});
							}
						},
						cancel: {
							label: "取消",
							className: "btn-default nomargin"
						}
					}
				});
			});

			$('[id="form_cancel"]').on('click', clean_form);

			$('#cate_filter').on('change', function (){
				update_list();
			});

			$('[id="filter_view"]').on('change', function (){
				update_list();
			});
			
			$('#btn_search').on('paste', function (){
				var $this = $(this);
				setTimeout(function(){$this.keyup();}, 4);
			}).on('keyup', function (){
				update_list(1, true);
			});

			$('#btn_search_clean').on('click', function(){
				$('#btn_search').val('').keyup();
			});

			$('#list_tbody').on('click', '.btn_switch', function(){
				var $this = $(this),
					_name = $this.attr('name'),
					_data = {
						'edit_id': $this.data('id')
					};
					_data[_name] = ($this.is(':checked'))? 1:0;

				doajax({
					url: 'control',
					type: 'json',
					data: 'type=switch_change&'+jQuery.param(_data),
					beforeSend: function(xhr, opts) {
						$.blockUI();
					},
					callback:function(msg){
						$.unblockUI();
						(msg.sts)? do_notify.success():do_notify.error();
					}
				});
			});

			$('#module_form form').on('submit', function(){
				var $this = $(this),
					_error = false;

				if($('#edit_id').val() != 'add'){
					$this.find(":password").each(function(){
						var $this = $(this);
						if($this.val() == '') $this.attr('disabled', true);
					});
				}

				$this.find(":required:enabled,[data-required='required']").each(function(){
					var that = $(this),
						str_req = $("label[for='"+that.attr('id')+"']").text().replace(':','').replace('：',''),
						str_mode = (that.is(':file'))? '上傳':'輸入',
						regExp = /^[\d]+$/;
					if(that.hasClass('wysiwyg')){
						var _name = that.attr('id');
						if(eval('CKEDITOR.instances.'+_name+'.getData()') == ''){
							_error = true;
						}
					}
					else if(that.val() == '' || that.val() == null){
						_error = true;
					}

					if(_error){
						do_notify.error('<i class="fa fa-exclamation-triangle fa-lg"></i> 請'+str_mode+' '+str_req+'');
						$('body').animate({
							scrollTop:$('#'+that.attr('id')).parents('.form-group').offset().top - 130
						 });
						return false;
					}
				});

				if(!_error){
					$this.find("[data-number='true']").each(function(){
						var that = $(this),
							str_req = $("label[for='"+that.attr('id')+"']").text().replace(':','').replace('：',''),
							str_mode = (that.is(':file'))? '上傳':'輸入',
							regExp = /^[\d]|-[\d]+$/;
						if(!that.data('required') && that.val() == '') return;
						if(that.data('number') && !regExp.test(that.val())){
							_error = true;
							str_mode += '數字:';
						}

						if(_error){
							do_notify.error('<i class="fa fa-exclamation-triangle fa-lg"></i> 請'+str_mode+' '+str_req+'');
							$('body').animate({
								scrollTop:$('#'+that.attr('id')).parents('.form-group').offset().top - 130
							 });
							return false;
						}
					});
				}
				if(!_error){
					$this.find('.div_checkbox[data-check_required="required"]').each(function(){
						var that = $(this);
						if(that.find(':checked').length < 1){
							var str_req = $("label[for='"+that.find(':checkbox:first').attr('id')+"']").text().replace(':','').replace('：','');
							_error = true;
							do_notify.error('<i class="fa fa-exclamation-triangle fa-lg"></i> 至少要選擇一個 '+str_req+'');
							$('body').animate({
								scrollTop:$('#'+that.find(':checkbox:first').attr('id')).parents('.form-group').offset().top - 130
							});
							return false;
						}
					});
				}

				if(_error) return false;

				$this.find('[data-unique="true"]').each(function(){
					var that = $(this),
						str_req = $("label[for='"+that.attr('id')+"']").text().replace(':','').replace('：',''),
						obj = {
							type: 'unique',
							id : $('#edit_id').val(),
							value: that.val(),
							column : that.data('column')
						};

					doajax({
						url: 'control',
						data: jQuery.param(obj),
						type: 'json',
						async: false,
						callback: function(msg){
							_error = (msg.unique)? false:true;
						}
					});

					if(_error){
						do_notify.error('<i class="fa fa-exclamation-triangle fa-lg"></i> 此 '+str_req+' 已經使用過了');
						$('body').animate({
							scrollTop:$('#'+that.attr('id')).parents('.form-group').offset().top - 130
						});
						return false;
					}
				});

				if(_error) return false;

				if(window.chk_plugins) {
					_error = chk_plugins();
				}

				if(_error) return false;

				$this.find(".wysiwyg").each(function(){
					var that = $(this);
					that.val(eval('CKEDITOR.instances.'+that.attr('id')+'.getData()'));
				});

				var now_type = $('#edit_id').val();
				doajax({
					url: 'control',
					type: 'json',
					data: 'type=edit&'+$this.serialize(),
					files: $(":file:enabled"),
					beforeSend: function(xhr, opts) {
						$.blockUI();
					},
					callback:function(msg){
						var add_msg = null,
							edit_id = $('#edit_id'),
							page = (edit_id == 'add')? 1:$('.pagination .active').text();
						if(msg.cid){
							edit_id.val(msg.cid);
							add_msg = '<i class="fa fa-check"></i> 資料新增完成';
						}
						if($('#dont_back_list').val() == 'N'){
							clean_form();
						}
						else{
							var eid = edit_id.val();
							if(now_type == 'add'){
								history.replaceState({eid:eid},'',"./"+_act+"-"+eid);   //新增紀錄
							}
							update_form(eid);
						}
						update_list(page);
						$.unblockUI();
						do_notify.success(add_msg);
					}
				});
				return false;
			});


			function clean_form(){
				display_area('list');
				$('#form_data').html('');
				history.pushState(null,'',"./"+_act);   //新增紀錄
			}

			function update_form(eid){
				$('.act_title').text((eid == 'add')? '新增':'修改');

				doajax({
					url: 'form',
					type: 'json',
					data: 'id=' + eid,
					beforeSend: function(xhr, opts) {
						$.blockUI();
					},
					callback:function(msg){
						if(msg.sts){
							display_area('form');
							$('#edit_id').val(eid);
							$('#form_data').html(msg.form);
						}
						else{
							clean_form();
							display_area('list');
						}
						$.unblockUI();
					}
				});
			}
			
			$('.footable').footable();

			$('.pagination').on('click', 'li[data-page]', function(){
				var page = $(this).data('page');
				update_list(page);
			});

			$('.btn-sort').on('click', function(){
				var $this = $(this),
					that = $this.parents('th'),
					mode = ($this.data('mode') == 'd')? 'a':'d';

				$('.btn-sort[data-mode]:visible').hide().parents('th').find('.fa-sort').show();
				that.find('.btn-sort').hide().filter('[data-mode="'+mode+'"]').show();
				update_list();
			});

			$('.module_tabs').on('click', function(){
				$('#now_tab').val($(this).data('tab'));
				update_list();
			});

			<?php
				//AJAX重新整理
				if(!empty($default_form)){
					$arr_act = array('categorie','sort');
					if(array_search(B_VAR_1, $arr_act) === false){
						echo <<< JAVASCRIPT
							update_form('{$default_form}');
JAVASCRIPT;
					}
					else{
						$d_act = B_VAR_1;
						echo <<< JAVASCRIPT
							$('[data-act="{$d_act}"]').click();
JAVASCRIPT;
					}
				}
			?>
		});
		function display_area(area){
			switch(area){
				case 'form':
					$('#module_form, #breadcrumb_edit').fadeIn(200);
					$('#module_list, #module_sort, #breadcrumb_list, #module_categorie').hide();
					break;
				case 'list':
					$("#module_list, #breadcrumb_list").fadeIn(200);
					$("#module_form, #module_sort, #breadcrumb_edit, #module_categorie").hide();
					break;
				case 'sort':
					$('#module_sort, #breadcrumb_edit').fadeIn(200);
					$('#module_list, #breadcrumb_list, #module_form, #module_categorie').hide();
					break;
				case 'categorie':
					$('#module_categorie, #breadcrumb_edit').fadeIn(200);
					$('#module_list, #breadcrumb_list, #module_form, #module_sort').hide();
					break;
			}
		}
		update_list();
		function update_list(page, issearch){
			if(!page) page = 1;
			var _sort = $(".btn-sort[data-mode]:visible"),
				v_data = {
					nowpage: page.toString(),
					page_view: $('#page_view').val(),
					cate_filter: $('#cate_filter').val(),
					search: $('#btn_search').val(),
					sortby: _sort.parents('th').data('column'),
					sort: _sort.data('mode'),
					tabs: ($('#now_tab').get(0))? $('#now_tab').val():''
				};
			doajax({
				url: 'page',
				type: 'json',
				data: jQuery.param(v_data)+'&'+$('[id="filter_view"], .daterange').serialize(),
				beforeSend: function(xhr, opts) {
					if(!issearch) $.blockUI();
				},
				callback:function(msg){
					$('#list_tbody').html(msg.list); 
					$('.page_info').html(msg.page_info);
					$('.pagination').html(msg.pagination);
					$('.footable').trigger('footable_redraw');
					$.unblockUI();
					if(msg.count_tabs){
						jQuery.each(msg.count_tabs, function(item, val) {
							$('.nav-tabs [data-tab="'+item+'"]').find('span').text(val);
						});
					}
				}
			});
		}
		function update_categorie_form(){
			doajax({
				url: 'categories',
				type: 'json',
				callback:function(msg){
					$('#form_categorie').html(msg.data);
					$('.act_title').text('分類');
					display_area('categorie');
				}
			});
		}
	</script>
	<?=$_module->button_js();?>
</body>

</html>
