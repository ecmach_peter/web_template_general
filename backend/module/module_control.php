<?php
	$_POST = array_map('fix_ajax_urlencode', $_POST); 
	$_JSON = json_decode(SYSTEM_JSON);

	//外掛模組
	$_plugins = $_JSON->set->plugins;
	$_act = INCLUDE_ACT;
	if($_plugins->control){
		include(dirname(__FILE__)."/plugins/{$_act}/{$_act}.control.php");
	}

	$_type = get_post_var('type');

	//資料表名稱
	$is_external = false;
	if(!empty(get_post_var('external'))){	//外部使用表單模組(如分類)
		$external = get_post_var('external');
		$table = $external;
		$is_external = true;
	}
	else{
		$table = INCLUDE_TABLE;
	}

	//外掛路徑模組
	if($_JSON->set->plugins->extra_path && !$is_external){
		include(dirname(__FILE__)."/plugins/{$_act}/{$_act}.extra_path.php");
	}

	//取PRIMARY欄位名稱，用來計算筆數、操作用ID
	$primary_key = $crud->sql("SHOW KEYS FROM `{$table}` WHERE Key_name = 'PRIMARY'")[0]['Column_name'];

	//取得是否有最後編輯紀錄欄位
	$arr_creator = $crud->sql("SHOW COLUMNS FROM `{$table}` WHERE `Field` = 'creator'");
	$have_creator = (count($arr_creator) > 0 && $arr_creator !== false)? true:false;

	switch($_type){
		//修改資料
		case 'edit':
		// print_r($_POST);exit;
			$_activity = '';
			$edit_id = get_post_var('edit_id');
			$arr_do_file = array();
			$arr_del_file = array();	//刪除檔案(單檔上傳)
			foreach ($_POST as $key => $value) {
				$_typ = substr($key, 0, 3);
				$_column = substr($key, 4);
				if(gettype($value) == 'string') $value = urldecode($value);

				switch ($_typ) {
					//文字輸入
					case 'txt':
						$post_data[$_column] = htmlspecialchars($value, ENT_QUOTES);
						break;

					//複選按鈕
					case 'cxt':
						$post_data[$_column] = (empty($value))? '':join(',', $value);
						break;

					//連結輸入
					case 'txl':
						$post_data[$_column] = serialize($value);
						break;

					//加密欄位
					case 'sxt':
						if(!empty($value)){
							$post_data[$_column] = encrypt($value, 'iMVFDeWhYq');
						}
						break;

					//密碼欄位
					case 'pxt':
						$post_data[$_column] = password_hash($value, PASSWORD_DEFAULT, array('cost'=>12));;
						break;

					//相簿排序、刪除
					case 'fxt':
						$file_json = json_decode($value, true);

						//若是多圖上傳，儲存data資料待後面處理
						if($edit_id <> 'add' && !empty($file_json['multiple'])){
							$arr_do_file[$key] = $file_json;
						}
						break;

					//單檔上傳刪除檔案
					case 'del':
						$arr_del_file[] = $_column;
						$post_data[$_column] = '';
						break;

					//JSON
					case 'jxt':
						$post_data[$_column] = json_encode($value);
						break;
				}
			}

			if($edit_id <> 'add'){
				if($_JSON->set->plugins->extra_path && !$is_external){
					$dir = get_extra_path('edit');
				}
				else{
					//無主資料夾時新增
					$_pdir = "../uploadimages/{$table}/";
					if (!is_dir($_pdir)){
						mkdir($_pdir,0777);
					}

					//無ID資料夾時新增
					$dir = "{$_pdir}{$edit_id}/";
					if (!is_dir($dir)){
						mkdir($dir,0777);
					}
				}
			}

			//刪除檔案
			if(count($arr_del_file) > 0){
				$arr_old_file = $crud->getid($table, array($primary_key => $edit_id));
				if(!empty($arr_old_file[$primary_key])){
					foreach ($arr_del_file as $value) {
						//刪除檔案
						if(!empty($arr_old_file[$value])){
							$old_photo = explode('.', $arr_old_file[$value]);

							//搜尋資料夾內所有相關檔案
							$_arr_thumbnail = glob($dir.$old_photo[0].'*');
							foreach ($_arr_thumbnail as $d_value) {
								if(!empty($d_value) && file_exists($d_value)){
									unlink($d_value);
								}
							}
						}
					}
				}
			}

			//紀錄最後編輯人員
			if($have_creator){
				$_MM_Username = $_SESSION['MM_Username'];
				$_now_time = date('Y-m-d H:i:s');

				$post_data['editor'] = $_MM_Username;
			}

			if($edit_id == 'add'){
				$_activity = '新增';
				//紀錄建檔人員
				if($have_creator){
					$post_data['creator'] = $_MM_Username;
					$post_data['create_time'] = $_now_time;
				}
				$arr['cid'] = $crud->create($table, $post_data);
				$arr['sts'] = ($arr['cid'] === false)? false:true;

				$edit_id = $arr['cid'];
				$_is_add = true;
			}
			else{
				$_activity = '修改';
				$arr['cid'] = $_is_add = false;
				if(count($post_data) > 0) $arr['sts'] = $crud->update($table, $post_data, array($primary_key => $edit_id));

				//修改權限前先將該用戶所有權限刪除
				// if(INCLUDE_ACT == 'user') $crud->delete('rights', array('user_id' => $edit_id));
			}

			// if(INCLUDE_ACT == 'user'){
			// 	$_rights = $_POST['rights'];
			// 	foreach ($_rights as $key => $value) {
			// 		$crud->create('rights', array('system_code' => $value, 'user_id' => $edit_id));
			// 	}
			// }

			//額外資料處理外掛
			if($_plugins->edit){
				plugins('edit');
			}

			//外掛路徑模組
			if($_JSON->set->plugins->extra_path && !$is_external){
				$dir = get_extra_path('edit');
			}
			else{
				//無主資料夾時新增
				$_pdir = "../uploadimages/{$table}/";
				if (!is_dir($_pdir)){
					mkdir($_pdir,0777);
				}

				//無ID資料夾時新增
				$dir = "{$_pdir}{$edit_id}/";
				if (!is_dir($dir)){
					mkdir($dir,0777);
				}
			}

			//圖片處理
			// print_r($_POST);
			// print_r($_FILES);
			// exit;
			$sql_in_album = array();
			if(count($_FILES) > 0){
				include_once('./include/imgresize.php');
				include_once('./include/ImageResize.php');
				$arr_ok_file = array('.pdf', '.doc', '.docx', '.xls', '.xlsx', '.txt', '.rar', '.zip', '.7z', '.jpg', '.jpeg', '.png', '.gif');
				foreach ($_FILES as $key => $value) {
					$file_data = json_decode($_POST[$key], true);
					$is_multiple = ($file_data['multiple'])? true:false;

					//取得縮圖別名
					$del_thumbnail = $_JSON->set->del->file->column->$file_data['column']->thumbnail;
					if(empty($del_thumbnail)){
						$del_thumbnail = array('');
					}
					else{
						$del_thumbnail[] = '';
					}

					foreach($value['name'] as $_key => $_value){
						$tmp_file = $value['tmp_name'][$_key];	//伺服器檔案位置
						$file_type = explode('/',$value['type'][$_key]);	//檔案格式
						$exname = strtolower(substr($value['name'][$_key], strrpos($value['name'][$_key],'.')));	//副檔名
						$random_name = substr($table, 0, 3).uniqid(random_str(4));
						$new_name = $random_name.$exname;	//產生隨機檔名，資料表前三個字母+隨機亂數(banwFAG5833ae529504f.jpg)

						//不允許上傳的檔案直接略過
						if($value['size'][$_key] > 4950000){	//檔案大小超過限制
							continue;
						}
						else if($isphoto == 'Y' && $file_type[0] <> 'image'){	//非圖檔
							continue;
						}
						else if($isphoto == 'N' && array_search($exname, $arr_ok_file) === false){	//非允許的副檔名
							continue;
						}

						$origin_img = $dir.$new_name;	//上傳的檔案路徑
						if(move_uploaded_file($tmp_file, $origin_img)){
							if($is_multiple){	//多圖上傳處理
								//將檔名資料存進相簿資料表裡
								$arr_data = array('album_photo' => $new_name, 'host_id' => $edit_id);
								$result = $crud->create($file_data['table'], $arr_data);

								//儲存資料列的ID用於保留(後續會刪除其他資料列)
								$sql_in_album[$file_data['table']] .= $result.',';
							}
							else{	//單張圖上傳處理
								//刪除舊照片
								if(!empty($file_data['old_photo'])){
									$old_photo = explode('.', $file_data['old_photo']);

									//搜尋資料夾內所有相關檔案
									$_arr_thumbnail = glob($dir.$old_photo[0].'*');
									foreach ($_arr_thumbnail as $d_value) {
										if(!empty($d_value) && file_exists($d_value)){
											unlink($d_value);
										}
									}
								}

								//將照片名稱回存至主資料
								$crud->update($table, array($file_data['column'] => $new_name), array($primary_key => $edit_id));
							}
						}

						//縮圖處理
						/*---
							將取得 b,115,90,a|b,2000,133
							以 | 分隔不同縮圖
							[0]	縮圖模式(b-等比縮;c-裁切)
							[1]	縮圖寬度
							[2]	縮圖高度
							[3]	縮圖別名(未設定此參數則代表覆蓋原檔)
						---*/
						if(!empty($file_data['thumbnail'])){
							$_thumbnail = explode('|', $file_data['thumbnail']);
							if(count($_thumbnail) > 0){
								foreach ($_thumbnail as $t_key => $t_value) {
									$arr_thumbnail_set = explode(',', $t_value);

									$set_mode = $arr_thumbnail_set[0];											//縮圖模式(b-等比縮;c-裁切)
									$set_width = $arr_thumbnail_set[1];											//縮圖寬度
									$set_height = $arr_thumbnail_set[2];										//縮圖高度
									$set_rename = (empty($arr_thumbnail_set[3]))? '':$arr_thumbnail_set[3];		//aaa_x.jpg 中的 x
									$target_img = (empty($set_rename))? $origin_img:$dir.$random_name.'_'.$set_rename.$exname;	//若無設定別名參數則直接覆蓋原檔

									switch ($set_mode) {
										//等比縮
										case 'b':
											$image_rs = new \Eventviva\ImageResize($origin_img);
											$image_rs->resizeToBestFit($set_width, $set_height);
											$image_rs->save($target_img);
											break;

										//裁切
										case 'c':
											$image_rs = new \Eventviva\ImageResize($origin_img);
											$image_rs->crop($set_width, $set_height);
											$image_rs->save($target_img);
											break;
									}
								}
							}
						}
					}
				}

				if(count($sql_in_album) > 0){
					foreach ($sql_in_album as $s_key => $s_value) {
						$sql_in_album[$s_key] = substr($sql_in_album[$s_key], 0, strlen($sql_in_album[$s_key]) - 1);
					}
				}
			}

			//是否有多圖上傳需要處理
			if(count($arr_do_file) > 0){
				foreach ($arr_do_file as $key => $file_json) {
					if($edit_id <> 'add' && !empty($file_json['multiple'])){
						//進行排序
						if (empty($sql_in_album[$file_json['table']]) && !empty($file_json['sort'])) {	//無新增圖檔的排序
							$sql_in_album[$file_json['table']] = join(',', $file_json['sort']);
						}
						else if(!empty($sql_in_album[$file_json['table']])){		//有新增圖檔的排序
							$sql_in_album[$file_json['table']] = (empty($file_json['sort']))? $sql_in_album[$file_json['table']]:join(',', $file_json['sort']).','.$sql_in_album[$file_json['table']];
						}
						$crud->multi_sql("SET @sort = 0;
											UPDATE `{$file_json['table']}` SET `album_sort`=(@sort:=@sort+1) ORDER BY FIELD(`album_id`, {$sql_in_album[$file_json['table']]});");

						//刪除不再列表內的圖片
						$sql_in_album[$file_json['table']] = (empty($sql_in_album[$file_json['table']]))? '':"AND `album_id` NOT IN ({$sql_in_album[$file_json['table']]})";
						$arr_notin_album = $crud->sql("SELECT *
														FROM {$file_json['table']}
														WHERE `host_id` = '{$edit_id}' {$sql_in_album[$file_json['table']]}");
						if(count($arr_notin_album) > 0){
							foreach ($arr_notin_album as $na_value) {
								if(!empty($na_value['album_photo'])){

									$album_photo = explode('.', $na_value['album_photo']);

									//搜尋資料夾內所有相關檔案
									$_arr_thumbnail = glob($dir.$album_photo[0].'*');
									foreach ($_arr_thumbnail as $d_value) {
										if(!empty($d_value) && file_exists($d_value)){
											unlink($d_value);
										}
									}
								}
							}

							//刪除相簿資料
							$crud->sql("DELETE FROM {$file_json['table']}
										WHERE `host_id` = '{$edit_id}' {$sql_in_album[$file_json['table']]}");
						}
					}
				}
			}

			//----- 紀錄修改資訊
			$_json_activity = $_JSON->set->activity_column;
			$_before = (empty($_json_activity->before))? '':$_json_activity->before;
			$_after = (empty($_json_activity->after))? '':$_json_activity->after;

			//需使用JOIN語法
			if($_json_activity->join){
				//SQL JOIN語法產生
				$arr_join = $_JSON->set->join;
				if($arr_join){
					$sql_join = '';
					foreach($arr_join as $value){
						$on_table = ($value->on_table)? $value->on_table:$table;
						$sql_join .= ' INNER JOIN `'.$value->table.'` ON `'.$value->table.'`.`'.$value->primary.'` = `'.$on_table.'`.`'.$value->on_primary.'` ';
					}
				}
			}

			//取得資料列
			$arr_data = $crud->sql("SELECT * 
									FROM `{$table}` 
									{$sql_join}
									WHERE `{$primary_key}` = '{$edit_id}'")[0];
			if($_json_activity->column){
				//資料主名稱欄位
				$activity_column = $_json_activity->column;
			}
			else{
				//若無資料欄未設定，則取得資料標題(以第二欄位為主)，用於紀錄
				$_DB_NAME = DB_NAME;
				$activity_column = $crud->sql("SELECT `COLUMN_NAME`
												FROM `INFORMATION_SCHEMA`.`COLUMNS`
												WHERE `TABLE_SCHEMA` = '{$_DB_NAME}' AND `TABLE_NAME`='{$table}'
												LIMIT 1,1")[0]['COLUMN_NAME'];
			}

			//修改明細
			$_activity_detail = "{$_activity} {$_before}{$arr_data[$activity_column]}{$_after}";

			$arr_activity = array(
								'activity_unit' => $arr_system['system_name'],
								'activity_user' => $_SESSION['MM_Username'],
								'activity_detail' => $_activity_detail
							);
			$crud->create('activity_log', $arr_activity);

			echo json_encode($arr);
			break;

		//刪除資料
		case 'del':
			$del_id = get_post_var('del_id');
			$_JSON = json_decode(SYSTEM_JSON);
			$_DEL = $_JSON->set->del;

			$used = false;
			if($_DEL->check){
				$_check = $_DEL->check;

				foreach($_check as $c_key => $c_value){
					$chk_table = $c_value->table;
					$chk_column = $c_value->column;
					$column_value = $del_id;
					if($c_value->column_name){
						$arr_column_name = $crud->getid($table, array($primary_key => $del_id));
						$column_value = $arr_column_name[$c_value->column_name];
					}
					$arr_check_data = $crud->select($chk_table, array($chk_column => $column_value));
					$used = (count($arr_check_data) > 0)? true:false;
					$result = false;
					$_msg = $c_value->msg;
					if($used) break;
				}
			}

			if(!$used){
				$_del_file = $_DEL->file;

				//外掛路徑模組
				if($_JSON->set->plugins->extra_path){
					$dir = get_extra_path('del');
				}
				else{
					$dir = "../uploadimages/{$table}/{$del_id}/";
				}
				if( is_dir($dir) ){
					rm_dir($dir);
					rmdir($dir);
				}

				//刪除關聯資料
				if($_DEL->related_data && count($_DEL->related_data) > 0){
					$arr_related_data = $crud->getid($table, array($primary_key => $del_id));
					foreach ($_DEL->related_data as $value) {
						$result = $crud->delete($value->related_table, array($value->related_column => $arr_related_data[$value->column]));
					}
				}

				//額外資料刪除外掛
				if($_plugins->del){
					plugins('del');
				}

				$result = $crud->delete($table, array($primary_key => $del_id));

				if(!$result) $_msg = '資料刪除失敗，請您稍後再試或與管理員聯絡';
			}

			$arr['sts'] = $result;
			$arr['msg'] = $_msg;
			echo json_encode($arr);

			break;

		//列表開關按鈕處理
		case 'switch_change':
			$edit_id = get_post_var('edit_id');
			if(!empty(get_post_var('n_table'))) $table = get_post_var('n_table');
			if(!empty(get_post_var('n_primary_key'))) $primary_key = get_post_var('n_primary_key');

			foreach ($_POST as $key => $value) {
				$_typ = substr($key, 0, 3);
				$_column = substr($key, 4);

				switch ($_typ) {
					case 'txt':
						$post_data[$_column] = $value;
						break;
				}
			}

			$result = $crud->update($table, $post_data, array($primary_key => $edit_id));

			$arr['sts'] = $result;
			echo json_encode($arr);
			break;

		//圖片上傳
		case 'upload':
			// print_r($_FILES);
			// print_r($_POST);
			// exit;
			$column = get_post_var('column');
			$now_id = get_post_var('now_id');
			$isphoto = get_post_var('isphoto');
			$unit = get_post_var('unit');
			$lang = get_post_var('lang');
			$is_multiple = (empty($unit))? false:true;

			//
			if(!empty(get_post_var('table'))){
				$table = get_post_var('table');

				//取PRIMARY欄位名稱
				$primary_key = (empty(get_post_var('primary_key')))? $crud->sql("SHOW KEYS FROM `{$table}` WHERE Key_name = 'PRIMARY'")[0]['Column_name']:get_post_var('primary_key');

				//客製化的主KEY
				if(!empty(get_post_var('primary_id'))) $now_id = get_post_var('primary_id');
			}

			if(count($_FILES) > 0){
				$dir = "../uploadimages/{$table}/{$edit_id}/";
				if (!is_dir($dir)) mkdir($dir,0777);
				$arr_photo = $_FILES['photos'];
				$arr_ok_file = array('.pdf', '.doc', '.docx', '.xls', '.xlsx', '.txt', '.rar', '.zip', '.7z', '.jpg', '.jpeg', '.png', '.gif');
				foreach($arr_photo['name'] as $key => $_value){
					$tmp_file = $arr_photo['tmp_name'][$key];	//伺服器檔案位置
					$file_type = explode('/',$arr_photo['type'][$key]);	//檔案格式
					$exname = substr($arr_photo['name'][$key], strrpos($arr_photo['name'][$key],'.'));	//副檔名
					$new_name = uniqid(random_str(6)).strtolower($exname);	//產生隨機檔名

					if($arr_photo['size'][$key] > 4950000){
						continue;
					}
					else if($isphoto == 'Y' && $file_type[0] <> 'image'){
						continue;
					}
					else if($isphoto == 'N' && array_search($exname, $arr_ok_file) === false){
						continue;
					}

					if(move_uploaded_file($tmp_file, $dir.$new_name)){
						if($is_multiple){
							$arr_data = array($column => $new_name, $primary_key => $now_id);
							/*if(!empty($lang))*/ $arr_data[$unit.'_lang'] = $lang;
							$crud->create(INCLUDE_ACT.'_'.$unit, $arr_data);
						}
						else{
							//刪除舊照片
							if(!empty(get_post_var('old_photo'))){
								$_old_photo = get_post_var('old_photo');
								$_path = $dir.$_old_photo;
								if(file_exists($_path)) unlink($_path);
							}
							$crud->sql("UPDATE `{$table}`
										SET `{$column}` = '{$new_name}'
										WHERE `{$primary_key}` = '{$now_id}'");
						}
					}

				}
			}
			break;

		//檢查唯一值
		case 'unique':
			$_id = get_post_var('id');
			$_column = get_post_var('column');
			$_value = get_post_var('value');

			$arr_row = $crud->sql("SELECT COUNT(*) AS `count_unique`
									FROM `{$table}`
									WHERE `{$_column}` = '{$_value}' AND `{$primary_key}` <> '{$_id}'");
			$arr['unique'] = ($arr_row[0]['count_unique'] > 0)? false:true;
			echo json_encode($arr);
			break;

		//資料排序(使用多筆SQL指令)
		case 'sort':
			$str_sort = get_post_var('sort');
			$crud->multi_sql("SET @sort = 0;
							UPDATE `{$table}` SET `{$table}_sort`=(@sort:=@sort+1) 
							WHERE `{$primary_key}` IN ({$str_sort}) 
							ORDER BY FIELD(`{$primary_key}`, {$str_sort});");
			break;

		//外掛模組
		case 'plugins':
			plugins();
			break;

		//分類排序
		case 'cate_sort':
			$_cate_json = json_decode($_POST['data']);
			cate_sort($_cate_json, 0);
			break;

		//分類刪除
		case 'cate_del':
			//取出此分類的所有子分類
			$del_id = (int)get_post_var('del_id');
			$arr_cate_del = array($del_id);
			cate_children($del_id);

			//確認是否有主資料正在使用此分類
			$str_sql_in = join(',', $arr_cate_del);
			if(!empty($str_sql_in)){
				$arr_use_cate = $crud->sql("SELECT *
											FROM `{$table}`
											INNER JOIN `{$table}_cate` ON `{$table}_cate` = `cate_id`
											WHERE `{$table}_cate` IN ({$str_sql_in})
											GROUP BY `{$table}_cate`");
				if(count($arr_use_cate) > 0){
					$arr_used = array();
					foreach ($arr_use_cate as $key => $value) {
						array_push($arr_used, ($key + 1).'.'.$value['cate_title']);
					}

					$arr['sts'] = false;
					$arr['msg'] = '無法刪除！<br>還有主資料正在使用下列分類，請先將主資料分類修改為其他分類<br><br>'.join('<br>', $arr_used);
				}
				else{
					//啟用交易模式
					$crud->beginTransaction();

					foreach ($arr_cate_del as $key => $value) {
						$can_del = $crud->delete("{$table}_cate", array('cate_id' => $value));
						if($can_del){
							$del_cate = true;
						}
						else{
							//錯誤，返回所有指令
							$crud->rollBack();
							$del_cate = false;
							break;
						}
					}
					if($del_cate){
						//送出指令
						$crud->commit();
						$arr['sts'] = true;
					}
					else{
						$arr['sts'] = false;
						$arr['msg'] = '系統異常！請您稍後再試，或將錯誤代碼告知客服人員<br>錯誤代碼：#CD001';
					}
				}
			}

			echo json_encode($arr);
			break;

		//下載檔案令牌
		case 'token':
			parse_str(urldecode($_POST['token_data']), $arr_data);
			$arr_data = json_encode($arr_data);
			$arr['token'] = encrypt($arr_data, 'iMVFDeWhYq');
			echo json_encode($arr);
			break;

		//隱蔽內容資料(如顯示密碼)，此功能需先驗證密碼
		case 'secret':
			$user_id = $_SESSION['user_id'];
			$user_name = $_SESSION['MM_Username'];
			$password = get_post_var('key');
			$arr_data = array(
							'u_id' => $user_id, 
							'username' => $user_name
						);
			$arr_user = $crud->getid('user', $arr_data);
			if(!empty($arr_user['u_id']) && password_verify($password , $arr_user['password'])){
				$secret_json = json_decode(decrypt(urldecode($_POST['token']), 'iMVFDeWhYq'));
				if(!empty($secret_json->column)){
					$arr_secret = $crud->getid($table, array($primary_key => $secret_json->id));
					if(!empty($arr_secret[$secret_json->column])){
						$arr['sts'] = true;
						$arr['secret'] = decrypt($arr_secret[$secret_json->column], 'iMVFDeWhYq');
					}
					else{
						$arr['sts'] = false;
						$arr['msg'] = '驗證錯誤！';
					}
				}
				else{
					$arr['sts'] = false;
					$arr['msg'] = '驗證錯誤！';
				}
			}
			else{
				$arr['sts'] = false;
				$arr['msg'] = '驗證錯誤！';
			}

			echo json_encode($arr);
			break;
	}

	//取出所有子分類
	function cate_children($par_id){
		Global $crud, $arr_cate_del;

		$arr_cate = $crud->select(INCLUDE_TABLE.'_cate', array('cate_par_id' => $par_id));

		if(count($arr_cate) > 0){
			foreach ($arr_cate as $key => $value) {
				array_push($arr_cate_del, $value['cate_id']);
				cate_children($value['cate_id']);
			}
		}
		else{
			return false;
		}
	}

	//分類層次更動、排序
	function cate_sort($_cate_json, $par_id){
		Global $crud;

		foreach ($_cate_json as $key => $value) {
			$arr_data = array(
							'cate_par_id' => $par_id,
							'cate_sort' => $key
						);
			$crud->update(INCLUDE_TABLE.'_cate', $arr_data, array('cate_id' => $value->id));

			if($value->children) cate_sort($value->children, $value->id);
		}
	}

	//刪除資料夾內所有檔案
	function rm_dir($path){
		if( is_dir($path) ){
			$handle = opendir($path);
			while($file = readdir($handle)){
				if( $file != '.' && $file != '..' ){
					$file = "$path/$file";
					(is_dir($file))? rm_dir($file):unlink($file);
				}
			}
		}
	}
?>