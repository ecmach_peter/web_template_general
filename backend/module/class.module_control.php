<?php
	class module_control{
		function __construct($_JSON){
			global $crud, $B_ARR_UNIT_RIGHTS;
			$this->_JSON = $_JSON;
			$this->crud = $crud;
			$this->B_ARR_UNIT_RIGHTS = $B_ARR_UNIT_RIGHTS;

			//外掛按鈕模組
			if($this->_JSON->set->plugins->button){
				$_act = INCLUDE_ACT;
				include(dirname(__FILE__)."/plugins/{$_act}/{$_act}.button.php");
			}
		}

		//控制按鈕
		public function control_button(){
			$str_result = '';
			$_button = $this->_JSON->button;
			if($_button){
				foreach ($_button as $key => $value) {
					switch($key){
						//新增
						case 'add':
							if($this->B_ARR_UNIT_RIGHTS['add']){
								echo <<<HTML
								<button type="button" class="btn btn-danger" id="btn_add" title="增加新項目"><i class="fa fa-plus-circle fa-lg"></i>&nbsp;新增</button> 
HTML;
							}
							break;

						//排序
						case 'sort':
							if($this->B_ARR_UNIT_RIGHTS['edit']){
								echo <<<HTML
								<button type="button" class="btn btn-warning" id="btn_sort" data-act="sort" title="排序"><i class="fa fa-bars fa-lg"></i>&nbsp;排序</button> 
HTML;
							}
							break;

						//分類(未完成)
						case 'categories':
							$arr_cate_par = $this->crud->select(INCLUDE_TABLE.'_cate', array('cate_par_id' => 0), array('cate_title' => 'ASC'));
							if(count($arr_cate_par) > 0){
								foreach ($arr_cate_par as $key => $value) {
									$_categories = $this->_JSON->set->categories;

									if($_categories->level > 1){
										$arr_cate = $this->crud->select(INCLUDE_TABLE.'_cate', array('cate_par_id' => $value['cate_id']), array('cate_title' => 'ASC'));
										$tmp_option = '';
										if(count($arr_cate) > 0){
											foreach ($arr_cate as $_key => $_value) {
												$tmp_option .= <<<HTML
																<option value="{$_value['cate_id']}" {$_selected}>　{$_value['cate_title']}</option>
HTML;
											}
										}
										$str_cate .= <<<HTML
														<option data-divider="true"></option>
														<option value="{$value['cate_id']}" {$_selected}>{$value['cate_title']}</option>
														{$tmp_option}
HTML;
									}
									else{
										$str_cate .= <<<HTML
														<option value="{$value['cate_id']}" {$_selected}>{$value['cate_title']}</option>
HTML;
									}
								}
							}

							//一層分類選單加入分隔線
							if($_categories->level < 2){
								$_tmp = <<<HTML
											<option data-divider="true"></option>
HTML;
								$str_cate = $_tmp.$str_cate;
							}

							// 唯讀分類處理
							if(!$_categories->readonly && $this->B_ARR_UNIT_RIGHTS['edit']){
								$btn_data = ' id="btn_categorie" data-act="categorie" title="分類管理"';
							}
							else{
								$_disabled = 'disabled';
							}

							echo <<<HTML
									<div class="input-group">
										<span class="input-group-btn">
											<a class="btn btn-primary {$_disabled}" {$btn_data}><i class="fa fa-sitemap fa-lg"></i>&nbsp;分類</a> 
										</span>
										<select id="cate_filter" class="selectpicker" data-live-search="true" data-width="fit">
											<option value="all" selected>全部分類</option>
											{$str_cate}
										</select>
									</div>
HTML;
							break;

						//搜尋資料
						case 'search':
							echo <<<HTML
									<div class="input-group">
										<input type="text" class="form-control" id="btn_search" placeholder="資料搜尋">
										<span class="input-group-btn">
											<button class="btn btn-default nomargin" type="button" id="btn_search_clean" data-dir="../uploadimages/" data-img="kiCyUF57fdfaf386ea7.jpg">
												<i class="fa fa-times fa-lg"></i>
											</button>
										</span>
									</div>
HTML;
							break;

						//匯出資料
						case 'export':
							$act = INCLUDE_ACT;
							echo <<<HTML
									<a href="./module/plugins/{$act}/csv_export.php" class="btn btn-primary" id="btn_export"><i class="fa fa-cloud-download fa-lg"></i>&nbsp;匯出</a> 
HTML;
							break;

						//篩選選單
						case 'filter_view':
							$arr_filter_view = $this->_JSON->set->filter_view;
							foreach ($arr_filter_view as $_key => $_value) {
								$_option = '';
								foreach ($_value as $o_key => $o_value) {
									$_selected = ($o_value->selected)? 'selected':'';
									$divider = ($o_value->divider)? 'data-divider="true"':'';
									$_option .= <<<HTML
												<option value="{$o_value->value}" {$_selected} {$divider}>{$o_value->title}</option>
HTML;
								}
								echo <<<HTML
										<select id="filter_view" name="filter_view[{$_key}]" class="selectpicker" data-width="fit" style="display:none;">
											{$_option}
										</select>
HTML;
							}
							break;

						//日期選擇
						case 'date_filter':
							echo <<<HTML
									<div class="input-group">
										<input type="text" class="form-control daterange" name="daterange[{$value->column}]" placeholder="{$value->title}" readonly />
										<span class="input-group-btn">
											<button class="btn btn-default text-danger nomargin btn_date_cancel" {$_lock} type="button" data-for="txt_{$_column}">
												<i class="fa fa-times fa-lg"></i>
											</button>
										</span>
									</div>
HTML;
						$this->_JS .= <<<HTML
									<script>
										$('.daterange').daterangepicker({
											"locale": {
												"format": "YYYY/MM/DD"
											}
										}).on('change', function(){
											var _this = $(this);
											update_list();
										});
										$('.btn_date_cancel').on('click', function(){
											var _this = $(this);
											_this.parents('.input-group').find(':text').val('').change();
										}).click();
									</script>
HTML;
						break;

						//外掛
						default:
							if($this->_JSON->set->plugins->button){
								$arr = plugins_button($key);
								$this->_JS .= $arr['js'];
								echo $arr['append'];
							}
							break;
					}
				}
			}
		}

		//按鈕用JS
		public function button_js(){
			return $this->_JS;
		}

		//列表表頭
		public function table_header(){
			$str_result = '';
			$_list = $this->_JSON->list;
			foreach ($_list as $key => $value) {
				$toggle = ($value->toggle)? ' data-toggle="true"':'';	//可適性展開按鈕
				$hide = '';
				if($value->hide){
					switch ((string)$value->hide) {
						case 'all':
						case 'phone':
						case 'tablet':
							$hide = $value->hide;
							break;
						
						default:
							$hide = 'phone,tablet';
							break;
					}
					$hide = 'data-hide="'.$hide.'"';
				}

				$sort = '';
				if($value->sort){	//若有排序設定加入排序控制項
					$d = '';
					$d_a = $d_d = 'display: none;';
					if($value->issort){
						$d = 'display: none;';
						$d_a = ($value->issort == 'a')? '':'display: none;';
						$d_d = ($value->issort == 'd')? '':'display: none;';
					}
					$sort = <<<HTML
							<a href="javascript:;"><i class="fa fa-sort btn-sort" style="{$d}"></i></a>
							<a href="javascript:;"><i class="fa fa-caret-up btn-sort" data-mode="a" style="{$d_a}"></i></a>
							<a href="javascript:;"><i class="fa fa-caret-down btn-sort" data-mode="d" style="{$d_d}"></i></a>
HTML;
				}
				
				echo <<<HTML
					<th class="{$value->class}" data-column="{$value->column}" {$hide} {$toggle}>
						{$value->title}
						{$sort}
					</th>
HTML;
			}
		}
	}
?>

