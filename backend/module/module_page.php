<?php
	include_once './include/alias_function.php';

	//JSON字串解析
	$_JSON = json_decode(SYSTEM_JSON);
	$_JSON_list = $_JSON->list;

	//外掛模組
	if($_JSON->set->plugins->page){
		$_act = INCLUDE_ACT;
		include(dirname(__FILE__)."/plugins/{$_act}/{$_act}.page.php");
	}

	//資料表名稱
	$table = INCLUDE_TABLE;

	//取PRIMARY欄位名稱，用來計算筆數、操作用ID
	$primary_key = $crud->sql("SHOW KEYS FROM `{$table}` WHERE Key_name = 'PRIMARY'")[0]['Column_name'];

	//SQL JOIN語法產生
	$arr_join = $_JSON->set->join;
	if($arr_join){
		$sql_join = '';
		foreach($arr_join as $value){
			$on_table = ($value->on_table)? $value->on_table:$table;
			$sql_join .= ' INNER JOIN `'.$value->table.'` ON `'.$value->table.'`.`'.$value->primary.'` = `'.$on_table.'`.`'.$value->on_primary.'` ';
		}
	}

	//----- 組合欄位
	$concat = $_JSON->set->concat;
	$_column = '';
	if($concat && count($concat) > 0){
		foreach ($concat as $key => $value) {
			$tmp = join(',', $value);
			$_column .= ", CONCAT({$tmp}) AS `{$key}`";
		}
	}

	//----- 額外欄位
	$extra_column = $_JSON->set->extra_column;
	if(count($extra_column) > 0){
		foreach ($extra_column as $key => $value) {
			$_column .= ", ({$value}) AS `{$key}`";
		}
	}

	//----- 分類篩選
	$_cate_filter = get_post_var('cate_filter');
	if(!empty($_cate_filter)){
		$filter .= ($_cate_filter == 'all')? '':"AND `{$table}_cate` = '{$_cate_filter}'";
	}

	//----- 篩選處理
	$arr_filter = $_JSON->set->filter;
	if(count($arr_filter) > 0){
		foreach ($arr_filter as $key => $value) {
			$filter .= " {$value}";
		}
	}

	//----- 篩選處理(SQL IN)
	$arr_filter = $_JSON->set->filter_in;
	if(count($arr_filter) > 0){
		foreach ($arr_filter as $key => $value) {
			if(count($value) > 0){
				$_in = join("','", $value);
				$filter .= " AND `{$key}` IN ('{$_in}')";
			}
		}
	}

	//----- 下拉式篩選處理
	if(isset($_POST['filter_view'])){
		if(count($_POST['filter_view']) > 0){
			foreach ($_POST['filter_view'] as $key => $value) {
				if($value <> '') $filter .= " AND `{$key}` = '{$value}'";
			}
		}
	}

	//----- 下拉式篩選處理
	if(isset($_POST['daterange'])){
		if(count($_POST['daterange']) > 0){
			foreach ($_POST['daterange'] as $key => $value) {
				$value = urldecode($value);
				$arr_date = explode('-', $value);
				if(!empty($arr_date[0])) $filter .= " AND `{$key}` >= '{$arr_date[0]}'";
				if(!empty($arr_date[1])) $filter .= " AND `{$key}` <= '{$arr_date[1]}'";
			}
		}
	}

	//----- 搜尋參數
	$search = htmlspecialchars(trim(urldecode(get_post_var('search'))),ENT_QUOTES);
	if(!empty($search)){
		$arr_search = $_JSON->set->search;
		$_search = '';
		foreach($arr_search as $value){
			$_search .= " `{$value}` LIKE '%{$search}%' OR";
		}
		$filter .= ' AND ('.substr($_search,0,strlen($_search)-2).')';
	}

	//計算各頁籤的數量
	$arr_count_tabs = array();
	$select_tabs = (!empty(get_post_var('tabs')))? get_post_var('tabs'):'';
	$_tmp_filter = '';
	if($_JSON->set->tabs && count($_JSON->set->tabs) > 0){
		foreach ($_JSON->set->tabs as $key => $value) {
			$arr_count = $crud->sql("SELECT COUNT(*) AS `count_tabs`
									FROM `{$table}` 
									{$sql_join}
									WHERE 1 = 1 {$filter} {$value->where}");
			$arr_count_tabs[] = (int)$arr_count[0]['count_tabs'];

			if($select_tabs == $key){
				$_tmp_filter .= $value->where;
			}
		}
	}
	$filter .= $_tmp_filter;
	$arr['count_tabs'] = (count($arr_count_tabs) > 0)? $arr_count_tabs:false;
	
	//----- 分頁處理
	$count_page = count($crud->sql("SELECT * 
									FROM `{$table}` 
									{$sql_join}
									WHERE 1 = 1 {$filter}"));	//計算筆數
	$default_size = (empty($_JSON->set->pagesize))? 10:$_JSON->set->pagesize;
	$pagesize = (empty(get_post_var('page_view')))? $default_size:get_post_var('page_view');    //一頁顯示筆數
	$tot_page = ceil($count_page/$pagesize);    //總頁數
	$now_page = (empty(get_post_var('nowpage')))? 1:get_post_var('nowpage');
	$begin_num = ($now_page - 1) * $pagesize;	//計算開始筆數

	//一列顯示幾個item
	$_oneline_items = 5;
	$_page_link = './news/';
	$_use_link = false;

	$_pagination = '';
	if($tot_page > 1){
		//上一頁處理
		$prev_page = $now_page - 1;
		$prev = ($now_page > 1)? 'data-page="'.$prev_page.'"':'';
		$prev_link = ($now_page > 1 && $_use_link)? "{$_page_link}{$prev_page}":'javascript:;';

		//下一頁處理
		$next_page = $now_page + 1;
		$next = ($now_page < $tot_page)? 'data-page="'.$next_page.'"':'';
		$next_link = ($now_page < $tot_page && $_use_link)? "{$_page_link}{$next_page}":'javascript:;';

		//起始頁處理
		$start = ($now_page > 1)? 'data-page="1"':'';
		$start_link = ($now_page > 1 && $_use_link)? $_page_link.'1':'javascript:;';

		//結束頁處理
		$end = ($now_page < $tot_page)? 'data-page="'.$tot_page.'"':'';
		$end_link = ($now_page < $tot_page && $_use_link)? $_page_link.$tot_page:'javascript:;';

		$_pagination .= <<<HTML
							<li {$start}><a href="{$start_link}"><i class="fa fa-angle-double-left" aria-hidden="true"></i></a></li>
							<li {$prev}><a href="{$prev_link}"><i class="fa fa-angle-left" aria-hidden="true"></i></a></li>
HTML;

		$_half_items = floor($_oneline_items / 2);
		$fix_base = ($_oneline_items % 2 == 0)? 2:1;
		$base = $now_page - $_half_items;
		if($tot_page - $now_page < $_half_items) $base = $tot_page - ($_oneline_items - $fix_base);
		if($base < 1) $base = 1;

		$max = ($now_page <= $_half_items)? $_oneline_items:$now_page + $_half_items;
		if($max > $tot_page) $max = $tot_page;

		for($i = $base; $i <= $max; $i++){
			if($i > $tot_page) break;
			$sel_page = ($i <> $now_page)? 'data-page="'.$i.'"':'';
			$active = ($i == $now_page)? 'class="active"':'';
			$_link = ($i <> $now_page && $_use_link)? $_page_link.$i:'javascript:;';
			$_pagination .= <<<HTML
							<li {$sel_page} {$active}><a href="{$_link}">{$i}</a></li>
HTML;
		}
		$_pagination .= <<<HTML
							<li {$next}><a href="{$next_link}"><i class="fa fa-angle-right" aria-hidden="true"></i></a></li>
							<li {$end}><a href="{$end_link}"><i class="fa fa-angle-double-right" aria-hidden="true"></i></a></li>
HTML;
	}

	$arr['page_info'] = $now_page.'/'.$tot_page.' 共'.$count_page.'筆';
	$arr['pagination'] = $_pagination;

	//----- 排序
	if(empty(get_post_var('sortby'))){
		$sorby = ($_JSON->button->sort)? "{$table}_sort":"{$table}`.`{$primary_key}";
		$sormode = ($_JSON->set->sort->mode)? $_JSON->set->sort->mode:"ASC";
	}
	else{
		$sorby = get_post_var('sortby');
		$sormode = (get_post_var('sort') == 'd')? 'DESC':'ASC';
	}

	//----- 列表處理
	$arr_list = $crud->sql("SELECT * {$_column}
							FROM `{$table}` 
							{$sql_join}
							WHERE 1 = 1 {$filter}
							ORDER BY `{$sorby}` {$sormode}, `{$table}`.`{$primary_key}`
							LIMIT {$begin_num}, {$pagesize}");
	if(count($arr_list) > 0){
		foreach ($arr_list as $key => $value) {
			//額外查詢
			if($_JSON->set->extra_query){
				foreach ($_JSON->set->extra_query as $extra_query_key => $extra_query_value) {
					${'arr_'.$extra_query_key} = $crud->getid($extra_query_key, array($extra_query_value->column => $value[$extra_query_value->value]));
				}
			}

			$str_append = '';
			$primary_id = $value[$primary_key];
			$_lock = ($value["{$table}_lock"] == 1 || !$B_ARR_UNIT_RIGHTS['edit'])? 'disabled':'';
			foreach ($_JSON_list as $_key => $_value) {
				$_type = $_value->type;
				$_column = $_value->column;
				$_class = $_value->class;
				$_alias = $_value->alias;
				$append = '';
				switch($_type){
					//純文字顯示
					case 'text':
						if(!empty($_value->extra_query)){
							$_text = ${'arr_'.$_value->extra_query}[$_column];
						}
						else if($_alias){
							$_text = $_alias($value[$_column]);
						}
						else{
							$_text = $value[$_column];
						}

						if($_value->number_format){
							$_text = number_format($_text);
						}

						if(empty($_text)){
							$_text = '--';
						}
						else{
							//額外前後字串處理
							$_before = ($_value->before)? $_value->before:'';
							$_after = ($_value->after)? $_value->after:'';
							$_text = $_before.$_text.$_after;
						}
						$append = $_text;
						break;

					//日期顯示
					case 'date':
						$_text = $value[$_column];
						$append = ($_text == '0000-00-00' || $_text == '0000-00-00 00:00:00')? '--':$_text;
						break;

					//圖片
					case 'photo':
						$p_path = "../uploadimages/{$table}/{$primary_id}/{$value[$_column]}";
						if(file_exists($p_path) && !empty($value[$_column])){
							$append = <<<HTML
										<img src="{$p_path}" style="max-width:100%; max-height: 200px;" />
HTML;
						}
						else{
							$append = '--';
						}
						break;

					//開關按鈕
					case 'switch':
						$checked = ($value[$_column] == 1)? 'checked="checked"':'';
						$append = <<<HTML
									<input id="btn_{$_column}-{$primary_id}" name="txt_{$_column}" data-id="{$primary_id}" class="cmn-toggle cmn-toggle-round btn_switch" type="checkbox" {$checked} {$_lock}>
									<label for="btn_{$_column}-{$primary_id}"></label>
HTML;
						break;

					//操作按鈕
					case 'edit':
						$btn_icon = ($_JSON->button->add && $_lock == '' && $B_ARR_UNIT_RIGHTS['edit'])? 'pencil':'search';
						$append = <<<HTML
									<a href="javascript:;" id="btn_edit" data-id="{$primary_id}">
										<i class="fa fa-{$btn_icon} fa-2x text-success fa-fw" title=""></i>
									</a>
HTML;
						if($_JSON->button->del && $_lock == '' && $B_ARR_UNIT_RIGHTS['del']){
							$append .= <<<HTML
										<a href="javascript:;" id="btn_del" data-id="{$primary_id}">
											<i class="fa fa-trash fa-2x text-danger fa-fw" title=""></i>
										</a>
HTML;
						}
						break;

					//純刪除按鈕
					case 'del':
						$append = <<<HTML
									<a href="javascript:;" id="btn_del" data-id="{$primary_id}">
										<i class="fa fa-trash fa-2x text-danger fa-fw" title=""></i>
									</a>
HTML;
						break;

					//純檢視按鈕
					case 'view':
						$append = <<<HTML
									<a href="javascript:;" id="btn_edit" data-id="{$primary_id}">
										<i class="fa fa-search fa-2x text-success fa-fw" title=""></i>
									</a>
HTML;
						break;


					//外掛模組
					case 'plugins':
						$append = plugins($value, $_value->_type);
						break;
				}

				$str_append .= <<<HTML
								<td class="{$_class}">{$append}</td>
HTML;
			}


			$arr['list'] .= <<<HTML
							<tr>
								{$str_append}
							</tr>
HTML;
		}

		$arr['list'] .= $plugins_JS;
	}
	else{
		$_colspan = count($_JSON_list);
		$arr['list'] = <<<HTML
							<tr>
								<th class="center" colspan="{$_colspan}">目前沒有資料</th>
							</tr>
HTML;
	}

	echo preg_replace('/\\\\[rnt]/', '', json_encode($arr));
?>