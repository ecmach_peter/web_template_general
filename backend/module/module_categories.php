<?php
	include_once('./module/class.module_categories.php');
	$_JSON = json_decode(SYSTEM_JSON);
	$_JSON_categories = $_JSON->set->categories;

	$_categories = new module_categories($crud);

	//鎖住主分類，不可新增、變更層級
	if($_JSON_categories->lock_primary){
		// && that.data('par-id') != 0
		$reject = <<<JAVASCRIPT
					,reject: [{
							rule: function(draggedElement) {
								var that = $(draggedElement);
								return (that.parentsUntil('.dd', 'ol').length <= 1) || (that.data('par-id') == 0 && that.parentsUntil('.dd', 'ol').length > 1); // The rule here is that it is forbidden drag elements to first-level children
							},
							action: function(draggedElement) {
								var that = $(draggedElement),
									errmsg = (that.data('par-id') == 0)? '變更層級':'新增';
								do_notify.error('<i class="fa fa-exclamation-triangle fa-lg"></i> 主分類無法'+errmsg);
							}
						}
					]
JAVASCRIPT;
	}

	$arr['data'] = <<<HTML
						{$_categories->cate_data()}
						<script>
							$('#form_categorie .dd').nestable({
								maxDepth: {$_JSON_categories->level}
								{$reject}
							}).on('change', function(){
								do_notify.success('<i class="fa fa-info fa-lg"></i> 提醒您，請記得按下儲存按鈕更動才會生效');
							});

							$('#form_categorie').off().on('click', '.btn_switch', function(){
								var _this = $(this),
									_name = _this.attr('name'),
									_data = {
										'n_table': _this.data('table'),
										'n_primary_key': _this.data('primary_key'),
										'edit_id': _this.data('id')
									};
									_data[_name] = (_this.is(':checked'))? 1:0;

								doajax({
									url: 'control',
									type: 'json',
									data: 'type=switch_change&'+jQuery.param(_data),
									beforeSend: function(xhr, opts) {
										$.blockUI();
									},
									callback:function(msg){
										$.unblockUI();
										(msg.sts)? do_notify.success():do_notify.error();
									}
								});
							});

							$('#form_categorie').on('click', '#btn_cate_edit', function(){
								var eid = $(this).parents('li:first').data('id')
								get_categories_form(eid);
							});

							$('[id="btn_save_cate"]').off().on('click', function(){
								bootbox.dialog({
									message: "<h4><strong>此操作將可能影響分類的階層關係，確定要儲存嗎?</strong></h4>",
									size: 'small',
									buttons: {
										success: {
											label: "確認",
											className: "btn-danger nomargin",
											callback: function() {
												doajax({
													url: 'control',
													data: 'type=cate_sort&data='+JSON.stringify($('#form_categorie .dd').nestable('serialize')),
													beforeSend: function(xhr, opts) {
														$.blockUI();
													},
													callback: function(){
														$.unblockUI();
														do_notify.success('<i class="fa fa-check fa-lg"></i> 分類階層及排序完成');
													}
												});
											}
										},
										cancel: {
											label: "取消",
											className: "btn-default nomargin"
										}
									}
								});
							});
							$('[id="btn_add_cate"]').off().on('click', function(){
								get_categories_form('add');
							});
							var dialog;
							function get_categories_form(eid){
								var is_add = (eid == 'add')? true:false;
								doajax({
									url: 'form',
									data: 'external=categories&id='+eid,
									type: 'json',
									callback: function(msg){
										if(dialog) dialog.modal('hide');
										$('#form_categorie_data').html(msg.form);
										dialog = bootbox.dialog({
											message: $('#div_form_categorie').html(),
											buttons: {
												success: {
													label: "確認",
													className: "btn-danger nomargin",
													callback: function() {
														var _this = $('.modal-dialog .form_categorie'),
															_error = false;

														_this.find(":required:enabled,[data-required='required']").each(function(){
															var that = $(this),
																str_req = $("label[for='"+that.attr('id')+"']:first").text().replace(':','').replace('：',''),
																str_mode = (that.is(':file'))? '上傳':'輸入';
															if(that.hasClass('wysiwyg')){
																var _name = that.attr('id');
																if(eval('CKEDITOR.instances.'+_name+'.getData()') == ''){
																	_error = true;
																}
															}
															else if(that.val() == ''){
																_error = true;
															}

															if(_error){
																do_notify.error('<i class="fa fa-exclamation-triangle fa-lg"></i> 請'+str_mode+' '+str_req+'');
																$('body').animate({
																 	scrollTop:$('#'+that.attr('id')).parents('.form-group').offset().top - 130
																 });
																return false;
															}
														});
														
														if(_error) return false;

														_this.find('[data-unique="true"]').each(function(){
															var that = $(this),
																str_req = $("label[for='"+that.attr('id')+"']").text().replace(':','').replace('：',''),
																obj = {
																	type: 'unique',
																	id : _this.find('#edit_categorie_id').val(),
																	value: that.val(),
																	column : that.data('column'),
																	external: _this.find('#external').val()
																};

															doajax({
																url: 'control',
																data: jQuery.param(obj),
																type: 'json',
																async: false,
																callback: function(msg){
																	_error = (msg.unique)? false:true;
																}
															});

															if(_error){
																do_notify.error('<i class="fa fa-exclamation-triangle fa-lg"></i> 此 '+str_req+' 已經使用過了');
																$('body').animate({
																	scrollTop:$('#'+that.attr('id')).parents('.form-group').offset().top - 130
																});
																return false;
															}
														});

														if(_error) return false;

														doajax({
															url: 'control',
															type: 'json',
															data: 'type=edit&'+_this.serialize(),
															files: $(".modal-dialog .form_categorie .btn_file :file"),
															beforeSend: function(xhr, opts) {
																$.blockUI();
															},
															callback:function(msg){
																var add_msg = null,
																	edit_id = $('#edit_id'),
																	page = (edit_id == 'add')? 1:$('.pagination .active').text();
																if(msg.cid){
																	edit_id.val(msg.cid);
																	add_msg = '<i class="fa fa-check"></i> 資料新增完成';
																}
																$.unblockUI();
																do_notify.success(add_msg);

																if(is_add){
																	var _content = replaceAll($('#cate_init').html(), 'varCateID', edit_id.val());
																	$('#form_categorie [data-id="'+$('.modal-dialog #txt_cate_par_id').val()+'"] .dd-list:first').append(replaceAll(_content, 'varCateTitle', $('.modal-dialog #txt_cate_title').val()));
																}
																else{
																	$('li[data-id="'+edit_id.val()+'"] #cate_title:first').text($('.modal-dialog #txt_cate_title').val());
																}
																update_categorie_form();
															}
														});
													}
												},
												cancel: {
													label: "取消",
													className: "btn-default nomargin"
												}
											}
										}).on('shown.bs.modal', function () {
											$('.form_categorie').on('submit', function(){
												return false;
											});
											$('.modal-dialog #form_categorie_data input:first').focus();
											$('#form_categorie_data').html('');
											$('.modal-dialog .selectpicker').selectpicker('destroy').selectpicker();
										});
										
										$('[id="edit_categorie_id"]').val(eid);
									}
								});
							}

							$('#form_categorie').on('click', '.btn_del_cate', function(){
								var _this = $(this),
									del_id = _this.parents('li:first').data('id');
								bootbox.dialog({
									message: "<h4><strong>確定要刪除這個分類嗎?</strong></h4><br>※此分類下的子分類也會一併刪除",
									size: 'small',
									buttons: {
										success: {
											label: "確認",
											className: "btn-danger nomargin",
											callback: function() {
												doajax({
													url: 'control',
													type: 'json',
													data: 'type=cate_del&del_id='+del_id,
													beforeSend: function(xhr, opts) {
														$.blockUI();
													},
													callback:function(msg){
														$.unblockUI();
														if(msg.sts){
															_this.parents('li:first').remove();
															do_notify.success('<i class="fa fa-check fa-lg"></i> 資料刪除完成');
														}
														else{
															do_notify.error('<i class="fa fa-exclamation-triangle fa-lg"></i> '+msg.msg);
														}
														update_categorie_form();
													}
												});
											}
										},
										cancel: {
											label: "取消",
											className: "btn-default nomargin"
										}
									}
								});
							});
							function replaceAll(txt, replace, with_this) {
								return txt.replace(new RegExp(replace, 'g'),with_this);
							}
						</script>
HTML;
	echo json_encode($arr);
?>