<?php
	//預防加入參數
	if(!empty(B_VAR_1)){
		header("Location: ./".INCLUDE_ACT); 
		exit;
	}

	include_once('./module/class.module_control.php');
	$_JSON = json_decode(SYSTEM_JSON);
	if($_JSON->set->single_account){
		$edit_id = $_SESSION['user_id'];
	}
	else{
		$table = INCLUDE_TABLE;
		//取PRIMARY欄位名稱，用來計算筆數、操作用ID
		$primary_key = $crud->sql("SHOW KEYS FROM `{$table}` WHERE Key_name = 'PRIMARY'")[0]['Column_name'];
		$arr_data = $crud->getid($table, array());
		$edit_id = $arr_data[$primary_key];
	}
?>
<!DOCTYPE html>
<html lang="zh-TW">
<?php include_once 'back_head.php'; ?>
</head>
<body>
	<div id="wrapper">

		<!-- Navigation -->
		<nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
			<?php include_once 'back_navbar.php'; ?>

			<div class="navbar-default sidebar" role="navigation">
				<?php include_once 'back_navbar_left.php'; ?>
			</div>
			<!-- /.navbar-static-side -->
		</nav>
		
		<!-- Page Content -->
		<div id="page-wrapper">
			<div class="container-fluid">
				<div class="row">
					<div class="col-lg-12">
					</div>
				</div>
				<div class="row">
					<div id="module_form">
						<div class="col-md-12">
							<h2 class="page-header text-success">
								<i class="fa fa-<?=$_navbar_icon;?>"></i>&nbsp;<?=$_navbar_name;?>&nbsp;<small><span class="act_title"></span></small>
							</h2>
						</div>
						<div class="col-md-12">
							<form class="form-horizontal">
								<div id="form_data"></div>
										<hr>
								<div class="row">
									<div class="col-md-7 col-md-offset-5">
										<button type="submit" class="btn btn-success" ><i class="fa fa-save fa-lg"></i> 確認</button>
									</div>
								</div>
								<input id="edit_id" name="edit_id" type="hidden">
							</form>
						</div>
					</div>
				</div>
				<!-- /.row -->
			</div>
			<!-- /.container-fluid -->
		</div>
		<!-- /#page-wrapper -->
	<div class="modal fade" id="image_view" tabindex="-1" role="dialog">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title">圖片預覽</h4>
				</div>
				<div class="modal-body">
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-primary" data-dismiss="modal">關閉</button>
				</div>
			</div><!-- /.modal-content -->
		</div><!-- /.modal-dialog -->
	</div><!-- /.modal -->
	<input id="hx_act" type="hidden" value="<?=$act;?>" />
	<!-- /#wrapper -->
	<?php include_once 'back_footer.php'; ?>
	<script type="text/javascript">
		$(function(){
			update_form(<?=$edit_id;?>)
			var _act = $('#hx_act').val();
			$(window).bind("popstate", function () {
				var state = event.state;
				if(state){
					if(state.eid){
						var eid = state.eid;
						history.replaceState({eid:eid},'',"./"+_act+"-"+eid);   //新增紀錄
						update_form(eid);
					}
					else if(state.act){
						var act = state.act,
							btn = state.btn;
						history.replaceState(null,'',"./"+_act+"-"+act);   //新增紀錄
						$('#'+btn).click();
					}
				}
				else{
					history.replaceState(null,'',"./"+_act);   //新增紀錄
				}
	        });

	        $('.select2').select2({
				width: 100,
				minimumResultsForSearch: Infinity
			});

			$('#module_form form').on('submit', function(){
				var $this = $(this),
					_error = false;

				if($('#edit_id').val() != 'add'){
					$this.find(":password").each(function(){
						var $this = $(this);
						if($this.val() == '') $this.attr('disabled', true);
					});
				}

				$this.find(":required:enabled,[data-required='required']").each(function(){
					var that = $(this),
						str_req = $("label[for='"+that.attr('id')+"']").text().replace(':','').replace('：',''),
						str_mode = (that.is(':file'))? '上傳':'輸入',
						regExp = /^[\d]+$/;
					if(that.hasClass('wysiwyg')){
						var _name = that.attr('id');
						if(eval('CKEDITOR.instances.'+_name+'.getData()') == ''){
							_error = true;
						}
					}
					else if(that.val() == '' || that.val() == null){
						_error = true;
					}
					else if(that.data('number') && !regExp.test(that.val())){
						_error = true;
						str_mode += '數字:';
					}

					if(_error){
						do_notify.error('<i class="fa fa-exclamation-triangle fa-lg"></i> 請'+str_mode+' '+str_req+'');
						$('body').animate({
						 	scrollTop:$('#'+that.attr('id')).parents('.form-group').offset().top - 130
						 });
						return false;
					}
				});

				if(_error) return false;

				$this.find('[data-unique="true"]').each(function(){
					var that = $(this),
						str_req = $("label[for='"+that.attr('id')+"']").text().replace(':','').replace('：',''),
						obj = {
							type: 'unique',
							id : $('#edit_id').val(),
							value: that.val(),
							column : that.data('column')
						};

					doajax({
						url: 'control',
						data: jQuery.param(obj),
						type: 'json',
						async: false,
						callback: function(msg){
							_error = (msg.unique)? false:true;
						}
					});

					if(_error){
						do_notify.error('<i class="fa fa-exclamation-triangle fa-lg"></i> 此 '+str_req+' 已經使用過了');
						$('body').animate({
							scrollTop:$('#'+that.attr('id')).parents('.form-group').offset().top - 130
						});
						return false;
					}
				});

				if(_error) return false;

				if($('[name="rights[]"]').get(0) && !$('[name="rights[]"]:checked').get(0)){
					do_notify.error('<i class="fa fa-exclamation-triangle fa-lg"></i> 至少要設定一個權限');
					_error = true;
				}

				if(window.chk_plugins) {
					_error = chk_plugins();
				}

				if(_error) return false;

				$this.find(".wysiwyg").each(function(){
					var that = $(this);
					that.val(eval('CKEDITOR.instances.'+that.attr('id')+'.getData()'));
				});

				doajax({
					url: 'control',
					type: 'json',
					data: 'type=edit&'+$this.serialize(),
					files: $(":file"),
					beforeSend: function(xhr, opts) {
						$.blockUI();
					},
					callback:function(msg){
						var add_msg = null,
							edit_id = $('#edit_id'),
							page = (edit_id == 'add')? 1:$('.pagination .active').text();
						if(msg.cid){
							edit_id.val(msg.cid);
							add_msg = '<i class="fa fa-check"></i> 資料新增完成';
						}
						$this.find(':password').val('').attr('disabled', false);
						$.unblockUI();
						do_notify.success(add_msg);
					}
				});
				return false;
			});

			function update_form(eid){
				$('.act_title').text((eid == 'add')? '新增':'修改');

				doajax({
					url: 'form',
					type: 'json',
					data: 'id=' + eid,
					beforeSend: function(xhr, opts) {
						$.blockUI();
					},
					callback:function(msg){
						if(msg.sts){
							$('#edit_id').val(eid);
							$('#form_data').html(msg.form);
						}
						$.unblockUI();
					}
				});
			}
		});
	</script>
</body>
</html>