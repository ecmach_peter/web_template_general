<?php
	include_once './include/alias_function.php';

	$_JSON = json_decode(SYSTEM_JSON);
	if((!$_JSON->button->add && get_post_var('id') == 'add') || (!$B_ARR_UNIT_RIGHTS['add'] && get_post_var('id') == 'add')){
		$arr['sts'] = false;
		echo json_encode($arr);
		exit;
	}
	$arr['sts'] = true;

	//資料表名稱
	$table = INCLUDE_TABLE;
	$_act = INCLUDE_ACT;

	//外部使用表單模組(如分類)
	$is_external = false;
	if(!empty(get_post_var('external'))){
		$external = get_post_var('external');
		$is_external = true;
		$_JSON_external = $_JSON->set->$external;
		$_JSON_form = $_JSON_external->form;
		$table = $_JSON_external->table;
	}
	else{
		$_JSON_form = $_JSON->form;
	}

	//外掛模組
	if($_JSON->set->plugins->form){
		include(dirname(__FILE__)."/plugins/{$_act}/{$_act}.form.php");
	}

	//編輯ID
	$edit_id = get_post_var('id');

	//取PRIMARY欄位名稱，用來計算筆數、操作用ID
	$primary_key = $crud->sql("SHOW KEYS FROM `{$table}` WHERE Key_name = 'PRIMARY'")[0]['Column_name'];

	//組合欄位
	$concat = $_JSON->set->concat;
	$_concat = '';
	if($concat && count($concat) > 0){
		foreach ($concat as $key => $value) {
			$tmp = join(',', $value);
			$_concat .= ", CONCAT({$tmp}) AS `{$key}`";
		}
	}

	//----- 額外欄位
	$extra_column = $_JSON->set->extra_column;
	if(count($extra_column) > 0){
		foreach ($extra_column as $key => $value) {
			$_column .= ", ({$value}) AS `{$key}`";
		}
	}

	//SQL JOIN語法產生
	$arr_join = $_JSON->set->join;
	if($arr_join && !$is_external){
		$sql_join = '';
		foreach($arr_join as $value){
			$on_table = ($value->on_table)? $value->on_table:$table;
			$sql_join .= ' INNER JOIN `'.$value->table.'` ON `'.$value->table.'`.`'.$value->primary.'` = `'.$on_table.'`.`'.$value->on_primary.'` ';
		}
	}

	//取資料，新增模式不需取資料
	if($edit_id <> 'add'){
		$arr_data = $crud->sql("SELECT * {$_concat} {$_column}
								FROM `{$table}`
								{$sql_join}
								WHERE `{$primary_key}` = '{$edit_id}'")[0];

		if(empty($arr_data[$primary_key])){
			$arr['sts'] = false;
			echo json_encode($arr);
			exit;
		}

		//額外查詢
		if($_JSON->set->extra_query){
			foreach ($_JSON->set->extra_query as $extra_query_key => $extra_query_value) {
				${'arr_'.$extra_query_key} = $crud->getid($extra_query_key, array($extra_query_value->column => $arr_data[$extra_query_value->value]));
			}
		}
		$arr['sts'] = true;
	}

	//部分單元不返回列表設定
	$_dont_back_list = ($_JSON->set->dont_back_list)? 'Y':'N';
	$str_dont_back_list = <<<HTML
	<input type="hidden" value="{$_dont_back_list}" id="dont_back_list">
HTML;

	//外掛路徑模組
	if($_JSON->set->plugins->extra_path && !$is_external){
		include(dirname(__FILE__)."/plugins/{$_act}/{$_act}.extra_path.php");
		$_base_path = get_extra_path('read');
	}
	else{
		$_base_path = "../uploadimages/{$table}/{$edit_id}/";
	}

	foreach ($_JSON_form as $key => $value) {
		$_fix_for = 'txt';																//label for前綴
		$_type = $value->type;															//表單型態
		$_column = $value->column;														//欄位名稱
		$_alias = $value->alias;														//資料別名(此為別名function名稱，function請放置在 /back/include/alias_function.php)
		$_placeholder = $value->placeholder;											//說明文字
		$_isrequired = ($value->required)? true:false;									//是否為必填項目
		$_language = ($value->language)? true:false;									//多語系
		$_required = ($_isrequired)? 'required="required"':'';							//修正隱藏項目必填問題
		$_unique = ($value->unique)? 'data-unique="true"':'';							//唯一值欄位
		$_lock = (($arr_data["{$table}_lock"] == 1 && !$value->unlock) || (!$B_ARR_UNIT_RIGHTS['edit'] && $edit_id <> 'add'))? 'disabled="disabled"':'';	//不可修改的項目(這邊指的是該"資料列")
		// $_readonly = (!$B_ARR_UNIT_RIGHTS['edit'])? 'readonly':'';
		if($edit_id <> 'add' && $value->readonly) $_type = 'txt';											//唯讀項目(可新增不可修改)

		if($_act == 'user' && $_SESSION['MM_Username'] == 'letgo') $_lock = '';

		//某些資訊只有主分類才須設定，其餘不顯示
		if($external == 'categories' && $value->primary_only && ($arr_data['cate_par_id'] <> 0 || $edit_id == 'add')) continue;

		$append = $_extra_info = '';
		switch($_type){
			//純照片
			case 'photo_view':
				$_photo_dir = ($value->dir)? $value->dir:$_base_path;
				$_photo_dir .= $arr_data[$_column];
				if(file_exists($_photo_dir) && !empty($arr_data[$_column])){
					$append = <<<HTML
								<a href="{$_photo_dir}" target="_blank"><img src="{$_photo_dir}" class="img-thumbnail" style="max-width: 300px;"></a>
HTML;
				}
				else{
					$append = <<<HTML
								<p class="form-control-static">--</p>
HTML;
				}
				break;
				
			//純文字
			case 'txt':
					if($value->extra_query){
						$_text = ${'arr_'.$value->extra_query}[$_column];
					}
					else if($_alias){
						$_text = $_alias($arr_data[$_column]);
					}
					else{
						$_text = $arr_data[$_column];
					}
					// $_text = ($_alias)? $_alias($arr_data[$_column]):$arr_data[$_column];
					if(empty($_text) || $_text == '0000-00-00 00:00:00' || $_text == '0000-00-00'){
						$_text = '--';
					}
					else{
						if($value->number_format){
							$_text = number_format($_text);
						}
						//額外前後字串處理
						$_before = ($value->before)? $value->before:'';
						$_after = ($value->after)? $value->after:'';
						$_text = $_before.$_text.$_after;

						if($value->link){
							$url = '@(http)?(s)?(://)?(([a-zA-Z])([-\w]+\.)+([^\s\.]+[^\s]*)+[^,.\s])@';
							$_text = preg_replace($url, '<a href="http$2://$4" target="_blank" title="$0">$0</a>', $_text);
						}
					}
					$append = <<<HTML
								<p class="form-control-static" id="txt_{$_column}">{$_text}</p>
HTML;
				break;
			//文字輸入框
			case 'text':
				$_number = ($value->number)? 'data-number="true"':'';
				if($_language){
					foreach ($_LANGUAGE as $l_key => $l_value) {
						$mt = (empty($append))? '':'mt5';
						$append .= <<<HTML
						<div class="input-group {$mt}">
							<span class="input-group-addon" id="basic-addon1">{$l_value}</span>
							<input type="text" class="form-control" placeholder="{$_placeholder}" name="txt_{$_column}{$l_key}" id="txt_{$_column}{$l_key}" value="{$arr_data[$_column.$l_key]}" data-column="{$_column}{$l_key}" {$_required} {$_unique} {$_lock} {$_number} {$_readonly}/>
						</div>
HTML;
					}
				}
				else{
					$append = <<<HTML
								<input type="text" class="form-control" placeholder="{$_placeholder}" name="txt_{$_column}" id="txt_{$_column}" value="{$arr_data[$_column]}" data-column="{$_column}" {$_required} {$_unique} {$_lock} {$_number} {$_readonly}/>
HTML;
				}
				break;

			//隱蔽內容，再次驗證密碼才可看查看
			case 'secret':
				$_fix_for = 'sxt';
				if(!empty($arr_data[$_column])){
					//資料表資訊加密
					$_secret = array(
									random_str(rand(5,9)) => md5(random_str(rand(5,9))),
									'column' => $_column,
									'id' => $edit_id,
									md5(random_str(rand(5,9))) => random_str(rand(5,9))
								);
					$_secret = shuffle_assoc($_secret);	//打亂陣列(盡量避免看出規則)
					$token = encrypt(json_encode($_secret), 'iMVFDeWhYq');


					if($value->view_only){
						$view_button = <<<HTML
										<button class="btn btn-default text-danger nomargin btn_secret_hide" type="button" data-for="sxt_{$_column}" style="display:none;">
											<i class="fa fa-eye-slash fa-lg"></i>
										</button>
HTML;
					}
					else{
						$view_button = <<<HTML
										<button class="btn btn-default text-danger nomargin btn_secret_edit" type="button" data-for="sxt_{$_column}" style="display:none;">
											<i class="fa fa-edit fa-lg"></i>
										</button>
HTML;
					}
					$append = <<<HTML
							<div class="input-group">
								<input type="text" class="form-control input_secret" name="sxt_{$_column}" id="sxt_{$_column}" data-column="{$_column}"  readonly data-token="{$token}" placeholder="●●●●●●●●" {$_required} {$_unique}/>
								<span class="input-group-btn">
									{$view_button}
									<button class="btn btn-default text-danger nomargin btn_secret" type="button" data-for="sxt_{$_column}">
										<i class="fa fa-eye fa-lg"></i>
									</button>
								</span>
							</div>
HTML;
				}
				else if($value->view_only){
					$append = <<<HTML
								<p class="form-control-static">--</p>
HTML;
				}
				else{
					$input_data = '';
					$append = <<<HTML
							<div class="input-group">
								<input type="password" class="form-control" name="sxt_{$_column}" id="sxt_{$_column}" data-column="{$_column}" {$input_data} {$_required} {$_unique} />
								<span class="input-group-addon"><label class="nopt nomb"><input class="view_secret" data-item="sxt_{$_column}" type="checkbox"> 顯示</label></span>
							</div>
HTML;
				}
				break;

			//文字輸入區
			case 'textarea':
				if($_language){
					foreach ($_LANGUAGE as $l_key => $l_value) {
						$mt = (empty($append))? '':'mt5';
						$append .= <<<HTML
						<div class="input-group {$mt}">
							<span class="input-group-addon" id="basic-addon1">{$l_value}</span>
								<textarea class="form-control" name="txt_{$_column}{$l_key}" id="txt_{$_column}{$l_key}" data-column="{$_column}{$l_key}" {$_required} {$_readonly} {$_unique} {$_lock} rows="{$value->rows}">{$arr_data[$_column.$l_key]}</textarea>
						</div>
HTML;
					}
				}
				else{
					$append = <<<HTML
								<textarea class="form-control" name="txt_{$_column}" id="txt_{$_column}" data-column="{$_column}" {$_required} {$_readonly} {$_unique} {$_lock} rows="{$value->rows}">{$arr_data[$_column]}</textarea>
HTML;
				}
				break;

			//連結輸入
			case 'link':
				if($_language){
					foreach ($_LANGUAGE as $l_key => $l_value) {
						$arr_link = unserialize($arr_data[$_column.$l_key]);

						$_checked = (empty($arr_link['target']))? '':'checked';
						$_href = urldecode($arr_link['href']);
						$append = <<<HTML
								<div class="input-group">
									<span class="input-group-addon"><label class="nopt nomb"><input name="txl_{$_column}{$l_key}[target]" type="checkbox" {$_checked}> 另開視窗</label></span>
									<input type="text" class="form-control" name="txl_{$_column}{$l_key}[href]" id="txt_{$_column}{$l_key}" value="{$_href}">
								</div>
HTML;
					}
				}
				else{
					$arr_link = unserialize($arr_data[$_column]);

					$_checked = (empty($arr_link['target']))? '':'checked';
					$_href = urldecode($arr_link['href']);
					$append = <<<HTML
							<div class="input-group">
								<span class="input-group-addon"><label class="nopt nomb"><input name="txl_{$_column}[target]" type="checkbox" {$_checked}> 另開視窗</label></span>
								<input type="text" class="form-control" name="txl_{$_column}[href]" id="txt_{$_column}" value="{$_href}">
							</div>
HTML;
				}
				break;

			//密碼輸入框
			case 'password':
				$_required = ($_isrequired && $edit_id == 'add')? 'data-required="required"':'';
				$append = <<<HTML
							<div class="input-group">
								<input type="password" class="form-control" name="pxt_{$_column}" id="pxt_{$_column}" value="" {$_readonly} {$_required} {$_lock}/>
								<span class="input-group-addon"><label class="nopt nomb"><input class="view_secret" data-item="pxt_{$_column}" type="checkbox"> 顯示</label></span>
							</div>
HTML;
				break;

			//分類選擇
			case 'cate':
				$_required = ($_isrequired)? 'data-required="required"':'';
				$cate_table = ($external == 'categories')? $table:$table.'_cate';

				//預防自己為自己上層的問題
				$fix_self = ($external == 'categories')?  " AND `cate_id` <> '{$edit_id}'":'';
				$arr_cate_par = $crud->sql("SELECT *
											FROM `{$cate_table}`
											WHERE `cate_par_id` = 0 {$fix_self}
											ORDER BY `cate_title` ASC");
				$option = '';
				$_categories = $_JSON->set->categories;

				//主分類鎖定處理
				if($_categories->lock_primary && $external == 'categories' && $arr_data['cate_par_id'] == 0 && $edit_id <> 'add'){
					$_required = '';
					$_disabled = 'disabled';
				}
				else{
					$_disabled = '';
				}
				
				if(count($arr_cate_par) > 0){
					foreach ($arr_cate_par as $key => $c_value) {

						if($_categories->level > 1){
							$arr_cate = $crud->sql("SELECT *
													FROM `{$cate_table}`
													WHERE `cate_par_id` = {$c_value['cate_id']} {$fix_self}
													ORDER BY `cate_title` ASC");
							$tmp_option = '';
							if(count($arr_cate) > 0){
								foreach ($arr_cate as $_key => $_value) {
									$_selected = ($arr_data[$_column] == $_value['cate_id'])? 'selected':'';
									$tmp_option .= <<<HTML
													<option value="{$_value['cate_id']}" {$_selected}>　{$_value['cate_title']}</option>
HTML;
								}
							}
							$_selected = ($arr_data[$_column] == $c_value['cate_id'])? 'selected':'';
							$option .= <<<HTML
											<option data-divider="true"></option>
											<option value="{$c_value['cate_id']}" {$_selected}>{$c_value['cate_title']}</option>
											{$tmp_option}
HTML;
						}
						else{
							$_selected = ($arr_data[$_column] == $c_value['cate_id'])? 'selected':'';
							$option .= <<<HTML
											<option value="{$c_value['cate_id']}" {$_selected}>{$c_value['cate_title']}</option>
HTML;
						}
					}
				}

				//一層分類選單加入分隔線
				if($_categories->level < 2){
					$_tmp = <<<HTML
								<option data-divider="true"></option>
HTML;
					$option = $_tmp.$option;
				}

				$append = <<<HTML
							<select id="txt_{$_column}" name="txt_{$_column}" class="selectpicker" data-size="10" data-selected-text-format="count" data-live-search="true" data-title="請選擇" data-style="btn-default nomt nomb" {$_required} {$_disabled} {$_lock} style="display:none;">
								{$option}
							</select>
HTML;
				break;

			//下拉式選單
			case 'select':
				$_required = ($_isrequired)? 'data-required="required"':'';

				$_data = $value->data;
				$arr_value = array($arr_data[$_column]);

				//複選處理
				$ismultiple = $_data->multiple;
				if($ismultiple){
					$arr_value = explode(',', $arr_data[$_column]);
				}

				$option = '';
				if($_data->type == 'db'){	//資料庫來源
					$tb = $_data->table;
					$vl = $_data->value;
					$ti = $_data->title;
					$arr_select = $crud->select($tb, array());
					$option = '';
					foreach ($arr_select as $_key => $_value) {
						if($ismultiple){	//複選預設處理
							$selected = (array_search($_value[$vl], $arr_value) !== false)? 'selected':'';
						}
						else{	//單選預設處理
							$selected = ($arr_data[$_column] == $_value[$vl])? 'selected':'';
						}
						$option .= <<<HTML
									<option value="{$_value[$vl]}" {$selected}>{$_value[$ti]}</option>
HTML;
					}
				}
				else{	//自訂資料
					foreach ($_data->data as $_key => $_value) {
						if($ismultiple){	//複選預設處理
							$selected = (array_search($_value->value, $arr_value) !== false || ($edit_id == 'add' && $_value->selected))? 'selected':'';
						}
						else{	//單選預設處理
							$selected = ($arr_data[$_column] == $_value->value || ($edit_id == 'add' && $_value->selected))? 'selected="selected"':'';
						}

						$option .= <<<HTML
									<option value="{$_value->value}" {$selected}>{$_value->title}</option>
HTML;
					}
				}

				//搜尋功能
				$search = ($_data->search)? 'data-live-search="true"':'';
				$actions_box = ($_data->actions_box)? 'data-actions-box="true"':'';

				//單、複選參數、名稱
				if($ismultiple){
					$_multiple = 'multiple';
					$_name = "cxt_{$_column}[]";
				}
				else{
					$_multiple = '';
					$_name = "txt_{$_column}";
				}

				$str_extra_option = '';
				if($_data->extra){
					foreach($_data->extra as $e_value){
						$selected = (array_search($e_value->value, $arr_value) !== false)? 'selected':'';
						$str_extra_option .= <<<HTML
								<option value="{$e_value->value}" {$selected}>{$e_value->title}</option>
HTML;
					}
				}
				
				$append = <<<HTML
							<select id="txt_{$_column}" name="{$_name}" class="selectpicker" {$search} {$actions_box} {$_lock} data-size="10" data-selected-text-format="count" data-title="請選擇" data-style="btn-default nomt nomb" {$_multiple} {$_required} style="display:none;">
								{$str_extra_option}
								{$option}
							</select>
HTML;
				break;

			//日期選擇器
			case 'date':
				if($_language){
					foreach ($_LANGUAGE as $l_key => $l_value) {
						$_date_value = ($edit_id == 'add' && $value->default)? date("Y-m-d", strtotime(date("Y-m-d").$value->default)):$arr_data[$_column.$l_key];
						$date_vaule = ($_date_value == '0000-00-00' || $_date_value == '0000-00-00 00:00:00')? '':$_date_value;
						$append .= <<<HTML
						<div class="input-group">
							<span class="input-group-addon" id="basic-addon1">{$l_value}</span>
							<input type="text" class="form-control datepicker" name="txt_{$_column}{$l_key}" id="txt_{$_column}{$l_key}" value="{$date_vaule}" data-column="{$_column}{$l_key}" readonly {$_required} {$_unique} {$_lock} />
						</div>
HTML;
					}
				}
				else{
					$_date_value = ($edit_id == 'add' && $value->default)? date("Y-m-d", strtotime(date("Y-m-d").$value->default)):$arr_data[$_column];
					$date_vaule = ($_date_value == '0000-00-00' || $_date_value == '0000-00-00 00:00:00')? '':$_date_value;
					$append = <<<HTML
						<div class="input-group">
							<input type="text" class="form-control datepicker" name="txt_{$_column}" id="txt_{$_column}" value="{$date_vaule}" data-column="{$_column}" readonly {$_required} {$_unique} {$_lock} />
							<span class="input-group-btn">
								<button class="btn btn-default text-danger nomargin btn_date_cancel" {$_lock} type="button" data-for="txt_{$_column}">
									<i class="fa fa-times fa-lg"></i>
								</button>
							</span>
						</div>
HTML;
				}
				break;

			//日期時間選擇器
			case 'datetime':
				$_date_value = ($edit_id == 'add' && $value->default)? date("Y-m-d H:i:s", strtotime(date("Y-m-d H:i:s").$value->default)):$arr_data[$_column];
				$date_vaule = ($_date_value == '0000-00-00' || $_date_value == '0000-00-00 00:00:00')? '':$_date_value;
				$now_date = date('Y-m-d');
				$append = <<<HTML
					<div class="input-group">
						<input type="text" class="form-control daterangepicker" data-date-start-date="{$date_vaule}" name="txt_{$_column}" id="txt_{$_column}" value="{$date_vaule}" data-column="{$_column}" readonly {$_required} {$_unique} {$_lock} />
						<span class="input-group-btn">
							<button class="btn btn-default text-danger nomargin btn_date_cancel" {$_lock} type="button" data-for="txt_{$_column}">
								<i class="fa fa-times fa-lg"></i>
							</button>
						</span>
					</div>
HTML;
				break;

			//圖片上傳
			case 'photo':
				//多圖上傳參數
				$is_multiple = (empty($value->multiple))? false:true;
				$_thumbnail = (empty($value->thumbnail))? '':'data-thumbnail="'.$value->thumbnail.'"';
				if($is_multiple){
					$_multiple = 'multiple="multiple"';
					$_data_multiple = 'data-multiple="true"';
					$arr_album = $crud->sql("SELECT *
											FROM `{$value->table}`
											WHERE `host_id` = '{$edit_id}'
											ORDER BY `album_sort`, `album_id` DESC");
					$str_album = $str_album_sort = $_photo_list = '';
					$count_album = count($arr_album);
					if($count_album > 0){
						$btn_edit_info = ($value->edit_info)? '<a href="javascript:;" class="btn btn-default btn-sm nomt" id="btn_album_edit" ><i class="fa fa-edit fa-lg text-success" role="button"></i></a>':'';
						foreach ($arr_album as $a_value) {
							$_img = explode('.', $a_value['album_photo']);
							$_img = $_img[0].'_a.'.$_img[1];
							$str_album .= <<<HTML
											<li class="ui-state-default cursor_move" data-id="{$a_value['album_id']}">
												<div class="album_img" style="background-image: url('{$_base_path}{$_img}');"></div>
												<div class="btn-group btn-group-justified btn-group-sm mt5" role="group" aria-label="Justified button group">
													<a href="javascript:;" id="btn_album_del" class="btn btn-default nomt" role="button"><i class="fa fa-trash-o fa-lg text-danger"></i></a>
													<a href="javascript:;" class="btn btn-default nomt" id="btn_image_view" data-dir="{$_base_path}" data-img="{$a_value['album_photo']}" role="button"><i class="fa fa-photo fa-lg text-primary"></i></a>
													{$btn_edit_info}
												</div>
											</li>
HTML;
							$str_album_sort .= $a_value['album_id'].',';
						}
						$str_album_sort = '['.substr($str_album_sort, 0, strlen($str_album_sort) - 1).']';
					}
					if(!empty($str_album)){
						$_photo_list = <<<HTML
											<div class="well center-block mt10 album_data">
												<ul class="sortable" data-for="fxt_{$_column}" data-external="{$_column}">
													{$str_album}
												</ul>
												<p class="help-block nomb"><small>拖曳圖片可進行排序</small></p>
											</div>
HTML;
					}
					$btn_view = '';

					//總數限制
					if($value->limit){
						$now_limit = ($value->limit - $count_album);
						$_limit_info = "可再上傳：{$now_limit}張<br>";
						$now_limit = 'data-limit="'.$now_limit.'"';
					}

					$_extra_info = <<<HTML
						<p class="help-block">
							上傳模式：多檔<br>
							{$_limit_info}
							建議尺寸：{$value->need_size[0]}px * {$value->need_size[1]}px<br>
							單檔大小限制：5MB<br>
							單次上傳數量限制：10張
						</p>
HTML;
				}
				else{
					$disabled = (empty($arr_data[$_column]))? 'disabled':'';
					$_multiple = $_photo_list = $_data_multiple = '';
					$btn_view = <<<HTML
									<button {$disabled} class="btn btn-primary nomargin" type="button" id="btn_image_view" data-dir="{$_base_path}" data-img="{$arr_data[$_column]}" >
										<i class="fa fa-photo fa-lg"></i>
									</button>
HTML;

					$_extra_info = <<<HTML
						<p class="help-block">
							上傳模式：單檔<br>
							{$_limit_info}
							建議尺寸：{$value->need_size[0]}px * {$value->need_size[1]}px<br>
							單檔大小限制：5MB
						</p>
HTML;
				}

				if($_language){
					foreach ($_LANGUAGE as $l_key => $l_value) {
						$_required = ($_isrequired && ($edit_id == 'add' || empty($arr_data[$_column.$l_key])))? 'data-required="required"':'';
						$disabled = (empty($arr_data[$_column.$l_key]))? 'disabled':'';
						$append .= <<<HTML
									<div class="input-group btn_file">
										<span class="input-group-addon" id="basic-addon1">{$l_value}</span>
										<input type="text" class="form-control" readonly="readonly" value="{$arr_data[$_column.$l_key]}">
										<span class="input-group-btn">
											<button class="btn btn-default nomargin" type="button" id="btn_file_select" data-for="fxt_{$_column}{$l_key}" {$_lock}>
												<i class="fa fa-search fa-lg"></i> 瀏覽
											</button>
											{$btn_view}
										</span>
										<input type="file" name="fxt_{$_column}{$l_key}[]" id="fxt_{$_column}{$l_key}" data-column="{$_column}{$l_key}" data-old_photo="{$arr_data[$_column.$l_key]}" data-isphoto="true" accept="image/*" data-table="{$value->table}" data-sort="{$str_album_sort}" {$now_limit} {$_thumbnail} {$_required} {$_multiple} {$_data_multiple} {$_lock} style="display:none;">
									</div>
									{$_photo_list}
HTML;
					}
				}
				else{
					$_required = ($_isrequired && ($edit_id == 'add' || (empty($arr_data[$_column]) && !$is_multiple)))? 'data-required="required"':'';

					//刪除功能，新增、必填、空白 此三種狀態不提供刪除
					$_btn_del_file = '';
					if($edit_id <> 'add' && !empty($arr_data[$_column]) && !$_isrequired){
						$_btn_del_file = <<<HTML
										<span class="input-group-addon"><label class="nopt nomb"><input name="del_{$_column}" class="btn_del_file" type="checkbox" {$_checked} {$_lock}> 刪除</label></span>
HTML;
					}
					$append = <<<HTML
								<div class="input-group btn_file">
									{$_btn_del_file}
									<input type="text" class="form-control" readonly="readonly" value="{$arr_data[$_column]}">
									<input type="file" name="fxt_{$_column}[]" id="fxt_{$_column}" data-column="{$_column}" data-old_photo="{$arr_data[$_column]}" data-isphoto="true" data-table="{$value->table}" accept="image/*" data-sort="{$str_album_sort}" {$now_limit} {$_thumbnail} {$_required} {$_multiple} {$_data_multiple} {$_lock} style="display:none;">
									<span class="input-group-btn">
										<button class="btn btn-default nomargin" type="button" id="btn_file_select" data-for="fxt_{$_column}" {$_lock}>
											<i class="fa fa-search fa-lg"></i> 瀏覽
										</button>
										{$btn_view}
									</span>
								</div>
								{$_photo_list}
HTML;
				}
				$_fix_for = 'fxt';
				break;

			//檔案上傳
			case 'file':
				if($_language){
					foreach ($_LANGUAGE as $l_key => $l_value) {
						$mt = (empty($append))? '':'mt5';
						$_required = ($_isrequired && ($edit_id == 'add' || empty($arr_data[$_column.$l_key])))? 'data-required="required"':'';
						$disabled = (empty($arr_data[$_column.$l_key]))? 'disabled':'';
						$append .= <<<HTML
									<div class="input-group btn_file {$mt}">
										<span class="input-group-addon" id="basic-addon1">{$l_value}</span>
										<input type="text" class="form-control" readonly="readonly" value="{$arr_data[$_column.$l_key]}">
										<span class="input-group-btn">
											<button class="btn btn-default nomargin" type="button" id="btn_file_select" data-for="fxt_{$_column}{$l_key}" {$_lock}>
												<i class="fa fa-search fa-lg"></i> 瀏覽
											</button>
										</span>
										<span class="input-group-btn">
											<a href="{$_base_path}{$arr_data[$_column.$l_key]}" target="_blank">
												<button {$disabled} class="btn btn-primary nomargin" type="button">
													<i class="fa fa-file fa-lg"></i>
												</button>
											</a>
										</span>
										<input type="file" name="fxt_{$_column}{$l_key}[]" id="fxt_{$_column}{$l_key}" data-column="{$_column}{$l_key}" data-old_photo="{$arr_data[$_column.$l_key]}" data-table="{$value->table}" accept="{$value->accept}" {$_required} {$_lock} style="display:none;">
									</div>
HTML;
					}
				}
				else{
					$_required = ($_isrequired && ($edit_id == 'add' || empty($arr_data[$_column])))? 'data-required="required"':'';

					//刪除功能，新增、必填、空白 此三種狀態不提供刪除
					$_btn_del_file = '';
					if($edit_id <> 'add' && !empty($arr_data[$_column]) && !$_isrequired){
						$_btn_del_file = <<<HTML
										<span class="input-group-addon"><label class="nopt nomb"><input name="del_{$_column}" class="btn_del_file" type="checkbox" {$_checked} {$_lock}> 刪除</label></span>
HTML;
					}
					$disabled = (empty($arr_data[$_column]))? 'disabled':'';
					$append = <<<HTML
								<div class="input-group btn_file">
									{$_btn_del_file}
									<input type="text" class="form-control" readonly="readonly" value="{$arr_data[$_column]}">
									<span class="input-group-btn">
										<button class="btn btn-default nomargin" type="button" id="btn_file_select" data-for="fxt_{$_column}" {$_lock}>
											<i class="fa fa-search fa-lg"></i> 瀏覽
										</button>
									</span>
									<span class="input-group-btn">
										<a href="{$_base_path}{$arr_data[$_column]}" target="_blank">
											<button {$disabled} class="btn btn-primary nomargin" type="button">
												<i class="fa fa-file fa-lg"></i>
											</button>
										</a>
									</span>
									<input type="file" name="fxt_{$_column}[]" id="fxt_{$_column}" data-column="{$_column}" data-old_photo="{$arr_data[$_column]}" data-table="{$value->table}" accept="{$value->accept}" {$_required} {$_lock} style="display:none;">
								</div>
HTML;
				}
				$_fix_for = 'fxt';
				break;

			//單選按鈕
			case 'radio':
				foreach ($value->data as $_key => $_value) {
					$checked = ($arr_data[$_column] == $_value->value || ($edit_id == 'add' && $_value->checked))? 'checked="checked"':'';

					$append .= <<<HTML
								<label class="radio-inline">
									<input type="radio" name="txt_{$_column}" id="txt_{$_column}" value="{$_value->value}" {$checked} {$_lock}>{$_value->title}
								</label>
HTML;
				}
				break;

			//複選按鈕
			case 'checkbox':
				$_required = ($_isrequired)? 'data-check_required="required"':'';
				$arr_value = explode(',', $arr_data[$_column]);
				foreach ($value->data as $_key => $_value) {
					$checked = (array_search($_value->value, $arr_value) !== false)? 'checked="checked"':'';

					$tmp_append .= <<<HTML
								<label class="checkbox-inline">
									<input type="checkbox" name="cxt_{$_column}[]" id="txt_{$_column}" value="{$_value->value}" {$checked} {$_lock}>{$_value->title}
								</label>
HTML;
				}
				$append = <<<HTML
							<div class="div_checkbox" {$_required}>
								{$tmp_append}
							</div>
HTML;
				break;

			//圖文編輯器
			case 'wysiwyg':
				$_required = ($_isrequired)? 'data-required="required"':'';
				if($_language){
					foreach ($_LANGUAGE as $l_key => $l_value) {
						$append .= <<<HTML
									<h3 class="nomt">{$l_value}：</h3>
									<div class="well center-block">
										<textarea name="txt_{$_column}{$l_key}" id="txt_{$_column}{$l_key}" class="form-control wysiwyg" rows="5" placeholder="" {$_required}>{$arr_data[$_column.$l_key]}</textarea>
									</div>
									
HTML;
					}
				}
				else{
					$append = <<<HTML
								<textarea name="txt_{$_column}" id="txt_{$_column}" class="form-control wysiwyg" rows="5" placeholder="消息內容" {$_required}>{$arr_data[$_column]}</textarea>
HTML;
				}
				break;

			//操作按鈕
			case 'edit':
				$append = <<<HTML
							<a href="javascript:;" id="btn_edit">
								<i class="fa fa-pencil fa-2x text-success fa-fw" title=""></i>
							</a>&nbsp;
							<a href="javascript:;" id="btn_del">
								<i class="fa fa-trash fa-2x text-danger fa-fw" title=""></i>
							</a>
HTML;
				break;

			//地址選取器
			case 'address':
				$append .= <<<HTML
								<div class="input-group address" data-city="{$arr_data[$_column.'_city']}" data-county="{$arr_data[$_column.'_county']}">
									<span class="input-group-addon zipcode"></span>
									<input id="txt_{$_column}_zipcode" name="txt_{$_column}_zipcode" type="hidden" class="zipcode" readonly="readonly" value="">
									<select id="txt_{$_column}_city" name="txt_{$_column}_city" class="selectpicker city" data-live-search="true" data-width="fit" style="display:none">
										<option value="" selected>請選擇</option>
									</select>
									<select id="txt_{$_column}_county" name="txt_{$_column}_county" class="selectpicker county" data-live-search="true" data-width="fit" style="display:none">
										<option value="" selected>請選擇</option>
									</select>
									<input id="txt_{$_column}_addr" name="txt_{$_column}_addr" type="text" class="form-control" value="{$arr_data[$_column.'_addr']}">
								</div>
HTML;
				break;

			//開關
			case 'switch':
				$checked = ($arr_data[$_column] == 1 || ($edit_id == 'add' && $value->checked))? 'checked="checked"':'';
				$switch_value = (empty($checked))? 0:1;
				$_disabled = (empty($_readonly))? '':'disabled';
				$append = <<<HTML
							<p class="form-control-static btn_form_switch npm">
								<input id="btn_{$_column}-{$primary_id}" class="cmn-toggle cmn-toggle-round" type="checkbox" ${$_disabled} {$checked} {$_lock}>
								<label for="btn_{$_column}-{$primary_id}"></label>
								<input id="txt_{$_column}" name="txt_{$_column}" type="hidden" value="{$switch_value}">
							</p>
HTML;
				break;

			//分隔線處理
			case 'hr':
				$str_append_html .= <<<HTML
										<h3 class="page-header text-info">
											{$value->title}
										</h3>
HTML;
				continue 2;
				break;

			//外掛模組
			case 'plugins':
				$append = plugins($value);
				break;
		}

		//必填標記
		$required = ($_isrequired)? '<i class="fa fa-star text-danger"></i> ':'';

		//說明文字
		$info = ($value->info)? '<p class="help-block">'.$value->info.'</p>':'';
		//純程式處理不顯示
		$hidden = ($value->hidden)? 'style="display: none;"':'';

		if($value->nolabel){
			$str_append_html .= <<<HTML
							<div class="form-group" {$hidden}>
								<div class="col-sm-offset-{$value->offset} col-sm-{$value->size}">
									{$append}
									{$info}
									{$_extra_info}
								</div>
							</div>
HTML;
		}
		else{
			$str_append_html .= <<<HTML
							<div class="form-group" {$hidden}>
								<label for="{$_fix_for}_{$_column}" class="col-sm-3 control-label">{$required}{$value->title}：</label>
								<div class="col-sm-{$value->size}">
									{$append}
									{$info}
									{$_extra_info}
								</div>
							</div>
HTML;
		}
	}

	//不返回處理
	$str_append_html .= $str_dont_back_list;

	//表單使用JAVASCRIPT，請注意跳脫字元，如 \n 請打 \\n
	//若為外部使用表單模組則不須初始化selectpicker，否則會造成錯誤
	$str_selectpicker = (!$is_external)? "$('.selectpicker').selectpicker('destroy').selectpicker();":'';
	$str_append_js = <<<HTML
		<script type="text/javascript">
			{$str_selectpicker}
			$('#form_data input:first').focus();
			$('.address').each(function(){
				var _this = $(this);

				_this.ajaddress({ city: _this.data('city'), county: _this.data('county') }).find('.city').on('change', function(){
					$('.county').selectpicker('refresh');
				}).change();
				$('.city').selectpicker('refresh');
			});
			$('#form_data input:first').focus();
			var opt={
						dayNames:["星期日","星期一","星期二","星期三","星期四","星期五","星期六"],
						dayNamesMin:["日","一","二","三","四","五","六"],
						monthNames:["一月","二月","三月","四月","五月","六月","七月","八月","九月","十月","十一月","十二月"],
						monthNamesShort:["一月","二月","三月","四月","五月","六月","七月","八月","九月","十月","十一月","十二月"],
						prevText:"上月",
						nextText:"次月",
						weekHeader:"週",
						showMonthAfterYear:true,
						dateFormat:"yy-mm-dd",
						changeMonth: true,
						changeYear: true
					};
			$('.datepicker').datepicker(opt);
			$('.daterangepicker').daterangepicker({
				"singleDatePicker": true,
				"timePicker": true,
				"timePicker24Hour": true,
				"timePickerSeconds": true,
				"autoUpdateInput": false,
				"locale": {
					"format": 'YYYY/MM/DD HH:mm:ss',
					"daysOfWeek": ["日","一","二","三","四","五","六"],
					"monthNames": ["一月","二月","三月","四月","五月","六月","七月","八月","九月","十月","十一月","十二月"],
				}
			}).on('apply.daterangepicker', function(ev, picker) {
				$(this).val(picker.startDate.format('YYYY/MM/DD HH:mm:ss'));
			}).each(function(){
				var _this = $(this);
				if(_this.data('date-start-date')){
					_this.data('daterangepicker').setStartDate(_this.data('date-start-date'));
				}
			});
			$('.select2_cate').select2({
				width: '100%'
			});
			$('.wysiwyg').each(function(){
				var this_name = $(this).attr('name');
				CKEDITOR.replace(this_name, {
					language:"zh"
				} );
				
				function triggerUploadImages(url){
					if(this_name){
						CKEDITOR.dialog.getCurrent().hide();
						eval('CKEDITOR.instances.'+this_name+'.insertHtml("<img src="+url+" />")');
					}
				}
			});

			$('[id="btn_image_view"]').on('click', function(){
				var _this = $(this),
					dir = _this.data('dir'),
					img = _this.data('img');
				if(img != '') $('#image_view .modal-body').html('<img style="max-height:100%;max-width:100%;" src="'+dir+img+'"/>');
				$('.modal').css('z-index', 10000);
				$('#image_view').modal('show').css('z-index', 10001);
			});

			$('[id="btn_file_select"]').on('click', function(){
				var _this = $(this);
				_this.parents('.btn_file').find('#'+_this.data('for')).click();
			});

			$('.btn_file :file').on('change', function() {
				var input = $(this),
					files = input.get(0).files,
					numFiles = files ? files.length : 1,
					label = input.val().replace(/\\\\/g, '/').replace(/.*\//, ''),
					limit = input.data('limit');
				if(input[0].multiple && numFiles > 10){
					do_notify.error('<i class="fa fa-exclamation-triangle fa-lg"></i> 選取的檔案數量超過單次數量限制(10個)');
					input.val('');
					return false;
				}
				else if(input[0].multiple && numFiles > limit){
					do_notify.error('<i class="fa fa-exclamation-triangle fa-lg"></i> 選取的檔案數量超過可再上傳數量限制('+limit+'張)');
					input.val('');
					return false;
				}

				for(var i = 0; i < numFiles; i++){
					if(files[i]['size'] > 4950000){
						do_notify.error('<i class="fa fa-exclamation-triangle fa-lg"></i> 檔案【'+files[i]['name']+'】超過檔案大小限制5MB');
						input.val('');
						return false;
					}
				}

				var input_text = input.parent().find(':text'),
					log = numFiles > 1 ? '已選擇 ' + numFiles + ' 個檔案' : label;
				if( input_text.length ) {
					input_text.val(log);
				}
				else {
					if( log ) alert(log);
				}
			});

			$('.btn_date_cancel').on('click', function(){
				var _this = $(this);
				$('#' + _this.data('for')).val('');
			});

			$('.btn_secret').on('click', function(){
				var _this = $(this),
					that = _this.parents('.input-group').find('input');
				bootbox.prompt({
					title: "為了資訊安全您必須輸入密碼才能查看",
					closeButton: false,
					inputType: 'password',
					size: 'small',
					callback: function (result) {
						if(result !== null){
							if(result == ''){
								do_notify.error('<i class="fa fa-exclamation-triangle fa-lg"></i> 請輸入密碼');
								return false;
							}
							else{
								var v_data = {
										type: 'secret',
										token: that.data('token'),
										key: result
									};
								doajax({
									url: 'control',
									type: 'json',
									data: jQuery.param(v_data),
									beforeSend: function(xhr, opts) {
										$.blockUI();
									},
									callback:function(msg){
										if(msg.sts){
											var _that = _this.parents('.input-group-btn');
											that.val(msg.secret).focus();
											if(_that.find('.btn_secret_hide').get(0)){
												_that.find('.btn_secret').hide();
											}
											else{
												_that.find('.btn_secret').remove();
											}
											_that.find('.btn_secret_edit, .btn_secret_hide').show();
										}
										else{
											do_notify.error('<i class="fa fa-exclamation-triangle fa-lg"></i> '+msg.msg);
										}
										$.unblockUI();
									}
								});
							}
						}
					}
				});
			}).parents('.input-group').find(':input').each(function(){
				var _this = $(this),
					token = _this.data('token');
				_this.removeAttr('data-token').data('token', token);
			});

			$('.btn_secret_edit').on('click', function(){
				var _this = $(this);
				_this.parents('.input-group').find('input').attr('readonly', false).focus();
				_this.attr('disabled', true);
			});

			$('.btn_secret_hide').on('click', function(){
				var _this = $(this);
				_this.parents('.input-group').find('input').val('');
				_this.parents('.input-group').find('.btn_secret').show();
				_this.hide();
			});

			$('.view_secret').on('click', function(){
				var _this = $(this);
				_this.parents('.input-group').find('#'+_this.data('item')).attr('type', (_this.is(':checked'))? 'text':'password');
			});

			$('.input_secret').on('click, focus', function(){
				$(this).select();
			});

			$('[id="btn_album_del"]').on('click', function(){
				var _this = $(this),
					that = _this.parents('.form-group').find('.sortable');


				bootbox.dialog({
					message: "<h4><strong>確定要刪除這張圖片嗎?</strong></h4>",
					size: 'small',
					buttons: {
						success: {
							label: "確認",
							className: "btn-danger nomargin",
							callback: function() {
								_this.parents('li').remove();
								that.trigger('sortupdate');
							}
						},
						cancel: {
							label: "取消",
							className: "btn-default nomargin"
						}
					}
				});
			});

			$( ".sortable" ).sortable({
				items: "> li"
			}).on("sortupdate", function(event, ui) {
				var _this = $(this),
					that = _this.parents('.form-group');
				that.find(':file#'+_this.data('for')).data('sort', _this.sortable("toArray", { attribute: "data-id" }));
			});
			$('.btn_del_file').on('click', function(){
				var _this = $(this);
				_this.parents('.btn_file').find('button, :file').attr('disabled', (_this.is(':checked'))? true:false);
			});

			$('.btn_form_switch').on('click', ':checkbox', function(){
				var _this = $(this);
				_this.parents('.btn_form_switch').find(':hidden').val((_this.is(':checked'))? 1:0);
			});

			$('[id="btn_album_edit"]').off().on('click', function(){
				var that = $(this).parents('li'),
					eid = that.data('id'),
					external_name = that.parents('ul').data('external'),
					v_data = {
						external: external_name,
						id: eid
					};
				doajax({
					url: 'form',
					data: jQuery.param(v_data),
					type: 'json',
					callback: function(msg){
						bootbox.dialog({
							message: '<form class="form-horizontal form_'+external_name+'">'+
										'<div id="form_'+external_name+'_data">'+msg.form+'</div>'+
										'<input id="edit_'+external_name+'_id" name="edit_id" type="hidden" value="'+eid+'">'+
										'<input id="external" name="external" type="hidden" value="'+external_name+'">'+
									'</form>',
							buttons: {
								success: {
									label: "確認",
									className: "btn-danger nomargin",
									callback: function() {
										var _this = $('.modal-dialog .form_'+external_name),
											_error = false;

										_this.find(":required:enabled,[data-required='required']").each(function(){
											var that = $(this),
												str_req = $("label[for='"+that.attr('id')+"']:first").text().replace(':','').replace('：',''),
												str_mode = (that.is(':file'))? '上傳':'輸入';
											if(that.hasClass('wysiwyg')){
												var _name = that.attr('id');
												if(eval('CKEDITOR.instances.'+_name+'.getData()') == ''){
													_error = true;
												}
											}
											else if(that.val() == ''){
												_error = true;
											}

											if(_error){
												do_notify.error('<i class="fa fa-exclamation-triangle fa-lg"></i> 請'+str_mode+' '+str_req+'');
												$('body').animate({
												 	scrollTop:$('#'+that.attr('id')).parents('.form-group').offset().top - 130
												 });
												return false;
											}
										});
										
										if(_error) return false;

										_this.find('[data-unique="true"]').each(function(){
											var that = $(this),
												str_req = $("label[for='"+that.attr('id')+"']").text().replace(':','').replace('：',''),
												obj = {
													type: 'unique',
													id : _this.find('#edit_'+external_name+'_id').val(),
													value: that.val(),
													column : that.data('column'),
													external: _this.find('#external').val()
												};

											doajax({
												url: 'control',
												data: jQuery.param(obj),
												type: 'json',
												async: false,
												callback: function(msg){
													_error = (msg.unique)? false:true;
												}
											});

											if(_error){
												do_notify.error('<i class="fa fa-exclamation-triangle fa-lg"></i> 此 '+str_req+' 已經使用過了');
												$('body').animate({
													scrollTop:$('#'+that.attr('id')).parents('.form-group').offset().top - 130
												});
												return false;
											}
										});

										if(_error) return false;

										doajax({
											url: 'control',
											type: 'json',
											data: 'type=edit&'+_this.serialize(),
											files: $('.modal-dialog .form_'+external_name+' .btn_file :file'),
											beforeSend: function(xhr, opts) {
												$.blockUI();
											},
											callback:function(msg){
												var add_msg = null,
													edit_id = $('#edit_id'),
													page = (edit_id == 'add')? 1:$('.pagination .active').text();
												if(msg.cid){
													edit_id.val(msg.cid);
													add_msg = '<i class="fa fa-check"></i> 資料新增完成';
												}
												$.unblockUI();
												do_notify.success(add_msg);
											}
										});
									}
								},
								cancel: {
									label: "取消",
									className: "btn-default nomargin"
								}
							}
						});
					}
				});
			});
			{$plugins_JS}
		</script>
HTML;

	$arr['form'] = $str_append_html.$str_append_js;

	// echo preg_replace('/\\\\[rnt]/', '', json_encode($arr));
	echo json_encode($arr);
?>