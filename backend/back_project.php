<!DOCTYPE html>
<html lang="en">

<head>
	<?php include_once 'back_head.php'; ?>
</head>

<body>

	<div id="wrapper">

		<!-- Navigation -->
		<nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
			<?php include_once 'back_navbar.php'; ?>

			<div class="navbar-default sidebar" role="navigation">
				<?php include_once 'back_navbar_left.php'; ?>
			</div>
			<!-- /.navbar-static-side -->
		</nav>

		<!-- Page Content -->
		<div id="page-wrapper">
			<div class="container-fluid">
				<!-- Page Heading -->
				<div class="row">
					<div class="col-lg-12">
						<a href="./">
							<h1 class="page-header text-center">
								近七日訪客概況
							</h1>
						</a>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-12">
						<div class="panel panel-default">
							<div class="panel-heading">
								<h3 class="panel-title">造訪人數</h3>
							</div>
							<div class="panel-body">
								<div id="chart_visits" style="height: 250px;"></div>
							</div>
						</div>
					</div>
					<div class="col-sm-7">
						<div class="panel panel-default">
							<div class="panel-heading">
								<h3 class="panel-title">來源網站</h3>
							</div>
							<div class="panel-body">
								<div id="chart_referrers" style="height: 250px;"></div>
							</div>
						</div>
					</div>
					<div class="col-sm-5">
						<div class="panel panel-default">
							<div class="panel-heading">
								<h3 class="panel-title">來源國家</h3>
							</div>
							<div class="panel-body">
								<div id="chart_country" style="height: 250px;"></div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- /.container-fluid -->
		</div>
		<!-- /#page-wrapper -->

	</div>
	<!-- /#wrapper -->

	<?php include_once 'back_footer.php'; ?>
<script type="text/javascript">
	<?=$_JAVASCRIPT;?>
</script>
</body>

</html>
