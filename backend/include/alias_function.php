<?php 
	function get_order_payment($str){
		switch ($str) {
			case 'credit':
				$_str = '線上刷卡';
				break;

			case 'atm':
				$_str = 'ATM';
				break;
			
			default:
				$_str = $str;
				break;
		}

		return $_str;
	}

	function get_online($str){
		switch ($str) {
			case '1':
				$_str = '開啟';
				break;

			case '0':
				$_str = '關閉';
				break;
			
			default:
				$_str = $str;
				break;
		}

		return $_str;
	}

	function get_gender($str){
		switch ($str) {
			case '1':
				$_str = '女';
				break;

			case '2':
				$_str = '男';
				break;
			
			default:
				$_str = $str;
				break;
		}

		return $_str;
	}

	function get_invoice_status($str){
		switch ($str) {
			case '1':
				$_str = '申請中';
				break;
			case '2':
				$_str = '未付';
				break;
			case '3':
				$_str = '已繳清';
				break;
			case '4':
				$_str = '其他';
				break;
			
			default:
				$_str = $str;
				break;
		}

		return $_str;
	}

	function get_invoice_status_en($str){
		switch ($str) {
			case '1':
				$_str = 'Application';
				break;
			case '2':
				$_str = 'Unpaid';
				break;
			case '3':
				$_str = 'Has been paid';
				break;
			case '4':
				$_str = 'Other';
				break;
			
			default:
				$_str = $str;
				break;
		}

		return $_str;
	}
?>