<?php 
	function array_push_after($src,$in,$pos){
		if(is_int($pos)){
			$R=array_merge(array_slice($src,0,$pos+1), $in, array_slice($src,$pos+1));
		}
		else{
			foreach($src as $k=>$v){
				$R[$k]=$v;
				if($k==$pos)$R=array_merge($R,$in);
			}
		}return $R;
	}

	function recursive_array_search($needle,$haystack) {
		foreach($haystack as $key=>$value) {
			$current_key=$key;
			if($needle===$value OR (is_array($value) && recursive_array_search($needle,$value) !== false)) {
				return $current_key;
			}
		}
		return false;
	}

	// creates random strings
	function random_str($length, $space = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ'){
		$str = '';
		$max = mb_strlen($space, '8bit') - 1;
		for ($i = 0; $i < $length; ++$i) {
			if(function_exists('random_int')){ // only php 7
				$str .= $space[random_int(0, $max)];
			}
			else{
				$str .= $space[mt_rand(0, $max)];
			}
		}
		return $str;
	}

	
	function encrypt($data, $key) {
		$key = md5($key);
		$x = 0;
		$len = strlen($data);
		$l = strlen($key);
		for ($i = 0; $i < $len; $i++){
			if ($x == $l){
				$x = 0;
			}
			$char .= $key{$x};
			$x++;
		}
		for ($i = 0; $i < $len; $i++){
			$str .= chr(ord($data{$i}) + (ord($char{$i})) % 256);
		}
		return base64_encode($str);
	}

	function decrypt($data, $key) { 
		$key = md5($key);
		$x = 0;
		$data = base64_decode($data);
		$len = strlen($data);
		$l = strlen($key);
		for ($i = 0; $i < $len; $i++){
			if ($x == $l){
				$x = 0;
			}
			$char .= substr($key, $x, 1);
			$x++;
		}
		for ($i = 0; $i < $len; $i++){
			if (ord(substr($data, $i, 1)) < ord(substr($char, $i, 1))){
				$str .= chr((ord(substr($data, $i, 1)) + 256) - ord(substr($char, $i, 1)));
			}
			else {
				$str .= chr(ord(substr($data, $i, 1)) - ord(substr($char, $i, 1)));
			}
		}
		return $str;
	}

	//打亂陣列(保存key值)
	function shuffle_assoc($array){
		$keys = array_keys($array);
		shuffle($keys);
		return array_merge(array_flip($keys), $array);
	}

	function getcurl($xurl){
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); 
		curl_setopt($ch, CURLOPT_URL,$xurl); 
		curl_setopt($ch, CURLOPT_USERAGENT, "Mozilla/4.0 (compatible;)");
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION,true); //true ,false 
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false); /* SSL prevent */
		//curl_setopt($ch, CURLOPT_USERAGENT, "Google Bot");
		$ffp = curl_exec($ch);
		curl_close($ch);
		return $ffp;
	}

	//修正AJAX使用POST urlencode的字元
	function fix_ajax_urlencode($value){
		switch (gettype($value)) {
			case 'string':
				return urldecode($value);
				break;
			case 'array':
				return array_map('fix_ajax_urlencode', $value);
				break;
			
			default:
				return $value;
				break;
		}
	}

	function get_serial_number($key, $type, $bits = 4){
		global $crud;
		// $type = 'B';
		$arr_num = $crud->getid('number_control', array('number_type' => $type, 'number_key' => $key));
		if(empty($arr_num['number_id'])){
			$new_num = 1;
			$result = $crud->create('number_control', array('number_type' => $type, 'number_cout' => 1, 'number_key' => $key));
			// if(!$result){
			// 	$crud->rollBack();
			// 	errstop('BettingDebug', 'O001', 'json');
			// }
		}
		else{
			$new_num = $arr_num['number_cout'] + 1;
			$result = $crud->update('number_control', array('number_cout' => $new_num), array('number_id' => $arr_num['number_id']));
			// if(!$result){
			// 	$crud->rollBack();
			// 	errstop('BettingDebug', 'O002', 'json');
			// }
		}
		$serial_number = str_pad($new_num, $bits, '0', STR_PAD_LEFT);   //下注編號

		return $serial_number;
	}
	//sprintf 使用array key方式，較好閱讀
	function vksprintf($str, $args) { 
		if (is_object($args)) { 
			$args = get_object_vars($args); 
		} 
		$map = array_flip(array_keys($args)); 
		$new_str = preg_replace_callback('/(^|[^%])%([a-zA-Z0-9_-]+)\$/', 
				function($m) use ($map) { return $m[1].'%'.($map[$m[2]] + 1).'$'; }, 
				$str); 
		return vsprintf($new_str, $args); 
	}
?>