<?php
class log_bruteforce{
	private $db;  // database connection
	
	// Variables
	private $BF_allowed_false_tries = 5; 	// IP will be locked after amount of false tries
	private $BF_in_timeframe = 30; 			// in a timeframe in minutes
	private $BF_blocktime = 30; 			// Blocktime in minutes

	function __construct($DB_con){
		$this->db = $DB_con;
	}
	
	public function logIP($loginUsername, $loginFoundUser) // logIP('username', true/false)
	{
		$log_ar_data = array("l_ip"=>self::getIP(), "l_browser"=>$_SERVER['HTTP_USER_AGENT'], "l_username"=>$loginUsername, "l_success"=>$loginFoundUser);
		self::create("login_log", $log_ar_data);
	}
	
	public function bruteforcecheck()
	{
		$sql_brute="SELECT `l_date` FROM `login_log` WHERE `l_ip`=\"".self::getIP()."\" AND `l_success`=\"0\" ORDER BY `l_date` DESC LIMIT ".$this->BF_allowed_false_tries;
		$date_array = self::sql($sql_brute);
		
		// decide if too many false tries and exit
		if(count($date_array)==$this->BF_allowed_false_tries)
		{
			date_default_timezone_set('Asia/Taipei');

			$time_last_login = new DateTime($date_array[0]["l_date"]);
			$time_older_login = new DateTime($date_array[($this->BF_allowed_false_tries)-1]["l_date"]);
			$time_now = new DateTime(date("Y-m-d H:i:s"));
			
			$interval_btw_wrong_logins = intval(abs($time_last_login->getTimestamp() - $time_older_login->getTimestamp()) / 60);
			$interval_to_last_wrong_login = intval(abs($time_last_login->getTimestamp() - $time_now->getTimestamp()) / 60);

			if($interval_btw_wrong_logins<=$this->BF_in_timeframe && $interval_to_last_wrong_login<=$this->BF_blocktime)
			{
				//echo "BLOCKED: ".$this->BF_allowed_false_tries." wrong logins in ".$interval_btw_wrong_logins." Minutes, ".$interval_to_last_wrong_login." Minutes ago.<br>";
				echo "Your IP address is blocked. You entered wrong passwords too many times. Please try again later.";
				
				header("Location: back_block_ip.php");
				exit;
			}
		}
	}
	
	private function getIP()
	{
		$client_ip=NULL;
		if (!isset($_SERVER['HTTP_X_FORWARDED_FOR'])){$client_ip = $_SERVER['REMOTE_ADDR'];}
		else{$client_ip = $_SERVER['HTTP_X_FORWARDED_FOR'];}
		if($client_ip=="::1"||$client_ip=="127.0.0.1"){$client_ip="localhost";}
		return $client_ip;
	}
	
	private function sql($sql){ //自訂sql
		try{
			$stmt = $this->db->prepare($sql);
			$stmt->execute();
			$rows = $stmt->fetchAll(PDO::FETCH_ASSOC);

			return $rows;
		}catch(PDOException $e){
			echo $e->getMessage();
		}
	}
	
	private function create($table, $columnsArray) {
		try{
			$a = array();
			$c = "";
			$v = "";
			foreach ($columnsArray as $key => $value) {
				$c .= $key. ", ";
				$v .= ":".$key. ", ";
				$a[":".$key] = $value;
			}
			$c = rtrim($c,', ');
			$v = rtrim($v,', '); 
			$stmt = $this->db->prepare("INSERT INTO `$table`($c) VALUES($v)");
			$stmt->execute($a);
			$id = $this->db->lastInsertId(); //取得id
			$affected_rows = $stmt->rowCount(); //取得影響幾筆
			return $id;
			//return true;
		}catch(PDOException $e){
			echo $e->getMessage(); 
			return false;
		}
	}
}