<?php
	if(!isset($_SESSION)){
		session_start();
		define('SESSION_TIMEOUT', 1800); // in seconds
	}
	
	// header("Content-Type:text/html; charset=utf-8");
	include_once('config/function.php');
	include_once('config/dblink.php');
	include_once('config/config_user.php');
	include_once('config/class.crud.basic.php');
	include_once(PATH_FRONTEND_HELPER_SRC."/debug.php");		//debug
	
	$_act = (isset($_GET['act']))? $_GET['act']:'';
	$_var1 = (isset($_GET['var1']))? $_GET['var1']:'';
	$_var2 = (isset($_GET['var2']))? $_GET['var2']:'';
	$_step_ignore = false;
	$_BASE = BASE;

	//30分鐘後登出
	if(isset($_SESSION[SESSION_NAME.'_user_uniqid'])){
		if(date("Y-m-d H:i:s") > date("Y-m-d H:i:s", strtotime($_SESSION[SESSION_NAME.'_user_login_time']."+30 minute"))){
			unset($_SESSION[SESSION_NAME.'_user_uniqid']);
			unset($_SESSION[SESSION_NAME.'_user_login_time']);
		}
		else{
			//未逾時修改登入時間
			$_SESSION[SESSION_NAME.'_user_login_time'] = date('Y-m-d H:i:s');
		}
	}

	switch($_SERVER['SERVER_NAME']){
		default:
		case LANGUAGE_DEFAULT_DOMAIN:
			$_LANG = '';
			break;
		case LANGUAGE_EN_DOMAIN:
			$_LANG = '_en';
			break;
	}
	include("frontend/lang/lang{$_LANG}.php");//語系檔

	//現在幣別
	if(!isset($_SESSION[SESSION_NAME.'_currency'])) $_SESSION[SESSION_NAME.'_currency'] = 'TWD';

	switch ($_act) {
		//----- 單元頁面
		#關於我們
		case 'about_us':
			$page_name = $_GET_LANG['about_us'];
			$_file = 'contact';
			break;
		#關於我們
		case 'contact':
			$page_name = $_GET_LANG['contact_us'];
			$_file = 'contact';
			break;
		#標籤資訊
		case 'label':
			$page_name = $_GET_LANG['legal_requirements'];
			$_file = 'label';
			break;
		#精選產品
		case 'brand':
			$page_name = $_GET_LANG['featured_products'];
			$_file = 'brand';
			break;
		#最新消息
		case 'news':
			$page_name = $_GET_LANG['news'];
			$_file = 'news/news';
			break;
		#最新消息內頁
		case 'news-detail':
			$page_name = $_GET_LANG['news'];
			$_file = 'news/news_detail';
			break;
		#最新消息內頁
		case 'brand-news-detail':
			$page_name = $_GET_LANG['news'];
			$_file = 'news/news_detail';
			break;

	// ============================== product  				=================
		#產品列表
		case 'products':
			$page_name = $_GET_LANG['all'].$_GET_LANG[' '].$_GET_LANG['product'];
			$_file = 'product/product_list_test';
			break;

		#產品資料
		case 'product_data':
			$_file = 'product/product_data_func';
			$_step_ignore = true;
			break;

		#產品資訊
		case 'product':
			$page_name = $_GET_LANG['product'];
			//$_file = 'product/product';				//ecmach version
			$_file = 'product/product-detail';		//alex version
			break;
	// ============================== end of product 		=================

	// ============================== product test			=================
		//二手機械
		case 'products_test':
			$page_name = $_GET_LANG['all'].$_GET_LANG[' '].$_GET_LANG['product'];
			$_file = 'product/product_list_test';
			break;
			
		//產品資料
		case 'product_data_test':			
			// Any mobile device (phones or tablets).
			if ( $detect->isMobile() ) {
				$_file = 'product/product_data_func_test_m';
			} else {
				$_file = 'product/product_data_func_test';
			}
			break;
		
	// ============================== end of product test 	=================

		#會員中心
		case 'member':
			$page_name = $_GET_LANG['account_nav'];
			$_file = 'member';
			break;
		#已批出標籤
		case 'epd-nrmm-label':
			$page_name = $_GET_LANG['epd_nrmm_label'];
			$_file = 'download';
			break;
		#參數表及其他文件
		case 'download-files':
			$page_name = $_GET_LANG['documents_download'];
			$_file = 'down-other';
			break;
		#參數表及其他文件
		case 'v6021':
			$page_name = $_GET_LANG['documents_download'];
			$_file = 'alex';
			break;
		#過往工作
		case 'works':
			$page_name = $_GET_LANG['past_work'];
			if(empty($_var1)){
				$_file = 'works';
			}
			else{
				$_file = 'case-detail';
			}
			break;
		#隱私權政策
		case 'privacy':
			$page_name = $_GET_LANG['privacy'];
			$_file = 'privacy';
			$_step_ignore = true;
			break;
		#服務條款
		case 'service_rule':
			$page_name = $_GET_LANG['terms_of_service'];
			$_file = 'service_rule';
			$_step_ignore = true;
			break;
		#聯絡我們送出
		case 'verify':
			$page_name = $_GET_LANG['thank_you_for_your_message'];
			$_file = 'user_verify';
			$_step_ignore = false;
			break;
		#忘記密碼
		case 'forget':
			$page_name = $_GET_LANG['forgotten_password'];
			$_file = 'forget';
			$_step_ignore = true;
			break;
		#忘記密碼信件修改密碼
		case 'pwdchange':
			$page_name = $_GET_LANG['change_password'];
			$_file = 'pwdchange';
			$_step_ignore = true;
			break;
		#會員登入
		case 'login':
			$page_name = $_GET_LANG['account_login'];
			$_file = 'mem_login';
			$_step_ignore = true;
			break;
		#會員註冊
		case 'register':
			$page_name = $_GET_LANG['account_register'];
			$_file = 'mem_login';
			$_step_ignore = true;
			break;

		//----- 程式處理
		#聯絡我們處理
		case 'do_contact':
			$_file = 'fun_contact_control';
			$_step_ignore = true;
			break;
			
		#會員程式處理
		case 'domem':
			$_file = 'fun_member_control';
			$_step_ignore = true;
			break;
			
		#會員程式處理
		case 'domem_alex':
			$_file = 'func_member_ctrl';
			$_step_ignore = true;
			break;
				
		#登出處理
		case 'logout':
			$_step = $_SESSION[SESSION_NAME.'_nowstep'];
			ob_start();
			session_destroy();
			session_regenerate_id();
			session_start();
			header("location: ./{$_step}");
			exit;
			break;

		case 'SESSION_debug':
			exit;
			// session_destroy();
			// session_regenerate_id();
			// exit;
			
			header("Content-Type:text/json; charset=utf-8");
			print_r($_SESSION);
			// print_r(array_keys($_SESSION[DB_NAME.'_cart']));
			exit;
			break;

		default:
			if(!empty($_act)){
				header('HTTP/1.0 404 Not Found');
				$_file = 'sys_404';
			}
			else if($_SERVER['REQUEST_URI'] == "{$_BASE}index.php"){
				// 將 index.php 轉至首頁
				header("HTTP/1.1 301 Moved Permanently");
				header("Location: {$_BASE}");
				exit;
			}
			else{
				$_file = 'index';
			}
			break;
	}

	$_meta_title = (empty($page_name))? '':$page_name.' :: ';
	$_meta_title .= (empty($_meta_title))? SITETITLE: SITENAME;

	if(!empty($_file)){
		$_nowstep = '';
		if(!empty($_var1)){
			$_nowstep .= "/{$_var1}";
		}
		else if(!empty($_var2)){
			$_nowstep .= "/{$_var2}";
		}
		if(!$_step_ignore){
			$_SESSION[SESSION_NAME.'_nowstep'] = $_nowstep;
		}
		$_currency = $_SESSION[SESSION_NAME.'_currency'];
		$_customize_title = '';
		$crud = new crud_basic($DB_con);

		include_once("frontend/src/{$_file}.php");
	}
?>