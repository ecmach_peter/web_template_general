<?php
	function db_news_category(){
		global $crud;
		$ary_news_cate = $crud->select(
									'news_cate',
									array('cate_online' => 1),
									array('cate_sort' => 'ASC'),
									array(
										'cate_id',
										'cate_title' => "cate_title{$_LANG}"
									)
								);
		return $ary_news_cate;
	}

	function db_news_detail(){
		global $crud;						
		$arr_product = $crud->sql("
									SELECT *, `news_title{$_LANG}` AS `news_title_display`, `news_content{$_LANG}` AS `news_content_display`
									FROM `news`
								");
		return $arr_product;
	}

	function db_news_detail_usr($category_id, $news_num){
		global $crud;		
		$now_date_time = date('Y-m-d H:i:s');			
		$query_str = "
						SELECT *, `news_title{$_LANG}` AS `news_title_display`, `news_content{$_LANG}` AS `news_content_display`
						FROM `news`
						WHERE `news_online` = 1
							AND `news_date` <= '{$now_date_time}' 
							AND (`news_date_end` >= '{$now_date_time}' || `news_date_end` = '0000-00-00 00:00:00') 
					";
		if ($category_id != '0') {
			$query_str .= " AND `news_cate` = {$category_id} ";		
		}
		$query_str .= " ORDER BY `news_date` DESC, `news_id` DESC LIMIT " .$news_num;

		$arr_product = $crud->sql($query_str);

		return $arr_product;
	}

