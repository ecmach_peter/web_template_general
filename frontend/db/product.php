<?php
	//slider
	function db_feature_product() {
		global $crud;
		$sql_query_str = "SELECT `cate_id`, `cate_title`, `cate_title_en`, `product_code`, `product_create_year`, `product_photo`, 
								`product_name{$_LANG}` AS `product_name`
							FROM `product`
							INNER JOIN `model` ON `product_model` = `model_id`
							INNER JOIN `model_cate` ON `model_cate` = `cate_id`
							WHERE `product_featured` = 1";
		$ary_product = $crud->sql($sql_query_str);
		return $ary_product;
	}

	function db_model_category() {
		global $crud;
		$sql_query_str = "SELECT *, (
									SELECT COUNT(*)
									FROM `model_cate` AS `child`
									WHERE `child`.`cate_par_id` = `model_cate`.`cate_id`
								) AS `count_child`
							FROM `model_cate`
							WHERE `cate_par_id` = 0 AND `cate_online` = 1
							ORDER BY `cate_sort` ASC";
		$ary_model_cate = $crud->sql($sql_query_str);
		return $ary_model_cate;
	}

	function db_model_category_child($parent_id) {
		global $crud;
		$sql_query_str = "SELECT *
						FROM `model_cate`
						WHERE `cate_par_id` = {$parent_id} AND `cate_online` = 1
						ORDER BY `cate_sort` ASC";
		$ary_model_cate_child = $crud->sql($sql_query_str);
		return $ary_model_cate_child;
	}

	function db_product() {
		global $crud;
		$sql_query_str = "SELECT *
							FROM `product`
							INNER JOIN `model` ON `product_model` = `model_id`
							WHERE `product_online` = 1";
		$ary_product = $crud->sql($sql_query_str);

		return $ary_product;
	}

	function db_product_range() {
		global $crud;
		$sql_query_str = "SELECT 
								Max(product_price) AS 'max_price', 
								Min(product_price) AS 'min_price', 
								Max(model_weight) AS 'max_weight', 
								Min(model_weight) AS 'min_weight', 
								Max(product_year) AS 'max_year', 
								Min(product_year) AS 'min_year', 
								Max(product_hours) AS 'max_hours', 
								Min(product_hours) AS 'min_hours'
							FROM `product`
							INNER JOIN `model` ON `product_model` = `model_id`
							WHERE `product_online` = 1";
		$ary_product = $crud->sql($sql_query_str);

		return $ary_product;
	}

	function db_product_detail($product_code) {
		global $crud;
		$sql_query_str = "SELECT 
							`product_id`, `product_country`, `product_start_price`, `product_price`, `product_create_year`, `product_code`, `product_photo`, `product_year`, `product_hours`, 
							`product_remark{$_LANG}` AS `product_remark`, 
							`product_name{$_LANG}` AS `product_name`, 
							`product_report`, `model_number`, `model_cate`, `model_weight`, `model_datasheet`, `brand_id`, `brand_title`, `brand_title_en`, `cate_par_id`, 
							`cate_title{$_LANG}` AS `cate_title`
							FROM `product`
							INNER JOIN `model` ON `product_model` = `model_id`
							INNER JOIN `model_cate` ON `model_cate` = `cate_id`
							INNER JOIN `brand` ON `model_brand` = `brand_id`
							WHERE `product_code` = '{$product_code}' AND `product_online` = 1"; 
		$ary_product = $crud->sql($sql_query_str)[0];

		return $ary_product;
	}

	function db_product_similar($product_src, $qty) {
		global $crud;
		$join = 'join';
		$arr_not_in = array($product_src['product_id']);
		$sql_query_str = "SELECT `product_id`, `product_photo`, `product_code`, `product_create_year`,
								`product_name{$_LANG}` AS `product_name`, `product_price`
							FROM `product`
							INNER JOIN `model` ON `product_model` = `model_id`
							WHERE `product_online` = 1 AND `product_paid` = 0 AND `model_cate` = {$product_src['model_cate']} AND `product_id` NOT IN ({$join($arr_not_in, ',')})
							ORDER BY RAND()
							LIMIT {$qty}";
		$ary_product = $crud->sql($sql_query_str);

		return $ary_product;
	}
	
	function db_product_rand($product_src, $qty) {
		global $crud;
		$join = 'join';
		$arr_not_in = array($product_src['product_id']);
		$sql_query_str = "SELECT `product_id`, `product_photo`, `product_code`, `product_create_year`,
								`product_name{$_LANG}` AS `product_name`, `product_price`
							FROM `product`
							INNER JOIN `model` ON `product_model` = `model_id`
							WHERE `product_online` = 1 AND `product_paid` = 0 AND `product_id` NOT IN ({$join($arr_not_in, ',')})
							ORDER BY RAND()
							LIMIT {$qty}";
		$ary_product = $crud->sql($sql_query_str);

		return $ary_product;
	}
