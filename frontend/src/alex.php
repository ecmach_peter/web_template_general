<?php
	//產品分類
	$arr_cate = $crud->sql("SELECT `c1`.`cate_id`, `c1`.`cate_title{$_LANG}` AS `cate_title`, `c2`.`cate_id` AS `par_id`, `c2`.`cate_title{$_LANG}` AS `par_title`
							FROM `model_cate` AS `c1`
							INNER JOIN `model_cate` AS `c2` ON `c1`.`cate_par_id` = `c2`.`cate_id`
							WHERE `c1`.`cate_online` = 1 AND `c2`.`cate_online` = 1
							ORDER BY `c2`.`cate_sort`, `c1`.`cate_sort`");
	$str_cate = $_tmp_id = '';
	if(count($arr_cate) > 0){
		foreach($arr_cate as $key => $value){
			if($value['par_id'] <> $_tmp_id){
				if(!empty($_tmp_id)) $str_cate .= '</optgroup>';
				$str_cate .= <<<HTML
				<optgroup label="{$value['par_title']}">
HTML;
				$_tmp_id = $value['par_id'];
			}
			$str_cate .= <<<HTML
			<option value="{$value['cate_id']}">{$value['cate_title']}</option>
HTML;
		}
		$str_cate .= '</optgroup>';
	}

	$arr_document_title = array('table&other', '參數表及其他文件');
	if($_LANG == '') $arr_document_title = array_reverse($arr_document_title);
?>
<!doctype html>
<html lang="zh-Hant-TW">

<head>
	<?php include_once("include/head.php");?>
	<!-- 專案的CSS -->
	<link rel="stylesheet" href="css/import/page.css">
</head>

<body>
	<?php include_once("include/navbar.php");?>
	<section class="content">
		<div class="container">
			<div class="row">
				<div class="col-xs-12">
					<ol class="breadcrumb">
						<li><a href="./"><i class="fa fa-home" aria-hidden="true"></i> <?=$_GET_LANG['home'];?></a></li>
						<li><a href="javascript:;"><?=$_GET_LANG['member_download'];?></a></li>
						<li class="active"><?=$_GET_LANG['documents_download'];?></li>
					</ol>
				</div>
				<div class="col-xs-12">
					<div class="titlebar line_bottom mb30 clearfix">
						<h2 class="line_title"><?=$arr_document_title[0];?> &#8260; <small><?=$arr_document_title[1];?></small></h2>
					</div>
					<div class="row">
						<div class="col-xs-12 col-lg-9 col-lg-offset-3">
							<div class="control_btn right_bar form-inline">
								<div class="form-group">
									<select id="brand_id" class="selectpicker" data-live-search="true">
									</select>
								</div>
								<div class="form-group">
									<div class="input-group">
										<span class="input-group-btn">
											<button class="btn btn-default btn_search" type="button"><i class="fa fa-search" aria-hidden="true"></i></button>
										</span>
										<input type="text" class="form-control" id="file_keyword" placeholder="Search">
										<span class="input-group-btn">
											<button class="btn btn-default btn_search_remove" type="button"><i class="fa fa-times" aria-hidden="true"></i></button>
										</span>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="other_table">
						<table class="table table-striped">
							<thead>
								<tr>
									<th data-breakpoints="xs"><?=$_GET_LANG['product_subcategories'];?></th>
									<th><?=$_GET_LANG['mechanical_brand'];?></th>
									<th><?=$_GET_LANG['mechanical_model'];?></th>
									<th data-breakpoints="xs">HKD</th>
								</tr>
							</thead>
							<tbody id="download-files-data">
								<tr data-expanded="true">
									<td colspan="6"><?=$_GET_LANG['no_matching_files'];?></td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</section>
	<?php include_once("include/footer.php");?>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.2/js/bootstrap-select.min.js"></script>
	<script src="js/footable.min.js"></script>
	<script>
		jQuery(function($){
			$('.sort-data .btn-filter').on('click', function(){
				var $this = $(this);
				$('#span_sort').text($this.text()).data('id', $this.data('id'));
				update_label();
			});
			$('.btn_search').on('click', update_label);
			$('.btn_search_remove').on('click', function(){
				$('#file_keyword').val('');
				$('.btn_search').click();
			});

			$('#file_keyword').keypress(function (e) {
				var key = e.which;
				if(key == 13){
					$(this).parents('.input-group').find('.btn_search').click();
					return false;  
				}
			}); 
			$('#cate_id, #brand_id').on('change', update_label);
			update_label();
			function update_label(){
				var v_data = {
						type: 'download_files',
						sort: $('#span_sort').data('id'),
						keyword: $('#file_keyword').val(),
						cate_id: $('#cate_id').val(),
						brand_id: $('#brand_id').val()
					};
				doajax({
					url: './domem_alex',
					data: jQuery.param(v_data),
					type: 'json',
					callback: function(msg){
						$('#download-files-data').html(msg.data);
						$('#brand_id').html(msg.brand).selectpicker('refresh');
						$('.member_table .table').footable();
					}
				});
			}
		});
	</script>
</body>

</html>
