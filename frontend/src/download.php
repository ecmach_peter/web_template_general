<?php
	if(!isset($_SESSION[SESSION_NAME.'_user_uniqid'])){
		header("location: ./login");
		exit;
	}

	$arr_label_title = array('EPD NRMM LABEL', '已批出標籤');
	if($_LANG == '') $arr_label_title = array_reverse($arr_label_title);
?>
<!doctype html>
<html lang="zh-Hant-TW">

<head>
	<?php include_once("include/head.php");?>
	<!-- 專案的CSS -->
	<link rel="stylesheet" href="css/import/page.css">
</head>

<body>
	<?php include_once("include/navbar.php");?>
	<section class="content">
		<div class="container">
			<div class="row">
				<div class="col-xs-12">
					<ol class="breadcrumb">
						<li><a href="./"><i class="fa fa-home" aria-hidden="true"></i> <?=$_GET_LANG['home'];?></a></li>
						<li><a href="javascript:;"><?=$_GET_LANG['member_download'];?></a></li>
						<li class="active"><?=$_GET_LANG['epd_nrmm_label'];?></li>
					</ol>
				</div>
				<div class="col-xs-12">
					<div class="titlebar line_bottom mb30 clearfix">
						<h2 class="line_title"><?=$arr_label_title[0];?> &#8260; <small><?=$arr_label_title[1];?></small></h2>
					</div>
					<div class="row">
						<div class="col-md-6 col-md-offset-6">
							<div class="control_btn right_bar form-inline">
								<div class="form-group">
									<div class="input-group">
										<span class="input-group-btn">
											<button class="btn btn-default btn_search" type="button"><i class="fa fa-search" aria-hidden="true"></i></button>
										</span>
										<input type="text" class="form-control" id="label_keyword" placeholder="Search">
										<span class="input-group-btn">
											<button class="btn btn-default btn_search_remove" type="button"><i class="fa fa-times" aria-hidden="true"></i></button>
										</span>
									</div>
								</div>
								<div class="form-group">
									<div class="btn-group">
										<button class="btn btn-default dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
											<span id="span_sort"><?=$_GET_LANG['label_date_desc'];?></span> <span class="caret"></span>
										</button>
										<ul class="dropdown-menu filter_ul sort-data" data-mode="sort">
											<li><a href="javascript:;" class="btn-filter" data-id=""><?=$_GET_LANG['label_date_desc'];?></a></li>
											<li><a href="javascript:;" class="btn-filter" data-id="new-asc"><?=$_GET_LANG['label_date_asc'];?></a></li>
											<li><a href="javascript:;" class="btn-filter" data-id="model"><?=$_GET_LANG['label_model_desc'];?></a></li>
											<li><a href="javascript:;" class="btn-filter" data-id="model-asc"><?=$_GET_LANG['label_model_asc'];?></a></li>
										</ul>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="member_table">
						<table class="table table-striped">
							<thead>
								<tr>
									<th><?=$_GET_LANG['application_type'];?></th>
									<th><?=$_GET_LANG['mechanical_brand'];?></th>
									<th><?=$_GET_LANG['mechanical_model'];?></th>
									<th data-breakpoints="xs sm"><?=$_GET_LANG['mechanical_serial_number'];?></th>
									<th data-breakpoints="xs sm md"><?=$_GET_LANG['epd_application_number'];?></th>
									<th data-breakpoints="xs sm"><?=$_GET_LANG['batch_number'];?></th>
									<th data-breakpoints="xs sm md"><?=$_GET_LANG['invoice'];?></th>
									<th data-breakpoints="xs"><?=$_GET_LANG['invoice_status'];?></th>
								</tr>
							</thead>
							<tbody id="label_data">
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</section>
	<?php include_once("include/footer.php");?>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.2/js/bootstrap-select.min.js"></script>
	<script src="js/footable.min.js"></script>
	<script>
		jQuery(function($){
			$('.sort-data .btn-filter').on('click', function(){
				var $this = $(this);
				$('#span_sort').text($this.text()).data('id', $this.data('id'));
				update_label();
			});
			$('.btn_search').on('click', update_label);
			$('.btn_search_remove').on('click', function(){
				$('#label_keyword').val('');
				$('.btn_search').click();
			});

			$('#label_keyword').keypress(function (e) {
				var key = e.which;
				if(key == 13){
					$(this).parents('.input-group').find('.btn_search').click();
					return false;  
				}
			}); 
			update_label();
			function update_label(){
				var v_data = {
								type: 'label',
								sort: $('#span_sort').data('id'),
								keyword: $('#label_keyword').val()
							};
				doajax({
					url: './domem',
					data: jQuery.param(v_data),
					type: 'json',
					callback: function(msg){
						$('#label_data').html(msg.data);
						$('.member_table .table').footable();
					}
				});
			}
		});
	</script>
</body>

</html>
