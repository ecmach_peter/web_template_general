<?php
	//取得標籤資訊
	$arr_label = $crud->getid(
							'works_type', 
							array(
								'works_type_url' => $_var1, 
								'works_type_online' => 1
							), 
							array(), 
							array(
								'works_type_id',
								'works_type_title' => "works_type_title{$_LANG}"
							)
						);
	if($arr_label === false){
		include("./include.page/sys_404.php");
		exit;
	}

	//取得slider
	$arr_label_album = $crud->select(
									'works_type_album', 
									array('host_id' => $arr_label['works_type_id']), 
									array('album_sort' => 'ASC'), 
									array('album_photo')
								);
	$str_label_album = $str_label_item = $tmp_label_item = '';
	if(count($arr_label_album) > 0){
		foreach($arr_label_album as $key => $value){
			$active = (empty($str_label_album))? 'active':'';
			$str_label_album .= <<<HTML
					<div class="item {$active}" style="background-image: url('./uploadimages/works_type/{$arr_label['works_type_id']}/{$value['album_photo']}');"></div>
HTML;
			$tmp_label_item .= <<<HTML
					<div data-target="#alexslider" data-slide-to="{$key}" class="thumb" style="background-image: url('./uploadimages/works_type/{$arr_label['works_type_id']}/{$value['album_photo']}');"></div>
HTML;
			//一排4個
			if(($key + 1) % 4 == 0){
				$active_item = (empty($str_label_item))? 'active':'';
				$str_label_item .= <<<HTML
					<div class="item {$active_item} {$key}">
						{$tmp_label_item}
					</div>
HTML;
				$tmp_label_item = '';
			}
		}

		//未滿4個的一組
		if($key % 4 != 0){
			$active_item = ($key < 4)? 'active':'';
			$str_label_item .= <<<HTML
				<div class="item {$active_item}">
					{$tmp_label_item}
				</div>
HTML;
		}
	}

	//取得此標籤的成功案例
	$arr_case = $crud->select(
							'case', 
							array(
								'works_type_id' => $arr_label['works_type_id'],
								'case_online' => 1
							),
							array('case_sort' => 'ASC'),
							array(
								'case_id',
								'case_left_menu',
								'case_photo',
								'case_title' => "case_title{$_LANG}",
								'case_content' => "case_content{$_LANG}",
							)
						);
	$str_case = $str_left_menu = '';
	if(count($arr_case) > 0){
		//取出相簿資料
		$sql_all_id = join(',', array_column($arr_case, 'case_id'));
		$arr_album = $crud->sql("SELECT `host_id`, `album_photo`
								FROM `case_album`
								WHERE `host_id` IN ({$sql_all_id})
								ORDER BY `host_id`, `album_sort`");
		$arr_album_data = array();
		if(count($arr_album) > 0){
			foreach($arr_album as $key => $value){
				$thumb = str_replace('.', '_a.', $value['album_photo']);
				$arr_album_data[$value['host_id']] .= <<<HTML
					<a href="./uploadimages/case/{$value['host_id']}/{$value['album_photo']}" data-type="image" data-fancybox="group0{$value['host_id']}" data-thumb="./uploadimages/case/{$value['host_id']}/{$thumb}"></a>
HTML;
			}
		}
		foreach($arr_case as $key => $value){
			$case_content = htmlspecialchars_decode($value['case_content']);

			$str_case .= <<<HTML
						<li id="case0{$key}">
							<div class="thumbnail np_box clearfix">
								<div class="album">
									<a href="./uploadimages/case/{$value['case_id']}/{$value['case_photo']}" data-type="image" data-fancybox="group0{$value['case_id']}">
										<div class="np_box_img" style="background-image: url('./uploadimages/case/{$value['case_id']}/{$value['case_photo']}');"></div>
									</a>
									<div style="display: none;">
										{$arr_album_data[$value['case_id']]}
									</div>
								</div>
								<div class="album_infobox box-close">
									<ul class="album_info">
										<li><span>{$_GET_LANG['case_name']}：</span>{$value['case_title']}</li>
									</ul>
									<div class="textedit case-box box-close">
										{$case_content}
									</div>
									<p class="text-center visible-lg">
										<button class="btn btn-green btn-xs btn_op"></button>
									</p>
								</div>
							</div>
						</li>
HTML;
			if($value['case_left_menu'] == 1){
				$str_left_menu .= <<<HTML
						<li data-key="{$key}"><a href="#case0{$key}">- {$value['case_title']}</a></li>
HTML;
			}
		}
	}
	
	$_customize_title = $arr_label['works_type_title'].' :: ';
?>
<!doctype html>
<html lang="zh-Hant-TW">

<head>
	<?php include_once("include/head.php");?>

	<!-- 專案的CSS -->
	<link rel="stylesheet" href="css/import/page.css">
</head>

<body>
	<?php include_once("include/navbar.php");?>

	<div class="content">
		<div class="container">
			<div class="row">
				<div class="col-xs-12">
					<ol class="breadcrumb">
						<li><a href="./"><i class="fa fa-home" aria-hidden="true"></i> <?=$_GET_LANG['home'];?></a></li>
						<li><a href="javascript:;"><?=$_GET_LANG['past_work'];?></a></li>
						<li><a href="javascript:;"><?=$arr_label['works_type_title'];?></a></li>
						<li class="active"><?=$_GET_LANG['success_case'];?></li>
					</ol>
				</div>
				<div class="col-xs-12 col-md-2">
					<div class="sidebar visible-md visible-lg">
						<ul id="mainNav" class="sidebar_list">
							<?=$str_left_menu;?>
						</ul>
					</div>
					<div class="visible-xs visible-sm">
						<select class="selectpicker">
							<option value=""><?=$_GET_LANG['featured_case'];?></option>
						</select>
					</div>
				</div>
				<div class="col-xs-12 col-md-10">
					<div class="titlebar line_bottom mb30 clearfix">
						<h2 class="line_title"><?=$_GET_LANG['success_case'];?> - <?=$arr_label['works_type_title'];?> &#8260; <small>success case</small></h2>
					</div>
					<div class="bannerbar hidden-sm hidden-xs">
						<div id="alexslider" class="carousel slide alex_slider" data-ride="carousel">
							<div class="carousel-inner">
								<?=$str_label_album;?>
							</div>
						</div>
						<div class="clearfix">
							<div id="thumbcarousel" class="carousel slide alex_thumb" data-interval="false">
								<div class="carousel-inner">
									<?=$str_label_item;?>
								</div>
								<!-- /carousel-inner -->
								<a class="left carousel-control" href="#thumbcarousel" role="button" data-slide="prev">
									<span class="glyphicon glyphicon-chevron-left"></span>
								</a>
								<a class="right carousel-control" href="#thumbcarousel" role="button" data-slide="next">
									<span class="glyphicon glyphicon-chevron-right"></span>
								</a>
							</div>
							<!-- /thumbcarousel -->
						</div>
					</div>
					<ul class="list-unstyled boxstyle_list">
						<?=$str_case;?>
					</ul>
				</div>
			</div>
		</div>
	</div>
	
	<?php include_once("include/footer.php");?>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.2/js/bootstrap-select.min.js"></script>
	<script>
		$(document).ready( function() {
			$('#alexslider').carousel({
				interval:   4000
			});
			
			var clickEvent = false;
			$('#alexslider').on('click', '.nav a', function() {
					clickEvent = true;
					$('.nav li').removeClass('active');
					$(this).parent().addClass('active');        
			}).on('slid.bs.carousel', function(e) {
				if(!clickEvent) {
					var count = $('.nav').children().length -1;
					var current = $('.nav li.active');
					current.removeClass('active').next().addClass('active');
					var id = parseInt(current.data('slide-to'));
					if(count == id) {
						$('.nav li').first().addClass('active');    
					}
				}
				clickEvent = false;
			});

			$('.sidebar_list li').each(function(){
				var $this = $(this);
				$('.selectpicker').append($('<option>').val($this.data('key')).text($this.text()));
			});
			$('.selectpicker').on('change', function(){
				var _val = $(this).val();
				if(_val != ''){
					$('html, body').animate({
						scrollTop: $('#case0' + _val).offset().top - 20
					});
				}
			});
		});

		$('.btn_op').on('click', function(){
			var that = $(this).parents('.album_infobox').toggleClass('box-close box-open');
			var case_box = that.find('.case-box');
			case_box.animate({height: (case_box.height() > 200)? 162:case_box[0].scrollHeight}, 10);
		});
	</script>
</body>

</html>
