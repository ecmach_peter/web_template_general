<!doctype html>
<html lang="zh-Hant-TW">

<head>
    <?php include_once("include/head.php");?>

    <!-- 專案的CSS -->
    <link rel="stylesheet" href="css/import/page.css">
</head>

<body>
    <?php include_once("include/navbar.php");?>
		
    <div class="content">
        <div class="container">
            <div class="row">
            	<div class="col-xs-12">
                    <div class="titlebar line_bottom clearfix">
                        <h2 class="line_title">搜尋結果 &#8260; <small>Search results</small></h2>
                    </div>
                    <ul class="list-unstyled search_list">
                        <li class="clearfix">
                            <div class="search_box">
                                <p class="searchimg" style="background-image: url('images/products01.jpg');"></p>
                                <h3><a href="#" class="blueText">news標題news標題news標題news標題</a></h3>
                                <p class="search_content">內文摘要內文摘要內文摘要內文摘要內文摘要,內文摘要內文摘要內文摘要內文摘要內文摘要內文摘要,內文摘要內文摘要內文摘要內文摘要內文摘要內文摘要內文摘要內文摘要,內文摘要內文摘要內文摘要,內文摘要<span class="searchmark">KEYWORD</span>內文摘要內文摘要,內文摘要內文摘要內文摘要...</p>
                                <!-- 中文字數含空格: 127字 -->
                            </div>
                        </li>
                        <li>
                            <div class="search_box">
                                <p class="searchimg" style="background-image: url('images/products01.jpg');"></p>
                                <h3><a href="#" class="blueText">news標題news標題news標題news標題</a></h3>
                                <p class="search_content">內文摘要內文摘要內文摘要內文摘要內文摘要,內文摘要內文摘要內文摘要內文摘要內文摘要內文摘要,內文摘要內文摘要內文摘要內文摘要內文摘要內文摘要內文摘要內文摘要,內文摘要內文摘要內文摘要,內文摘要<span class="searchmark">KEYWORD</span>內文摘要內文摘要,內文摘要內文摘要內文摘要...</p>
                                <!-- 中文字數含空格: 127字 -->
                            </div>
                        </li>
                        <li>
                            <div class="search_box">
                                <p class="searchimg" style="background-image: url('images/products01.jpg');"></p>
                                <h3><a href="#" class="blueText">news標題news標題news標題news標題</a></h3>
                                <p class="search_content">內文摘要內文摘要內文摘要內文摘要內文摘要,內文摘要內文摘要內文摘要內文摘要內文摘要內文摘要,內文摘要內文摘要內文摘要內文摘要內文摘要內文摘要內文摘要內文摘要,內文摘要內文摘要內文摘要,內文摘要<span class="searchmark">KEYWORD</span>內文摘要內文摘要,內文摘要內文摘要內文摘要...</p>
                                <!-- 中文字數含空格: 127字 -->
                            </div>
                        </li>
                    </ul>
                    <!-- 頁碼 -->
                    <ul class="pagebox">
                        <li><a href="javascript:;"><i class="fa fa-angle-double-left" aria-hidden="true"></i></a></li>
                        <li class="on"><a href="javascript:;">1</a></li>
                        <li><a href="javascript:;">2</a></li>
                        <li><a href="javascript:;">3</a></li>
                        <li><a href="javascript:;">4</a></li>
                        <li><a href="javascript:;">5</a></li>
                        <li><a href="javascript:;"><i class="fa fa-angle-double-right" aria-hidden="true"></i></a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    
    <?php include_once("include/footer.php");?>
</body>

</html>
