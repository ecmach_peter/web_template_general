<?php
	if(!isset($_SERVER['HTTP_X_REQUESTED_WITH']) || !strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' || empty($_act)){
		header("X-Robots-Tag: noindex, nofollow", true);
		header('HTTP/1.0 403 Forbidden');
		exit;
	}
	try{
		// print_r($_POST);
		//引入驗證CLASS
		include './include/class.validation.php';
		$check_input = array(
							array(
								'name' => 'name',
								'column' => 'contact_name',
								'verify' => array(
												'required' => true,
												'max_length' => array(30, sprintf($_GET_LANG['input_too_long'], ($_LANG == '')? '姓名':'Name', 30)),
											)
							),
							array(
								'name' => 'phone',
								'column' => 'contact_phone',
								'verify' => array(
												'required' => true,
												'max_length' => array(20, sprintf($_GET_LANG['input_too_long'], ($_LANG == '')? '電話':'Phone number', 20)),
											)
							),
							array(
								'name' => 'email',
								'column' => 'contact_email',
								'verify' => array(
												'required' => true,
												'filter_var' => array(FILTER_VALIDATE_EMAIL, $_GET_LANG['email_invalid'].'， e.g: demo@yourmail.com'),
											)
							),
							array(
								'name' => 'company',
								'column' => 'contact_company',
								'verify' => array(
												'required' => true,
												'max_length' => array(30, sprintf($_GET_LANG['input_too_long'], ($_LANG == '')? '公司名稱':'Company name', 30)),
											)
							),
							array(
								'name' => 'country',
								'column' => 'contact_country',
								'verify' => array(
												'required' => '請選擇國家',
												'max_length' => array(10, $_GET_LANG['country_invalid']),
											)
							),
							array(
								'name' => 'message',
								'column' => 'contact_message',
								'verify' => array(
												'required' => true,
												'max_length' => array(1500, sprintf($_GET_LANG['input_too_long'], ($_LANG == '')? '諮詢內容':'Content', 1500)),
											)
							),
						);

		$_contact = new Validation();

		//驗證表單
		foreach($check_input as $arr_form_info){
			foreach($arr_form_info['verify'] as $verify_type => $verify_info){
				$_verify_type = "check_{$verify_type}_input";
				$_contact->$_verify_type($arr_form_info['name'], $verify_info);
			}

			if(isset($arr_form_info['column'])) $arr_data[$arr_form_info['column']] = get_post_var($arr_form_info['name']);
		}

		//確認國家代碼是否合法
		$chk_country = $crud->getid('country_code', array('country_letter2_code' => get_post_var('country')), array(), array('country_id'));
		if($chk_country === false){
			throw new Exception($_GET_LANG['country_invalid']);
		}

		//暫停送出指令
		$crud->beginTransaction();

		//記錄到資料庫
		$arr_data['contact_time'] = date('Y-m-d H:i:s');
		$result = $crud->create('contact', $arr_data);
		if($result === false){
			throw new Exception($_GET_LANG['server_error']);
		}

		$arr_mail_data = array(
							'send_type' => 'contact',
							'send_data' => serialize($arr_data),
						);
		$result = $crud->create('email_send_temp', $arr_mail_data);
		if($result === false){
			throw new Exception($_GET_LANG['server_error']);
		}

		//送出指令
		$crud->commit();

		//執行非同步寄信
		async_connection($_SERVER['HTTP_HOST'], BASE.'cron_sendmail.php?auth='.MAIL_CRON_AUTH);

		$_contact->success();
	}
	catch(Exception $e){
		echo json_encode(
				array(
					'sts' => false,
					'msg' => $e->getMessage(),
					'item' => $_contact->error_item,
				)
			);
	}
?>