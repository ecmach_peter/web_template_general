<?php
	header('HTTP/1.0 404 Not Found');
	header("X-Robots-Tag: noindex, nofollow", true);
?>
<!doctype html>
<html lang="zh-Hant-TW">

<head>
	<?php include_once("include/head.php");?>

	<!-- 專案的CSS -->
	<link href="https://fonts.googleapis.com/css?family=Archivo+Black" rel="stylesheet">
	<link rel="stylesheet" href="css/import/page.css">
</head>

<body>
	<?php include_once("include/navbar.php");?>
		
	<div class="content">
		<div class="container">
			<div class="row">
				<div class="col-xs-12 col-md-10 col-md-offset-1 text-center">
					<h2 class="bigfour_font">404</h2>
					<div class="bigfour_text">
						<p><?=$_GET_LANG['page_not_find'];?></p>
					</div>
					<p class="text-center">
						<a href="./" class="btn btn-green"><i class="fa fa-reply" aria-hidden="true"></i> <?=$_GET_LANG['home'];?></a>
					</p>
				</div>
			</div>
		</div>
	</div>
	
	<?php include_once("include/footer.php");?>
</body>

</html>
