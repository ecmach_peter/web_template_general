	<base href="<?=BASE;?>">
	<meta charset="utf-8">
	<link rev="made" href=" ">
	<link rev="made" href=" ">
	<link rel="shortcut icon" href="frontend/img/sys/favicon.png?153">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta http-equiv="content-type" content="text/html; charset=utf-8"/>
	<meta http-equiv="content-language" content="zh-Hant-TW">
	<meta http-equiv="expires" content="-1">
	<meta http-equiv="imagetoolbar" content="false">
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="window-target" content="_top">
	<meta name="author" content=" ">
	<meta name="company" content="<?=USER_NAME;?>">
	<meta name="copyright" content="<?=USER_COPYRIGHT;?>">
	<meta name="creation-date" content="01-jan-2001 20:40:01 GMT+8">
	<meta name="distribution" content="global">
	<meta name="revisit-after" content="1 days">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="<?=(empty($_customize_description))? DESCRIPTION:$_customize_description;?>">
	<meta name="keywords" content="<?=KEYWORDS;?>">
	<link rel="alternate" hreflang="zh" href="<?=PROTOCOL.'://'.LANGUAGE_DEFAULT_DOMAIN.$_nowstep_url;?>" />
	<link rel="alternate" hreflang="en" href="<?=PROTOCOL.'://'.LANGUAGE_EN_DOMAIN.$_nowstep_url;?>" />

	<?php /*facebook上分享的設定*/ ?>
	<meta property="og:url" content="<?=PROTOCOL."://{$_SERVER['HTTP_HOST']}".$_nowstep_url;?>">
	<meta property="og:type" content="website">
	<meta property="og:title" content="<?=$_customize_title.$_meta_title;?>">
	<meta property="og:site_name" content="<?=SITENAME;?>">
	<meta property="og:description" content="<?=(empty($_customize_description))? DESCRIPTION:$_customize_description;?>">
	<?=(empty($_customize_og_image))? '<meta property="og:image" content="'.OG_IMAGE.'">':$_customize_og_image;?>
	<!--[if lt IE 9]>
		<script type="text/javascript" src="js/html5shiv.js"></script>
		<script type="text/javascript" src="js/respond.js"></script>
	<![endif]-->
	<title><?=$_customize_title.$_meta_title;?></title>
	<!-- 共用的CSS -->
	<link rel="stylesheet" href="frontend/css/bootstrap.min.css?<?=RECACHE_NUM;?>">
	<!-- 元件的CSS -->
	<link rel="stylesheet" href="frontend/css/sweetalert.css?<?=RECACHE_NUM;?>">
	<link rel="stylesheet" href="frontend/css/main.css?<?=RECACHE_NUM;?>">
	<link rel="stylesheet" href="frontend/css/style.css?<?=RECACHE_NUM;?>">

