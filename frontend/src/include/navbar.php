<?php
	//代理產品
	$arr_brand = $crud->select(
							'brand', 
							array(
								'brand_online' => 1,
								'brand_listed' => 1, 
							), 
							array('brand_sort' => 'ASC'), 
							array(
								'brand_id',
								'brand_url',
								'brand_logo',
								'brand_title' => "brand_title{$_LANG}"
							)
						);
	$str_brand = '';
	if(count($arr_brand) > 0){
		foreach($arr_brand as $value){
			$str_brand .= <<<HTML
					<li>
						<span class="iconbg" style="background-image: url(./uploadimages/brand/{$value['brand_id']}/{$value['brand_logo']});"></span>
						<a href="brand/{$value['brand_url']}">{$value['brand_title']}</a>
					</li>
HTML;
		}
	}

	$str_btn_account = '';
	if(CONF_HAS_MEMBER) {
		if(isset($_SESSION[SESSION_NAME.'_user_uniqid'])){
			$str_btn_account = <<<HTML
					<li><a href="logout" class="btns black btn_logout" data-title="{$_GET_LANG['confirm_logout']}"><i class="fa fa-sign-out" aria-hidden="true"></i> Logout</a></li>
					<li><a href="member" class="btns black"><i class="fa fa-user" aria-hidden="true"></i> Account</a></li>
HTML;
		}
		else{
			$str_btn_account = <<<HTML
					<li><a href="login"><i class="fa fa-sign-in" aria-hidden="true"></i> Login</a></li>
HTML;
		}
	}
?>
<header>
	<div class="header_style03">
		<nav class="navbar">
			<div class="navbar_topnav">
				<div class="container">
					<div class="row">
						<div class="col-xs-12 clearfix">

<!-- Mobile UI -->
							<ul class="media list-inline navbar-left hidden-xs">
<?php
						if(USER_FACEBOOK) {
?>
							<li><a href=<?=USER_FACEBOOK?> target="_blank"><i class="fa fa-facebook-square" aria-hidden="true"></i><?=USER_NAME?></a></li>
<?php
						}
?>
<?php
						if(USER_TEL) {
?>
							<li><a href=<?="tel:".USER_TEL?>> <i class="fa fa-phone-square" aria-hidden="true"></i><?=" ".USER_TEL_SHOW?></a></li>
<?php
						}
?>
								<li><a href=<?="mailto:".USER_EMAIL?>><i class="fa fa-envelope" aria-hidden="true"></i> <?=USER_EMAIL?></a></li>
								<?=$str_btn_account;?>
								<!-- <li><a data-fancybox data-src="#searchbox" href="javascript:;"><i class="fa fa-search" aria-hidden="true"></i></a></li> -->
							</ul>

<!-- Desktop UI -->
							<ul class="media list-inline hidden-sm hidden-md hidden-lg">
<?php
						if(USER_FACEBOOK) {
?>
								<li><a href=<?=USER_FACEBOOK?> target="_blank"><i class="fa fa-facebook-square" aria-hidden="true"></i></a></li>
<?php
						}
?>
								<li><a href=<?="mailto:".USER_EMAIL?>><i class="fa fa-envelope" aria-hidden="true"></i></a></li>
								<?=$str_btn_account;?>
								<!-- <li><a data-fancybox data-src="#searchbox" href="javascript:;"><i class="fa fa-search" aria-hidden="true"></i></a></li> -->
							</ul>

							<div class="navbar-right hidden-xs">
<?php
				if(CONF_BILINGUAL) {
?>
								<ul class="lang clearfix">
									<li><a href="<?=PROTOCOL.'://'.LANGUAGE_EN_DOMAIN.$_nowstep_url;?>">EN</a></li>
									<li><a href="<?=PROTOCOL.'://'.LANGUAGE_DEFAULT_DOMAIN.$_nowstep_url;?>">CH</a></li>
								</ul>
<?php
						}
?>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="mobile_nav hidden-lg">
				<div class="navbar-header">
					<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#header_s03" aria-expanded="false">
						<span class="sr-only">Toggle navigation</span>
						選單
					</button>
					<a class="navbar-brand hidden-lg hidden-md" href="./">
						<img src="frontend/img/sys/logo.png" data-rjs="frontend/img/sys/logo@2x.png" alt="" class="img-responsive">
					</a>
				</div>
				<div class="collapse navbar-collapse" id="header_s03">
<!-- bilingual  -->
<?php
				if(CONF_BILINGUAL) {
?>
					<div class="btn-group btn-group-justified lang_btn visible-xs" role="group" aria-label="...">
						<div class="btn-group" role="group">
							<a href="<?=PROTOCOL.'://'.LANGUAGE_DEFAULT_DOMAIN.$_SERVER['REQUEST_URI'];?>" class="btn btn-green">CH</a>
						</div>
						<div class="btn-group" role="group">
							<a href="<?=PROTOCOL.'://'.LANGUAGE_EN_DOMAIN.$_SERVER['REQUEST_URI'];?>" class="btn btn-green">EN</a>
						</div>
					</div>
<?php
						}
?>
<!-- Mobile UI -->
					<ul class="nav navbar-nav">
						<!-- <li><a href="about_us"><?=$_GET_LANG['about_us'];?></a></li> -->
<?php
						if(BRAND_OWNER) {
?>
						<li class="dropdown">
							<a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><?=$_GET_LANG['featured_products'];?></a>
							<ul class="dropdown-menu">
								<?=$str_brand;?>
								<li><a href="products"><?=$_GET_LANG['stock_product'];?></a></li>
							</ul>
						</li>
<?php
						} else {
?>
						<li><a href="products"><?=$_GET_LANG['stock_product'];?></a></li>
<?php
						}
?>
<?php
						if(CONF_CASE) {
?>
						<li><a href="works"><?=$_GET_LANG['past_work'];?></a></li>
<?php
						}
?>
						<li><a href="news"><?=$_GET_LANG['news'];?></a></li>
<?php
						if(CONF_HAS_MEMBER) {
?>
						<li class="dropdown">
							<a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><?=$_GET_LANG['member_download'];?></a>
							<ul class="dropdown-menu">
								<li><a href="epd-nrmm-label"><?=$_GET_LANG['epd_nrmm_label'];?></a></li>
								<li><a href="download-files"><?=$_GET_LANG['documents_download'];?></a></li>
							</ul>
						</li>
<?php
						}
?>
						<li><a href="contact"><?=$_GET_LANG['about_us'];?></a></li>
					</ul>
				</div>
			</div>
<!-- PC UI -->
			<div class="header_st3_navbar visible-lg">
				<div class="container">
					<div class="row">
						<div class="col-lg-4">
							<div class="header_st3_logo text-center">
								<a href="./">
									<img src="frontend/img/sys/logo.png" alt="" class="img-responsive">
								</a>
							</div>
						</div>
						<div class="col-lg-8">
							<ul class="hover_nav clearfix">
								<!-- <li><a href="about_us"><?=$_GET_LANG['about_us'];?></a></li> -->
<?php
						if(BRAND_OWNER) {
?>
								<li class="dropnav">
									<a href="javascript:;"><?=$_GET_LANG['featured_products'];?></a>
									<ul class="hover_dropdown nav_pro">
										<?=$str_brand;?>
										<li><a href="products"><?=$_GET_LANG['stock_product'];?></a></li>
									</ul>
								</li>
<?php
						} else {
?>
							<li><a href="products"><?=$_GET_LANG['stock_product'];?></a></li>
<?php
						}
?>
<?php
						if(CONF_CASE) {
?>
								<li><a href="works"><?=$_GET_LANG['past_work'];?></a></li>
<?php
						}
?>
								<li><a href="news"><?=$_GET_LANG['news'];?></a></li>
<?php
						if(CONF_HAS_MEMBER) {
?>
								<li class="dropnav">
									<a href="javascript:;"><?=$_GET_LANG['member_download'];?></a>
									<ul class="hover_dropdown">
										<li><a href="epd-nrmm-label"><?=$_GET_LANG['epd_nrmm_label'];?></a></li>
										<li><a href="download-files"><?=$_GET_LANG['documents_download'];?></a></li>
									</ul>
								</li>
<?php
						}
?>
								<li><a href="contact"><?=$_GET_LANG['about_us'];?></a></li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</nav>
	</div>
	<div style="display: none;" id="searchbox">
		<div class="container">
			<div class="row">
				<div class="col-xs-12">
					<div class="input-group">
					  <input type="text" class="form-control" placeholder="搜尋...">
					  <span class="input-group-btn">
						<button class="btn btn-green" type="button"><span class="glyphicon glyphicon-search" aria-hidden="true"></span></button>
					  </span>
					</div>
				</div>
			</div>
		</div>
	</div>
</header>
