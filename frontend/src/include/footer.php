<footer>
    <div class="footer_b_copyright">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-4 col-sm-push-8 col-md-4 col-md-push-8 text-center footer_chat">
			<div class="copyright text-center"><?=$_GET_LANG['footer_contract_us'];?></div>
			<a href=<?="tel:".USER_TEL_SHOW?>>電話: <?=USER_TEL_SHOW?></a>
			<div class="row">
				<div class="col-xs-10 col-xs-offset-1 col-md-8 col-md-offset-2 footer_contact">
				</div>
			</div>
		</div>
		<div class="col-xs-12 col-sm-8 col-sm-pull-4 col-md-8 col-md-pull-4">
			<div class="row">
				<div class="col-xs-12 col-sm-5 text-center">
				</div>
				<div class="col-xs-12 col-sm-7">
					<div class="copyright text-center">
	                        <?=USER_COPYRIGHT?> <span class="web_author"><a href="http://www.google.com.tw" target="_blank" rel="nofollow"> <?=DESIGNER_NAME?> </a></span>
	                    </div>
				</div>
			</div>
		</div>
            </div>
        </div>
    </div>
    <div id="gotop">
        <img src="frontend/img/sys/go-top_s.png" data-rjs="img/sys/go-top.png" alt="" class="img-responsive">
    </div>
</footer>

<script src="frontend/js/jquery.min.js?<?=RECACHE_NUM;?>"></script>
<script src="frontend/js/jquery.224.min.js?<?=RECACHE_NUM;?>"></script>
<script src="frontend/js/bootstrap.min.js?<?=RECACHE_NUM;?>"></script>
<script src="frontend/js/jquery.fancybox.min.js?<?=RECACHE_NUM;?>"></script>
<script src='frontend/js/jquery.flagstrap.min.js?<?=RECACHE_NUM;?>'></script>
<script src='frontend/js/owl.carousel.min.js?<?=RECACHE_NUM;?>'></script>
<script src='frontend/js/sweetalert.min.js?<?=RECACHE_NUM;?>'></script>
<script src="frontend/js/style.js?<?=RECACHE_NUM;?>"></script>
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = 'https://connect.facebook.net/zh_TW/sdk.js#xfbml=1&version=v2.10';
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>