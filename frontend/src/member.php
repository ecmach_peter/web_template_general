<?php
	$_arr_member = $crud->getid('member', array('member_uniqid' => $_SESSION[SESSION_NAME.'_user_uniqid']));

	//確認登入狀態
	if(!isset($_SESSION[SESSION_NAME.'_user_uniqid']) || empty($_arr_member['member_uniqid'])){
		header("location: ./");
		exit;
	}

	$arr_member_title = array('MEMBERS', '會員專區');
	$arr_info_title = array('BASIC INFO', '基本資料');
	$arr_chgpwd_title = array('CHANGE PASSWORD', '修改密碼');
	if($_LANG == ''){
		$arr_member_title = array_reverse($arr_member_title);
		$arr_info_title = array_reverse($arr_info_title);
		$arr_chgpwd_title = array_reverse($arr_chgpwd_title);
	}
?>
<!doctype html>
<html lang="zh-Hant-TW">

<head>
	<?php include_once("include/head.php");?>

	<!-- 專案的CSS -->
	<link rel="stylesheet" href="css/import/page.css">
</head>

<body>
	<?php include_once("include/navbar.php");?>
		
	<div class="content">
		<div class="container">
			<div class="row">
				<div class="col-xs-12">
					<ol class="breadcrumb">
						<li><a href="./"><i class="fa fa-home" aria-hidden="true"></i> <?=$_GET_LANG['home'];?></a></li>
						<li class="active"><?=$_GET_LANG['my_account'];?></li>
					</ol>
					<div class="titlebar line_bottom mb30 clearfix">
						<h2 class="line_title"><?=$arr_member_title[0];?> &#8260; <small><?=$arr_member_title[1];?></small></h2>
					</div>
					<div class="row">
						<div class="col-xs-12 col-md-6">
							<div class="row">
								<div class="col-xs-12">
									<div class="titlebar line_bg mb20 clearfix">
										<h2 class="line_title"><?=$arr_info_title[0];?> &#8260; <small><?=$arr_info_title[1];?></small></h2>
									</div>
								</div>
								<div class="col-xs-12">
									<form id="form_member_info" class="mem_info">
										<div class="form-group">
											<label for="member_name"><?=$_GET_LANG['account'];?>：</label>
											<p class="form-control-static">+<?=$_arr_member['member_calling_code'].$_arr_member['member_account'];?></p>
										</div>
										<div class="form-group">
											<label for="member_name"><?=$_GET_LANG['contact_email'];?>：</label>
											<p class="form-control-static"><?=$_arr_member['member_email'];?></p>
										</div>
										<div class="form-group">
											<label for="member_name"><?=$_GET_LANG['contact_name'];?>：</label>
											<input type="text" class="form-control" id="member_name" name="member_name" value="<?=$_arr_member['member_name'];?>" autofocus>
										</div>
										<div class="form-group">
											<label for="member_nickname"><?=$_GET_LANG['company_contact_nickname'];?>：</label>
											<input type="text" class="form-control" id="member_nickname" name="member_nickname" value="<?=$_arr_member['member_nickname'];?>">
										</div>
										<div class="form-group">
											<label for="member_mobile1"><?=$_GET_LANG['contact_phone'];?>1：</label>
											<input type="text" class="form-control" id="member_mobile1" name="member_mobile1" value="<?=$_arr_member['member_mobile1'];?>">
										</div>
										<div class="form-group">
											<label for="member_mobile2"><?=$_GET_LANG['contact_phone'];?>2：</label>
											<input type="text" class="form-control" id="member_mobile2" name="member_mobile2" value="<?=$_arr_member['member_mobile2'];?>">
										</div>
										<div class="form-group">
											<label for="member_lineID">LINE ID：</label>
											<input type="text" class="form-control" id="member_lineID" name="member_lineID" value="<?=$_arr_member['member_lineID'];?>">
										</div>
										<div class="form-group">
											<label for="member_skypeID">Skype ID：</label>
											<input type="text" class="form-control" id="member_skypeID" name="member_skypeID" value="<?=$_arr_member['member_skypeID'];?>">
										</div>
										<button type="submit" class="btn btn-block btn-green"><?=$_GET_LANG['submit'];?></button>
									</form>
								</div>
							</div>
						</div>
						<div class="col-xs-12 col-md-6">
							<div class="row">
								<div class="col-xs-12">
									<div class="titlebar line_bg mb20 clearfix">
										<h2 class="line_title"><?=$arr_chgpwd_title[0];?> &#8260; <small><?=$arr_chgpwd_title[1];?></small></h2>
									</div>
								</div>
								<div class="col-xs-12">
									<form class="mem_infos" id="form_member_pwd">
										<div class="form-group">
											<label for="password_o"><?=$_GET_LANG['old_password'];?>：</label>
											<input type="password" class="form-control" id="password_o" name="password_o" required="">
										</div>
										<div class="form-group">
											<label for="password_n"><?=$_GET_LANG['old_newpassword'];?>：</label>
											<input type="password" class="form-control" id="password_n" name="password_n" required="">
										</div>
										<div class="form-group">
											<label for="password_nr"><?=$_GET_LANG['old_renewpassword'];?>：</label>
											<input type="password" class="form-control" id="password_nr" name="password_nr" required="">
										</div>
										<button type="submit" class="btn btn-block btn-green"><?=$_GET_LANG['submit'];?></button>
									</form>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	
	<?php include_once("include/footer.php");?>
	<script type="text/javascript">
		$(function(){
			$('#form_member_pwd').on('submit', function(){
				var $this = $(this),
					btn_send = $this.find('#btn_send');

				btn_send.attr('disabled', true);
				
				swal({
					title: "Are you sure?",
					text: "<?=$_GET_LANG['confirm_modify'];?>",
					showCancelButton: true,
					confirmButtonColor: "#DD6B55",
					confirmButtonText: "Yes",
					cancelButtonText: "No",
				},
				function(isConfirm){
					if (isConfirm) {
						doajax({
							url: './domem',
							type: 'json',
							data: 'type=mempwdchang&'+$this.serialize(),
							callback: function(msg){
								if(msg.sts){
									swal({
										title: "<?=$_GET_LANG['modify_successfully'];?>!",
										text: msg.msg,
										timer: 5000,
										type: 'success'
									},
									function(){
										location.href = "./logout";
									});
								}
								else{
									swal(msg.msg);
									if(msg.item){
										$('body').animate({
											scrollTop:$('#'+msg.item).offset().top - 130
										});
									}
									btn_send.attr('disabled', false);
								}
							}
						});
					}
					else{
						btn_send.attr('disabled', false);
					}
				});
				return false;
			});

			$('#form_member_info').on('submit', function(){
				var $this = $(this),
					btn_send = $this.find('#btn_send');

				btn_send.attr('disabled', true);
				
				swal({
					title: "Are you sure?",
					text: "<?=$_GET_LANG['confirm_modify'];?>",
					showCancelButton: true,
					confirmButtonColor: "#DD6B55",
					confirmButtonText: "Yes",
					cancelButtonText: "No",
					closeOnConfirm: false
				},
				function(isConfirm){
					if (isConfirm) {
						doajax({
							url: './domem',
							type: 'json',
							data: 'type=edit&'+$this.serialize(),
							callback: function(msg){
								if(msg.sts){
									swal({
										title: "<?=$_GET_LANG['modify_successfully'];?>!",
										text: msg.msg,
										type: 'success'
									},
									function(){
									});
								}
								else{
									swal(msg.msg);
									if(msg.item){
										$('body').animate({
											scrollTop:$('#'+msg.item).offset().top - 130
										});
									}
									btn_send.attr('disabled', false);
								}
							}
						});
					}
					else{
						btn_send.attr('disabled', false);
					}
				});
				return false;
			});
		});
	</script>
</body>

</html>
