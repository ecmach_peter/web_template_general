<!doctype html>
<html lang="zh-Hant-TW">

<head>
	<?php include_once("include/head.php");?>
	<!-- <meta http-equiv="refresh" content="5; url=./"> -->

	<!-- 專案的CSS -->
</head>

<body>
	<?php include_once("include/navbar.php");?>
	<div class="content">
		<div class="container">
			<div class="row">
				<div class="col-sm-8 col-sm-offset-2">
					<h3 class="text-center mb50"><?=$_GET_LANG['contact_verify'];?></h3>
					<img src="./images/tks_smile.png" alt="" class="img-responsive center-block">
				</div>
			</div>
		</div>
	</div>
	
	<?php include_once("include/footer.php");?>
</body>

</html>
