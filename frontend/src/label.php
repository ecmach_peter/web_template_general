<?php
	//取得標籤資訊
	$arr_label = $crud->getid(
							'label_info', 
							array(
								'label_info_url' => $_var1, 
								'label_info_online' => 1
							), 
							array(), 
							array(
								'label_info_title' => "label_info_title{$_LANG}", 
								'label_info_content' => "label_info_content{$_LANG}"
							)
						);
	if($arr_label === false){
		include("./include.page/sys_404.php");
		exit;
	}

	$_customize_title = $arr_label['label_info_title'].' :: ';
?>
<!doctype html>
<html lang="zh-Hant-TW">

<head>
	<?php include_once("include/head.php");?>

	<!-- 專案的CSS -->
	<link rel="stylesheet" href="css/import/page.css">
</head>

<body>
	<?php include_once("include/navbar.php");?>
		
	<div class="content">
		<div class="container">
			<div class="row">
				<div class="col-xs-12 col-md-10 col-md-offset-1">
					<ol class="breadcrumb">
						<li><a href="./"><i class="fa fa-home" aria-hidden="true"></i> <?=$_GET_LANG['home'];?></a></li>
						<li><a href="javascript:;"><?=$_GET_LANG['apply_label'];?></a></li>
						<li><a href="javascript:;"><?=$arr_label['label_info_title'];?></a></li>
						<li class="active"><?=$_GET_LANG['legal_requirements'];?></li>
					</ol>
				</div>
				<div class="col-xs-12 col-md-10 col-md-offset-1">
					<div class="textedit text-justify clearfix">
						<h3 class="text-center mb20"><?=$_GET_LANG['legal_requirements'];?></h3>
						<?=htmlspecialchars_decode($arr_label['label_info_content']);?>
					</div>
				</div>
			</div>
		</div>
	</div>
	
	<?php include_once("include/footer.php");?>
	<div id="fb-root"></div>
	<script>(function(d, s, id) {
	  var js, fjs = d.getElementsByTagName(s)[0];
	  if (d.getElementById(id)) return;
	  js = d.createElement(s); js.id = id;
	  js.src = "//connect.facebook.net/zh_TW/sdk.js#xfbml=1&version=v2.10";
	  fjs.parentNode.insertBefore(js, fjs);
	}(document, 'script', 'facebook-jssdk'));</script>
</body>

</html>
