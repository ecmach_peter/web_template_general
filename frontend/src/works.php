<?php
	//過往工作類型
	$arr_label_work = $crud->select(
								'works_type', 
								array('works_type_online' => 1), 
								array('works_type_sort' => 'ASC'), 
								array(
									'works_type_id',
									'works_type_url',
									'works_type_photo',
									'works_type_title' => "works_type_title{$_LANG}",
									'works_type_content' => "works_type_content{$_LANG}",
								)
							);
	$str_works_type_left = '';
	if(count($arr_label_work) > 0){
		//取出相簿資料
		$sql_all_id = join(',', array_column($arr_label_work, 'works_type_id'));
		$arr_album = $crud->sql("SELECT `host_id`, `album_photo`
								FROM `works_type_album`
								WHERE `host_id` IN ({$sql_all_id})
								ORDER BY `host_id`, `album_sort`");
		$arr_album_data = $arr_album_cover = array();
		if(count($arr_album) > 0){
			foreach($arr_album as $key => $value){
				$thumb = str_replace('.', '_a.', $value['album_photo']);
				if(empty($arr_album_data[$value['host_id']])) 
				$arr_album_data[$value['host_id']] .= <<<HTML
					<a href="./frontend/img/usr/works_type/{$value['host_id']}/{$value['album_photo']}" data-type="image" data-fancybox="group0{$value['host_id']}" data-thumb="./frontend/img/usr/works_type/{$value['host_id']}/{$thumb}"></a>
HTML;
			}
		}
		/*
		20171026 peter remove
		<a href="./uploadimages/works_type/{$value['works_type_id']}/{$value['works_type_photo']}" data-type="image" data-fancybox="group0{$value['works_type_id']}">
		*/
		foreach($arr_label_work as $key => $value){
			$str_works_type_left .= <<<HTML
						<li><a href="./works/{$value['works_type_url']}">{$value['works_type_title']}</a></li>
HTML;
			$works_type_content = nl2br($value['works_type_content']);

			$str_case .= <<<HTML
						<li id="case0{$key}">
							<div class="thumbnail np_box clearfix">
								<div class="album">
									<a href="./works/{$value['works_type_url']}">
										<div class="np_box_img" style="background-image: url('./frontend/img/usr/works_type/{$value['works_type_id']}/{$value['works_type_photo']}');"></div>
									</a>
									<div style="display: none;">
										{$arr_album_data[$value['works_type_id']]}
									</div>
								</div>
								<div class="album_infobox box-close">
									<ul class="album_info">
										<li><span>{$_GET_LANG['category']}：</span>{$value['works_type_title']}</li>
									</ul>
									<div class="textedit case-box box-close">
										{$works_type_content}
									</div>
									<p class="text-center">
										<button class="btn btn-green btn-xs btn_op"></button>
									</p>
								</div>
							</div>
						</li>
HTML;
		}
	}
?>
<!doctype html>
<html lang="zh-Hant-TW">

<head>
	<?php include_once("frontend/src/include/head.php");?>

	<!-- 專案的CSS -->
	<link rel="stylesheet" href="frontend/css/import/page.css">
</head>

<body>
	<?php include_once("include/navbar.php");?>

	<div class="content">
		<div class="container">
			<div class="row">
				<div class="col-xs-12">
					<ol class="breadcrumb">
						<li><a href="./"><i class="fa fa-home" aria-hidden="true"></i> <?=$_GET_LANG['home'];?></a></li>
						<li class="active"><?=$_GET_LANG['past_work'];?></li>
					</ol>
				</div>
				<div class="col-xs-12 col-md-2">
					<div class="sidebar visible-md visible-lg">
						<ul class="sidebar_list">
							<?=$str_works_type_left;?>
						</ul>
					</div>
					<div class="sidebar mb20 visible-xs visible-sm">
						<ul class="sidebar_list side_flex">
							<?=$str_works_type;?>
						</ul>
					</div>
				</div>
				<div class="col-xs-12 col-md-10">
					<div class="titlebar line_bottom mb30 clearfix">
						<h2 class="line_title"><?=($_LANG == '')? '過往工作':'Past works';?> &#8260; <small><?=($_LANG == '')? 'Past works':'過往工作';?></small></h2>
					</div>
					<ul class="list-unstyled boxstyle_list">
						<?=$str_case;?>
					</ul>
				</div>
			</div>
		</div>
	</div>
	
	<?php include_once("include/footer.php");?>
	<script>
		$('.btn_op').on('click', function(){
			var that = $(this).parents('.album_infobox').toggleClass('box-close box-open');
			var case_box = that.find('.case-box');
			case_box.animate({height: (case_box.height() > 200)? 162:case_box[0].scrollHeight}, 10);
		});
	</script>
</body>

</html>
