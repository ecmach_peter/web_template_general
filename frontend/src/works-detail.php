<?php
	//取得標籤資訊
	$arr_works_cate = $crud->getid(
							'works_cate', 
							array(
								'works_cate_url' => $_var1, 
								'works_cate_online' => 1
							), 
							array(), 
							array(
								'works_cate_id',
								'works_cate_title' => "works_cate_title{$_LANG}"
							)
						);
	if($arr_works_cate === false){
		include("./include.page/sys_404.php");
		exit;
	}

	//取得slider
	$arr_works_cate_album = $crud->select(
									'works_cate_album', 
									array('host_id' => $arr_works_cate['works_cate_id']), 
									array('album_sort' => 'ASC'), 
									array('album_photo')
								);
	$str_works_cate_album = $str_works_cate_item = $tmp_works_cate_item = '';
	if(count($arr_works_cate_album) > 0){
		foreach($arr_works_cate_album as $key => $value){
			$active = (empty($str_works_cate_album))? 'active':'';
			$str_works_cate_album .= <<<HTML
					<div class="item {$active}" style="background-image: url('./frontend/img/usr/works_cate/{$arr_works_cate['works_cate_id']}/{$value['album_photo']}');"></div>
HTML;
			$tmp_works_cate_item .= <<<HTML
					<div data-target="#alexslider" data-slide-to="{$key}" class="thumb" style="background-image: url('./frontend/img/usr/works_cate/{$arr_works_cate['works_cate_id']}/{$value['album_photo']}');"></div>
HTML;
			//一排4個
			if(($key + 1) % 4 == 0){
				$active_item = (empty($str_works_cate_item))? 'active':'';
				$str_works_cate_item .= <<<HTML
					<div class="item {$active_item} {$key}">
						{$tmp_works_cate_item}
					</div>
HTML;
				$tmp_works_cate_item = '';
			}
		}

		//未滿4個的一組
		if($key % 4 != 0){
			$active_item = ($key < 4)? 'active':'';
			$str_works_cate_item .= <<<HTML
				<div class="item {$active_item}">
					{$tmp_works_cate_item}
				</div>
HTML;
		}
	}

	//取得此標籤的成功案例
	$arr_works = $crud->select(
							'works', 
							array(
								'works_cate_id' => $arr_works_cate['works_cate_id'],
								'works_online' => 1
							),
							array('works_sort' => 'ASC'),
							array(
								'works_id',
								'works_left_menu',
								'works_photo',
								'works_title' => "works_title{$_LANG}",
								'works_content' => "works_content{$_LANG}",
							)
						);
	$str_works = $str_left_menu = '';
	if(count($arr_works) > 0){
		//取出相簿資料
		$sql_all_id = join(',', array_column($arr_works, 'works_id'));
		$arr_album = $crud->sql("SELECT `host_id`, `album_photo`
								FROM `works_album`
								WHERE `host_id` IN ({$sql_all_id})
								ORDER BY `host_id`, `album_sort`");
		$arr_album_data = array();
		if(count($arr_album) > 0){
			foreach($arr_album as $key => $value){
				$thumb = str_replace('.', '_a.', $value['album_photo']);
				$arr_album_data[$value['host_id']] .= <<<HTML
					<a href="./frontend/img/usr/works/{$value['host_id']}/{$value['album_photo']}" data-type="image" data-fancybox="group0{$value['host_id']}" data-thumb="./frontend/img/usr/works/{$value['host_id']}/{$thumb}"></a>
HTML;
			}
		}
		foreach($arr_works as $key => $value){
			$works_content = nl2br($value['works_content']);

			$str_works .= <<<HTML
						<li id="works{$key}">
							<div class="thumbnail np_box clearfix">
								<div class="album">
									<a href="./frontend/img/usr/works/{$value['works_id']}/{$value['works_photo']}" data-type="image" data-fancybox="group0{$value['works_id']}">
										<div class="np_box_img" style="background-image: url('./frontend/img/usr/works/{$value['works_id']}/{$value['works_photo']}');"></div>
									</a>
									<div style="display: none;">
										{$arr_album_data[$value['works_id']]}
									</div>
								</div>
								<div class="album_infobox box-close">
									<ul class="album_info">
										<li><span>名稱：</span>{$value['works_title']}</li>
									</ul>
									<div class="textedit case-box box-close">
										{$works_content}
									</div>
									<p class="text-center visible-lg">
										<button class="btn btn-green btn-xs btn_op"></button>
									</p>
								</div>
							</div>
						</li>
HTML;
			if($value['works_left_menu'] == 1){
				$str_left_menu .= <<<HTML
						<li data-key="{$key}"><a href="#works{$key}">{$value['works_title']}</a></li>
HTML;
			}
		}
	}
?>
<!doctype html>
<html lang="zh-Hant-TW">

<head>
	<?php include_once("frontend/src/include/head.php");?>

	<!-- 專案的CSS -->
	<link rel="stylesheet" href="frontend/css/import/page.css">
</head>

<body>
	<?php include_once("include/navbar.php");?>

	<div class="content">
		<div class="container">
			<div class="row">
				<div class="col-xs-12">
					<ol class="breadcrumb">
						<li><a href="./"><i class="fa fa-home" aria-hidden="true"></i> 首頁</a></li>
						<li><a href="./works">過往工作</a></li>
						<li class="active"><?=$arr_works_cate['works_cate_title'];?></li>
					</ol>
				</div>
				<div class="col-xs-12 col-md-2">
					<div class="sidebar visible-md visible-lg">
						<ul id="mainNav" class="sidebar_list">
							<?=$str_left_menu;?>
						</ul>
						<!-- 最多10格選項，由Peter自選要哪10個放在這兒 -->
					</div>
					<div class="visible-xs visible-sm">
						<select class="selectpicker">
							<option value="">精選案例</option>
						</select>
					</div>
				</div>
				<div class="col-xs-12 col-md-10">
					<div class="titlebar line_bottom mb30 clearfix">
						<h2 class="line_title">過往工作-<?=$arr_works_cate['works_cate_title'];?> &#8260; <small>works</small></h2>
					</div>
					<div class="bannerbar hidden-sm hidden-xs">
						<div id="alexslider" class="carousel slide alex_slider" data-ride="carousel">
							<div class="carousel-inner">
								<?=$str_works_cate_album;?>
							</div>
						</div>
						<div class="clearfix">
							<div id="thumbcarousel" class="carousel slide alex_thumb" data-interval="false">
								<div class="carousel-inner">
									<?=$str_works_cate_item;?>
								</div>
								<!-- /carousel-inner -->
								<a class="left carousel-control" href="#thumbcarousel" role="button" data-slide="prev">
									<span class="glyphicon glyphicon-chevron-left"></span>
								</a>
								<a class="right carousel-control" href="#thumbcarousel" role="button" data-slide="next">
									<span class="glyphicon glyphicon-chevron-right"></span>
								</a>
							</div>
							<!-- /thumbcarousel -->
						</div>
					</div>
					<ul class="list-unstyled boxstyle_list">
						<?=$str_works;?>
					</ul>
				</div>
			</div>
		</div>
	</div>
	
	<?php include_once("include/footer.php");?>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.2/js/bootstrap-select.min.js"></script>
	<script>
		$(document).ready( function() {
			$('#alexslider').carousel({
				interval:   4000
			});
			
			var clickEvent = false;
			$('#alexslider').on('click', '.nav a', function() {
					clickEvent = true;
					$('.nav li').removeClass('active');
					$(this).parent().addClass('active');        
			}).on('slid.bs.carousel', function(e) {
				if(!clickEvent) {
					var count = $('.nav').children().length -1;
					var current = $('.nav li.active');
					current.removeClass('active').next().addClass('active');
					var id = parseInt(current.data('slide-to'));
					if(count == id) {
						$('.nav li').first().addClass('active');    
					}
				}
				clickEvent = false;
			});

			$('.sidebar_list li').each(function(){
				var $this = $(this);
				$('.selectpicker').append($('<option>').val($this.data('key')).text($this.text()));
			});
			$('.selectpicker').on('change', function(){
				var _val = $(this).val();
				if(_val != ''){
					$('html, body').animate({
						scrollTop: $('#works' + _val).offset().top - 20
					});
				}
			});
		});
		
		var toggleNode = document.querySelector('.btn_op');

				toggleNode.addEventListener("click", toggleFunc);

				function toggleFunc(E) {
						var _class = "album_infobox box-close"
					E.preventDefault();
					box_class = document.querySelector('.album_infobox');
					if (box_class.className.match(/box-close/)) {
						_class = "album_infobox box-open";
					}
					box_class.className = _class;
					
					box = document.querySelector('.case-box');
					var orgHeight = parseInt(box.style.height, 10);
					box.style.height = (orgHeight > 200) ? "162px": box.scrollHeight + "px"; 
				}
	</script>
</body>

</html>
