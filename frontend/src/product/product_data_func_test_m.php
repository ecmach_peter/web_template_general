<?php
// print_r($_POST);exit;

	//取會員資料
	$arr_member = $crud->getid('member', array('member_uniqid' => $_SESSION[SESSION_NAME.'_user_uniqid']));

	$_type = get_post_var('type');
	$_filter = get_post_var('filter');
	$arr_url = $_record_data = array();
	$SQL_filter = '';
	$SQL_filter_for_tmp = '';
	$SQL_sort = '';
	if(count($_filter) > 0){
		foreach ($_filter as $key => $value) {
			$var_error = false;
			switch ($key) {
				//地區參數處理
				case 'location':
					$arr_country = array('HKG', 'TWN', 'OTHER');
					if(array_search($value, $arr_country) !== false){
						if($value == 'OTHER'){
							$SQL_filter .= " AND `product_country` NOT IN ('HKG', 'TWN')";
							$SQL_filter_for_tmp .= " AND `product_country` NOT IN ('HKG', 'TWN')";
						}
						else{
							$SQL_filter .= " AND `product_country` = '{$value}'";
							$SQL_filter_for_tmp .= " AND `product_country` = '{$value}'";
						}

						$_record_data[] = 'location='.$value;
					}
					else{
						$var_error = true;
					}
					break;

				//分類參數處理
				case 'category':
					//確認有此分類
					$arr_chk_cate = $crud->getid('model_cate', array('cate_id' => $value));
					if(empty($arr_chk_cate['cate_id'])){
						$var_error = true;
					}
					else{
						//確認有無子分類
						$arr_child_cate = $crud->select('model_cate', array('cate_par_id' => $value));
						if(count($arr_child_cate) > 0){
							$arr_cate_in = array();
							foreach ($arr_child_cate as $_key => $_value) {
								$arr_cate_in[] = $_value['cate_id'];
							}
							$cate_sql_mode = 'IN ('.join(',', $arr_cate_in).')';
						}
						else{
							$cate_sql_mode = '= '.$value;
						}

						$SQL_filter .= " AND `model_cate` {$cate_sql_mode}";
						$SQL_filter_for_tmp .= " AND `product_cate` {$cate_sql_mode}";

						$_record_data[] = 'category='.$arr_chk_cate['cate_title'];
					}
					break;

				//品牌參數處理
				case 'brand':
					//確認有此品牌
					$SQL_filter_brand = '';
					$SQL_filter_brand_for_tmp = '';
					$arr_chk_brand = $crud->getid('brand', array('brand_id' => $value));
					if(empty($arr_chk_brand['brand_id'])){
						$var_error = true;
					}
					else{
						$SQL_filter_brand = " AND `model_brand` = {$value}";
						$SQL_filter_brand_for_tmp = " AND `product_brand` = {$value}";

						$_record_data[] = 'brand='.$arr_chk_brand['brand_name'];
					}
					break;

				//檢視模式處裡
				case 'view':
					$arr_view = array(/*'grid', */'list');
					if(array_search($value, $arr_view) !== false){
						$_type = $value;
					}
					else{
						$var_error = true;
					}
					break;

				//標章處理
				case 'label':
					$arr_label = array('NRMMG', 'NRMMY');
					if(array_search($value, $arr_label) !== false){
						$SQL_filter .= " AND `product_epd_nrmm_label` LIKE ('%{$value}%')";

						$_record_data[] = 'label='.$value;
					}
					else{
						$var_error = true;
					}
					break;

				//排序處理
				case 'sort':
					switch ($value) {
						case 'new-asc':
							$SQL_sort = ' `product_id` ASC';
							break;
						case 'price':
							$SQL_sort = ' `product_selling_price` DESC';
							break;
						case 'price-asc':
							$SQL_sort = ' `product_selling_price` ASC';
							break;
						case 'popular':
							$SQL_sort = ' `product_id` DESC';
							break;
					}
					break;

				//使用時數
				case 'price':
				case 'year':
				case 'hours':
				case 'weight':
					if(!empty($value) && $value != '-'){
						${'arr_range_'.$key} = explode('-', $value);
						${'arr_range_'.$key}[0] = (int)${'arr_range_'.$key}[0];
						${'arr_range_'.$key}[1] = (int)${'arr_range_'.$key}[1];
						switch ($key) {
							case 'year':
								$limit = array(1800, date('Y'));
								break;

							default:
								$limit = array(0, 999999999);
								break;
						}

						switch ($key) {
							case 'weight':
								$_column = "`model_{$key}`";
								break;
							case 'price':
								${'arr_range_'.$key}[0] *= 10000;
								${'arr_range_'.$key}[1] *= 10000;
								$_column = "IFNULL(
												(SELECT `product_selling_price_currency`.`selling_price`
												FROM `product_selling_price`
												INNER JOIN `product_selling_price_currency` ON `product_selling_price`.`selling_id` = `product_selling_price_currency`.`selling_id`
												WHERE `product_selling_price`.`product_id` = `product`.`product_id` AND `selling_online` = 1 AND CONCAT(',', `selling_target`, ',') LIKE '%,{$arr_member['member_id']},%' AND `product_selling_price_currency`.`product_currency` = '{$_currency}'
												ORDER BY `selling_sort`
												LIMIT 1),
												`product_price_currency`.`product_default_price`
											)";
								break;
							default:
								$_column = "`product_{$key}`";
								break;
						}

						if($key == 'price' && $value != '-'){
							$arr_tmp = array();
							if(${'arr_range_'.$key}[0] <> '' || ${'arr_range_'.$key}[0] == 0) $arr_tmp[] = " {$_column} >= {${'arr_range_'.$key}[0]}";
							if(${'arr_range_'.$key}[1] <> '' || ${'arr_range_'.$key}[0] == 0) $arr_tmp[] = " {$_column} <= {${'arr_range_'.$key}[1]}";

							$str_tmp = join(' AND ', $arr_tmp);
							$SQL_filter .= " AND ({$str_tmp} OR `product`.`product_default_price` = 0) ";

							//暫存產品
							$arr_tmp = array();
							if(${'arr_range_'.$key}[0] <> '' || ${'arr_range_'.$key}[0] == 0) $arr_tmp[] = " `product_tmp_price_currency`.`product_default_price` >= {${'arr_range_'.$key}[0]}";
							if(${'arr_range_'.$key}[1] <> '' || ${'arr_range_'.$key}[0] == 0) $arr_tmp[] = " `product_tmp_price_currency`.`product_default_price` <= {${'arr_range_'.$key}[1]}";

							$str_tmp = join(' AND ', $arr_tmp);
							$SQL_filter_for_tmp .= " AND ({$str_tmp} OR `product_tmp`.`product_default_price` = 0) ";
						}
						else if($key == 'weight'){
							$tmp_filter = array();
							if(${'arr_range_'.$key}[0] <> '') $tmp_filter[] = " {$_column} >= {${'arr_range_'.$key}[0]}";
							if(${'arr_range_'.$key}[1] <> '') $tmp_filter[] = " {$_column} <= {${'arr_range_'.$key}[1]}";

							if(!empty($tmp_filter)){
								$SQL_filter .= " AND ((".join(' AND ', $tmp_filter).") OR {$_column} = 0)";
							}

							//暫存產品
							$tmp_filter = array();
							if(${'arr_range_'.$key}[0] <> '') $tmp_filter[] = " `product_model_weight` >= {${'arr_range_'.$key}[0]}";
							if(${'arr_range_'.$key}[1] <> '') $tmp_filter[] = " `product_model_weight` <= {${'arr_range_'.$key}[1]}";

							if(!empty($tmp_filter)){
								$SQL_filter_for_tmp .= " AND ((".join(' AND ', $tmp_filter).") OR `product_model_weight` = 0)";
							}
						}
						else{
							$tmp_filter = array();
							if(${'arr_range_'.$key}[0] <> '') $tmp_filter[] = " {$_column} >= {${'arr_range_'.$key}[0]}";
							if(${'arr_range_'.$key}[1] <> '') $tmp_filter[] = " {$_column} <= {${'arr_range_'.$key}[1]}";

							if(!empty($tmp_filter)){
								$SQL_filter .= " AND ((".join(' AND ', $tmp_filter).") OR {$_column} = 0)";
								$SQL_filter_for_tmp .= " AND ((".join(' AND ', $tmp_filter).") OR {$_column} = 0)";
							}
						}

						$_record_data[] = $key.'='.${'arr_range_'.$key}[0];
						$_record_data[] = "{$key}={${'arr_range_'.$key}[0]}-${'arr_range_'.$key}[1]";
					}
					else{
						$var_error = true;
					}
					break;

				//頁數參數
				case 'page':
					$now_page = $value;

					if(!empty($value)) $_record_data[] = 'page='.$value;
					break;

				//一頁幾個商品
				case 'items':
					$page_view = $value;
					break;

				//搜尋商品
				case 'search':
					$value = addslashes(trim(strip_tags($value)));

					if(!empty($value)){
						$str_search_sql = '';

						//已登入會員才需搜尋 - 20171011客戶確認移除功能
						//if(!empty($arr_member['member_uniqid'])){
							//店家搜尋
							$arr_search_store = $crud->sql("SELECT `store_id`
															FROM `store`
															WHERE `store_name{$_LANG}` LIKE ('%{$value}%')");
							$arr_search_IN = array();
							if(count($arr_search_store) > 0){
								foreach ($arr_search_store as $s_key => $s_value) {
									$arr_search_IN[] = $s_value['store_id'];
								}
							}

							if(count($arr_search_IN) > 0){
								$str_search_IN = "'".join("', '", $arr_search_IN)."'";
								$str_search_sql .= " OR `product_store` IN({$str_search_IN})";
							}

							//會員搜尋
							$arr_search_member = $crud->sql("SELECT `member_uniqid`
															FROM `member`
															WHERE IF(`member_nickname` = '', `member_name`, `member_nickname`) LIKE ('%{$value}%')");
							$arr_search_IN = array();
							if(count($arr_search_member) > 0){
								foreach ($arr_search_member as $s_key => $s_value) {
									$arr_search_IN[] = $s_value['member_uniqid'];
								}
							}

							if(count($arr_search_IN) > 0){
								$str_search_IN = "'".join("', '", $arr_search_IN)."'";
								$str_search_sql .= " OR `product_owner` IN({$str_search_IN})";
							}
						// }  //已登入會員才需搜尋 - 20171011客戶確認移除功能<--


						$SQL_filter .= " AND (
											`product_name{$_LANG}` LIKE ('%{$value}%') OR
											`product_code` LIKE ('%{$value}%') OR
											`model_number` LIKE ('%{$value}%') OR
											`cate_title` LIKE ('%{$value}%') OR
											`brand_name{$_LANG}` LIKE ('%{$value}%')
											{$str_search_sql}
										)";
						$SQL_filter_for_tmp .= " AND (
														`product_name{$_LANG}` LIKE ('%{$value}%') OR
														`product_code` LIKE ('%{$value}%') OR
														`product_model` LIKE ('%{$value}%') OR
														`cate_title` LIKE ('%{$value}%') OR
														`brand_name{$_LANG}` LIKE ('%{$value}%')
														{$str_search_sql}
													)";

						$_record_data[] = 'search='.$value;
					}
					break;

				//其他參數不處理
				default:
					$var_error = true;
					break;
			}

			if(!empty($value) && !$var_error){
				$arr_filter[$key] = $value;
				$arr_url[] = $key.'.'.$value;
			}
		}
	}

	switch ($_type) {
		case 'grid':
			$class = 'row list-unstyled boxstyle_grid';

			//方塊式
			$init_list = <<<HTML
								<li class="col-xs-6 col-sm-3">
									<div class="thumbnail np_box">
										<a href="./product/%product_id\$d"><img src="./uploadimages/product/%product_create_year\$d/%product_code\$s/%product_photo\$s" alt="%product_name\$s" class="img-responsive"></a>
										%product_sold\$s
										%product_new\$s
										<div class="caption text-center">
											<a href="./product/%product_id\$d"><h3>%product_name\$s</h3></a>
											<ul class="list-inline">
												<li>%product_year\$s</li>
												<li>%product_hours\$shr</li>
											</ul>
											<p>%product_country\$s</p>
											<div class="np_price"><p>%product_price\$s</p></div>
										</div>
									</div>
								</li>
HTML;
			break;

		default:
		case 'list':
			$class = 'list-unstyled boxstyle_list';

			//列表式
			/*<p>%product_country\$s</p>*/
			$init_list = <<<HTML
						<li>
							<div class="thumbnail np_box clearfix">
								<a href="./product/%product_code\$s" style="background-image: url(./uploadimages/product/%product_create_year\$d/%product_code\$s/%product_photo\$s);" title="%product_name\$s"></a>
								<div class="caption">
									<a href="./product/%product_code\$s"><h3>%product_name\$s</h3></a>
									<ul class="list-inline">
										%owner_info\$s
										%owner_addr\$s
									</ul>
									<ul class="list-inline">
										<li>%sales_info\$s</li>
									</ul>
									<ul class="list-inline">
										<li>%label_info\$s</li>
									</ul>
									<div class="np_price">%product_start_price\$s<p>%product_price\$s</p></div>
								</div>
							</div>
						</li>
HTML;
			break;
	}

	switch ($_type) {
		case 'grid':
			$class = 'row list-unstyled boxstyle_grid';

			//方塊式
			$ad_list = <<<HTML
								<li class="col-xs-6 col-sm-3">
									<div class="thumbnail np_box">
										<a href="./product/%product_id\$d"><img src="./uploadimages/product/%product_create_year\$d/%product_code\$s/%product_photo\$s" alt="%product_name\$s" class="img-responsive"></a>
										%product_sold\$s
										%product_new\$s
										<div class="caption text-center">
											<a href="./product/%product_id\$d"><h3>%product_name\$s</h3></a>
											<ul class="list-inline">
												<li>%product_year\$s</li>
												<li>%product_hours\$shr</li>
											</ul>
											<p>%product_country\$s</p>
											<div class="np_price"><p>%product_price\$s</p></div>
										</div>
									</div>
								</li>
HTML;
			break;

		default:
		case 'list':
			$class = 'list-unstyled boxstyle_list';

			//列表式
			/*<p>%product_country\$s</p>*/
			$ad_list = <<<HTML
						<li>
							<div class="thumbnail np_box clearfix">
								<a href="./product/%product_code\$s" style="background-image: url(./uploadimages/product/%product_create_year\$d/%product_code\$s/%product_photo\$s);" title="%product_name\$s"></a>
							</div>
						</li>
HTML;
			break;
	}

	// ----- 取出商品資料
	// 取得最近七天的商品
	$arr_new_product = $crud->sql("SELECT `product_code`
									FROM `product`
									WHERE `product_brand_new_unique` != 1 AND `product_online` = 1 AND `product_user_delete` = 0 AND `product_status` = 1 AND `product_create_date` > DATE_SUB(CURDATE(), INTERVAL 7 DAY)
									UNION ALL
									SELECT `product_code`
									FROM `product_tmp`
									WHERE `product_brand_new_unique` != 1 AND `product_online` = 1 AND `product_user_delete` = 0 AND `product_status` = 1 AND `product_create_date` > DATE_SUB(CURDATE(), INTERVAL 7 DAY)");
	$arr_is_new = array();
	if(count($arr_new_product) > 0){
		foreach ($arr_new_product as $key => $value) {
			$arr_is_new[] = $value['product_code'];
		}
	}
	// 最新的5個商品
	$arr_new_product = $crud->sql("SELECT *
									FROM (
											(
												SELECT `product_code`, `product_create_date`
												FROM `product`
												WHERE `product_brand_new_unique` != 1 AND `product_online` = 1 AND `product_user_delete` = 0 AND `product_status` = 1
											)
											UNION ALL
											(
												SELECT `product_code`, `product_create_date`
												FROM `product_tmp`
												WHERE `product_brand_new_unique` != 1 AND `product_online` = 1 AND `product_user_delete` = 0 AND `product_status` = 1
											)
										) AS `all_product`
									ORDER BY `product_create_date` DESC
									LIMIT 5");
	if(count($arr_new_product) > 0){
		foreach ($arr_new_product as $key => $value) {
			if(array_search($value['product_code'], $arr_is_new) === false){
				$arr_is_new[] = $value['product_code'];
			}
		}
	}

	//優先排序
	if(empty($SQL_sort)){
		switch ($_SESSION[SESSION_NAME.'_country']) {
			case 'Taiwan':
				$SQL_sort = " FIELD(`product_country`, 'HKG', 'TWN') DESC, ";
				break;
			case 'Hong Kong':
				$SQL_sort = " FIELD(`product_country`, 'TWN', 'HKG') DESC, ";
				break;
			default:
				break;
		}
//		$SQL_sort .= "`product_sort`";
		$SQL_sort .= "`product_create_date` DESC";
	}

	//付費會員產品
	if($arr_member['member_paid'] <> 1){
		$SQL_filter .= ' AND `product_paid` = 0';
	}

	//----- 分頁處理
	$count_page = $crud->sql("SELECT SUM(`count_page`) AS `count_page`
							FROM (
									SELECT COUNT(*) AS `count_page`
									FROM `product`
									INNER JOIN `product_price_currency` ON `product_price_currency`.`product_id` = `product`.`product_id`
									INNER JOIN `model` ON `product_model` = `model_id`
									INNER JOIN `model_cate` ON `cate_id` = `model_cate`
									INNER JOIN `brand` ON `model_brand` = `brand_id`
									WHERE `product_brand_new_unique` != 1 AND `product_online` = 1 AND `product_user_delete` = 0 AND `product_status` = 1 AND `product_price_currency`.`product_currency` = '{$_currency}' {$SQL_filter} {$SQL_filter_brand}

									UNION ALL
									(
										SELECT COUNT(*) AS `count_page`
										FROM `product_tmp`
										INNER JOIN `product_tmp_price_currency` ON `product_tmp_price_currency`.`product_id` = `product_tmp`.`product_id`
										INNER JOIN `model_cate` ON `cate_id` = `product_cate`
										INNER JOIN `brand` ON `product_brand` = `brand_id`
										WHERE `product_brand_new_unique` != 1 AND `product_online` = 1 AND `product_user_delete` = 0 AND `product_status` = 1 AND `product_tmp_price_currency`.`product_currency` = '{$_currency}' {$SQL_filter_for_tmp} {$SQL_filter_brand_for_tmp}
									)
								) AS `all_product`")[0]['count_page'];	//計算筆數
	$pagesize = (empty($page_view))? 16:$page_view;    //一頁顯示筆數
	$now_page = (empty($now_page) || $now_page > $count_page)? 1:$now_page;
	$begin_num = ($now_page - 1) * $pagesize;	//計算開始筆數
	$tot_page = ceil($count_page/$pagesize);    //總頁數

	//一列顯示幾個item
	$_oneline_items = 5;
	$_page_link = './products/page.';

	$_pagination = '';
	if($tot_page > 1){
		//上一頁處理
		$prev_page = $now_page - 1;
		$prev = ($now_page > 1)? 'data-page="'.$prev_page.'"':'';
		$prev_link = ($now_page > 1)? "{$_page_link}{$prev_page}":'javascript:;';

		//下一頁處理
		$next_page = $now_page + 1;
		$next = ($now_page < $tot_page)? 'data-page="'.$next_page.'"':'';
		$next_link = ($now_page < $tot_page)? "{$_page_link}{$next_page}":'javascript:;';

		//起始頁處理
		$start = ($now_page > 1)? 'data-page="1"':'';
		$start_link = ($now_page > 1)? $_page_link.'1':'javascript:;';

		//結束頁處理
		$end = ($now_page < $tot_page)? 'data-page="'.$tot_page.'"':'';
		$end_link = ($now_page < $tot_page)? $_page_link.$tot_page:'javascript:;';

		$_pagination .= <<<HTML
							<li {$start}><a href="{$start_link}"><i class="fa fa-angle-double-left"></i></a></li>
							<li {$prev}><a href="{$prev_link}"><i class="fa fa-angle-left fa-lg"></i></a></li>
HTML;

		$_half_items = floor($_oneline_items / 2);
		$fix_base = ($_oneline_items % 2 == 0)? 2:1;
		$base = $now_page - $_half_items;
		if($tot_page - $now_page < $_half_items) $base = $tot_page - ($_oneline_items - $fix_base);
		if($base < 1) $base = 1;

		$max = ($now_page <= $_half_items)? $_oneline_items:$now_page + $_half_items;
		if($max > $tot_page) $max = $tot_page;

		for($i = $base; $i <= $max; $i++){
			if($i > $tot_page) break;
			$sel_page = ($i <> $now_page)? 'data-page="'.$i.'"':'';
			$active = ($i == $now_page)? 'class="on"':'';
			$_link = ($i == $now_page)? 'javascript:;':$_page_link.$i;
			$_pagination .= <<<HTML
							<li {$sel_page} {$active}><a href="{$_link}">{$i}</a></li>
HTML;
		}
		$_pagination .= <<<HTML
							<li {$next}><a href="{$next_link}"><i class="fa fa-angle-right fa-lg"></i></a></li>
							<li {$end}><a href="{$end_link}"><i class="fa fa-angle-double-right"></i></a></li>
HTML;
	}
	$arr['pagination'] = $_pagination;
	$arr_product = $crud->sql("SELECT *
								FROM (
										(
											SELECT 'curr' AS `product_mode`, `product_create_date`, `model_brand`, `product_price_currency`.`product_start_price`, `product_store`, `product_owner`, `product_sold`, `product`.`product_id`, `product_name`, `product_name_en`, `product_epd_nrmm_label`, `product_photo`, `product_country`, `product_year`, `product_hours`, `model_weight`, `product_code`, `product_create_year`,
													IFNULL(
														(SELECT `product_selling_price_currency`.`selling_price`
														FROM `product_selling_price`
														INNER JOIN `product_selling_price_currency` ON `product_selling_price`.`selling_id` = `product_selling_price_currency`.`selling_id`
														WHERE `product_selling_price`.`product_id` = `product`.`product_id` AND `selling_online` = 1 AND CONCAT(',', `selling_target`, ',') LIKE '%,{$arr_member['member_id']},%' AND `product_selling_price_currency`.`product_currency` = '{$_currency}'
														ORDER BY `selling_sort`
														LIMIT 1),
														`product_price_currency`.`product_default_price`
													) AS `product_selling_price`
											FROM `product`
											INNER JOIN `product_price_currency` ON `product_price_currency`.`product_id` = `product`.`product_id`
											INNER JOIN `model` ON `product_model` = `model_id`
											INNER JOIN `model_cate` ON `cate_id` = `model_cate`
											INNER JOIN `brand` ON `model_brand` = `brand_id`
											WHERE `product_brand_new_unique` != 1 AND `product_online` = 1 AND `product_user_delete` = 0 AND `product_status` = 1 AND `product_price_currency`.`product_currency` = '{$_currency}' {$SQL_filter} {$SQL_filter_brand}
										)
										UNION ALL
										(
											SELECT 'tmp' AS `product_mode`, `product_create_date`, `product_brand` AS `model_brand`, `product_tmp_price_currency`.`product_start_price`, `product_store`, `product_owner`, 0 AS `product_sold`, `product_tmp`.`product_id`, `product_name`, `product_name_en`, `product_epd_nrmm_label`, `product_photo`, `product_country`, `product_year`, `product_hours`, `product_model_weight` AS `model_weight`, `product_code`, `product_create_year`, `product_tmp_price_currency`.`product_default_price` AS `product_selling_price`
											FROM `product_tmp`
											INNER JOIN `product_tmp_price_currency` ON `product_tmp_price_currency`.`product_id` = `product_tmp`.`product_id`
											INNER JOIN `model_cate` ON `cate_id` = `product_cate`
											INNER JOIN `brand` ON `product_brand` = `brand_id`
											WHERE `product_brand_new_unique` != 1 AND `product_online` = 1 AND `product_user_delete` = 0 AND `product_status` = 1 AND `product_tmp_price_currency`.`product_currency` = '{$_currency}' {$SQL_filter_for_tmp} {$SQL_filter_brand_for_tmp}
										)
									) AS `all_product`
								ORDER BY {$SQL_sort}
								LIMIT {$begin_num}, {$pagesize}");

	$str_product = '';
	$number_format = 'number_format';
	$arr_count_brand = array();
	$ad_cnt = 0
	if(count($arr_product) > 0){
		foreach ($arr_product as $key => $value) {
			//品牌處理
			$arr_count_brand[$value['model_brand']][] = $value['product_id'];

			$product_default_price = $value['product_selling_price'];
			$product_start_price = $value['product_start_price'];

			$product_sold = ($value['product_sold'] == 1)? '<a class="m_tag grey">Sold</a>':'';
			$product_new = ((array_search($value['product_code'], $arr_is_new) !== false) && empty($product_sold))? '<a class="m_tag red_back">New</a>':'';

			$product_nrmm_g = ((strpos($value['product_epd_nrmm_label'], 'NRMMG')) !== false)? '<a class="m_tag green_back">NRMM</a>':'';
			$product_nrmm_y = ((strpos($value['product_epd_nrmm_label'], 'NRMMY')) !== false)? '<a class="m_tag yellow_back">NRMM</a>':'';
			$product_qpme = ((strpos($value['product_epd_nrmm_label'], 'QPME')) !== false)? '<a class="m_tag purple_back">QPME</a>':'';
			
			//店家資訊
			$_owner_line_id = $_owner_fb_id = $_owner_tel_show = $_owner_tel = $_owner_location = '';
			if(empty($value['product_store'])){
				$_owner_member = $crud->getid('member', array('member_uniqid' => $value['product_owner']));
				$_owner_name = (empty($_owner_member['member_nickname']))? $_owner_member['member_name']:$_owner_member['member_nickname'];
				$_owner_tel = $_owner_member['member_mobile1'];
				$_owner_tel_show = (empty($_owner_member['member_mobile_show']))? $_owner_member['member_mobile1']:$_owner_member['member_mobile_show'];
				$_owner_line_id = $_owner_member['member_lineID'];
				$_owner_fb_id = $_owner_member['member_fb_id'];
			}
			else{
				$_owner_store = $crud->getid('store', array('store_id' => $value['product_store']));
				$_owner_name = $_owner_store['store_name'];
				$_owner_tel = $_owner_store['store_tel'];
				$_owner_tel_show = $_owner_store['store_tel_show'];
		
				//地址處理
				if($_owner_store['store_country'] == 'TWN'){
					$_owner_location = $_GET_LANG['taiwan'].' '.$_owner_store['store_city'].$_owner_store['store_county'];
					$str_addr = $_owner_store['store_zipcode'].$_owner_store['store_city'].$_owner_store['store_county'].$_owner_store['store_addr'];
				}
				else if($_owner_store['store_country'] == 'HKG'){
					$_owner_location = $_GET_LANG['hong_kong'];
					$str_addr = $_owner_store['store_addr'];
				}
				else{
					$arr_country = $crud->getid('country_code', array('country_letter3_code' => $_owner_store['store_country']));
					$_country_lang = ($_LANG == '')? '_ch':'';
					$_owner_location = $arr_country['country_name'.$_country_lang];
					$str_addr = $_owner_store['store_addr'];
				}
			}

			//店家資訊
			$_owner_info = '';
			if(!empty($_owner_name)){
				if(empty($value['product_store'])){
					$_owner_info .= <<<HTML
									<li>{$_owner_name}</li>
HTML;
				}
				else {
					$_owner_info .= <<<HTML
									<li><a href="./{$_owner_store['store_code']}">{$_owner_name}</a></li>
HTML;
				}
			}
			/*
			if(!empty($_owner_tel)){
				$_owner_info .= <<<HTML
								<li><span class="list_tit">{$_GET_LANG['seller_tel']}：</span><a href="tel:{$_owner_tel}">{$_owner_tel_show}</a></li>
HTML;
			}

			if(!empty($_owner_line_id)){
				$_owner_info .= <<<HTML
								<li><span class="list_tit">LINE ID：</span>{$_owner_line_id}</a></li>
HTML;
			}
			if(!empty($_owner_fb_id)){
				$_owner_info .= <<<HTML
								<li><span class="list_tit">FB Message：</span><a href="https://www.facebook.com/{$_owner_fb_id}" target="_blank">Facebook Message</a></li>
HTML;
			}
			*/
			$_owner_addr='';
			if(!empty($_owner_location)){
				$_owner_addr.= <<<HTML
								<li>
									<a href="https://www.google.com.tw/maps?q={$str_addr}" target="_blank"> {$_owner_location}</a>
								<li>
HTML;
			}

			if($product_default_price < 1 || $product_default_price == $product_start_price){
				$product_start_price = '';
			}
			else{
				$product_start_price = <<<HTML
						<del>{$_currency} {$number_format($product_start_price, 0)}</del>
HTML;
			}
            $_product_price_display = ($product_default_price < 1)? $_GET_LANG['store_call_us']:"{$_currency} ".number_format($product_default_price, 0);
            $_product_price_display = ($value['product_sold'] == 1)? $_GET_LANG['store_sold']:$_product_price_display;
            $_sales_info = (empty($value['product_year']))? '':$value['product_year'].$_GET_LANG['year'].' ';
            $_sales_info .= (empty($value['product_hours']))? '':$value['product_hours'].'hr'.' ';
            $_sales_info .= $product_sold;
            $_sales_info .= $product_new;

            $_label_info = $product_nrmm_g;
            $_label_info .= $product_nrmm_y;
            $_label_info .= $product_qpme;
			$arr_data = array(
							'product_id' => $value['product_id'],
							'product_code' => $value['product_code'],
							'product_name' => $value['product_name'.$_LANG].$value['product_epd_nrmm_label'],
							'product_photo' => $value['product_photo'],
							'product_price' => $_product_price_display,
							'product_start_price' => $product_start_price,
							'product_sold' => $product_sold,
							'product_new' => $product_new,
							'product_country' => $value['product_country'],
							'product_year' => (empty($value['product_year']))? '':$value['product_year'].$_GET_LANG['year'],
							'product_hours' => (empty($value['product_hours']))? '':'<li>'.$value['product_hours'].'hr</li>',
							'model_weight' => $value['model_weight'],
							'product_code' => $value['product_code'],
							'product_create_year' => $value['product_create_year'],
							//'owner_info' => (!empty($arr_member['member_id']))? $_owner_info:'<li><button onclick="javascript:location.href=\'./register\';" class="btn btn-default">'.$_GET_LANG['get_store_info'].'</button></li>'	// 20170406非會員隱藏賣家資訊
							'sales_info' => $_sales_info , // 20170605 peter modify
							'label_info' => $_label_info, // 20170605 peter modify
							'owner_info' => $_owner_info, // 20170605 peter modify
							'owner_addr' => $_owner_addr// 20170605 peter modify
						);
			$str_product .= vksprintf($init_list, $arr_data);

			
			$ad_cnt++;
			if (($ad_cnt%5) == 0) {
				$str_product .= vksprintf($ad_list, $arr_data);
			}
		}
	}

	//品牌
	$arr_brand = $crud->sql("SELECT *,COUNT(*) AS `count_model`
							FROM (
									(
										SELECT `brand`.*
										FROM `product`
										INNER JOIN `product_price_currency` ON `product_price_currency`.`product_id` = `product`.`product_id`
										INNER JOIN `model` ON `product_model` = `model_id`
										INNER JOIN `model_cate` ON `cate_id` = `model_cate`
										INNER JOIN `brand` ON `brand_id` = `model_brand`
										WHERE `product_brand_new_unique` != 1 AND `product_online` = 1 AND `product_user_delete` = 0 AND `product_status` = 1 AND `brand_online` = 1 AND `product_price_currency`.`product_currency` = '{$_currency}' {$SQL_filter}
									)
									UNION ALL
									(
										SELECT `brand`.*
										FROM `product_tmp`
										INNER JOIN `product_tmp_price_currency` ON `product_tmp_price_currency`.`product_id` = `product_tmp`.`product_id`
										INNER JOIN `model_cate` ON `cate_id` = `product_cate`
										INNER JOIN `brand` ON `brand_id` = `product_brand`
										WHERE `product_brand_new_unique` != 1 AND `product_online` = 1 AND `product_user_delete` = 0 AND `product_status` = 1 AND `brand_online` = 1 AND `product_tmp_price_currency`.`product_currency` = '{$_currency}' {$SQL_filter_for_tmp}
									)
								) AS `all_product`
							GROUP BY `brand_id`
							ORDER BY `brand_sort`");
	$str_brand = $str_brand_option = '';
	if(count($arr_brand) > 0){
		$num = 1;
		foreach ($arr_brand as $key => $value) {
			$count_model = number_format($value['count_model']);
			if($key < 6){
				$str_brand .= <<<HTML
						<li><a href="./products/brand.{$value['brand_id']}" class="btn-filter" data-id="{$value['brand_id']}">{$value['brand_name'.$_LANG]}</a> ({$count_model})</li>
HTML;
			}

			${'str_brand_'.$num} .= <<<HTML
						<li><a href="./products/brand.{$value['brand_id']}" class="btn-filter" data-id="{$value['brand_id']}">{$value['brand_name'.$_LANG]}</a> ({$count_model})</li>
HTML;
			$num++;
			if($num > 3) $num = 1;

			$str_brand_option .= <<<HTML
						<option value="{$value['brand_id']}">{$value['brand_name'.$_LANG]}</option>
HTML;
		}
	}

	$str_brand = <<<HTML
				<li><a href="javascript:;" class="btn-filter" data-id="">{$_GET_LANG['all']}</a></li>
				{$str_brand}
HTML;
	for ($i = 1; $i < 4; $i++) {
		$str_brand_all .= <<<HTML
					<div class="col-md-4">
						<ul class="sidebar_list filter_ul" data-mode="brand">
							${'str_brand_'.$i}
						</ul>
					</div>
HTML;
	}

	$arr_brand_select = <<<HTML
		<select class="form-control">
			<option value="" selected>{$_GET_LANG['all']}</option>
			{$str_brand_option}
		</select>
HTML;
	$arr['brand'] = array($str_brand, $str_brand_all, $arr_brand_select);

	// 新增會員行為紀錄
	if(!empty($arr_member['member_id'])){
		include_once("include/class.analytics.php");
		$analytics = new member_analytics();
		$arr_data = array(
							'data_type' => 1,			//裝置類型(1-WEB;2-APP)
							'data_action_type' => 1,	//操作類型(1-篩選產品;2-瀏覽產品;3-篩選店家;4-瀏覽店家;5-全站搜尋)
							'data_target' => join('&', $_record_data)
						);
		$_record_id = $analytics->chk_record($arr_data);

		//產生離開頁面事件，紀錄停留時間
		$encrypt_id = str_replace('=', '', encrypt($_record_id, 'O1O3MOeJnp'));
		$record_JS = <<<HTML
			<script>
				$(function(){
					window.onbeforeunload = function(){
						doajax({
							url: './unload_page',
							async:false,
							data: 'id={$encrypt_id}'
						});
					}
				})
			</script>
HTML;
	}

	$_url = join('/', $arr_url);
	$_url = (empty($_url))? '':'/'.$_url;
	$arr['url'] = './products'.$_url;
	$arr['url_data'] = $arr_filter;
	$str_product = <<<HTML
						<ul class="{$class} open">{$str_product}</ul>
HTML;
	$arr['data'] = $str_product.$record_JS;
	echo json_encode($arr);
?>