<?php
	$product_mode = substr($_var1, strlen($_var1) - 1);
	$is_tmp_product = ($product_mode == 'T');

	//產品資料
	if($is_tmp_product){
		$arr_product = $crud->sql_prepare("SELECT *, `product_cate` AS `model_cate`, `product_model_weight` AS `model_weight`
										FROM `product_tmp`
										INNER JOIN `model_cate` ON `product_cate` = `cate_id`
										INNER JOIN `brand` ON `product_brand` = `brand_id`
										WHERE `product_code` = :product_code AND `product_online` = 1 AND `product_user_delete` = 0",
										array('product_code' => $_var1))[0];
	}
	else{
		$arr_product = $crud->sql_prepare("SELECT *
										FROM `product`
										INNER JOIN `model` ON `product_model` = `model_id`
										INNER JOIN `model_cate` ON `model_cate` = `cate_id`
										INNER JOIN `brand` ON `model_brand` = `brand_id`
										WHERE `product_code` = :product_code AND `product_online` = 1 AND `product_user_delete` = 0",
										array('product_code' => $_var1))[0];
	}
	if(empty($arr_product['product_id'])){
		header("location: /{$_BASE}products");
		exit;
	}

	//若為登入狀態，確認是否可瀏覽
	$str_check_product = '';
	if($arr_product['product_status'] == 0){
		if(!empty($_SESSION[SESSION_NAME.'_user_uniqid']) && $arr_product['product_owner'] == $_SESSION[SESSION_NAME.'_user_uniqid']){
			$str_check_product = <<<HTML
						<h4>{$_GET_LANG['check_product_title']}</h4>
						<div class="pro_contact">
							<div class="sales_yes">
								<div class="textedit">
									{$_GET_LANG['check_product_info']}
								</div>
								<button type="button" data-id="{$arr_product['product_code']}" class="btn btn-block btn-black btn_check_product">{$_GET_LANG['check_product']}</button>
							</div>
						</div>
HTML;
		}
		else{
			header("location: /{$_BASE}products");
			exit;
		}
	}

	//確認是否有上層分類
	if($arr_product['cate_par_id'] <> 0){
		$arr_par_cate = $crud->getid('model_cate', array('cate_id' => $arr_product['cate_par_id']));
		$str_breadcrumb = <<<HTML
						<li><a href="./products/category.{$arr_par_cate['cate_id']}">{$arr_par_cate['cate_title'.$_LANG]}</a></li>
HTML;
	}

	//地區處理
	switch ($arr_product['product_country']) {
		case 'TWN':
			$_location = $_GET_LANG['taiwan'];
			break;
		case 'HKG':
			$_location = $_GET_LANG['hong_kong'];
			break;
		default:
			$arr_country = $crud->getid('country_code', array('country_letter3_code' => $arr_product['product_country']));
			$_country_lang = ($_LANG == '')? '_ch':'';
			$_location = $arr_country['country_name'.$_country_lang];
			break;
	}

	//取得會員價
	$arr_member = $crud->getid('member', array('member_uniqid' => $_SESSION[SESSION_NAME.'_user_uniqid']));
	if($is_tmp_product){
		$arr_currency = $crud->sql("SELECT *
									FROM `product_tmp_price_currency`
									WHERE `product_id` = {$arr_product['product_id']}");
		foreach ($arr_currency as $key => $value) {
			$arr_product_price[$value['product_currency']] = $value['product_default_price'];
		}
	}
	else{
		$arr_product_selling_price = array();
		if(!empty($arr_member['member_id'])){
			$arr_product_selling_price = $crud->sql("SELECT *
									FROM `product_selling_price`
									WHERE `product_id` = {$arr_product['product_id']} AND CONCAT(',', `selling_target`, ',') LIKE '%,{$arr_member['member_id']},%'
									ORDER BY `selling_sort`
									LIMIT 1")[0];
		}

		$arr_product_price = array();
		if(empty($arr_product_selling_price['selling_id'])){
			$arr_currency = $crud->sql("SELECT *
									FROM `product_price_currency`
									WHERE `product_id` = {$arr_product['product_id']}");
			foreach ($arr_currency as $key => $value) {
				$arr_product_price[$value['product_currency']] = $value['product_default_price'];
			}
		}
		else{
			$arr_currency = $crud->sql("SELECT *
									FROM `product_selling_price_currency`
									WHERE `selling_id` = {$arr_product_selling_price['selling_id']}");
			foreach ($arr_currency as $key => $value) {
				$arr_product_price[$value['product_currency']] = $value['selling_price'];
			}
		}
	}

	//產品相簿
	$album_table = ($is_tmp_product)? 'product_tmp_album_basic':'product_album_basic';
	$arr_product_album_basic = $crud->select($album_table, array('host_id' => $arr_product['product_id']), array('album_sort' => 'ASC'));
	$str_product_album_basic = $str_cover = '';
	$arr_og_images = array(PROTOCOL."://{$_SERVER['HTTP_HOST']}/".BASE."uploadimages/product/{$arr_product['product_create_year']}/{$arr_product['product_code']}/{$arr_product['product_photo']}");
	if(count($arr_product_album_basic) > 0){
		foreach ($arr_product_album_basic as $key => $value) {
			if(empty($str_cover)) $str_cover = "./uploadimages/product/{$arr_product['product_create_year']}/{$arr_product['product_code']}/{$value['album_photo']}";
			$str_product_album_basic .= <<<HTML
								<li>
									<a href="./uploadimages/product/{$arr_product['product_create_year']}/{$arr_product['product_code']}/{$value['album_photo']}" class="fancybox-thumb" rel="fancybox-thumb" title="{$arr_product['model_number']}">
										<p class="imgbox" style="background-image: url(./uploadimages/product/{$arr_product['product_create_year']}/{$arr_product['product_code']}/{$value['album_photo']});  background-size: cover;"></p>
									</a>
								</li>
HTML;
			//og:image
			$arr_og_images[] = PROTOCOL."://{$_SERVER['HTTP_HOST']}/".BASE."uploadimages/product/{$arr_product['product_create_year']}/{$arr_product['product_code']}/{$value['album_photo']}";
		}
	}

	//meta title, url, description, image 處理
	$_customize_title = $arr_product['product_name'.$_LANG].' :: ';
	$_customize_url = 'product/'.$arr_product['product_code'];
	// $_customize_description = mb_strimwidth(str_replace("\r\n", '', strip_tags(htmlspecialchars_decode($arr_store['store_info']))), 0, 140, '...', 'UTF-8');
	$_customize_og_image = '';
	if(count($arr_og_images) > 0){
		foreach ($arr_og_images as $value) {
			$_customize_og_image .= '<meta property="og:image" content="'.$value.'">';
		}
	}

	//付費會員相簿
	if($arr_member['member_paid'] == 1 && !$is_tmp_product){
		$arr_product_album = $crud->select('product_album', array('host_id' => $arr_product['product_id']), array('album_sort' => 'ASC'));
		if(count($arr_product_album) > 0){
			foreach ($arr_product_album as $key => $value) {
				$str_product_album_basic .= <<<HTML
									<li>
										<a href="./uploadimages/product/{$arr_product['product_create_year']}/{$arr_product['product_code']}/{$value['album_photo']}" class="fancybox-thumb" rel="fancybox-thumb" title="{$arr_product['model_number']}">
											<p class="imgbox" style="background-image: url(./uploadimages/product/{$arr_product['product_create_year']}/{$arr_product['product_code']}/{$value['album_photo']});  background-size: cover;"></p>
										</a>
									</li>
HTML;
			}
		}
	}


	//店家資訊
	//20170605 peter modify
	// if(!empty($arr_member['member_id'])){
		$_owner_line_id = $_owner_fb_id = $_owner_tel = $_owner_tel_show = $_owner_location = $_owner_fax = '';
		if(empty($arr_product['product_store'])){
			$_owner_member = $crud->getid('member', array('member_uniqid' => $arr_product['product_owner']));
			$_owner_name = (empty($_owner_member['member_nickname']))? $_owner_member['member_name']:$_owner_member['member_nickname'];
			$_owner_tel = $_owner_member['member_mobile1'];
			$_owner_tel_show = (empty($_owner_member['member_mobile_show']))? $_owner_member['member_mobile1']:$_owner_member['member_mobile_show'];
			$_owner_line_id = $_owner_member['member_lineID'];
			$_owner_fb_id = $_owner_member['member_fb_id'];
		}
		else{
			$_owner_store = $crud->getid('store', array('store_id' => $arr_product['product_store']));
			$_owner_name = $_owner_store['store_name'];
			$_owner_tel = $_owner_store['store_tel'];
			$_owner_tel_show = $_owner_store['store_tel_show'];
			$_owner_fax = $_owner_store['store_fax'];

			//地址處理
			if($_owner_store['store_country'] == 'TWN'){
				$_owner_location = $_GET_LANG['taiwan'].' '.$_owner_store['store_city'].$_owner_store['store_county'];
			}
			else if($_owner_store['store_country'] == 'HKG'){
				$_owner_location = $_GET_LANG['hong_kong'];
			}
			else{
				$arr_country = $crud->getid('country_code', array('country_letter3_code' => $_owner_store['store_country']));
				$_country_lang = ($_LANG == '')? '_ch':'';
				$_owner_location = $arr_country['country_name'.$_country_lang];
			}

			$_owner_location .= $_owner_store['store_addr'];
		}

		$_tmp_contact = '';
		if(!empty($_owner_tel)){
			$_tmp_contact .= <<<HTML
				<li><span class="list_tit">{$_GET_LANG['seller_tel']}：</span><a href="tel:{$_owner_tel}">{$_owner_tel_show}</a></li>
HTML;
		}
		if(!empty($_owner_location)){
			$_tmp_contact .= <<<HTML
				<li>
					<span class="list_tit">{$_GET_LANG['seller_address']}：</span><br/>
					<div class="infolist_other"><i class="fa fa-map-marker" aria-hidden="true"></i> <a href="https://www.google.com.tw/maps?q={$_owner_location}" target="_blank">{$_owner_location}</a></div>
				</li>
HTML;
		}

		if(!empty($_owner_line_id)){
			$_tmp_contact .= <<<HTML
							<li><span class="list_tit">LINE ID：</span>{$_owner_line_id}</a></li>
HTML;
		}
		if(!empty($_owner_fb_id)){
			$_tmp_contact .= <<<HTML
							<li><span class="list_tit">FB Message：</span><a href="https://www.facebook.com/{$_owner_fb_id}" target="_blank">Facebook Message</a></li>
HTML;
		}
		if(empty($arr_product['product_store'])){
			$str_contact = <<<HTML
						<li><span class="list_tit">{$_GET_LANG['seller_name']}：</span>{$_owner_name}</li>
						{$_tmp_contact}
HTML;
		}
		else {
			$str_contact = <<<HTML
						<li><span class="list_tit">{$_GET_LANG['seller_name']}：</span><a href="./{$_owner_store['store_code']}">{$_owner_name}</a></li>
						{$_tmp_contact}
HTML;
		}
// 	}
// 	else{
// 		$str_contact = <<<HTML
// 					<li><button onclick="javascript:location.href='./register';" class="btn btn-default">{$_GET_LANG['get_store_info']}</button></li>
// HTML;
// 	}

	//類似機械
	$join = 'join';
	$arr_not_in = array($arr_product['product_id']);
	$arr_product_similar = $crud->sql("SELECT *
										FROM (
												(
													SELECT `product_price_currency`.`product_default_price`, `product`.`product_id`, `product_photo`, `product_code`, `product_create_year`, `product_name{$_LANG}` AS `product_name`,
															IFNULL(
																(SELECT `product_selling_price_currency`.`selling_price`
																FROM `product_selling_price`
																INNER JOIN `product_selling_price_currency` ON `product_selling_price`.`selling_id` = `product_selling_price_currency`.`selling_id`
																WHERE `product_selling_price`.`product_id` = `product`.`product_id` AND `selling_online` = 1 AND CONCAT(',', `selling_target`, ',') LIKE '%,{$arr_member['member_id']},%' AND `product_selling_price_currency`.`product_currency` = '{$_currency}'
																ORDER BY `selling_sort`
																LIMIT 1),
																`product_price_currency`.`product_default_price`
															) AS `product_selling_price`
													FROM `product`
													INNER JOIN `product_price_currency` ON `product_price_currency`.`product_id` = `product`.`product_id`
													INNER JOIN `model` ON `product_model` = `model_id`
													WHERE `product_online` = 1 AND `product_user_delete` = 0 AND `product_status` = 1 AND `model_cate` = '{$arr_product['model_cate']}' AND `model_weight` = '{$arr_product['model_weight']}' AND `product_price_currency`.`product_currency` = '{$_currency}' AND
														IFNULL(
																(SELECT `product_selling_price_currency`.`selling_price`
																FROM `product_selling_price`
																INNER JOIN `product_selling_price_currency` ON `product_selling_price`.`selling_id` = `product_selling_price_currency`.`selling_id`
																WHERE `product_selling_price`.`product_id` = `product`.`product_id` AND `selling_online` = 1 AND CONCAT(',', `selling_target`, ',') LIKE '%,{$arr_member['member_id']},%' AND `product_selling_price_currency`.`product_currency` = '{$_currency}'
																ORDER BY `selling_sort`
																LIMIT 1),
																`product_price_currency`.`product_default_price`
														) <= {$arr_product_price[$_currency]} AND `product`.`product_id` NOT IN ({$join($arr_not_in, ',')})
												)
												UNION ALL
												(
													SELECT `product_tmp_price_currency`.`product_default_price`, `product_tmp`.`product_id`, `product_photo`, `product_code`, `product_create_year`, `product_name` AS `product_name`, `product_tmp_price_currency`.`product_default_price` AS `product_selling_price`
													FROM `product_tmp`
													INNER JOIN `product_tmp_price_currency` ON `product_tmp_price_currency`.`product_id` = `product_tmp`.`product_id`
													WHERE `product_online` = 1 AND `product_user_delete` = 0 AND `product_status` = 1 AND `product_cate` = '{$arr_product['model_cate']}' AND `product_model_weight` = '{$arr_product['model_weight']}' AND `product_tmp_price_currency`.`product_currency` = '{$_currency}' AND `product_tmp_price_currency`.`product_default_price` <= {$arr_product_price[$_currency]} AND `product_tmp`.`product_id` NOT IN ({$join($arr_not_in, ',')})
												)
											) AS `all_product`
										ORDER BY `product_default_price` ASC
										LIMIT 2");
	$str_product_similar = '';
	if(count($arr_product_similar) > 0){
		foreach ($arr_product_similar as $key => $value) {
			$_photo = str_replace('.', '_c.', $value['product_photo']);
			$arr_not_in[] = $value['product_id'];
			$_product_price = ($value['product_selling_price'] < 1)? $_GET_LANG['store_call_us']:"{$_currency} ".number_format($value['product_selling_price'], 0);
			$_product_price = ($value['product_sold'] == 1)? $_GET_LANG['store_sold']:$_product_price;
			$str_product_similar .= <<<HTML
									<li class="clearfix">
										<a href="./product/{$value['product_code']}" style="background-image: url(./uploadimages/product/{$value['product_create_year']}/{$value['product_code']}/{$_photo});  background-size: cover;"></a>
										<ul class="same_list_info">
											<li><a href="./product/{$value['product_code']}">{$value['product_name']}</a></li>
											<li>{$_product_price}</li>
										</ul>
									</li>
HTML;
		}
	}
	$arr_product_similar = $crud->sql("SELECT *
										FROM (
												(
													SELECT `product_price_currency`.`product_default_price`, `product`.`product_id`, `product_photo`, `product_code`, `product_create_year`, `product_name{$_LANG}` AS `product_name`,
															IFNULL(
																(SELECT `product_selling_price_currency`.`selling_price`
																FROM `product_selling_price`
																INNER JOIN `product_selling_price_currency` ON `product_selling_price`.`selling_id` = `product_selling_price_currency`.`selling_id`
																WHERE `product_selling_price`.`product_id` = `product`.`product_id` AND `selling_online` = 1 AND CONCAT(',', `selling_target`, ',') LIKE '%,{$arr_member['member_id']},%' AND `product_selling_price_currency`.`product_currency` = '{$_currency}'
																ORDER BY `selling_sort`
																LIMIT 1),
																`product_price_currency`.`product_default_price`
															) AS `product_selling_price`
													FROM `product`
													INNER JOIN `product_price_currency` ON `product_price_currency`.`product_id` = `product`.`product_id`
													INNER JOIN `model` ON `product_model` = `model_id`
													WHERE `product_online` = 1 AND `product_user_delete` = 0 AND `product_status` = 1 AND `model_cate` = '{$arr_product['model_cate']}' AND `model_weight` = '{$arr_product['model_weight']}' AND `product_price_currency`.`product_currency` = '{$_currency}' AND
														IFNULL(
																(SELECT `product_selling_price_currency`.`selling_price`
																FROM `product_selling_price`
																INNER JOIN `product_selling_price_currency` ON `product_selling_price`.`selling_id` = `product_selling_price_currency`.`selling_id`
																WHERE `product_selling_price`.`product_id` = `product`.`product_id` AND `selling_online` = 1 AND CONCAT(',', `selling_target`, ',') LIKE '%,{$arr_member['member_id']},%' AND `product_selling_price_currency`.`product_currency` = '{$_currency}'
																ORDER BY `selling_sort`
																LIMIT 1),
																`product_price_currency`.`product_default_price`
														) >= {$arr_product_price[$_currency]} AND `product`.`product_id` NOT IN ({$join($arr_not_in, ',')})
												)
												UNION ALL
												(
													SELECT `product_tmp_price_currency`.`product_default_price`, `product_tmp`.`product_id`, `product_photo`, `product_code`, `product_create_year`, `product_name` AS `product_name`, `product_tmp_price_currency`.`product_default_price` AS `product_selling_price`
													FROM `product_tmp`
													INNER JOIN `product_tmp_price_currency` ON `product_tmp_price_currency`.`product_id` = `product_tmp`.`product_id`
													WHERE `product_online` = 1 AND `product_user_delete` = 0 AND `product_status` = 1 AND `product_cate` = '{$arr_product['model_cate']}' AND `product_model_weight` = '{$arr_product['model_weight']}' AND `product_tmp_price_currency`.`product_currency` = '{$_currency}' AND `product_tmp_price_currency`.`product_default_price` <= {$arr_product_price[$_currency]} AND `product_tmp`.`product_id` NOT IN ({$join($arr_not_in, ',')})
												)
											) AS `all_product`
										ORDER BY `product_default_price` ASC
										LIMIT 2");

	if(count($arr_product_similar) > 0){
		foreach ($arr_product_similar as $key => $value) {
			$_photo = str_replace('.', '_c.', $value['product_photo']);
			$_product_price = ($value['product_selling_price'] < 1)? $_GET_LANG['store_call_us']:"{$_currency} ".number_format($value['product_selling_price'], 0);
			$_product_price = ($value['product_sold'] == 1)? $_GET_LANG['store_sold']:$_product_price;

			$str_product_similar .= <<<HTML
									<li class="clearfix">
										<a href="./product/{$value['product_code']}" style="background-image: url(./uploadimages/product/{$value['product_create_year']}/{$value['product_code']}/{$_photo});  background-size: cover;"></a>
										<ul class="same_list_info">
											<li><a href="./product/{$value['product_code']}">{$value['product_name']}</a></li>
											<li>{$_product_price}</li>
										</ul>
									</li>
HTML;
		}
	}

	//確認希望清單內有沒有此商品
	if(!empty($arr_member['member_id'])){
		$_wishlist_column = ($is_tmp_product)? 'member_wishlist_tmp':'member_wishlist';

		$arr_wishlist = (empty($arr_member[$_wishlist_column]))? array():json_decode($arr_member[$_wishlist_column]);
		$str_wishlist = array('data-item="'.$arr_product['product_code'].'"', 'plus', 'javascript:;');
		if(array_search($arr_product['product_id'], $arr_wishlist) !== false){
			$str_wishlist = array('', 'check', 'javascript:;');
		}
	}
	else{
		$str_wishlist = array('', 'plus', './login');
	}

	// 新增會員行為紀錄
	if(!empty($arr_member['member_id'])){
		include_once("include/class.analytics.php");
		$analytics = new member_analytics();
		$arr_data = array(
							'data_type' => 1,			//裝置類型(1-WEB;2-APP)
							'data_action_type' => 2,	//操作類型(1-篩選產品;2-瀏覽產品;3-篩選店家;4-瀏覽店家;5-全站搜尋)
							'data_target' => $arr_product['product_code']
						);
		$_record_id = $analytics->chk_record($arr_data);

		//產生離開頁面事件，紀錄停留時間
		$encrypt_id = str_replace('=', '', encrypt($_record_id, 'O1O3MOeJnp'));
		$record_JS = <<<JAVASCRIPT
			window.onbeforeunload = function(){
				doajax({
					url: './unload_page',
					async:false,
					data: 'id={$encrypt_id}'
				});
			}
JAVASCRIPT;
	}

	//聯絡店家國碼
	$arr_calling_code = $crud->sql("SELECT *
									FROM `country_calling_codes`
									ORDER BY FIELD(`country_calling_codes`, 886, 852) DESC, `country_calling_codes`");
	$str_calling_code = '';
	if(count($arr_calling_code) > 0){
		foreach ($arr_calling_code as $key => $value) {
			$selected = ($value['country_calling_codes'] <> 886)? '':'selected';
			$str_calling_code .= <<<HTML
				<option value="{$value['country_calling_codes']}" {$selected}>+{$value['country_calling_codes']} {$value['country_name']}</option>
HTML;
		}
	}
?>
<!doctype html>
<html lang="zh-Hant-TW">

<head>
	<?php include_once("include/head.php");?>

	<!-- 專案的CSS -->
	<link rel="stylesheet" href="css/jquery.fancybox.css">
</head>

<body>
	<?php include_once("include/navbar.php");?>

	<div class="content">
		<div class="container">
			<div class="row">
				<div class="col-xs-12">
					<ol class="breadcrumb">
						<li><a href="./"><i class="fa fa-home" aria-hidden="true"></i> <?=$_GET_LANG['home'];?></a></li>
						<li><a href="./products"><?=$_GET_LANG['used_machinery'];?></a></li>
						<?=$str_breadcrumb;?>
						<li><a href="./products/category.<?=$arr_product['cate_id'];?>"><?=$arr_product['cate_title'.$_LANG];?></a></li>
						<li class="active"><?=$arr_product['product_name'];?></li>
					</ol>
				</div>
				<div class="col-md-9">
					<div class="row productsbox">
						<div class="col-sm-12 col-md-6 products_photo">
							<div class="photo_big">
								<a href="<?=$str_cover;?>" class="fancybox-thumb" rel="fancybox-thumb" title="<?=$arr_product['model_number'];?>">
									<img src="<?=$str_cover;?>" alt="<?=$arr_product['model_number'];?>" class="img-responsive">
								</a>
							</div>
							<ul class="photo_list">
								<?=$str_product_album_basic;?>
							</ul>
						</div>
						<div class="col-sm-12 col-md-6">
							<div class="products_infobox">
								<h3><?=$arr_product['product_name'.$_LANG];?></h3>
								<div class="price_select clearfix">
						<?php
						if($arr_product['product_sold'] == 1 ){
							echo <<<HTML
									<span class="main_price">{$_GET_LANG['store_sold']}</span>
HTML;
						}
						else if($arr_product['product_default_price'] < 1){
							echo <<<HTML
									<span class="main_price">{$_GET_LANG['store_call_us']}</span>
HTML;
						}
						else{
							$_tmp_option = '';
							foreach ($arr_product_price  as $key => $value) {
								$selected = ($key == $_currency)? 'selected':'';
								$_price = number_format($value, 0);
								$_tmp_option .= <<<HTML
													<option value="{$_price}" {$selected}>{$key}</option>
HTML;
							}

							echo <<<HTML
									<select class="form-control sel_price">
										{$_tmp_option}
									</select>
									<span class="main_price"> <span id="product_price"></span></span>
HTML;
						}

										?>
									<span class="wishlist_btn" <?=$str_wishlist[0];?>>
										<a class="btn btn-default btn-xs" href="<?=$str_wishlist[2];?>" role="button">
										<i class="fa fa-<?=$str_wishlist[1];?>" aria-hidden="true"></i>
						<?php
							echo <<<HTML
										{$_GET_LANG['add_to_wishlist']}
HTML;
						?>
										</a>
									</span>
								</div>
								<div class="textedit">
									<?=$_GET_LANG['price_tax_is_not_included'];?>
								</div>
								<div class="warringbox">
									<?=$_GET_LANG['notification'];?>
								</div>							</div>
							<!-- Add 店家資訊欄 -->
							<?=(!empty($arr_member['member_id']))? $_GET_LANG['store_contact']:'';?>
							<ul class="products_infolist list-unstyled mb20">
								<?=$str_contact;?>
							</ul>

							<ul class="products_infolist list-unstyled">
								<li><span class="list_tit"><?=$_GET_LANG['ID'];?>：</span><?=$arr_product['product_code'].$arr_product['product_ver_code'];?></li>
								<li><span class="list_tit"><?=$_GET_LANG['product_category'];?>：</span><?=$arr_product['cate_title'.$_LANG];?></li>
								<li><span class="list_tit"><?=$_GET_LANG['maker'];?>：</span><?=$arr_product['brand_name'.$_LANG];?></li>
								<li><span class="list_tit"><?=$_GET_LANG['model'];?>：</span><?=($is_tmp_product)?$arr_product['product_model']:$arr_product['model_number'];?></li>
								<li><span class="list_tit"><?=$_GET_LANG['serial_no'];?>：</span><?=(!empty($arr_product['product_serial']))? $arr_product['product_serial']:$_GET_LANG['na'];?></li>
								<li><span class="list_tit"><?=$_GET_LANG['year_range'];?>：</span><?=($arr_product['product_year'] < 1)?$_GET_LANG['na']:$arr_product['product_year'];?></li>
								<li><span class="list_tit"><?=$_GET_LANG['hours_used_range'];?>：</span><?=($arr_product['product_hours'] < 1)?$_GET_LANG['na']:$arr_product['product_hours'];?></li>
								<?php
									if(!empty($arr_product['model_datasheet'])){
										$file_name = $arr_product['model_number'];
										echo <<<HTML
										<li><span class="list_tit">{$_GET_LANG['datasheet']}：</span><a download="{$file_name}" href="./uploadimages/model/Maker/{$arr_product['brand_name_en']}/{$arr_product['model_datasheet']}">{$_GET_LANG['download']}</a></li>
HTML;
									} else {
										echo <<<HTML
										<li><span class="list_tit">{$_GET_LANG['datasheet']}：</span>{$_GET_LANG['na']}</li>
HTML;
									}
									if(!empty($arr_product['product_report'])){
										$file_name = $arr_product['product_code'];
										echo <<<HTML
										<li><span class="list_tit">{$_GET_LANG['inspection_report']}：</span><a download="{$file_name}" href="./uploadimages/product/{$arr_product['product_create_year']}/{$arr_product['product_code']}/{$arr_product['product_report']}">{$_GET_LANG['download']}</a></li>
HTML;
									} else {
										echo <<<HTML
										<li><span class="list_tit">{$_GET_LANG['inspection_report']}：</span>{$_GET_LANG['na']}</li>
HTML;
									}
								?>
								<li><span class="list_tit"><?=$_GET_LANG['location'];?>：</span><?=$_location;?></li>
								<?php
									if($arr_product['product_country']==='HKG'){
										$product_nrmm_g=((strpos($arr_product['product_epd_nrmm_label'], 'NRMMG')) !== false)? '<a class="m_tag green_back">NRMM</a>':'';
										$product_nrmm_y=((strpos($arr_product['product_epd_nrmm_label'], 'NRMMY')) !== false)? '<a class="m_tag yellow_back">NRMM</a>':'';
										$product_qpme=((strpos($arr_product['product_epd_nrmm_label'], 'QPME')) !== false)? '<a class="m_tag purple_back">QPME</a>':'';
									        $_label_info = $product_nrmm_g;
									        $_label_info .= $product_nrmm_y;
									        $_label_info .= $product_qpme;
										echo <<<HTML
											<li><span class="list_tit">{$_GET_LANG['epd_label']}：</span>{$_label_info}</li>
HTML;
									}
								?>
								<li>
									<span class="list_tit"><?=$_GET_LANG['remark'];?>：</span><br/>
									<div class="infolist_other">
									    <!--
									        <?=nl2br($arr_product['product_remark'.$_LANG]);?>
									     -->
									    <?php
	                                        $arr_content = explode("\r", $arr_product['product_remark'.$_LANG]);
	                                        $_content = '<p>'.join('</p><p>', $arr_content).'</p>';
						                    echo html_entity_decode($_content);
						                ?>
									</div>
								</li>
							</ul>
						</div>
					</div>
				</div>
				<div class="col-md-3">
					<?=$str_check_product;?>
					<!-- 原本的聯絡賣家區塊 -->
					<h4><?=$_GET_LANG['contact_seller'];?></h4>
					<div class="pro_contact">
						<form data-item="<?=$arr_product['product_code'];?>">
							<div class="form-group">
								<label for="name"><?=$_GET_LANG['contact_name'];?>*</label>
								<input type="text" class="form-control" name="name" id="name" placeholder="<?=$_GET_LANG['contact_type_name'];?>" required>
							</div>
							<div class="form-group">
								<label for="email"><?=$_GET_LANG['contact_email'];?>*</label>
								<input type="email" class="form-control" name="email" id="email" placeholder="<?=$_GET_LANG['contact_type_email'];?>" required>
							</div>
							<div class="form-group">
								<label for="phone"><?=$_GET_LANG['contact_phone'];?>*</label>
								<div class="input-group">
								    <span class="input-group-btn">
										<select class="selectpicker" data-live-search="true" id="calling_code" name="calling_code" required>
											<?=$str_calling_code;?>
										</select>
									</span>
								    <input type="text" class="form-control" id="phone" name="phone" placeholder="e.g. 912345678" required>
								</div>
							</div>
							<div class="form-group">
								<label for="message"><?=$_GET_LANG['contact_content'];?>*</label>
								<textarea class="form-control" rows="3" id="message" name="message" placeholder="<?=$_GET_LANG['contact_type_content'];?>"></textarea>
							</div>
							<div id="recaptcha_contact_owner"></div>
							<button type="submit" class="btn btn-block btn-black btn_submit"><?=$_GET_LANG['submit'];?></button>
						</form>
					</div>
		<?php
			if(!empty($str_product_similar)){
				echo <<<HTML
					<h4>{$_GET_LANG['similar_product']}</h4>
					<ul class="same_list">
						{$str_product_similar}
					</ul>
HTML;
			}
		?>
				</div>
			</div>
		</div>
	</div>
	<script src="https://www.google.com/recaptcha/api.js?onload=onloadCallback&render=explicit" defer></script>
	<?php include_once("include/footer.php");?>
	<script type="text/javascript" defer>
		$('.sel_price').on('change', function(){
			$('#product_price').text($(this).val());
		}).change();
		<?=$record_JS;?>

		var recaptcha_contact_owner,
			onloadCallback = function() {
				recaptcha_contact_owner = grecaptcha.render('recaptcha_contact_owner', {
					'sitekey': '<?=RECAPTCHA_SITEKEY;?>',
					'callback': send_contact,
					'size': 'invisible'
				});
			};
		function send_contact(){
			var $this = $('.pro_contact form'),
				v_data = {
					item: $this.data('item')
				};
			if($this.data('item')){
				doajax({
					url: './contact_owner',
					type: 'json',
					data: jQuery.param(v_data)+'&'+$this.serialize(),
					callback: function(msg){
						if(msg.sts){
							$this[0].reset();
						}
						swal(msg.msg);
						grecaptcha.reset(recaptcha_contact_owner);
						$('.btn_submit').attr('disabled', false);
					}
				});
			}
		}
		$(function() {
			$('.pro_contact form').on('submit', function(){
				$('.btn_submit').attr('disabled', true);
				grecaptcha.execute(recaptcha_contact_owner);
				return false;
			});
		});

		$('.wishlist_btn').on('click', function(){
			var $this = $(this),
				v_data = {
					type: 'add_wishlist',
					item: $this.data('item')
				};
			if($this.data('item')){
				doajax({
					url: './domem',
					type: 'json',
					data: jQuery.param(v_data),
					callback: function(msg){
						if(!msg.sts){
							swal(msg.msg);
						}
						else{
							$this.data('item', null).find('i').removeClass('fa-plus').addClass('fa-check');
						}
					}
				});
			}
		});

		$('.btn_check_product').on('click', function(){
			var $this = $(this);
			$this.attr('disabled', true);
			swal({
				title: "Are you sure?",
				text: "<?=$_GET_LANG['confirm_send'];?>",
				showCancelButton: true,
				confirmButtonColor: "#DD6B55",
				confirmButtonText: "Yes",
				cancelButtonText: "No",
				closeOnConfirm: false
			},
			function(isConfirm){
				if (isConfirm) {
					var v_data = {
							type: 'check_product',
							id: $this.data('id')
						};
					if($this.data('id')){
						doajax({
							url: './domem',
							type: 'json',
							data: jQuery.param(v_data),
							callback: function(msg){
								if(!msg.sts){
									swal(msg.msg);
								}
								else{
									location.reload();
								}
								$this.attr('disabled', false);
							}
						});
					}
				}
				else{
					$this.attr('disabled', false);
				}
			});
		});
	</script>
</body>

</html>
