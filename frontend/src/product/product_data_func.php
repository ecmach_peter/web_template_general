<?php
	if(!isset($_SERVER['HTTP_X_REQUESTED_WITH']) || !strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' || empty($_act)){
		header("X-Robots-Tag: noindex, nofollow", true);
		header('HTTP/1.0 403 Forbidden');
		exit;
	}
// print_r($_POST);exit;

	//取會員資料
	$arr_member = $crud->getid('member', array('member_uniqid' => $_SESSION[SESSION_NAME.'_user_uniqid']));

	$_type = get_post_var('type');
	$_filter = get_post_var('filter');
	$arr_url = $_record_data = array();
	$SQL_filter = '';
	$SQL_sort = '';
	if(count($_filter) > 0){
		foreach ($_filter as $key => $value) {
			$var_error = false;
			switch ($key) {
				//地區參數處理
				case 'location':
					$arr_country = array('HKG', 'TWN', 'OTHER');
					if(array_search($value, $arr_country) !== false){
						if($value == 'OTHER'){
							$SQL_filter .= " AND `product_country` NOT IN ('HKG', 'TWN')";
						}
						else{
							$SQL_filter .= " AND `product_country` = '{$value}'";
						}

						$_record_data[] = 'location='.$value;
					}
					else{
						$var_error = true;
					}
					break;

				//分類參數處理
				case 'category':
					//確認有此分類
					$arr_chk_cate = $crud->getid('model_cate', array('cate_id' => $value));
					if(empty($arr_chk_cate['cate_id'])){
						$var_error = true;
					}
					else{
						//確認有無子分類
						$arr_child_cate = $crud->select('model_cate', array('cate_par_id' => $value));
						if(count($arr_child_cate) > 0){
							$arr_cate_in = array();
							foreach ($arr_child_cate as $_key => $_value) {
								$arr_cate_in[] = $_value['cate_id'];
							}
							$cate_sql_mode = 'IN ('.join(',', $arr_cate_in).')';
						}
						else{
							$cate_sql_mode = '= '.$value;
						}

						$SQL_filter .= " AND `model_cate` {$cate_sql_mode}";

						$_record_data[] = 'category='.$arr_chk_cate['cate_title'];
					}
					break;

				//品牌參數處理
				case 'brand':
					//確認有此品牌
					$SQL_filter_brand = '';
					$arr_chk_brand = $crud->getid('brand', array('brand_id' => $value));
					if(empty($arr_chk_brand['brand_id'])){
						$var_error = true;
					}
					else{
						$SQL_filter_brand = " AND `model_brand` = {$value}";

						$_record_data[] = 'brand='.$arr_chk_brand['brand_title'];
					}
					break;

				//檢視模式處裡
				case 'view':
					$arr_view = array(/*'grid', */'list');
					if(array_search($value, $arr_view) !== false){
						$_type = $value;
					}
					else{
						$var_error = true;
					}
					break;

				//標章處理
				case 'label':
					$arr_label = array('NRMMG', 'NRMMY');
					if(array_search($value, $arr_label) !== false){
						$SQL_filter .= " AND `product_epd_nrmm_label` LIKE ('%{$value}%')";

						$_record_data[] = 'label='.$value;
					}
					else{
						$var_error = true;
					}
					break;

				//排序處理
				case 'sort':
					switch ($value) {
						case 'new-asc':
							$SQL_sort = ' `product`.`product_id` ASC';
							break;
						case 'price':
							$SQL_sort = ' `product_selling_price` DESC';
							break;
						case 'price-asc':
							$SQL_sort = ' `product_selling_price` ASC';
							break;
						case 'popular':
							$SQL_sort = ' `product_id` DESC';
							break;
					}
					break;

				//使用時數
				case 'price':
				case 'year':
				case 'hours':
				case 'weight':
					if(!empty($value) && $value != '-'){
						${'arr_range_'.$key} = explode('-', $value);
						${'arr_range_'.$key}[0] = (int)${'arr_range_'.$key}[0];
						${'arr_range_'.$key}[1] = (int)${'arr_range_'.$key}[1];
						switch ($key) {
							case 'year':
								$limit = array(1800, date('Y'));
								break;
							
							default:
								$limit = array(0, 999999999);
								break;
						}

						switch ($key) {
							case 'weight':
								$_column = "`model_{$key}`";
								break;
							case 'price':
								${'arr_range_'.$key}[0] *= 10000;
								${'arr_range_'.$key}[1] *= 10000;
								$_column = "IFNULL(
												(SELECT `product_selling_price_currency`.`selling_price`
												FROM `product_selling_price`
												INNER JOIN `product_selling_price_currency` ON `product_selling_price`.`selling_id` = `product_selling_price_currency`.`selling_id`
												WHERE `product_selling_price`.`product_id` = `product`.`product_id` AND `selling_online` = 1 AND CONCAT(',', `selling_target`, ',') LIKE '%,{$arr_member['member_id']},%' AND `product_selling_price_currency`.`product_currency` = '{$_currency}'
												ORDER BY `selling_sort`
												LIMIT 1), 
												`product_price_currency`.`product_default_price`
											)";
								break;
							default:
								$_column = "`product_{$key}`";
								break;
						}

						if($key == 'price' && $value != '-'){
							$arr_tmp = array();
							if(${'arr_range_'.$key}[0] <> '' || ${'arr_range_'.$key}[0] == 0) $arr_tmp[] = " {$_column} >= {${'arr_range_'.$key}[0]}";
							if(${'arr_range_'.$key}[1] <> '' || ${'arr_range_'.$key}[0] == 0) $arr_tmp[] = " {$_column} <= {${'arr_range_'.$key}[1]}";

							$str_tmp = join(' AND ', $arr_tmp);
							$SQL_filter .= " AND ({$str_tmp} OR `product`.`product_default_price` = 0) ";
						}
						else{
							$tmp_filter = array();
							if(${'arr_range_'.$key}[0] <> '') $tmp_filter[] = " {$_column} >= {${'arr_range_'.$key}[0]}";
							if(${'arr_range_'.$key}[1] <> '') $tmp_filter[] = " {$_column} <= {${'arr_range_'.$key}[1]}";

							if(!empty($tmp_filter)){
								$SQL_filter .= " AND ((".join(' AND ', $tmp_filter).") OR {$_column} = 0)";
							}
						}

						$_record_data[] = $key.'='.${'arr_range_'.$key}[0];
						$_record_data[] = "{$key}={${'arr_range_'.$key}[0]}-${'arr_range_'.$key}[1]";
					}
					else{
						$var_error = true;
					}
					break;

				//頁數參數
				case 'page':
					$now_page = $value;

					if(!empty($value)) $_record_data[] = 'page='.$value;
					break;

				//一頁幾個商品
				case 'items':
					$page_view = $value;
					break;

				//搜尋商品
				case 'search':
					$value = addslashes(trim(strip_tags($value)));
					if(!empty($value)){
						$SQL_filter .= " AND (
											`product_name{$_LANG}` LIKE ('%{$value}%') OR 
											`product_code` LIKE ('%{$value}%') OR 
											`model_number` LIKE ('%{$value}%') OR 
											`cate_title` LIKE ('%{$value}%') OR 
											`brand_title{$_LANG}` LIKE ('%{$value}%')
										)";

						$_record_data[] = 'search='.$value;
					}
					break;

				//其他參數不處理
				default:
					$var_error = true;
					break;
			}

			if(!empty($value) && !$var_error){
				$arr_filter[$key] = $value;
				$arr_url[] = $key.'.'.$value;
			}
		}
	}

	switch ($_type) {
		case 'grid':
			$class = 'row list-unstyled boxstyle_grid';

			//方塊式
			$init_list = <<<HTML
								<li class="col-xs-6 col-sm-3">
									<div class="thumbnail np_box">
										<a href="./product/%product_id\$d"><img src="./frontend/img/usr/product/%product_create_year\$d/%product_code\$s/%product_photo\$s" alt="%product_name\$s" class="img-responsive"></a>
										<div class="hidden-xs">
											%product_new\$s
										</div>
										<div class="caption text-center">
											<a href="./product/%product_id\$d"><h3>%product_name\$s</h3></a>
											<ul class="list-inline">
												<li>%product_year\$s</li>
												<li class="hidden-sm hidden-md hidden-lg">%product_new\$s</li>
												<li>%product_hours\$shr</li>
											</ul>
											<p>%product_country\$s</p>
											<div class="np_price"><p>%product_price\$s</p></div>
										</div>
									</div>
								</li>
HTML;
			break;
		
		default:
		case 'list':
			$class = 'list-unstyled boxstyle_list';

			//列表式
			/*<p>%product_country\$s</p>*/
			$init_list = <<<HTML
						<li>
							<div class="thumbnail np_box clearfix">
								<a href="./product/%product_code\$s" style="background-image: url(./frontend/img/usr/product/%product_create_year\$d/%product_code\$s/%product_photo\$s);" title="%product_name\$s"></a>
								<div class="hidden-xs">
									%product_new\$s
								</div>
								<div class="caption">
									<a href="./product/%product_code\$s"><h3>%product_name\$s</h3></a>
									<ul class="list-inline">
										<li>%product_year\$s</li>
										<li class="hidden-sm hidden-md hidden-lg">%product_new\$s</li>
										%product_hours\$s
									</ul>
									<div class="np_price">%product_start_price\$s<p>%product_price\$s</p></div>
								</div>
								<div class="perinfo">
									<ul class="products_infolist list-unstyled">
										%owner_info\$s
									</ul>
								</div>
							</div>
						</li>
HTML;
			break;
	}

	// ----- 取出商品資料
	// 取得最近七天的商品
	$arr_new_product = $crud->sql("SELECT `product_id`
									FROM `product`
									WHERE `product_online` = 1 AND `product_create_date` > DATE_SUB(CURDATE(), INTERVAL 7 DAY) 
									ORDER BY `product_id` DESC");
	$arr_is_new = array();
	if(count($arr_new_product) > 0){
		foreach ($arr_new_product as $key => $value) {
			$arr_is_new[] = $value['product_id'];
		}
	}
	// 最新的5個商品
	$arr_new_product = $crud->sql("SELECT `product_id`
									FROM `product`
									WHERE `product_online` = 1 
									ORDER BY `product_create_date` DESC
									LIMIT 5");
	if(count($arr_new_product) > 0){
		foreach ($arr_new_product as $key => $value) {
			if(array_search($value['product_id'], $arr_is_new) === false){
				$arr_is_new[] = $value['product_id'];
			}
		}
	}

	//優先排序
	if(empty($SQL_sort)){
		switch ($_SESSION[SESSION_NAME.'_country']) {
			case 'Taiwan':
				$SQL_sort = " FIELD(`product_country`, 'HKG', 'TWN') DESC, ";
				break;
			case 'Hong Kong':
				$SQL_sort = " FIELD(`product_country`, 'TWN', 'HKG') DESC, ";
				break;
			default:
				break;
		}
//		$SQL_sort .= "`product_sort`";
		$SQL_sort .= "`product_create_date` DESC";
	}

	//付費會員產品
	if($arr_member['member_paid'] <> 1){
		$SQL_filter .= ' AND `product_paid` = 0';
	}

	//----- 分頁處理
	$count_page = count($crud->sql("SELECT *
									FROM `product`
									INNER JOIN `product_price_currency` ON `product_price_currency`.`product_id` = `product`.`product_id`
									INNER JOIN `model` ON `product_model` = `model_id`
									INNER JOIN `model_cate` ON `cate_id` = `model_cate`
									INNER JOIN `brand` ON `model_brand` = `brand_id`
									WHERE `product_online` = 1 AND `product_price_currency`.`product_currency` = '{$_currency}' {$SQL_filter} {$SQL_filter_brand}"));	//計算筆數
	$pagesize = (empty($page_view))? 16:$page_view;    //一頁顯示筆數
	$now_page = (empty($now_page) || $now_page > $count_page)? 1:$now_page;
	$begin_num = ($now_page - 1) * $pagesize;	//計算開始筆數
	$tot_page = ceil($count_page/$pagesize);    //總頁數

	//一列顯示幾個item
	$_oneline_items = 5;
	$_page_link = './products/page.';

	$_pagination = '';
	if($tot_page > 1){
		//上一頁處理
		$prev_page = $now_page - 1;
		$prev = ($now_page > 1)? 'data-page="'.$prev_page.'"':'';
		$prev_link = ($now_page > 1)? "{$_page_link}{$prev_page}":'javascript:;';

		//下一頁處理
		$next_page = $now_page + 1;
		$next = ($now_page < $tot_page)? 'data-page="'.$next_page.'"':'';
		$next_link = ($now_page < $tot_page)? "{$_page_link}{$next_page}":'javascript:;';

		//起始頁處理
		$start = ($now_page > 1)? 'data-page="1"':'';
		$start_link = ($now_page > 1)? $_page_link.'1':'javascript:;';

		//結束頁處理
		$end = ($now_page < $tot_page)? 'data-page="'.$tot_page.'"':'';
		$end_link = ($now_page < $tot_page)? $_page_link.$tot_page:'javascript:;';

		$_pagination .= <<<HTML
							<li {$start}><a href="{$start_link}"><i class="fa fa-angle-double-left"></i></a></li>
							<li {$prev}><a href="{$prev_link}"><i class="fa fa-angle-left fa-lg"></i></a></li>
HTML;

		$_half_items = floor($_oneline_items / 2);
		$fix_base = ($_oneline_items % 2 == 0)? 2:1;
		$base = $now_page - $_half_items;
		if($tot_page - $now_page < $_half_items) $base = $tot_page - ($_oneline_items - $fix_base);
		if($base < 1) $base = 1;

		$max = ($now_page <= $_half_items)? $_oneline_items:$now_page + $_half_items;
		if($max > $tot_page) $max = $tot_page;

		for($i = $base; $i <= $max; $i++){
			if($i > $tot_page) break;
			$sel_page = ($i <> $now_page)? 'data-page="'.$i.'"':'';
			$active = ($i == $now_page)? 'class="on"':'';
			$_link = ($i == $now_page)? 'javascript:;':$_page_link.$i;
			$_pagination .= <<<HTML
							<li {$sel_page} {$active}><a href="{$_link}">{$i}</a></li>
HTML;
		}
		$_pagination .= <<<HTML
							<li {$next}><a href="{$next_link}"><i class="fa fa-angle-right fa-lg"></i></a></li>
							<li {$end}><a href="{$end_link}"><i class="fa fa-angle-double-right"></i></a></li>
HTML;
	}
	$arr['pagination'] = $_pagination;
	$arr_product = $crud->sql("SELECT *,IFNULL(
											(SELECT `product_selling_price_currency`.`selling_price`
											FROM `product_selling_price`
											INNER JOIN `product_selling_price_currency` ON `product_selling_price`.`selling_id` = `product_selling_price_currency`.`selling_id`
											WHERE `product_selling_price`.`product_id` = `product`.`product_id` AND `selling_online` = 1 AND CONCAT(',', `selling_target`, ',') LIKE '%,{$arr_member['member_id']},%' AND `product_selling_price_currency`.`product_currency` = '{$_currency}'
											ORDER BY `selling_sort`
											LIMIT 1), 
											`product_price_currency`.`product_default_price`
										) AS `product_selling_price`
								FROM `product`
								INNER JOIN `product_price_currency` ON `product_price_currency`.`product_id` = `product`.`product_id`
								INNER JOIN `model` ON `product_model` = `model_id`
								INNER JOIN `model_cate` ON `cate_id` = `model_cate`
								INNER JOIN `brand` ON `model_brand` = `brand_id`
								WHERE `product_online` = 1 AND `product_price_currency`.`product_currency` = '{$_currency}' {$SQL_filter} {$SQL_filter_brand}
								ORDER BY {$SQL_sort}
								LIMIT {$begin_num}, {$pagesize}");
	$str_product = '';
	$number_format = 'number_format';
	$arr_count_brand = array();
	if(count($arr_product) > 0){
		foreach ($arr_product as $key => $value) {
			//品牌處理
			$arr_count_brand[$value['model_brand']][] = $value['product_id'];

			$product_default_price = $value['product_selling_price'];
			$product_start_price = $value['product_start_price'];

			$product_new = (array_search($value['product_id'], $arr_is_new) !== false)? '<div class="new"><p>NEW</p></div>':'';

			if($product_default_price < 1 || $product_default_price == $product_start_price){
				$product_start_price = '';
			}
			else{
				$product_start_price = <<<HTML
						<del>{$_currency} {$number_format($product_start_price, 0)}</del>
HTML;
			}
            $_product_price_display = ($product_default_price < 1)? $_GET_LANG['store_call_us']:"{$_currency} ".number_format($product_default_price, 0);
            
			$arr_data = array(
							'product_id' => $value['product_id'],
							'product_code' => $value['product_code'],
							'product_name' => $value['product_name'.$_LANG],
							'product_photo' => $value['product_photo'],
							'product_price' => $_product_price_display,
							'product_start_price' => $product_start_price,
							'product_new' => $product_new,
							'product_country' => $value['product_country'],
							'product_year' => (empty($value['product_year']))? '':$value['product_year'].$_GET_LANG['year'],
							'product_hours' => (empty($value['product_hours']))? '':'<li>'.$value['product_hours'].'hr</li>',
							'model_weight' => $value['model_weight'],
							'product_code' => $value['product_code'],
							'product_create_year' => $value['product_create_year'],
							//'owner_info' => (!empty($arr_member['member_id']))? $_owner_info:'<li><button onclick="javascript:location.href=\'./register\';" class="btn btn-default">'.$_GET_LANG['get_store_info'].'</button></li>'	// 20170406非會員隱藏賣家資訊
							'owner_info' => $_owner_info// 20170605 peter modify
						);

			$str_product .= vksprintf($init_list, $arr_data);
		}
	}

	//品牌
	$arr_brand = $crud->sql("SELECT `brand`.*, COUNT(*) AS `count_model`
							FROM `product`
							INNER JOIN `product_price_currency` ON `product_price_currency`.`product_id` = `product`.`product_id`
							INNER JOIN `model` ON `product_model` = `model_id`
							INNER JOIN `model_cate` ON `cate_id` = `model_cate`
							INNER JOIN `brand` ON `brand_id` = `model_brand`
							WHERE `product_online` = 1 AND `brand_online` = 1 AND `product_price_currency`.`product_currency` = '{$_currency}' {$SQL_filter}
							GROUP BY `brand_id`");
	$str_brand = $str_brand_option = '';
	if(count($arr_brand) > 0){
		$num = 1;
		foreach ($arr_brand as $key => $value) {
			$count_model = number_format($value['count_model']);
			if($key < 6){
				$str_brand .= <<<HTML
						<li><a href="./products/brand.{$value['brand_id']}" class="btn-filter" data-id="{$value['brand_id']}">{$value['brand_title'.$_LANG]}</a> ({$count_model})</li>
HTML;
			}

			${'str_brand_'.$num} .= <<<HTML
						<li><a href="./products/brand.{$value['brand_id']}" class="btn-filter" data-id="{$value['brand_id']}">{$value['brand_title'.$_LANG]}</a> ({$count_model})</li>
HTML;
			$num++;
			if($num > 3) $num = 1;

			$str_brand_option .= <<<HTML
						<option value="{$value['brand_id']}">{$value['brand_title'.$_LANG]}</option>
HTML;
		}
	}

	$str_brand = <<<HTML
				<li><a href="javascript:;" class="btn-filter" data-id="">{$_GET_LANG['all']}</a></li>
				{$str_brand}
HTML;
	for ($i = 1; $i < 4; $i++) { 
		$str_brand_all .= <<<HTML
					<div class="col-md-4">
						<ul class="sidebar_list filter_ul" data-mode="brand">
							${'str_brand_'.$i}
						</ul>
					</div>
HTML;
	}

	$arr_brand_select = <<<HTML
		<select class="form-control">
			<option value="" selected>{$_GET_LANG['all']}</option>
			{$str_brand_option}
		</select>
HTML;
	$arr['brand'] = array($str_brand, $str_brand_all, $arr_brand_select);

	$_url = join('/', $arr_url);
	$_url = (empty($_url))? '':'/'.$_url;
	$arr['url'] = './products'.$_url;
	$arr['url_data'] = $arr_filter;
	$str_product = <<<HTML
						<ul class="{$class} open">{$str_product}</ul>
HTML;
	$arr['data'] = $str_product.$record_JS;
	echo json_encode($arr);
?>