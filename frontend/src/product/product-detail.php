<?php

	include_once(PATH_DB_HELPER_SRC."product.php");		//debug
	include_once(PATH_FRONTEND_HELPER_SRC."product.php");		//product

	//產品資料
	$ary_product = db_product_detail($_var1);
	//info_msg(__FILE__, __LINE__, print_r($ary_product, true));
	if(empty($ary_product['product_id'])){
		header("location: /{$_BASE}products");
		exit;
	}

	//幣值選單
	$str_price = product_detail_price($ary_product['product_price']);

	//相簿相關
	$ary_product_album = product_detail_album($ary_product);
	$str_cover = $ary_product_album['product_cover'];
	$str_product_album = $ary_product_album['product_album'];

	//meta title, url, description, image 處理
	$_customize_title = $ary_product['product_name'].' :: ';
	$_customize_url = 'product/'.$ary_product['product_code'];
	$share_url = PROTOCOL."://{$_SERVER['HTTP_HOST']}".BASE.$_customize_url;
	// $_customize_description = mb_strimwidth(str_replace("\r\n", '', strip_tags(htmlspecialchars_decode($arr_store['store_info']))), 0, 140, '...', 'UTF-8');
	$arr_og_images = $ary_product_album['og_images'];
	$_customize_og_image = '';
	if(count($arr_og_images) > 0){
		foreach ($arr_og_images as $value) {
			$_customize_og_image .= '<meta property="og:image" content="'.$value.'">';
		}
	}
	$_customize_title = $ary_product['product_name'].' :: ';

	//類似機械
	#同分類有產品時顯示同分類
	#同分類無產品時顯示同品牌
	$ary_product_similar = db_product_similar($ary_product, 4);
	if(count($ary_product_similar) == 0){
		$ary_product_similar = db_product_rand($ary_product, 4);
	}
	info_msg(__FILE__, __LINE__, print_r($ary_product_similar, true));
	$str_product_similar = product_similar_list($ary_product_similar);
?>



<!doctype html>
<html lang="zh-Hant-TW">

<head>
	<?php include_once("frontend/src/include/head.php");?>

	<!-- 專案的CSS -->
	<link rel="stylesheet" href="frontend/css/jquery.fancybox.css">
	<link rel="stylesheet" href="frontend/css/import/page.css">
</head>

<body>
	<?php include_once("frontend/src/include/navbar.php");?>

	<div class="content">
		<div class="container">
			<div class="row">
				<div class="col-xs-12">
					<ol class="breadcrumb">
						<li><a href="./"><i class="fa fa-home" aria-hidden="true"></i> <?=$_GET_LANG['home'];?></a></li>
						<li><a href="javascript:;"><?=$_GET_LANG['featured_products'];?></a></li>
						<li><a href="./products/brand.<?=$ary_product['brand_id'];?>"><?=$ary_product['brand_title'.$_LANG];?></a></li>
						<li class="active"><?=$ary_product['product_name'];?></li>
					</ol>
				</div>
				<div class="col-xs-12 col-sm-12 col-md-9">
					<div class="row productsbox">
						<div class="col-sm-12 col-md-6 products_photo">
							<div class="photo_big">
								<a href="<?=$str_cover;?>" data-fancybox="images">
									<img src="<?=$str_cover;?>" alt="<?=$ary_product['model_number'];?>" class="img-responsive">
								</a>
							</div>
							<ul class="photo_list">
								<?=$str_product_album;?>
							</ul>
						</div>
						<div class="col-sm-12 col-md-6">
							<div class="products_infobox">
								<h3><?=$ary_product['product_name'];?></h3>
								<div class="price_select clearfix">
									<?=$str_price;?>
								</div>
								<ul class="products_infolist list-unstyled">
								<?php
									if(!empty($ary_product['model_datasheet'])){
										$file_name = $ary_product['model_number'];
										echo <<<HTML
										<li><span class="list_tit">{$_GET_LANG['datasheet']}：</span><a download="{$file_name}" href="./frontend/img/usr/model/Maker/{$ary_product['brand_title_en']}/{$ary_product['model_datasheet']}">{$_GET_LANG['download']}</a></li>
HTML;
									} 
									if(!empty($ary_product['product_report'])){
										$file_name = $ary_product['product_code'];
										echo <<<HTML
										<li><span class="list_tit">{$_GET_LANG['inspection_report']}：</span><a download="{$file_name}" href="./frontend/img/usr/product/{$ary_product['product_create_year']}/{$ary_product['product_code']}/{$ary_product['product_report']}">{$_GET_LANG['download']}</a></li>
HTML;
									} 
								?>
								</ul>
							</div>
							<ul class="products_infolist list-unstyled">
								<li>
									<span class="list_tit">
										<div class="fb-share-button" data-href="<?=$share_url;?>" data-layout="button_count" data-size="large" data-mobile-iframe="true">
											<a class="fb-xfbml-parse-ignore" target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=<?=urlencode($share_url);?>&amp;src=sdkpreparse"><?=$_GET_LANG['share'];?>
												
											</a>
										</div>
									</span>
								</li>
								<!-- <div class="line-it-button" data-lang="en" data-type="share-a" data-url="'https://lineit.line.me/share/ui?url=' . $url" ></div> -->
								<!-- <li><span class="list_tit"><?=$_GET_LANG['ID'];?>：</span><?=$ary_product['product_code'].$ary_product['product_ver_code'];?></li> -->
								<li><span class="list_tit"><?=$_GET_LANG['product_category'];?>：</span><?=$ary_product['cate_title'];?></li>
								<li><span class="list_tit"><?=$_GET_LANG['maker'];?>：</span><?=$ary_product['brand_title'.$_LANG];?></li>
								<li><span class="list_tit"><?=$_GET_LANG['model'];?>：</span><?=$ary_product['model_number'];?></li>
								
								<?php
									if(!empty($ary_product['product_year'])){
										echo <<<HTML
										<li><span class="list_tit">{$_GET_LANG['year_range']}：</span>{$ary_product['product_year']}</li>
HTML;
									} 
									
									if((!empty($ary_product['product_hours'])) && (($ary_product['product_hours'])>1)){
										echo <<<HTML
										<li><span class="list_tit">{$_GET_LANG['hours_used_range']}：</span>{$ary_product['product_hours']}</li>
HTML;
									} 
								?>
								<li>
									<span class="list_tit"><?=$_GET_LANG['remark'];?>：</span><br/>
									<div class="infolist_other"><?=nl2br($ary_product['product_remark']);?></div>
								</li>
							</ul>
						</div>
					</div>
				</div>
	<?php
		//類似產品
		if(!empty($str_product_similar)) {
			echo <<<HTML
				<div class="col-xs-12 col-sm-12 col-md-3">
					<h4>{$ary_product['cate_title']}</h4>
					<ul class="same_list">
						{$str_product_similar}
					</ul>
				</div>
HTML;
		}
	?>
			</div>
		</div>
	</div>
	
	<script src="https://d.line-scdn.net/r/web/social-plugin/js/thirdparty/loader.min.js" async="async" defer="defer"></script>
	<?php include_once("frontend/src/include/footer.php");?>
	<script type="text/javascript">
		$('.sel_price').on('change', function(){
			$('#product_price').text($(this).val());
		}).change();
	</script>
</body>

</html>
