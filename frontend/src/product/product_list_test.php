<?php

	include_once(PATH_DB_HELPER_SRC."product.php");		//debug
	include_once(PATH_FRONTEND_HELPER_SRC."product.php");		//product

	// echo $_var1;exit;
	$arr_get_filter = explode('/', $_var1);
	$_arr_can_filter = array('location', 'category', 'label', 'view', 'page', 'items', 'brand', 'year', 'hours', 'sort', 'weight', 'price', 'search');
	if(count($arr_get_filter) > 0){
		foreach ($arr_get_filter as $key => $value) {
			$_filter = explode('.', $value);
			if(array_search($_filter[0], $_arr_can_filter) !== false) $arr_filter[$_filter[0]] = $_filter[1];
		}
	}

	//列表排序方式處理
	$now_sort = product_sorting_dropdown_list($arr_filter['sort']);

	//區間篩選處理
	$arr_filter_range = array('year', 'hours', 'price');
	foreach ($arr_filter_range as $key => $value) {
		if(empty($arr_filter[$value])) continue;

		${'arr_range_'.$value} = explode('-', $arr_filter[$value]);
		${'arr_range_'.$value}[0] = (empty(${'arr_range_'.$value}[0]))? '':(int)${'arr_range_'.$value}[0];
		${'arr_range_'.$value}[1] = (empty(${'arr_range_'.$value}[1]))? '':(int)${'arr_range_'.$value}[1];

	}

	//side bar
	$model_cate_list = product_sidebar_list();

	// set product filter range
	$ary_range_tmp1 = db_product_range();
	$ary_range_tmp = $ary_range_tmp1[0];
	//info_msg(__FILE__, __LINE__, print_r($ary_range_tmp, true));

	$arr_range = array(
					'price' => array(
									'min' => $ary_range_tmp['min_price']/10000,
									'max' => $ary_range_tmp['max_price']/10000
								),
					'weight' => array(
									'min' => $ary_range_tmp['min_weight'],
									'max' => $ary_range_tmp['max_weight']
								),
					'year' => array(
									'min' => $ary_range_tmp['min_year'],
									'max' => $ary_range_tmp['max_year']
								),
					'hours' => array(
									'min' => $ary_range_tmp['min_hours'],
									'max' => $ary_range_tmp['max_hours']
								),
				);
	//info_msg(__FILE__, __LINE__, print_r($arr_range, true));
?>
<!doctype html>
<html lang="zh-Hant-TW">

<head>
	<?php include_once("frontend/src/include/head.php");?>
	<link rel="stylesheet" href="frontend/css/import/product_main.css">
</head>

<body>
	<?php include_once("frontend/src/include/navbar.php");?>

	<div id="filter_item">
		<div class="content">
			<div class="container">
				<div class="row">
					<div class="col-xs-12 jp_machine">
						<div class="titlebar_dubline">
							<h2><?=$_GET_LANG['used_machinery'];?></h2>
						</div>
					</div>
					<div class="col-md-3 hidden-sm hidden-xs">
						<div class="sidebar filter">
							<!-- 產品搜尋 -->
							<h3 class="side_title_top"><?=$_GET_LANG['side_search'];?></h3>
							<div class="input-group">
								<span class="input-group-btn">
									<button class="btn btn-default btn_search" type="button"><i class="fa fa-search" aria-hidden="true"></i></button>
								</span>
								<?php
								echo <<<HTML
									<input type="text" class="form-control" id="product_keyword" placeholder="{$_GET_LANG['search_placeholder_short']}">
HTML;
								?>

								<span class="input-group-btn">
									<button class="btn btn-default btn_search_remove" type="button"><i class="fa fa-times" aria-hidden="true"></i></button>
								</span>
							</div>

							<!-- 依分類分 -->
							<h3 class="side_title_top"><?=$_GET_LANG['category'];?></h3>
							<ul class="sidebar_list extend filter_ul" data-mode="category">
								<li><a href="./products" class="btn-filter" data-id=""><?=$_GET_LANG['all'];?></a></li>
								<?php echo $model_cate_list['str_model_cate'];?>
							</ul>

							<!-- 依品牌分 -->
							<h3 class="side_title"><?=$_GET_LANG['brand'];?></h3>
							<ul class="sidebar_list filter_ul brand_top" data-mode="brand">
							</ul>
								<p><a href="#" data-toggle="modal" data-target="#myModal"><?=$_GET_LANG['showall'];?></a></p>

							<!-- 篩選區塊 -->
							<div class="selectbox">
								<ul class="list-unstyled filter_range">
								<!--
									<li>
										<div class="input_g" data-mode="price">
											<label><?=sprintf($_GET_LANG['price_range'], $arr_range['price']['min'].'-'.$arr_range['price']['max']);?></label>
											<input type="number" min="<?=$arr_range['price']['min'];?>" max="<?=$arr_range['price']['max'];?>" value="<?=$arr_range_price[0];?>" class="form-control start">
											<span class="ds">-</span>
											<input type="number" min="<?=$arr_range['price']['min'];?>" max="<?=$arr_range['price']['max'];?>" value="<?=$arr_range_price[1];?>" class="form-control end">
											<button type="button" class="btn btn-default btn_range"><span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span></button>
											<button type="button" class="btn btn-default clean_x">✖</button>
										</div>
									</li>
								-->
									<li>
										<div class="input_g" data-mode="weight">
											<label><?=$_GET_LANG['weight_range'];?> (<?=$arr_range['weight']['min'].'-'.$arr_range['weight']['max'];?>)</label>
											<input type="number" min="<?=$arr_range['weight']['min'];?>" max="<?=$arr_range['weight']['max'];?>" value="<?=$arr_range_weight[0];?>" class="form-control start">
											<span class="ds">-</span>
											<input type="number" min="<?=$arr_range['weight']['min'];?>" max="<?=$arr_range['weight']['max'];?>" value="<?=$arr_range_weight[1];?>" class="form-control end">
											<button type="button" class="btn btn-default btn_range"><span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span></button>
											<button type="button" class="btn btn-default clean_x">✖</button>
										</div>
									</li>
								<!--
									<li>
										<div class="input_g" data-mode="year">
											<label><?=$_GET_LANG['year_range'];?> (<?=$arr_range['year']['min'].'-'.$arr_range['year']['max'];?>)</label>
											<input type="number" min="<?=$arr_range['year']['min'];?>" max="<?=$arr_range['year']['max'];?>" value="<?=$arr_range_year[0];?>" class="form-control filter_date start">
											<span class="ds">-</span>
											<input type="number" min="<?=$arr_range['year']['min'];?>" max="<?=$arr_range['year']['max'];?>" value="<?=$arr_range_year[1];?>" class="form-control filter_date end">
											<button type="button" class="btn btn-default btn_range"><span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span></button>
											<button type="button" class="btn btn-default clean_x">✖</button>
										</div>
									</li>
								-->
									<li>
										<div class="input_g" data-mode="hours">
											<label><?=$_GET_LANG['hours_used_range'];?> (<?=$arr_range['hours']['min'].'-'.$arr_range['hours']['max'];?>)</label>
											<input type="number" min="<?=$arr_range['hours']['min'];?>" max="<?=$arr_range['hours']['max'];?>" value="<?=$arr_range_hours[0];?>" class="form-control start">
											<span class="ds">-</span>
											<input type="number" min="<?=$arr_range['hours']['min'];?>" max="<?=$arr_range['hours']['max'];?>" value="<?=$arr_range_hours[1];?>" class="form-control end">
											<button type="button" class="btn btn-default btn_range"><span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span></button>
											<button type="button" class="btn btn-default clean_x">✖</button>
										</div>
									</li>
								</ul>
							</div>
						</div>
					</div>
					<div class="col-sm-12 col-md-9">
						<button type="button" id="filter_btn" class="btn btn-primary hidden-md hidden-lg pro_filter"><?=$_GET_LANG['btn_filter'];?></button>
						<div class="control_btn clearfix">
							<div class="right_bar">
								<div class="btn-group">
									<button class="btn btn-default dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
										<span id="span_sort"><?=$now_sort;?></span> <span class="caret"></span>
									</button>
									<ul class="dropdown-menu filter_ul" data-mode="sort">
										<li><a href="javascript:;" class="btn-filter" data-id=""><?=$_GET_LANG['sort_new_to_old'];?></a></li>
										<li><a href="javascript:;" class="btn-filter" data-id="new-asc"><?=$_GET_LANG['sort_old_to_new'];?></a></li>
										<li><a href="javascript:;" class="btn-filter" data-id="price"><?=$_GET_LANG['sort_price_high_to_low'];?></a></li>
										<li><a href="javascript:;" class="btn-filter" data-id="price-asc"><?=$_GET_LANG['sort_price_low_to_high'];?></a></li>
										<!--
											<li><a href="javascript:;" class="btn-filter" data-id="popular">依熱門程度</a></li>
										-->
									</ul>
								</div>
							</div>
						</div>

						<div id="product_data">
							
						</div>

						<!-- 頁碼 -->
						<ul class="pagebox">
						</ul>
					</div>
				</div>
			</div>
		</div>

		<!-- 品牌show all -->
		<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
			<div class="modal-dialog modal-lg" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						<h4 class="modal-title" id="myModalLabel"><?=$_GET_LANG['brand'];?></h4>
					</div>
					<div class="modal-body">
						<div class="row brand_all">
						</div>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-primary" data-dismiss="modal"><?=$_GET_LANG['close_windows'];?></button>
					</div>
				</div>
			</div>
		</div>
	</div>

	<!-- pro_filter  -->
	<div id="filter_item_mobile">
		<div class="filterbox hidden-md hidden-lg">
			<div class="container-fluid">
				<div class="row">
					<div class="col-xs-12">
						<button type="button" class="close filter_close" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					</div>
					<!-- 產品搜尋 -->
					<div class="col-xs-12 mb20">
						<h4><?=$_GET_LANG['side_search'];?></h4>
						<div class="input-group">
							<input type="text" class="form-control" id="product_keyword" placeholder="Search">
							<span class="input-group-btn">
								<button class="btn btn-default btn_search_remove" type="button"><i class="fa fa-times" aria-hidden="true"></i></button>
							</span>
						</div>
					</div>

					<!-- 分類 -->
					<div class="col-xs-12 mb20">
						<h4><?=$_GET_LANG['category'];?></h4>
						<select class="form-control" id="mobile_category">
							<option value=""><?=$_GET_LANG['all'];?></option>
							<?php echo $model_cate_list['str_model_cate_select'];?>
						</select>
					</div>

					<!-- 品牌 -->
					<div class="col-xs-12 mb20">
						<h4><?=$_GET_LANG['brand'];?></h4>
						<select class="form-control" id="mobile_brand">
						</select>
					</div>

					<!-- 篩選區塊 -->
					<div class="col-xs-12 mb20">
						<div class="selectbox mobi">
							<ul class="list-unstyled filter_range_mobile">
								<!--
								<li>
									<div class="input_g" data-mode="price">
										<label><?=$_GET_LANG['price_range'];?></label>
										<input type="number" min="<?=$arr_range['price']['min'];?>" max="<?=$arr_range['price']['max'];?>" class="form-control start" value="<?=$arr_range_price[0];?>">
										<span class="ds">-</span>
										<input type="number" min="<?=$arr_range['price']['min'];?>" max="<?=$arr_range['price']['max'];?>" class="form-control end" value="<?=$arr_range_price[1];?>">
										<button type="submit" class="btn btn-default clean_x">✖</button>
									</div>
								</li>
								-->
								<li>
									<div class="input_g" data-mode="weight">
										<label><?=$_GET_LANG['weight_range'];?></label>
										<input type="number" min="<?=$arr_range['weight']['min'];?>" max="<?=$arr_range['weight']['max'];?>" class="form-control start" value="<?=$arr_range_weight[0];?>">
										<span class="ds">-</span>
										<input type="number" min="<?=$arr_range['weight']['min'];?>" max="<?=$arr_range['weight']['max'];?>" class="form-control end" value="<?=$arr_range_weight[0];?>">
										<button type="submit" class="btn btn-default clean_x">✖</button>
									</div>
								</li>
								<!--
								<li>
									<div class="input_g" data-mode="year">
										<label><?=$_GET_LANG['year_range'];?></label>
										<input type="number" min="<?=$arr_range['year']['min'];?>" max="<?=$arr_range['year']['max'];?>" class="form-control filter_date start" value="<?=$arr_range_year[0];?>">
										<span class="ds">-</span>
										<input type="number" min="<?=$arr_range['year']['min'];?>" max="<?=$arr_range['year']['max'];?>" class="form-control filter_date end" value="<?=$arr_range_year[1];?>">
										<button type="button" class="btn btn-default clean_x">✖</button>
									</div>
								</li>
								-->
								<li>
									<div class="input_g" data-mode="hours">
										<label><?=$_GET_LANG['hours_used_range'];?></label>
										<input type="number" min="<?=$arr_range['hours']['min'];?>" max="<?=$arr_range['hours']['max'];?>" class="form-control start" value="<?=$arr_range_hours[0];?>">
										<span class="ds">-</span>
										<input type="number" min="<?=$arr_range['hours']['min'];?>" max="<?=$arr_range['hours']['max'];?>" class="form-control end" value="<?=$arr_range_hours[1];?>">
										<button type="submit" class="btn btn-default clean_x">✖</button>
									</div>
								</li>
							</ul>
						</div>
					</div>
					<div class="col-xs-12 text-center">
						<button type="button" class="btn btn-success filter_success"><?=$_GET_LANG['filter'];?></button>
					</div>
				</div>
			</div>
		</div>
	</div>
	<?php include_once("frontend/src/include/footer.php");?>
<?php
	foreach ($_arr_can_filter as $value) {
		echo <<<HTML
			<input type="hidden" class="_filter" name="filter[{$value}]" data-filter="{$value}" value="{$arr_filter[$value]}">
HTML;
	}
?>
	<script>
		$(function() {
			var _in = false;
			$('#filter_item').on('click', '.btn-filter',function() {
				var $this = $(this),
					mode = $this.parents('.filter_ul').data('mode'),
					filter_item_mobile = $('#filter_item_mobile');
				$('[name="filter['+mode+']"]').val($this.data('id'));
				switch(mode){
					case 'sort':
					case 'items':
						$('[name="filter[page]"]').val('');
						$('#span_'+mode).text($this.text());
						break;
					case 'view':
						if($this.hasClass('on')) return false;
						$this.parents('.list_mode').find('a').removeClass('on');
						$this.addClass('on');
						break;
						break;
					case 'label':
						var _label = filter_item_mobile.find('[data-mode="label"]');
						_label.find('.selected').removeClass('selected');
						_label.find('[data-id="'+$this.data('id')+'"]').addClass('selected');
						break;
					case 'location':
						var _this = $('[data-mode="label"]'),
							that = _this.parent();
						if($this.data('id') == 'HKG' || $this.data('id') == ''){
							that.show();
						}
						else{
							that.hide();
							_this.find('[data-id=""]').click();
						}
						break;
					default:
						$this.parents('.filter_ul').find('.onstyle').removeClass('onstyle');
						$this.addClass('onstyle');
						break;
				}
				update_product();
				return false;
			});

			$('.pagebox').on('click', 'li[data-page]', function() {
				$('[name="filter[page]"]').val($(this).data('page'));
				update_product();
				$('body').animate({
					scrollTop:$('#product_data').offset().top - 130
				});

				return false;
			});

			$('.filter_range .btn_range').on('click', function() {
				var $this = $(this),
					that = $this.parents('.input_g'),
					_value = that.find('.start').val() + '-' + that.find('.end').val();
				$('[name="filter['+that.data('mode')+']"]').val(_value);
				update_product();
			});

			$('.filter_range, .filter_range_mobile').on('click', '.clean_x', function() {
				var $this = $(this),
					_that = $('.input_g[data-mode="'+$this.parents('.input_g').data('mode')+'"]')
				_that.find('.start, .end').val('');
				_that.find('.btn_range').click();
				$('.filter_success').click();
			});

			update_product();
			function update_product(){
				var v_data = {
								type: $('.list_mode .on').data('id')
							};
				doajax({
					url: './product_data',										//Peter
					data: jQuery.param(v_data)+'&'+$('._filter').serialize(),
					type: 'json',
					callback: function(msg){
						$('#product_data').hide().html(msg.data).fadeIn();
						$('.brand_top').html(msg.brand[0]);
						$('.brand_all').html(msg.brand[1]);
						$('#mobile_brand').html(msg.brand[2]);
						$('.pagebox').html(msg.pagination);
						if(_in){
							history.pushState(msg.url_data, '', msg.url);   //新增紀錄
						}
						else{
							history.replaceState(msg.url_data,'', msg.url);   //新增紀錄
						}
						update_active();
						_in = true;
					}
				});
			}

			$(window).bind("popstate", function () {
				var state = event.state,
					_base = $('base').attr('href');
				$('._filter').val('');
				if(state){
					var _url = [];
					$.each(state, function(key, value){
						_url.push(key+'.'+value);
						$('[name="filter['+key+']"]').val(value);
					});
					history.replaceState(state,'',_base+"products/"+_url.join('/'));   //新增紀錄
				}
				else{
					history.replaceState(null,'',_base+"products");   //新增紀錄
				}

				var view = $('[name="filter[view]"]').val();
				if(view != 'grid') view = 'list';
				$('.list_mode .btn-filter').removeClass('on').filter('.'+view).addClass('on');

				_in = false;
				update_product();
			});

			function update_active(){
				$('.onstyle').removeClass('onstyle');
				$('._filter').not('[name="filter[label]"], [name="filter[view]"], [name="filter[page]"], [name="filter[sort]"], [name="filter[items]"]').each(function(){
					var $this = $(this),
						that = $('.filter_ul[data-mode="'+$this.data('filter')+'"]');
					that.find('[data-id="'+$this.val()+'"]').addClass('onstyle');

					switch($this.data('filter')){
						case 'brand':
							$('#mobile_brand').val($this.val());
							break;
						case 'category':
							_category = that.find('[data-id="'+$this.val()+'"]').parents('.drup-add').find('.plus');
							if(_category.get(0) && !_category.hasClass('clicked')) _category.click();
							$('#mobile_category').val($this.val());
							break;
						case 'location':
							var _that = $('[data-mode="label"]').parent();
							if($this.val() == 'HKG' || $this.val() == ''){
								_that.show();
							}
							else{
								_that.hide();
							}
							break;
						case 'search':
							$('[id="product_keyword"]').val($this.val()).keyup();
							break;
					}
				});
			}

			$('.filter_range, .filter_range_mobile').on('change', 'input', function(){
				var $this = $(this),
					that = $this.parents('.input_g'),
					_val = parseInt($this.val(), 10),
					_min = parseInt($this.attr('min'), 10),
					_max = parseInt($this.attr('max'), 10),
					_start = that.find('.start'),
					_end = that.find('.end'),
					regExp = /^[\d]+$/;

				if(!regExp.test($this.val())){
					$this.val('');
				}
				else{
					if(_val < _min){
						$this.val(_min);
					}
					else if(_val > _max){
						$this.val(_max);
					}
					if(parseInt(_start.val(), 10) > _end.val() && _end.val() != ''){
						_end.val(_start.val())
					}
				}

				var _that = $('.input_g[data-mode="'+that.data('mode')+'"]');
				_that.find('.start').val(that.find('.start').val());
				_that.find('.end').val(that.find('.end').val());
			}).on('blur', function(){
				$(this).change();
			});

			$('.filter_success').on('click', function(){
				var that = $('#filter_item_mobile'),
					_range = that.find('.filter_range_mobile');
				that.find('.filter_range_mobile .input_g').each(function(){
					var $this = $(this),
						_value = $this.find('.start').val() + '-' + $this.find('.end').val();
					if($this.find('.start').val() == '' || $this.find('.end').val() == '') _value = '';
					$('[name="filter['+$this.data('mode')+']"]').val(_value);
				});

				$('[name="filter[label]"]').val(that.find('[data-mode="label"] .selected').data('id'));
				$('[name="filter[location]"]').val(that.find('#mobile_location').val());
				$('[name="filter[brand]"]').val(that.find('#mobile_brand').val());
				$('[name="filter[category]"]').val(that.find('#mobile_category').val());
				$('[name="filter[search]"]').val(that.find('#product_keyword').val());

				update_product();
				$('.filterbox').css('display', 'none');
			});

			$('#filter_item_mobile [data-mode="label"] .btn-filter').on('click', function(){
				var $this = $(this);
				$this.parents('.filter_ul').find('.selected').removeClass('selected');
				$this.addClass('selected');
			});

			$('.btn_search').on('click', function(){
				var that = $(this).parents('.input-group');
				$('[name="filter[search]"]').val(that.find('#product_keyword').val());
				update_product();
			});

			$('[id="product_keyword"]').keypress(function (e) {
				var key = e.which;
				if(key == 13){
					$('.btn_search').click();
					return false;
				}
			});

			var search_timer;
			$('[id="product_keyword"]').on('paste', function(){
				clearInterval(search_timer);
				var $this = $(this);
				search_timer = setTimeout(function(){$this.keyup();}, 400);
			}).on('keyup', function (){
				var $this = $(this),
					that = $(this).parents('.input-group');
				that.find('.btn_search, .btn_search_remove').attr('disabled', ($this.val() == '')? true:false);
			});

			$('.btn_search_remove').on('click', function(){
				var that = $(this).parents('.input-group');
				that.find('#product_keyword').val('');
				that.find('.btn_search').click();
			});
		});
	</script>
</body>

</html>
