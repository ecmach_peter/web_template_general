<?php
	include_once(PATH_DB_HELPER_SRC."slider.php");		//debug
	include_once(PATH_DB_HELPER_SRC."product.php");		//debug
	include_once(PATH_DB_HELPER_SRC."news.php");		//debug

	include_once(PATH_FRONTEND_HELPER_SRC."slider.php");		//slider
	include_once(PATH_FRONTEND_HELPER_SRC."news.php");			//news
	include_once(PATH_FRONTEND_HELPER_SRC."about_us.php");		//about_us
	include_once(PATH_FRONTEND_HELPER_SRC."misc.php");			//misc
	include_once(PATH_FRONTEND_HELPER_SRC."product.php");		//product

// perpare the photo fot index page
	$slider_data = db_slider();
	$slider_ary = [];
	$slider_ary = parse_to_slider($slider_ary, $slider_data, PATH_FRONTEND_IMG_USR_SRC."slider");

// perpare the news fot index page
	$news_ary = news_html_ary(3, 'page_style', 'style1', true);
//	info_msg(__FILE__, __LINE__, print_r($news_ary, true));

	$arr_about = $crud->getid('about', array(), array(), array('about_index' => "about_index{$_LANG}"));

?>

<!doctype html>
<html lang="zh-Hant-TW">

<head>
	<?php include_once(PATH_FRONTEND_INCLUDE."head.php");?>
	<!-- 專案的CSS -->
	<link rel="stylesheet" href="frontend/css/import/index.css">
</head>

<body>
	<?php include_once("frontend/src/include/navbar.php");?>
	<div class="slider" class="mb50">
		<?php slider_html($slider_ary, 'container_index', 'general');?>
	</div>

	<div class="content">
		<div class="container">
			<div class="row">
				<div class="col-xs-12">
					<div class="titlebar line_bg mb20 clearfix">
						<?php bilingual_title(0, '精選產品', 'FEATURED PRODUCTS');?>
					</div>
					<div id="index_pro" class="mb20">
						<div class="owl-carousel owl-theme index_pros">
							<?php feature_product_slider();?>
						</div>
					</div>
				</div>
				<div class="col-xs-12 col-sm-8">
					<div class="titlebar line_bg clearfix">
						<?php bilingual_title(1, '最新消息', 'NEWS');?>
					</div>
					<div class="tabs_news mt20">
						<ul class="nav nav-tabs nav-justified" role="tablist">
							<?php echo $news_ary['header'];?>
						</ul>
						<div class="tab-content">
							<?php echo $news_ary['content'];?>
						</div>
					</div>
				</div>
				<div class="col-xs-12 col-sm-4">
					<div class="titlebar line_bg clearfix">
						<?php bilingual_title(1, '關於我們', ' About Us');?>
					</div>
					<img alt class="img-responsive" src="frontend/img/sys/map.PNG">
					<?=htmlspecialchars_decode($arr_about['about_index']);?>
				</div>
			</div>
		</div>
	</div>
	
	<?php include_once(PATH_FRONTEND_INCLUDE."footer.php");?>
	<script>
		// 精選產品
		$('.owl-carousel.index_pros').owlCarousel({
			loop:true,
			margin:30,
					  autoplay: true,
					  autoplayHoverPause: true,
			responsive:{
				0:{
					items:1,
					nav:false
				},
				600:{
					items:3,
					nav:false
				},
				1000:{
					items:4,
							nav:true,
									  navText:[
										"<i class='fa fa-caret-left'></i>",
										"<i class='fa fa-caret-right'></i>"
									  ]
				}
			}
		});
	</script>
</body>

</html>