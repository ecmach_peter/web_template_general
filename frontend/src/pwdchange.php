<?php
	$arr_email_verify = $crud->getid('email_verify', array('email_verify_id' => $_var1, 'email_verify_code_hash' => sha1($_var2)));
	if(empty($arr_email_verify['email_verify_id'])){
		echo '<script>alert("'.$_GET_LANG['chg_pwd_link_err'].'");location.href="/'.BASE.'";</script>';
		exit;
	}
	else if(date("Y-m-d H:i:s") > date("Y-m-d H:i:s", strtotime($arr_email_verify['email_verify_created_on']."+20 minute"))){
		echo '<script>alert("'.$_GET_LANG['chg_pwd_link_expired'].'");location.href="/'.BASE.'";</script>';
		exit;
	}

	$arr_member = $crud->getid('member', array('member_uniqid' => $arr_email_verify['user_uniqid']));

	$arr_chgpwd_title = array('FORGOTTEN PASSWORD', '修改密碼');
	if($_LANG == '') $arr_chgpwd_title = array_reverse($arr_chgpwd_title);
?>
<!doctype html>
<html lang="zh-Hant-TW">

<head>
	<?php include_once("include/head.php");?>
	<!-- 專案的CSS -->
	<link rel="stylesheet" href="css/import/signin.css">
</head>

<body>
	<?php include_once("include/navbar.php");?>
	<section class="content">
		<div class="container">
			<div class="row">
				<div class="col-xs-12">
					<ol class="breadcrumb">
						<li><a href="./"><i class="fa fa-home" aria-hidden="true"></i> <?=$_GET_LANG['home'];?></a></li>
						<li class="active"><?=$_GET_LANG['change_password'];?></li>
					</ol>
				</div>
				<div class="col-xs-12 col-sm-10 col-sm-offset-1 col-md-6 col-md-offset-3">
					<div class="titlebar line_bottom mb30 clearfix">
						<h2 class="line_title"><?=$arr_chgpwd_title[0];?> &#8260; <small><?=$arr_chgpwd_title[1];?></small></h2>
					</div>
					<form id="form_member">
						<div class="form-group">
							<label for="password"><?=$_GET_LANG['password'];?>：</label>
							<input type="password" class="form-control" id="password_n" name="password_n" placeholder="<?=$_GET_LANG['passwords_format'];?>" pattern="\w{8,}" title="請輸入英數混合，至少八位數的組合" required>
						</div>
						<div class="form-group">
							<label for="repeat"><?=$_GET_LANG['re_password'];?>：</label>
							<input type="password" class="form-control" id="password_nr" name="password_nr" required>
						</div>
						<button type="submit" class="btn btn-block " id="btn_send"><?=$_GET_LANG['change_password'];?></button>
						<input type="hidden" name="id" value="<?=$_var1;?>">
						<input type="hidden" name="code" value="<?=$_var2;?>">
					</form>
				</div>
			</div>
		</div>
	</section>
	<?php include_once("include/footer.php");?>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.2/js/bootstrap-select.min.js"></script>
	<script src="https://www.google.com/recaptcha/api.js?onload=onloadCallback&render=explicit" async defer></script>
	<script>
		$(function () {
			$('#form_member').on('submit', function(){
				var $this = $(this),
					btn_send = $('#btn_send');
				btn_send.attr('disabled', true);
				
				swal({
					title: "Are you sure?",
					text: "<?=$_GET_LANG['confirm_modify'];?>",
					showCancelButton: true,
					confirmButtonColor: "#DD6B55",
					confirmButtonText: "Yes",
					cancelButtonText: "No",
					closeOnConfirm: false,
				},
				function(isConfirm){
					if (isConfirm) {
						doajax({
							url: './domem',
							type: 'json',
							data: 'type=pwdchang&'+$this.serialize(),
							callback: function(msg){
								if(msg.sts){
									swal(msg.msg);
									location.href = "./login";
								}
								else{
									swal(msg.msg);
									btn_send.attr('disabled', false);
								}
							}
						});
					}
					else{
						btn_send.attr('disabled', false);
					}
				});
				return false;
			});
		});
	</script>
</body>

</html>
