<?php
	if(isset($_SESSION[SESSION_NAME.'_user_uniqid'])){
		header("location: ./");
		exit;
	}

	// 國際碼
	$arr_calling_code = $crud->sql("SELECT `country_calling_codes`, `country_name`
									FROM `country_calling_codes` 
									ORDER BY FIELD(`country_calling_codes`, 886, 852) DESC, `country_calling_codes`");
	$str_calling_code = '';
	if(count($arr_calling_code) > 0){
		foreach ($arr_calling_code as $key => $value) {
			$selected = ($value['country_calling_codes'] <> 886)? '':'selected';
			$str_calling_code .= <<<HTML
				<option value="{$value['country_calling_codes']}" {$selected}>+{$value['country_calling_codes']} {$value['country_name']}</option>
HTML;
		}
	}

	$arr_forget_title = array('FORGOTTEN PASSWORD', '忘記密碼');
	if($_LANG == '') $arr_forget_title = array_reverse($arr_forget_title);
?>
<!doctype html>
<html lang="zh-Hant-TW">

<head>
	<?php include_once("include/head.php");?>
	<!-- 專案的CSS -->
	<link rel="stylesheet" href="css/import/signin.css">
</head>

<body>
	<?php include_once("include/navbar.php");?>
	<section class="content">
		<div class="container">
			<div class="row">
				<div class="col-xs-12">
					<ol class="breadcrumb">
						<li><a href="./"><i class="fa fa-home" aria-hidden="true"></i> <?=$_GET_LANG['home'];?></a></li>
						<li class="active"><?=$_GET_LANG['forgotten_password'];?></li>
					</ol>
				</div>
				<div class="col-xs-12 col-sm-10 col-sm-offset-1 col-md-6 col-md-offset-3">
					<div class="titlebar line_bottom mb30 clearfix">
						<h2 class="line_title"><?=$arr_forget_title[0];?> &#8260; <small><?=$arr_forget_title[1];?></small></h2>
					</div>
					<form id="form_forgot">
						<div class="form-group">
							<label for="email"><?=$_GET_LANG['account'];?> (<?=$_GET_LANG['member_calling_code_phone_number'];?>)：</label>
							<div class="input-group">
								<span class="input-group-btn">
									<select class="form-control selectpicker" data-width="180px" data-live-search="true" id="calling_code_register" name="calling_code">
									   <?=$str_calling_code;?>
									</select>
								</span>
								<input type="text" class="form-control" id="account_register" name="account" placeholder="e.g. 912345678" title="請輸入有效的手機號碼，例：912345678" autofocus required>
							</div>
						</div>
						<div class="form-group">
							<label for="email"><?=$_GET_LANG['contact_email'];?>：</label>
							<input type="email" class="form-control" id="email" name="email" placeholder="e.g. demo@gmail.com" title="請輸入有效的電子郵件信箱，例：demo@gmail.com" autofocus required>
							<p class="help-block"><?=$_GET_LANG['forgotten_info'];?></p>
						</div>
						<div id="recaptcha_forgot" data-sitekey=""></div>
						<button type="submit" class="btn btn-block btn-green btn-send"><?=$_GET_LANG['contact_send'];?></button>
					</form>
				</div>
			</div>
		</div>
	</section>
	<?php include_once("include/footer.php");?>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.2/js/bootstrap-select.min.js"></script>
	<script src="https://www.google.com/recaptcha/api.js?onload=onloadCallback&render=explicit" async defer></script>
	<script>
		var recaptcha_forgot,
			onloadCallback = function() {
				recaptcha_forgot = grecaptcha.render('recaptcha_forgot', {
					'sitekey': '<?=RECAPTCHA_SITEKEY;?>',
					'callback': send_forgot,
					'size': 'invisible'
				});
			};

		function send_forgot(){
			form_forgot_submit();
		}
		$(function () {
			$('#form_forgot').on('submit', function(){
				grecaptcha.execute(recaptcha_forgot);
				return false;
			});
		});

		function form_forgot_submit(){
			var btn_submit = $('#btn_submit');
			btn_submit.attr('disabled', true);
			swal({
				title: "Are you sure?",
				text: "<?=$_GET_LANG['confirm_send'];?>",
				showCancelButton: true,
				confirmButtonColor: "#DD6B55",
				confirmButtonText: "Yes",
				cancelButtonText: "No",
				closeOnConfirm: false,
			},
			function(isConfirm){
				if (isConfirm) {
					doajax({
						url: 'domem',
						type: 'json',
						data: 'type=forgot&'+$('#form_forgot').serialize(),
						callback: function(msg){
							if(msg.sts){
								swal({
									title: '<?=$_GET_LANG['sent_successfully'];?>',
									text: '<?=$_GET_LANG['send_remark'];?>',
									type: 'success'
								},
								function(isConfirm){
									location.href = "./login";
								});
							}
							else{
								swal(msg.msg);
								btn_submit.attr('disabled', false);
								grecaptcha.reset();
							}
						}
					});
				}
				else{
					btn_submit.attr('disabled', false);
					grecaptcha.reset();
				}
			});
		}
	</script>
</body>

</html>
