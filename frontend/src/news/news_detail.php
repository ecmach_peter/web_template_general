<?php
	//取得最新消息資料
	$arr_news = $crud->sql_prepare("SELECT `news_id`, `news_title{$_LANG}` AS `news_title`, `news_photo`, `news_date`, `news_content{$_LANG}` AS `news_content`,
											IFNULL(
												(SELECT `brand_title{$_LANG}` AS `brand_title` 
												FROM `brand` 
												WHERE `brand`.`brand_id` = `news`.`brand_id`
												), '{$_GET_LANG['other']}'
											) AS `brand_title`,
											(SELECT `brand_url` 
											FROM `brand` 
											WHERE `brand`.`brand_id` = `news`.`brand_id`
											) AS `brand_url`
									FROM `news`
									INNER JOIN `news_cate` ON `news_cate` = `cate_id`
									WHERE `news_id` = :news_id AND `cate_online` = 1 AND `news_date` <= '{$now_date_time}' AND (`news_date_end` >= '{$now_date_time}' || `news_date_end` = '0000-00-00 00:00:00') AND `news_online` = 1",
									array('news_id' => $_var1));
	if($arr_news === false){
		include("./include.page/sys_404.php");
		exit;
	}
	$arr_news = $arr_news[0];
	if(empty($arr_news['news_id'])){
		include("./include.page/sys_404.php");
		exit;
	}
	if($_act == 'news-detail'){
		$_breadcrumb = '<li><a href="news">'.$_GET_LANG['news'].'</a></li>';
	}
	else{
		$_breadcrumb = <<<HTML
					<li><a href="javascript:;">{$_GET_LANG['featured_products']}</a></li>
					<li><a href="brand/{$arr_news['brand_url']}">{$arr_news['brand_title']}</a></li>
HTML;
	}
	
	$_customize_title = $arr_news['news_title'].' :: ';
	$og_image = PROTOCOL."://{$_SERVER['HTTP_HOST']}".BASE."uploadimages/news/{$arr_news['news_id']}/{$arr_news['news_photo']}";
	$_customize_og_image = '<meta property="og:image" content="'.$og_image.'">';
	$_customize_url = "news-detail/{$arr_news['news_id']}";
	$share_url = PROTOCOL."://{$_SERVER['HTTP_HOST']}".BASE.$_customize_url;
?>
<!doctype html>
<html lang="zh-Hant-TW">

<head>
	<?php include_once("frontend/src/include/head.php");?>

	<!-- 專案的CSS -->
	<link rel="stylesheet" href="frontend/css/import/page.css">
</head>

<body>
	<?php include_once("frontend/src/include/navbar.php");?>
		
	<div class="content">
		<div class="container">
			<div class="row">
				<div class="col-xs-12 col-md-10 col-md-offset-1">
					<ol class="breadcrumb">
						<li><a href="./"><i class="fa fa-home" aria-hidden="true"></i> <?=$_GET_LANG['home'];?></a></li>
						<?=$_breadcrumb;?>
						<li class="active"><?=$arr_news['news_title'];?></li>
					</ol>
				</div>
				<div class="col-xs-12 col-md-10 col-md-offset-1">
					<div class="textedit text-justify clearfix">
						<h3 class="text-center mb20"><?=$arr_news['news_title'];?></h3>
						<div class="titlebar line_bg mb20 clearfix">
							<ul class="post_info">
								<li>
									<span>
										<i class="fa fa-calendar-o" aria-hidden="true"></i>
										<?=$_GET_LANG['date'];?>：
									</span> 
									<?=date('Y.m.d', strtotime($arr_news['news_date']));?>
								</li>
								<li>
									<span>
										<i class="fa fa-clock-o" aria-hidden="true"></i> <?=$_GET_LANG['time'];?>：
									</span> <?=date('A h:i', strtotime($arr_news['news_date']));?>
								</li>
								<li>
									<span>
										<i class="fa fa-share-alt" aria-hidden="true"></i>
										<?=$_GET_LANG['share'];?>：
									</span>
									<div class="fb-share-button" data-href="<?=$share_url;?>" data-layout="button_count" data-size="large" data-mobile-iframe="true">
										<a class="fb-xfbml-parse-ignore" target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=<?=urlencode($share_url);?>&amp;src=sdkpreparse"><?=$_GET_LANG['share'];?>
											
										</a>
									</div>
								</li>
							</ul>
						</div>
						<div class="edit_imgbox">
							<img src="./frontend/img/usr/<?="news/{$arr_news['news_id']}/{$arr_news['news_photo']}";?>" alt="" class="img-responsive">
						</div>
						<p><?=htmlspecialchars_decode($arr_news['news_content']);?></p>
					</div>
				</div>
			</div>
		</div>
	</div>
	
	<?php include_once("frontend/src/include/footer.php");?>
</body>

</html>
