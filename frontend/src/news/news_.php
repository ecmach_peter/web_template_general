<?php
	//確認分類ID是否合法
	$arr_execute = array();
	if(empty($_var1) || $_var1 == 'all'){
		$arr_chk_cate = array(
							'cate_id' => 'all',
							'cate_title' => ''
						);
	}
	else if(!empty($_var1)){
		$arr_chk_cate = $crud->getid(
									'news_cate',
									array('cate_id' => $_var1),
									array(),
									array(
										'cate_id',
										'cate_title',
										'cate_title_en',
									)
								);
		if($arr_chk_cate === false){
			include("./include.page/sys_404.php");
			exit;
		}
	}


	//----- 分頁處理
	//計算筆數
	$count_page = $crud->sql_prepare(
										"SELECT COUNT(*) AS `count_page`
										FROM `news`
										INNER JOIN `news_cate` ON `news_cate` = `cate_id`
										WHERE `cate_online` = 1 AND `news_date` <= '{$now_date_time}' AND (`news_date_end` >= '{$now_date_time}' || `news_date_end` = '0000-00-00 00:00:00') AND `news_online` = 1 {$str_sql}",
										$arr_execute
									)[0]['count_page'];
	$pagesize = 10;	//一頁顯示筆數
	$tot_page = ceil($count_page / $pagesize);	//總頁數
	$now_page = (int)$_var2;
	$now_page = (empty($now_page) || $now_page < 1)? 1:$now_page;
	$begin_num = ($now_page - 1) * $pagesize;   //計算開始筆數

	//最新消息列表
	$arr_news = $crud->sql_prepare(
										"SELECT `news_id`, `news_date`, `news_content{$_LANG}` AS `news_content`, `news_title{$_LANG}` AS `news_title`
										FROM `news`
										INNER JOIN `news_cate` ON `news_cate` = `cate_id`
										WHERE `cate_online` = 1 AND `news_date` <= '{$now_date_time}' AND (`news_date_end` >= '{$now_date_time}' || `news_date_end` = '0000-00-00 00:00:00') AND `news_online` = 1 {$str_sql} 
										ORDER BY `news_date` DESC, `news_id`
										LIMIT {$begin_num}, {$pagesize}",
										$arr_execute
									);
	if(count($arr_news) > 0){
		foreach($arr_news as $key => $value){
			$arr_date = date_parse($value['news_date']);
			$arr_date['month'] = str_pad($arr_date['month'], 2, '0', STR_PAD_LEFT);
			$arr_date['day'] = str_pad($arr_date['day'], 2, '0', STR_PAD_LEFT);
			$news_content = mb_strimwidth(strip_tags(htmlspecialchars_decode($value['news_content'])), 0, 200, '...', 'UTF-8');
			$str_news .= <<<HTML
							<li class="clearfix">
								<div class="datebox">
									<p class="month">{$arr_date['month']}</p>
									<p class="day">{$arr_date['day']}</p>
									<span class="year">{$arr_date['year']}</span>
								</div>
								<div class="news_content">
									<h3><a href="news-detail/{$value['news_id']}">{$value['news_title']}</a></h3>
									<div class="textedit">
										<p>{$news_content}</p>
									</div>
									<p class="text-right p_more"><a href="news-detail/{$value['news_id']}">More...</a></p>
								</div>
							</li>
HTML;
		}
	}

	$arr_news_title = array('NEWS', '最新消息');
	if($_LANG == '') $arr_news_title = array_reverse($arr_news_title);
?>
<!doctype html>
<html lang="zh-Hant-TW">

<head>
	<?php include_once("include/head.php");?>

	<!-- 專案的CSS -->
	<link rel="stylesheet" href="css/import/page.css">
</head>

<body>
	<?php include_once("include/navbar.php");?>
		
	<div class="content">
		<div class="container">
			<div class="row">
				<div class="col-xs-12">
					<ol class="breadcrumb">
						<li><a href="./"><i class="fa fa-home" aria-hidden="true"></i> <?=$_GET_LANG['home'];?></a></li>
			<?php
				if(empty($arr_chk_cate['cate_title'])){
					echo '<li class="active">'.$_GET_LANG['news'].'</li>';
				}
				else{
					echo <<<HTML
						<li><a href="news">{$_GET_LANG['news']}</a></li>
						<li class="active">{$arr_chk_cate['cate_title'.$_LANG]}</li>
HTML;
				}
			?>
					</ol>
				</div>
				<div class="col-xs-12">
					<div class="titlebar line_bottom clearfix">
						<h2 class="line_title"><?=(empty($arr_chk_cate['cate_title']))? '':"{$arr_chk_cate['cate_title']}-";?><?=$arr_news_title[0];?> &#8260; <small><?="{$arr_chk_cate['cate_title_en']} ";?><?=$arr_news_title[1];?></small></h2>
					</div>
					
					<ul class="news_list full_list">
						<?=$str_news;?>
					</ul>

					<!-- 頁碼 -->
					<ul class="pagebox">
						<?php
							//一列顯示幾個item
							$_oneline_items = 5;
							$_page_link = "./news/{$arr_chk_cate['cate_id']}/";
							$_use_link = true;

							$_pagination = '';
							if($tot_page > 1){
								//上一頁處理
								$prev_page = $now_page - 1;
								$prev = ($now_page > 1)? 'data-page="'.$prev_page.'"':'';
								$prev_link = ($now_page > 1 && $_use_link)? "{$_page_link}{$prev_page}":'javascript:;';

								//下一頁處理
								$next_page = $now_page + 1;
								$next = ($now_page < $tot_page)? 'data-page="'.$next_page.'"':'';
								$next_link = ($now_page < $tot_page && $_use_link)? "{$_page_link}{$next_page}":'javascript:;';

								//起始頁處理
								$start = ($now_page > 1)? 'data-page="1"':'';
								$start_link = ($now_page > 1 && $_use_link)? $_page_link.'1':'javascript:;';

								//結束頁處理
								$end = ($now_page < $tot_page)? 'data-page="'.$tot_page.'"':'';
								$end_link = ($now_page < $tot_page && $_use_link)? $_page_link.$tot_page:'javascript:;';

								$_pagination .= <<<HTML
													<li><a href="{$prev_link}"><i class="fa fa-angle-double-left" aria-hidden="true"></i></a></li>
HTML;

								$_half_items = floor($_oneline_items / 2);
								$fix_base = ($_oneline_items % 2 == 0)? 2:1;
								$base = $now_page - $_half_items;
								if($tot_page - $now_page < $_half_items) $base = $tot_page - ($_oneline_items - $fix_base);
								if($base < 1) $base = 1;

								$max = ($now_page <= $_half_items)? $_oneline_items:$now_page + $_half_items;
								if($max > $tot_page) $max = $tot_page;

								for($i = $base; $i <= $max; $i++){
									if($i > $tot_page) break;
									$sel_page = ($i <> $now_page)? 'data-page="'.$i.'"':'';
									$active = ($i == $now_page)? 'class="on"':'';
									$_link = ($i <> $now_page && $_use_link)? $_page_link.$i:'javascript:;';
									$_pagination .= <<<HTML
													<li {$active}><a href="{$_link}">{$i}</a></li>
HTML;
								}
								$_pagination .= <<<HTML
													<li><a href="{$next_link}"><i class="fa fa-angle-double-right" aria-hidden="true"></i></a></li>
HTML;
							}

							echo $_pagination;
						?>
					</ul>
				</div>
			</div>
		</div>
	</div>
	
	<?php include_once("include/footer.php");?>
</body>

</html>
