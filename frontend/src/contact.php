<?php
	$arr_contact_title = array('contact us', '聯絡我們');
	if($_LANG == '') $arr_contact_title = array_reverse($arr_contact_title);

	$arr_about = $crud->getid('about', array(), array(), array('about_contact' => "about_contact{$_LANG}"));
?>
<!doctype html>
<html lang="zh-Hant-TW">

<head>
	<?php include_once("include/head.php");?>

	<!-- 專案的CSS -->
	<link rel="stylesheet" href="css/import/flags.css">
	<link rel="stylesheet" href="css/import/page.css">
</head>

<body>
	<?php include_once("include/navbar.php");?>
	
	<div class="content">
		<div class="container">
			<div class="row">
				<div class="col-xs-12">
					<ol class="breadcrumb">
						<li><a href="./"><i class="fa fa-home" aria-hidden="true"></i> <?=$_GET_LANG['home'];?></a></li>
						<li class="active"><?=$_GET_LANG['contact_us'];?></li>
					</ol>
				</div>
				<div class="col-xs-12 contact">
					<div class="titlebar line_bottom clearfix">
						<h2 class="line_title"><?=$arr_contact_title[0];?> &#8260; <small><?=$arr_contact_title[1];?></small></h2>
					</div>
					<div class="row">
						<div class="col-xs-12">
							<div class="textedit text-justify">
								<?=htmlspecialchars_decode($arr_about['about_contact']);?>
							</div>
						</div>
					</div>
					<form id="form_contact">
						<div class="row mt30">
							<div class="col-sm-6">
								<div class="form-group">
									<label class="control-label" for="name"><?=$_GET_LANG['contact_name'];?>：</label>
									<input type="text" class="form-control" id="name" name="name" placeholder="<?=$_GET_LANG['contact_type_name'];?>" data-required="true" required>
								</div>
								<div class="form-group">
									<label class="control-label" for="phone"><?=$_GET_LANG['contact_phone'];?>：</label>
									<input type="text" class="form-control" id="phone" name="phone" placeholder="<?=$_GET_LANG['contact_type_phone'];?>" data-required="true" required>
								</div>
								<div class="form-group">
									<label class="control-label" for="email"><?=$_GET_LANG['contact_email'];?>：</label>
									<input type="text" class="form-control" id="email" name="email" placeholder="<?=$_GET_LANG['contact_type_email'];?>"  title="請輸入有效的信箱格式" data-required="true" required>
								</div>
								<div class="form-group">
									<label class="control-label" for="company"><?=$_GET_LANG['contact_company'];?>：</label>
									<input type="text" class="form-control" id="company" name="company" placeholder="<?=$_GET_LANG['contact_type_company'];?>" data-required="true" required>
								</div>
								<div class="form-group">
									<label class="control-label"><?=$_GET_LANG['contact_country'];?>：</label>
									<br>
									<div class="flagstrap" data-input-name="country" data-selected-country="TW"></div>
								</div>
								<div class="form-group">
									<div id="country"></div>
								</div>
							</div>
							<div class="col-sm-6">
								<div class="form-group">
									<label class="control-label" for="message"><?=$_GET_LANG['contact_content'];?>：</label>
									<textarea class="form-control" id="message" name="message" rows="15" placeholder="<?=$_GET_LANG['contact_type_content'];?>" data-required="true" required></textarea>
								</div>
								<div class="form-group">
									<div id="recaptcha_contact"></div>
								</div>
								<button type="submit" class="btn btn-block btn-green mt20"><?=$_GET_LANG['contact_send'];?></button>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
	
	<?php include_once("include/footer.php");?>
<script src="https://www.google.com/recaptcha/api.js?onload=onloadCallback&render=explicit" defer></script>
<script type="text/javascript">
	var recaptcha_contact,
		onloadCallback = function(){
			recaptcha_contact = grecaptcha.render('recaptcha_contact', {
				'sitekey': '<?=RECAPTCHA_SITEKEY;?>',
				'callback': send_contact,
				'size': 'invisible'
			});
		};
	function send_contact(){
		var $this = $('#form_contact');
		doajax({
			url: './do_contact',
			data: $this.serialize(),
			type: 'json',
			callback: function(msg){
				if(msg.sts){
					location.href = './verify';
				}
				else{
					var item_id = (msg.item)? msg.item:'recaptcha_contact';
					haserr($('#'+item_id), msg.msg);
					grecaptcha.reset(recaptcha_contact);
				}
			}
		});
	}

	function haserr(item, text, focus){
		var that = item.parents('.form-group');
		if(!focus) focus = false;
		if(!that.hasClass('has-error')){
			that.addClass('has-error');
			item.after('<span class="help-block">'+text+'</span>');
		}

		if(focus){
			$('body').animate({
				scrollTop:item.parents().offset().top - 130
			});
		}
	}
	$(function(){
		$('#form_contact').on('submit', function(){
			var $this = $(this),
				form_error = false,
				focus_tag = true;
			$this.find('.help-block').remove();
			$this.find('.has-error').removeClass('has-error');
			$this.find('[data-required="true"]:enabled').each(function(e){
				var _this = $(this),
					 placeholder = (_this.data('empty'))? _this.data('empty'):'';
				if(_this.val() == ''){
					 haserr(_this, placeholder, focus_tag);
					 form_error = true;
					 focus_tag = false;
				}
			});

			if(!form_error){
				grecaptcha.execute(recaptcha_contact);
			}
			return false;
		});
		$('.flagstrap').flagStrap({
			placeholder: {
				value: "",
				text: "請選擇您的國家"
			}
		});
	});
	</script>
</body>

</html>
