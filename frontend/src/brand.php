<?php
	$arr_brand_data = $crud->getid(
									'brand', 
									array(
										'brand_url' => $_var1, 
										'brand_online' => 1
									), 
									array(), 
									array(
										'brand_id',
										'brand_website',
										'brand_view_cate',
										'brand_title' => "brand_title{$_LANG}",
										'brand_info' => "brand_info{$_LANG}",
									)
								);
	if($arr_brand_data === false){
		include("./include.page/sys_404.php");
		exit;
	}

	//廠家網站處理
	$arr_website = unserialize($arr_brand_data['brand_website']);

	//取slider
	$arr_slider = $crud->select(
							'brand_album', 
							array('host_id' => $arr_brand_data['brand_id']), 
							array('album_sort' => 'ASC'),
							array('album_photo')
						);
	$str_slider = $str_slider_li = '';
	if(count($arr_slider) > 0){
		foreach($arr_slider as $key => $value){
			$active = (empty($key))? 'active':'';
			$str_slider .= <<<HTML
					<div class="item {$active}"><img src="./uploadimages/brand/{$arr_brand_data['brand_id']}/{$value['album_photo']}"></div>
HTML;
			$str_slider_li .= <<<HTML
					<li data-target="#carousel-example-generic" data-slide-to="{$key}" class="{$active}"></li>
HTML;
		}
	}

	//取影片
	$arr_video = $crud->select(
							'brand_video', 
							array(
								'host_id' => $arr_brand_data['brand_id'],
								'video_online' => 1
							),
							array('video_sort' => 'ASC'),
							array(
								'video_url',
								"video_title{$_LANG}",
							)
						);
	$str_video = '';
	if(count($arr_video) > 0){
		foreach($arr_video as $key => $value){
			$youtubeID = getYoutubeID($value['video_url']);
			$str_video .= <<<HTML
							<div class="thumbnail np_box">
								<div class="embed-responsive embed-responsive-16by9">
									<iframe class="embed-responsive-item" src="https://www.youtube.com/embed/{$youtubeID}" frameborder="0" allowfullscreen></iframe>
								</div>
								<div class="caption text-center">
									<a href="https://www.youtube.com/embed/{$youtubeID}" target="_blank"><h3>{$value['video_title']}</h3></a>
								</div>
							</div>
HTML;
		}
	}

	//取產品目錄
	$arr_brand_file = $crud->select(
								'brand_file',
								array(
									'host_id' => $arr_brand_data['brand_id'],
									'file_online' => 1
								),
								array('file_sort' => 'ASC'),
								array(
									'file_id',
									'file_file',
									'file_file_en',
									'file_title' => "file_title{$_LANG}",
								)
							);
	$str_brand_file = '';
	if(count($arr_brand_file) > 0){
		foreach($arr_brand_file as $key => $value){
			$_file_file = $_file_file_en = '';
			if(!empty($value['file_file'])){
				$_file_file = <<<HTML
									<li><a href="./uploadimages/brand_file/{$value['file_id']}/{$value['file_file']}" target="_blank">中文版</a></li>
HTML;
			}
			if(!empty($value['file_file_en'])){
				$_file_file_en = <<<HTML
									<li><a href="./uploadimages/brand_file/{$value['file_id']}/{$value['file_file_en']}" target="_blank">English</a></li>
HTML;
			}
			$str_brand_file .= <<<HTML
							<div class="col-xs-12 col-sm-6 col-md-3 catlog">
								<div class="thumbnail np_boxs">
									<img src="images/icon/pdf.svg" alt="">
									<div class="caption text-center">
										<h3>{$value['file_title']}</h3>
										<ul class="list-inline">
											{$_file_file}
											{$_file_file_en}
										</ul>
									</div>
								</div>
							</div>
HTML;
		}
	}

	//取最新消息
	$arr_news = $crud->sql("SELECT `news_id`, `news_title`, `news_date`
							FROM `news`
							WHERE `brand_id` = {$arr_brand_data['brand_id']} AND`news_date` <= '{$now_date_time}' AND (`news_date_end` >= '{$now_date_time}' || `news_date_end` = '0000-00-00 00:00:00') AND `news_online` = 1 {$str_sql} 
							ORDER BY `news_date` DESC, `news_id`
							LIMIT 3");
	$str_news = '';
	if(count($arr_news) > 0){
		foreach($arr_news as $key => $value){
			$news_date = date('Y.m.d', strtotime($value['news_date']));
			$str_news .= <<<HTML
				<li><span class="data">{$news_date}</span> <a href="./brand-news-detail/{$value['news_id']}">{$value['news_title']}</a></li>
HTML;
		}
	}

	//取得分類
	if(!empty($arr_brand_data['brand_view_cate'])){
		$arr_cate = $crud->sql("SELECT `c1`.`cate_id`, `c2`.`cate_id` AS `par_id`, `c1`.`cate_title{$_LANG}` AS `cate_title`, `c2`.`cate_title{$_LANG}` AS `par_title`
								FROM `product`
								INNER JOIN `model` ON `model_id` = `product_model`
								INNER JOIN `model_cate` AS `c1` ON `model_cate` = `c1`.`cate_id`
								INNER JOIN `model_cate` AS `c2` ON `c1`.`cate_par_id` = `c2`.`cate_id`
								WHERE `c1`.`cate_id` IN ({$arr_brand_data['brand_view_cate']}) AND `model_brand` = {$arr_brand_data['brand_id']} AND `c2`.`cate_online` = 1 AND `c1`.`cate_online` AND `model_online` = 1 AND `product_online` = 1
								GROUP BY `c1`.`cate_id`
								ORDER BY `c2`.`cate_sort`, `c2`.`cate_id`, `c1`.`cate_sort`, `c1`.`cate_id`");
		$str_cate = $str_cate_view = '';
		if(count($arr_cate) > 0){
			$_tmp_id = '';
			foreach($arr_cate as $key => $value){
				if($_tmp_id <> $value['par_id']){
					if(!empty($str_cate)){
						$str_cate .= '</ul></li>';
					}
					$str_cate .= <<<HTML
							<li class="drup-add">
								<a href="javascript:;" class="btn-filter">{$value['par_title']}</a>
								<ul class="sidebar_drownav">
HTML;
					$_tmp_id = $value['par_id'];
				}
				$str_cate .= <<<HTML
						<li><a href="#productbox_{$value['cate_id']}" class="btn-filter" data-id="{$value['cate_id']}">{$value['cate_title']}</a></li>
HTML;
			}
			$str_cate .= '</ul></li>';

			$str_cate_view = join(',', array_column($arr_cate, 'cate_id'));
		}

		//取得分類內的產品
		if(!empty($str_cate_view)){
			$arr_product = $crud->sql("SELECT `cate_id`, `cate_title`, `cate_title_en`, `product_name{$_LANG}` AS `product_name`, `product_code`, `product_create_year`, `product_photo`
										FROM `product`
										INNER JOIN `model` ON `product_model` = `model_id`
										INNER JOIN `model_cate` ON `model_cate` = `cate_id`
										WHERE `model_cate` IN ({$str_cate_view}) AND `model_brand` = {$arr_brand_data['brand_id']}
										ORDER BY `cate_sort`");
			$str_product = $_tmp_id = '';
			if(count($arr_product) > 0){
				$number_format = 'number_format';
				foreach($arr_product as $key => $value){
					$arr_cate_title = array($value['cate_title_en'], $value['cate_title']);
					if($_LANG == '') $arr_cate_title = array_reverse($arr_cate_title);

					if($_tmp_id <> $value['cate_id']){
						$str_product .= (!empty($_tmp_id))? '</div></div>':'';
						$str_product .= <<<HTML
							<div id="productbox_{$value['cate_id']}" class="spe_product">
								<div class="titlebar line_bg mb20 clearfix">
									<h2 class="line_title">{$arr_cate_title[0]} &#8260; <small>{$arr_cate_title[1]}</small></h2>
								</div>
								<div class="owl-carousel owl-theme productbox">
HTML;
						$_tmp_id = $value['cate_id'];
					}
					/*
					Peter remove 20171025
					<div class="np_price"><p>\${$number_format($value['product_selling_price'])}</p></div>
					*/
					$str_product .= <<<HTML
								<div class="thumbnail np_box">
									<a href="./product/{$value['product_code']}"><img src="./uploadimages/product/{$value['product_create_year']}/{$value['product_code']}/{$value['product_photo']}" alt=""></a>
									<div class="caption text-center">
										<a href="./product/{$value['product_code']}"><h3>{$value['product_name']}</h3></a>
									</div>
								</div>
HTML;
				}
				$str_product .= '</div></div>';
			}
		}

	}
	
	$_customize_title = $arr_brand_data['brand_title'].' :: ';
?>
<!doctype html>
<html lang="zh-Hant-TW">

<head>
	<?php include_once("include/head.php");?>

	<!-- 專案的CSS -->
	<link rel="stylesheet" href="css/import/page.css">
</head>

<body>
	<?php include_once("include/navbar.php");?>

	<div class="content">
		<div class="container">
			<div class="row">
				<div class="col-xs-12">
					<ol class="breadcrumb">
						<li><a href="./"><i class="fa fa-home" aria-hidden="true"></i> <?=$_GET_LANG['home'];?></a></li>
						<li class="active"><?=$arr_brand_data['brand_title'];?></li>
					</ol>
				</div>
				<div class="col-xs-12 mb30">
					<div class="titlebar line_bottom clearfix">
						<h2 class="line_title"><?=$arr_brand_data['brand_title'];?></h2>
					</div>
				</div>
				<div class="col-xs-12 col-md-2">
					<div class="sidebar visible-md visible-lg">
						<ul id="mainNav" class="sidebar_list">
							<li><a href="#news"><?=$_GET_LANG['manufacturers_news'];?></a></li>
							<li><a href="#catalog"><?=$_GET_LANG['product_catalog_download'];?></a></li>
							<li><a href="#videobox"><?=$_GET_LANG['video'];?></a></li>
				<?php
					if(!empty($arr_website['href'])){
						$target = (isset($arr_website['target']))? 'target="_blank"':'';
						echo <<<HTML
							<li><a href="{$arr_website['href']}" {$target}>{$_GET_LANG['manufacturers_website']}</a></li>
HTML;
					}
				?>
							<?=$str_cate;?>
						</ul>
					</div>
				</div>
				<div class="col-xs-12 col-md-10">
					<div class="bannerbar">
						<div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
							<!-- Indicators -->
							<ol class="carousel-indicators">
								<?=$str_slider_li;?>
							</ol>
							<!-- Wrapper for slides -->
							<div class="carousel-inner" role="listbox">
								<?=$str_slider;?>
							</div>
							<!-- Controls -->
							<a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
								<span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
								<span class="sr-only">Previous</span>
							</a>
							<a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
								<span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
								<span class="sr-only">Next</span>
							</a>
						</div>
					</div>
					<div class="spe_product company_infobox">
						<div class="titlebar line_bg mb20 clearfix">
							<h2 class="line_title"><?=($_LANG == '')? '廠家介紹':'MANUFACTURERS PROFILE';?> &#8260; <small><?=($_LANG == '')? 'MANUFACTURERS PROFILE':'廠家介紹';?></small></h2>
						</div>
						<div class="textedit text-justify">
							<h5><?=$arr_brand_data['brand_title'].$_GET_LANG['profile'];?></h5>
							<?=nl2br($arr_brand_data['brand_info']);?>
						</div>
					</div>
					<div id="news" class="spe_product news">
						<div class="titlebar line_bg mb20 clearfix">
							<h2 class="line_title"><?=($_LANG == '')? '最新消息':'NEWS';?> &#8260; <small><?=($_LANG == '')? 'NEWS':'最新消息';?></small></h2>
						</div>
						<ul class="list-unstyled news_list_pro">
							<?=$str_news;?>
						</ul>
					</div>
					
<?php
	if(!empty($str_brand_file)){
		$arr_file_title = array('Download Catalog', '產品目錄下載');
		if($_LANG == '') $arr_file_title = array_reverse($arr_file_title);
		echo <<<HTML
					<div id="catalog" class="spe_product">
						<div class="titlebar line_bg mb20 clearfix">
							<h2 class="line_title">{$arr_file_title[0]} &#8260; <small>{$arr_file_title[1]}</small></h2>
						</div>
						<div class="row">
							{$str_brand_file}
						</div>
					</div>
HTML;
	}
	if(!empty($str_video)){
		$arr_video_title = array('Video', '影片介紹');
		if($_LANG == '') $arr_video_title = array_reverse($arr_video_title);
		echo <<<HTML
					<div id="videobox" class="spe_product">
						<div class="titlebar line_bg mb20 clearfix">
							<h2 class="line_title">{$arr_video_title[0]} &#8260; <small>{$arr_video_title[1]}</small></h2>
						</div>
						<div class="owl-carousel owl-theme videobox">
							{$str_video}
						</div>
					</div>
HTML;
	}
?>
					<?=$str_product;?>
				</div>
			</div>
		</div>
	</div>
	
	<?php include_once("include/footer.php");?>
	<script>
		var owl = $('.owl-carousel.productbox');

		// 影片
		$('.owl-carousel.productbox').owlCarousel({
			loop:true,
			dots: false,
			margin:30,
			navText: [
						"<i class='fa fa-caret-left'></i>",
						"<i class='fa fa-caret-right'></i>"
					  ],
			responsiveClass:true,
			responsive:{
				0:{
					items:1,
					nav:true
				},
				600:{
					items:3,
					nav:true
				},
				1000:{
					items:4,
					nav:true
				}
			}
		});

		//液壓
		$('.owl-carousel.other').owlCarousel({
			loop:true,
			dots: false,
			margin:30,
			navText: [
						"<i class='fa fa-caret-left'></i>",
						"<i class='fa fa-caret-right'></i>"
					  ],
			responsiveClass:true,
			responsive:{
				0:{
					items:1,
					nav:true
				},
				600:{
					items:3,
					nav:true
				},
				1000:{
					items:4,
					nav:true
				}
			}
		});

		//影片
		$('.owl-carousel.videobox').owlCarousel({
			loop:true,
			dots: false,
			margin:30,
			navText: [
						"<i class='fa fa-caret-left'></i>",
						"<i class='fa fa-caret-right'></i>"
					  ],
			responsiveClass:true,
			responsive:{
				0:{
					items:1,
					nav:true
				},
				600:{
					items:3,
					nav:true
				},
				1000:{
					items:3,
					nav:true
				}
			}
		});

		owl.on('mousewheel', '.owl-stage', function (e) {
			if (e.deltaY>0) {
				owl.trigger('next.owl');
			} else {
				owl.trigger('prev.owl');
			}
			e.preventDefault();
		});
	</script>
</body>

</html>
