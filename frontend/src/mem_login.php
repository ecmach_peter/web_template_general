<?php
	if(isset($_SESSION[SESSION_NAME.'_user_uniqid'])){
		header("location: ./");
		exit;
	}

	$arr_calling_code = $crud->sql("SELECT * 
									FROM `country_calling_codes` 
									ORDER BY FIELD(`country_calling_codes`, 886, 852) DESC, `country_calling_codes`");
	$str_calling_code = '';
	if(count($arr_calling_code) > 0){
		foreach ($arr_calling_code as $key => $value) {
			$selected = ($value['country_calling_codes'] <> 886)? '':'selected';
			$str_calling_code .= <<<HTML
				<option value="{$value['country_calling_codes']}" {$selected}>+{$value['country_calling_codes']} {$value['country_name']}</option>
HTML;
		}
	}

	//這側單元直接點選
	if($_act == 'register'){
		$str_default = <<<JAVASCRIPT
			$('#tab_register').click();
JAVASCRIPT;
	}

	$arr_login_title = array('LOGIN', '會員登入');
	$arr_register_title = array('REGISTER', '會員註冊');
	if($_LANG == ''){
		$arr_login_title = array_reverse($arr_login_title);
		$arr_register_title = array_reverse($arr_register_title);
	}
?>
<!doctype html>
<html lang="zh-Hant-TW">

<head>
	<?php include_once("include/head.php");?>
	<!-- 專案的CSS -->
	<link rel="stylesheet" href="css/import/signin.css">
</head>

<body>
	<?php include_once("include/navbar.php");?>
	<section class="content">
		<div class="container">
			<div class="row">
				<div class="col-xs-12 mb30">
				</div>
				<div class="col-xs-12 col-sm-10 col-sm-offset-1 col-md-6 col-md-offset-3 login_block">
					<!-- Nav tabs -->
					<ul class="nav nav-tabs nav-justified" role="tablist">
						<li role="presentation" class="active">
							<a href="#loginbox" aria-controls="loginbox" role="tab" data-toggle="tab">
								<h2><?=$arr_login_title[0];?></h2>
								<small><?=$arr_login_title[1];?></small>
							</a>
						</li>
						<li role="presentation">
							<a href="#registerbox" aria-controls="registerbox" role="tab" data-toggle="tab" id="tab_register">
								<h2><?=$arr_register_title[0];?></h2>
								<small><?=$arr_register_title[1];?></small>
							</a>
						</li>
					</ul>
					<!-- Tab panes -->
					<div class="tab-content">
						<div role="tabpanel" class="tab-pane active" id="loginbox">
							<form id="form_signin">
								<div class="form-group">
									<label for="email"><?=$_GET_LANG['account'];?> (<?=$_GET_LANG['member_calling_code_phone_number'];?>)：</label>
									<div class="input-group">
										<span class="input-group-btn">
											<select class="form-control selectpicker" data-width="180px" data-live-search="true" id="calling_code_register" name="calling_code">
												<?=$str_calling_code;?>
											</select>
										</span>
										<input type="text" class="form-control" id="account_register" name="account" placeholder="e.g. 912345678" title="請輸入有效的手機號碼，例：912345678" autofocus required>
									</div>
								</div>
								<div class="form-group">
									<label for="password"><?=$_GET_LANG['password'];?>：</label>
									<input type="password" class="form-control" id="password" name="password" required>
								</div>
								<div class="form-group">
									<div class="row">
										<div class="col-sm-12">
											<p class="text-right">
												<a href="./forget"><?=$_GET_LANG['forgotten_password'];?></a>
											</p>
										</div>
									</div>
								</div>
								<div id="recaptcha_login"></div>
								<button type="submit" class="btn btn-block btn-green" id="btn_submit"><?=$_GET_LANG['login'];?></button>
							</form>
						</div>
						<div role="tabpanel" class="tab-pane" id="registerbox">
							<form id="form_register">
								<div class="form-group">
									<label for="email"><?=$_GET_LANG['account'];?> (<?=$_GET_LANG['member_calling_code_phone_number'];?>)：</label>
									<div class="input-group">
										<span class="input-group-btn">
											<select class="form-control selectpicker" data-width="180px" data-live-search="true" id="calling_code_register" name="calling_code">
												<?=$str_calling_code;?>
											</select>
										</span>
										<input type="text" class="form-control" id="account_register" name="account" placeholder="e.g. 912345678" title="請輸入有效的手機號碼，例：912345678" autofocus required>
									</div>
									<p class="help-block"><?=$_GET_LANG['register_number_notify'];?></p>
								</div>
								<div class="form-group">
									<label for="name"><?=$_GET_LANG['contact_name'];?>/<?=$_GET_LANG['nickname'];?>：</label>
									<input type="name" class="form-control" id="name" name="name" required>
								</div>
								<div class="form-group">
									<label for="email"><?=$_GET_LANG['contact_email'];?>：</label>
									<input type="email" class="form-control" id="email" name="email" placeholder="<?=$_GET_LANG['contact_type_email'];?>" title="" required>
								</div>
								<div class="form-group">
									<label for="password_register"><?=$_GET_LANG['password'];?>：</label>
									<input type="password" class="form-control" id="password_register" name="password" placeholder="<?=$_GET_LANG['passwords_format'];?>" pattern="\w{8,}" title="<?=$_GET_LANG['passwords_format'];?>" required>
								</div>
								<div class="form-group">
									<label for="repeat_register"><?=$_GET_LANG['re_password'];?>：</label>
									<input type="password" class="form-control" id="repeat_register" name="re_password" required>
								</div>
								<div class="checkbox">
									<label>
										<input type="checkbox" name="chk_terms" value="ON" required> <?=$_GET_LANG['agree_privacy'];?>
									</label>
								</div>
								<div id="recaptcha_register"></div>
								<button type="submit" class="btn btn-block btn-green" id="btn_submit"><?=$_GET_LANG['register'];?></button>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<?php include_once("include/footer.php");?>
	<script src="https://www.google.com/recaptcha/api.js?onload=onloadCallback&render=explicit" async defer></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.2/js/bootstrap-select.min.js"></script>
	<script>
		var $_LANG = {
				'register_thk_title': '<?=$_GET_LANG['register_thk_title'];?>',
				'register_thk_msg': '<?=$_GET_LANG['register_thk_msg'];?>'
			};
		var recaptcha_login,
			recaptcha_register,
			onloadCallback = function() {
				recaptcha_login = grecaptcha.render('recaptcha_login', {
					'sitekey': '<?=RECAPTCHA_SITEKEY;?>',
					'callback': send_signin,
					'size': 'invisible'
				});
				recaptcha_register = grecaptcha.render('recaptcha_register', {
					'sitekey': '<?=RECAPTCHA_SITEKEY;?>',
					'callback': send_register,
					'size': 'invisible'
				});
			};

		function send_signin(){
			form_signin_submit();
		}
		function send_register(){
			form_register_submit();
		}
		$(function () {
			$('#form_signin').on('submit', function(){
				grecaptcha.execute(recaptcha_login);
				return false;
			});

			$('#form_register').on('submit', function(){
				grecaptcha.execute(recaptcha_register);
				return false;
			});
		});

		function form_signin_submit(){
			var $this = $('#form_signin'),
				btn_submit = $this.find('#btn_submit');
			btn_submit.attr('disabled', true);
			doajax({
				url: 'domem',
				type: 'json',
				data: 'type=login&'+$this.serialize(),
				callback: function(msg){
					(msg.sts)? location.href = "./"+msg.url:swal(msg.msg);
					btn_submit.attr('disabled', false);
					grecaptcha.reset(recaptcha_login);
				}
			});
		}

		function form_register_submit(){
			var $this = $('#form_register'),
				btn_submit = $this.find('#btn_submit');
			btn_submit.attr('disabled', true);
			doajax({
				url: 'domem',
				type: 'json',
				data: 'type=register&'+$this.serialize(),
				callback: function(msg){
					if(msg.sts){
						swal({
							title: $_LANG.register_thk_title,
							text: $_LANG.register_thk_msg,
							type: "success",
							confirmButtonColor: "#DD6B55",
							confirmButtonText: "OK",
						},
						function(){
							location.href="./"+msg.url
						});
					}
					else{
						swal(msg.msg);

						btn_submit.attr('disabled', false);
						grecaptcha.reset(recaptcha_register);
					}
				}
			});
		}
		<?=$str_default;?>
	</script>
</body>

</html>
