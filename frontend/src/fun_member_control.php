<?php
	if(!isset($_SERVER['HTTP_X_REQUESTED_WITH']) || !strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' || empty($_act)){
		header("X-Robots-Tag: noindex, nofollow", true);
		header('HTTP/1.0 403 Forbidden');
		exit;
	}
	$type = get_post_var('type');
	switch($type){
		//確認登入狀態
		case 'chk_login':
			$arr['sts'] = (isset($_SESSION[SESSION_NAME.'_user_uniqid']))? true:false;
			echo json_encode($arr);
			break;

		//註冊會員
		case 'register':
		// print_r($_POST);exit;
			require_once('include/sanitize.php'); 
			try{
				$arr['sts'] = true;
				//驗證檢查
				$url = sprintf('https://www.google.com/recaptcha/api/siteverify?secret=%s&response=%s&remoteip=%s', RECAPTCHA_SECRETKEY, $_POST['g-recaptcha-response'], $_SERVER['REMOTE_ADDR']);
				$res = json_decode(getcurl($url));  //不可使用file_get_contents，會抓不回來
				if(!$res->success){
					throw new Exception($_GET_LANG['verify_invalid']);
				}
				else if(empty(get_post_var('chk_terms'))){
					throw new Exception($_GET_LANG['agree_privacy']);
				}

				//國碼驗證
				$arr_country_code = $crud->getid('country_calling_codes', array('country_calling_codes' => get_post_var('calling_code')));
				if(empty($arr_country_code['country_calling_codes'])){
					throw new Exception($_GET_LANG['register_calling_code_invalid']);
				}

				if(empty(get_post_var('account'))){
					throw new Exception($_GET_LANG['empty_account']);
				}
				else if(!preg_match("/^([0-9]+)$/", get_post_var('account'))){
					throw new Exception($_GET_LANG['account_format_error']);
				}

				//檢查帳號是否註冊過
				$arr_member = $crud->getid('member', array('member_calling_code' => $arr_country_code['country_calling_codes'], 'member_account' => get_post_var('account')));
				if(!empty($arr_member['member_id'])){
					throw new Exception($_GET_LANG['account_already_register']);
				}
				else if(empty(get_post_var('email'))){
					throw new Exception($_GET_LANG['email_invalid']);
				}
				else if(!filter_var(get_post_var('email'), FILTER_VALIDATE_EMAIL)){
					throw new Exception($_GET_LANG['email_invalid']);
				}

				//檢查姓名
				if(!sanitize_input_srt(get_post_var('name'), 30)){
					$item = 'name';
					throw new Exception(sprintf($_GET_LANG['input_too_long'], ($_LANG == '')? '姓名':'Name', 30));
				}

				//檢查電子信箱是否使用過
				$arr_member = $crud->getid('member', array('member_email' => get_post_var('email')));
				if(!empty($arr_member['member_id'])){
					throw new Exception($_GET_LANG['email_already_register']);
				}
				else if(empty(get_post_var('password')) || empty(get_post_var('re_password'))){
					throw new Exception($_GET_LANG['empty_password']);
				}
				else if(get_post_var('password') <> get_post_var('re_password')){
					throw new Exception($_GET_LANG['passwords_not_match']);
				}
				else if(!preg_match("/^([0-9A-Za-z]{8,}+)$/", get_post_var('password'))){
					throw new Exception($_GET_LANG['passwords_format_error']);
				}
				else{
					$account = array($arr_country_code['country_calling_codes'], get_post_var('account'));
					$password = get_post_var('password');

					
					include_once("include/class.member.php");
					$_member = new member($crud);
					$_member->register($account, $password, get_post_var('email'));
					$arr['url'] = $_SESSION[SESSION_NAME.'_nowstep'];
					// $arr['url'] = 'member';
				}

				echo json_encode($arr);
			}
			catch(Exception $e){
				echo json_encode(
						array(
							'sts' => false,
							'msg' => $e->getMessage(),
						)
					);
			}
			break;

		//會員登入
		case 'login':
			// $arr['sts'] = true;
			try{
				$url = sprintf('https://www.google.com/recaptcha/api/siteverify?secret=%s&response=%s&remoteip=%s', RECAPTCHA_SECRETKEY, $_POST['g-recaptcha-response'], $_SERVER['REMOTE_ADDR']);
				$res = json_decode(getcurl($url));  //不可使用file_get_contents，會抓不回來
				if(!$res->success){
					throw new Exception($_GET_LANG['verify_invalid']);
				}
				else if(empty(get_post_var('account'))){
					throw new Exception($_GET_LANG['empty_account']);
				}
				else if(empty(get_post_var('password'))){
					throw new Exception($_GET_LANG['empty_password']);
				}
				else if(!preg_match("/^([0-9]+)$/", get_post_var('account'))){
					throw new Exception($_GET_LANG['account_format_error']);
				}

				//國碼驗證
				$arr_country_code = $crud->getid('country_calling_codes', array('country_calling_codes' => get_post_var('calling_code')));
				if(empty($arr_country_code['country_calling_codes'])){
					throw new Exception($_GET_LANG['register_calling_code_invalid']);
				}
				else{
					$account = array($arr_country_code['country_calling_codes'], get_post_var('account'));
					$password = get_post_var('password');
					
					include_once("include/class.member.php");
					$_member = new member($crud);
					$arr = $_member->login($account, $password);
					$arr['url'] = $_SESSION[SESSION_NAME.'_nowstep'];
				}

				echo json_encode($arr);
			}
			catch(Exception $e){
				echo json_encode(
						array(
							'sts' => false,
							'msg' => $e->getMessage(),
						)
					);
			}
			break;

		//忘記密碼
		case 'forgot':
			try{
				$url = sprintf('https://www.google.com/recaptcha/api/siteverify?secret=%s&response=%s&remoteip=%s', RECAPTCHA_SECRETKEY, $_POST['g-recaptcha-response'], $_SERVER['REMOTE_ADDR']);
				$res = json_decode(getcurl($url));  //不可使用file_get_contents，會抓不回來
				if(!$res->success && false){
					throw new Exception($_GET_LANG['verify_invalid']);
				}

				//國碼驗證
				$arr_country_code = $crud->getid('country_calling_codes', array('country_calling_codes' => get_post_var('calling_code')));
				if(empty($arr_country_code['country_calling_codes'])){
					throw new Exception($_GET_LANG['register_calling_code_invalid']);
				}
				else if(!preg_match("/^([0-9]+)$/", get_post_var('account'))){
					throw new Exception($_GET_LANG['account_format_error']);
				}
				else if(!filter_var(get_post_var('email'), FILTER_VALIDATE_EMAIL)){
					throw new Exception($_GET_LANG['email_invalid']);
				}
				else{
					$arr_member = $crud->getid('member', array('member_calling_code' => get_post_var('calling_code'), 'member_account' => get_post_var('account'), 'member_email' => get_post_var('email')));

					if(empty($arr_member['member_id'])){
						throw new Exception($_GET_LANG['can_not_find_account']);
					}
					// else if($arr_member['member_type'] == 2){
					// 	throw new Exception('您為Facebook會員');
					// }
					else{
						$code = substr(base64_encode(iizi_random_bytes(2)), 0, 6);
						$arr_data = array(
										'member_uniqid' => $arr_member['member_uniqid'],
										'email_verify_code_hash' => sha1($code),
										'email_verify_created_on' => date('Y-m-d H:i:s')
									);
						$email_verify_id = $crud->create('email_verify', $arr_data);

						$send_data = serialize(
										array(
											'email_verify_code' => $code,
											'email_verify_id' => $email_verify_id,
											'email_account' => '+'.$arr_country_code['country_calling_codes'].$arr_member['member_account'],
											'email_sendto' => $arr_member['member_email'],
											'email_verify_created_on' => date('Y-m-d H:i:s')
										)
									);
						$return = $crud->create('email_send_temp', array('send_type' => 'forget', 'send_data' => $send_data));
						$arr['sts'] = true;
					}
				}
				// print_r($_POST);
				echo json_encode($arr);
			}
			catch(Exception $e){
				echo json_encode(
						array(
							'sts' => false,
							'msg' => $e->getMessage(),
						)
					);
			}
			break;

		//忘記密碼使用的修改密碼
		case 'pwdchang':
			if(empty(get_post_var('password_n')) || empty(get_post_var('password_nr'))){
				$arr['sts'] = false;
				$arr['msg'] = '請輸入新密碼';
			}
			else if(get_post_var('password_n') <> get_post_var('password_nr')){
				$arr['sts'] = false;
				$arr['msg'] = '輸入的新密碼不一致';
			}
			else if(!preg_match("/^([0-9A-Za-z]{8,}+)$/", get_post_var('password_n'))){
				$arr['sts'] = false;
				$arr['msg'] = "密碼格式錯誤\n請輸入至少八位數的英數組合";
			}
			else{
				$id = get_post_var('id');
				$code = get_post_var('code');
				$arr_email_verify = $crud->getid('email_verify', array('email_verify_id' => $id, 'email_verify_code_hash' => sha1($code)));

				if(empty($arr_email_verify['email_verify_id'])){
					$arr['sts'] = false;
					$arr['msg'] = '連結異常，請重整再試或與客服人員聯絡';
				}
				else{
					$password = get_post_var('password_n');
					$hashed_password = password_hash($password, PASSWORD_DEFAULT, array('cost'=>12));

					//修改會員資料
					$result = $crud->update('member', array('member_password' => $hashed_password), array('member_uniqid' => $arr_email_verify['member_uniqid']));
					if($result){
						$arr['sts'] = true;
						$arr['msg'] = '密碼修改完成，請您以新密碼登入';
						$crud->delete('email_verify', array('email_verify_id' => $id));
					}
					else{
						$arr['sts'] = false;
						$arr['msg'] = '系統異常，請您重新整理再試或與客服人員聯絡';
					}
				}
			}

			echo json_encode($arr);
			break;

		//會員中心修改密碼
		case 'mempwdchang':
			$arr['sts'] = true;
			$arr_member = $crud->getid('member', array('member_uniqid' => $_SESSION[SESSION_NAME.'_user_uniqid']));

			if(empty(get_post_var('password_o'))){
				$arr['sts'] = false;
				$arr['msg'] = $_GET_LANG['empty_old_password'];
			}
			else if(!password_verify(get_post_var('password_o') , $arr_member['member_password'])){
				$arr['sts'] = false;
				$arr['msg'] = $_GET_LANG['invalid_password'];
			}
			else if(empty(get_post_var('password_n')) || empty(get_post_var('password_nr'))){
				$arr['sts'] = false;
				$arr['msg'] = $_GET_LANG['empty_new_password'];
			}
			else if(get_post_var('password_n') <> get_post_var('password_nr')){
				$arr['sts'] = false;
				$arr['msg'] = $_GET_LANG['passwords_not_match'];
			}
			else if(!preg_match("/^([0-9A-Za-z]{8,}+)$/", get_post_var('password_n'))){
				$arr['sts'] = false;
				$arr['msg'] = $_GET_LANG['passwords_format_error'];
			}
			else{
				$password = get_post_var('password_n');
				$hashed_password = password_hash($password, PASSWORD_DEFAULT, array('cost'=>12));
				$arr_data['member_password'] = $hashed_password;

				//修改會員資料
				$result = $crud->update('member', $arr_data, array('member_uniqid' => $arr_member['member_uniqid']));
				if(!$result){
					$arr['sts'] = false;
					$arr['msg'] = $_GET_LANG['server_error'];
				}
				else{
					$_SESSION[SESSION_NAME.'_nowstep'] = 'member';
					$arr['msg'] = $_GET_LANG['password_changed'];
				}
			}

			echo json_encode($arr);
			break;

		//會員資料修改
		case 'edit':
			$arr_data = array(
							'member_name' => (empty(get_post_var('member_name')))? '':get_post_var('member_name'),
							'member_nickname' => (empty(get_post_var('member_nickname')))? '':get_post_var('member_nickname'),
							'member_mobile1' => (empty(get_post_var('member_mobile1')))? '':get_post_var('member_mobile1'),
							'member_mobile2' => (empty(get_post_var('member_mobile2')))? '':get_post_var('member_mobile2'),
							'member_lineID' => (empty(get_post_var('member_lineID')))? '':get_post_var('member_lineID'),
							'member_skypeID' => (empty(get_post_var('member_skypeID')))? '':get_post_var('member_skypeID')
						);

			$arr_member = $crud->getid('member', array('member_uniqid' => $_SESSION[SESSION_NAME.'_user_uniqid'], 'member_get_full_info_point' => 0));
			if(!empty($arr_member['member_id'])){
				$full_info = true;
				foreach ($arr_data as $value) {
					if(empty($value)){
						$full_info = false;
						break;
					}
				}

				if($full_info){
					//完備資料贈送點數
					$arr_member_integral_project = $crud->select('member_integral_project', array('project_fullinfo' => 1));
					if(count($arr_member_integral_project > 0)){
						foreach ($arr_member_integral_project as $key => $value) {
							//新增會員點數異動明細
							$arr_data_record = array(
											'member_uniqid' => $arr_member['member_uniqid'],
											'record_integral' => $value['project_integral'],
											'record_note' => $value['project_note'],
											'record_time' => date('Y-m-d H:i:s'),
											'record_changer' => 'full_info'
										);
							$result = $crud->create('member_integral_record', $arr_data_record);

							//更新會員點數
							$result = $crud->sql("UPDATE `member`
												SET `member_get_full_info_point` = 1, `member_integral` = `member_integral` + {$value['project_integral']}
												WHERE `member_uniqid` = '{$arr_member['member_uniqid']}'");
						}
					}
				}
			}

			//修改會員資料
			$result = $crud->update('member', $arr_data, array('member_uniqid' => $_SESSION[SESSION_NAME.'_user_uniqid']));
			if(!$result){
				$arr['sts'] = false;
				$arr['msg'] = $_GET_LANG['server_error'];
			}
			else{
				$arr['sts'] = true;
				$arr['msg'] = $_GET_LANG['modify_successfully'];
			}
			echo json_encode($arr);
			break;

		//取得已批出標籤
		case 'label':
			include './backend/include/alias_function.php';
			//排序參數處理
			switch (get_post_var('sort')) {
				case 'model':
					$SQL_sort = ' `model_number` DESC, `model_version` DESC';
					break;
				case 'model-asc':
					$SQL_sort = ' `model_number` ASC, `model_version` ASC';
					break;
				case 'new-asc':
					$SQL_sort = ' `edit_time` ASC';
					break;
				default:
					$SQL_sort = ' `edit_time` DESC';
					break;
			}

			//搜尋參數處理
			$keyword = addslashes(trim(strip_tags(get_post_var('keyword'))));
			$sql_search = '';
			if(!empty($keyword)){
				$sql_search = " AND (
										`label_mode` LIKE '%{$keyword}%' OR
										`brand_title{$_LANG}` LIKE '%{$keyword}%' OR
										CONCAT (`model_number`, `model_version`) LIKE '%{$keyword}%' OR
										`label_seria_num` LIKE '%{$keyword}%' OR
										`label_NRMM_code` LIKE '%{$keyword}%' OR
										`label_NRMM_label_code` LIKE '%{$keyword}%' OR 
										`label_QPME_label_code` LIKE '%{$keyword}%'
									)";
			}

			//取出會員標籤
			$arr_label = $crud->sql("SELECT `invoice_status`, `label_mode`, `brand_title{$_LANG}` AS `brand_title`, `model_number`, `model_version`, `label_seria_num`, `label_NRMM_code`, `label_id`, `label_QPME_file`, `label_NRMM_file`, `label_NRMM_label_code`, `label_QPME_label_code`, `label`.`invoice_id`, `invoice_file`, `invoice_code`
									FROM `label`
									INNER JOIN `model` ON `model`.`model_id` = `label`.`model_id`
									INNER JOIN `brand` ON `model_brand` = `brand_id`
									INNER JOIN `invoice` ON `invoice`.`invoice_id` = `label`.`invoice_id`
									WHERE `label`.`member_uniqid` = '{$_SESSION[SESSION_NAME.'_user_uniqid']}' {$sql_search}
									ORDER BY {$SQL_sort}");
			if($arr_news === false){
				include("./include.page/sys_404.php");
			}
			if(count($arr_label) > 0){
				foreach($arr_label as $key => $value){
					$_function = 'get_invoice_status'.$_LANG;
					$invoice_status = $_function($value['invoice_status']);
					$str_label .= <<<HTML
								<tr data-expanded="true">
									<td>{$value['label_mode']}</td>
									<td>{$value['brand_title']}</td>
									<td>{$value['model_number']}-{$value['model_version']}</td>
									<td>{$value['label_seria_num']}</td>
									<td>{$value['label_NRMM_code']}</td>
									<td><a href="./uploadimages/label/{$value['label_id']}/{$value["label_{$value['label_mode']}_file"]}" download="{$value["label_{$value['label_mode']}_label_code"]}">{$value["label_{$value['label_mode']}_label_code"]}</a></td>
									<td><a href="./uploadimages/invoice/{$value['invoice_id']}/{$value['invoice_file']}" download="{$value['invoice_code']}">{$value['invoice_code']}</a></td>
									<td>{$invoice_status}</td>
								</tr>
HTML;
				}
			}


			echo json_encode(
					array(
						'sts' => true,
						'data' => $str_label,
					)
				);
			break;

		//參數表及其他文件
		case 'download_files':
			$_cate_id = get_post_var('cate_id');
			$_brand_id = get_post_var('brand_id');

			$arr_execute = array();

			//搜尋參數處理
			$keyword = addslashes(trim(strip_tags(get_post_var('keyword'))));
			$sql_search = '';
			if(!empty($keyword)){
				$sql_search = " AND (
										`c2`.`cate_title{$_LANG}` LIKE :keyword OR
										`c1`.`cate_title{$_LANG}` LIKE :keyword OR
										`brand_title{$_LANG}` LIKE :keyword OR
										CONCAT (`model_number`, `model_version`) LIKE :keyword
									)";
				$arr_execute['keyword'] = "%{$keyword}%";
			}

			$sql_filter = '';
			if(!empty($_cate_id)){
				$sql_filter = ' AND `c1`.`cate_id` = :cate_id';
				$arr_execute['cate_id'] = $_cate_id;
			}
			if(!empty($_brand_id)){
				$sql_filter .= ' AND `brand_id` = :brand_id';
				$arr_execute['brand_id'] = $_brand_id;
			}

			//取出會員標籤
			$arr_label = $crud->sql_prepare("SELECT `c2`.`cate_title{$_LANG}` AS `par_title`, `c1`.`cate_title{$_LANG}` AS `cate_title`, `brand_title{$_LANG}` AS `brand_title`, `model_number`, `model_version`, `file_price`, `file_id`, `file_file`
											FROM `download_files`
											INNER JOIN `model` ON `model`.`model_id` = `download_files`.`model_id`
											INNER JOIN `model_cate` AS `c1` ON `c1`.`cate_id` = `model_cate`
											INNER JOIN `model_cate` AS `c2` ON `c1`.`cate_par_id` = `c2`.`cate_id`
											INNER JOIN `brand` ON `model_brand` = `brand_id`
											WHERE `file_viewer` LIKE '%\"{$_SESSION[SESSION_NAME.'_user_uniqid']}\"%' AND `brand_online` = 1 AND `c2`.`cate_online` = 1 AND `c1`.`cate_online` = 1 AND `file_online` = 1 {$sql_filter} {$sql_search}
											ORDER BY `file_sort`",
											$arr_execute);
			if($arr_news === false){
				include("./include.page/sys_404.php");
			}
			if(count($arr_label) > 0){
				foreach($arr_label as $key => $value){
					$str_label .= <<<HTML
								<tr data-expanded="true">
									<td>{$value['par_title']}</td>
									<td>{$value['cate_title']}</td>
									<td>{$value['brand_title']}</td>
									<td>{$value['model_number']}-{$value['model_version']}</td>
									<td><a href="./uploadimages/download_files/{$value['file_id']}/{$value['file_file']}" download="{$value['brand_title']}_{$value['par_title']}_{$value['cate_title']}_{$value['model_number']}-{$value['model_version']}">Download</a></td>
									<td>{$value['file_price']}</td>
								</tr>
HTML;
				}
			}
			else{
				$str_label = <<<HTML
							<tr data-expanded="true">
								<td colspan="6">{$_GET_LANG['no_matching_files']}</td>
							</tr>
HTML;
			}

			//品牌列表
			$sql_where = '';
			$arr_execute = array();
			if(!empty($_cate_id)){
				$sql_where = 'WHERE `model_cate` = :cate_id';
				$arr_execute['cate_id'] = $_cate_id;
			}
			$arr_brand = $crud->sql_prepare("SELECT `brand_id`, `brand_title{$_LANG}` AS `brand_title`
											FROM `model_cate`
											INNER JOIN `model` ON `model_cate` = `cate_id`
											INNER JOIN `brand` ON `model_brand` = `brand_id`
											{$sql_where}
											GROUP BY `brand_id`",
											$arr_execute);
			$str_brand = '<option value="">'.$_GET_LANG['all'].$_GET_LANG[' '].$_GET_LANG['brand'].'</option>';
			if(count($arr_brand) > 0){
				foreach($arr_brand as $key => $value){
					$selected = ($value['brand_id'] == $_brand_id)? 'selected':'';
					$str_brand .= <<<HTML
						<option value="{$value['brand_id']}" {$selected}>{$value['brand_title']}</option>
HTML;
				}
			}
			else{
				$str_brand = <<<HTML
						<option value="">{$_GET_LANG['no_matching_data']}</option>
HTML;
			}


			echo preg_replace(
					'/\\\\[rnt]/', 
					'', 
					json_encode(
						array(
							'sts' => true,
							'data' => $str_label,
							'brand' => $str_brand
						)
					)
				);
			break;
	}
?>