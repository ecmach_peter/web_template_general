<?php
/*
			news_html_ary
	_____________________________________
	|________|________|________|________|   <=news_table_header
	|   ___                             |
	|  |   | xxxxxxxx                   |	<=news_content_summary_by_cat
	|  |___| xxxx                       |	<=news_content_summary
	|   ___                             |
	|  |   | xxxxxxxx                   |	
	|  |___| xxxx                       |	<=news_content_summary
	|                                   |
	|                                   |
	|___________________________________|




*/
	function news_table_header(){
		include(PATH_FRONTEND_LANG_SRC."/lang{$_LANG}.php");		//語系檔
		//user
		$str_news_cate = <<<HTML
				<li role="presentation" class="active"><a href="#tab_all" aria-controls="tab_all" role="tab" data-toggle="tab">{$_GET_LANG['news']}</a></li>
HTML;
		//loop
		$arr_news_cate = db_news_category();
		if(count($arr_news_cate) > 0){
			foreach($arr_news_cate as $value){
				$str_news_cate .= <<<HTML
						<li role="presentation"><a href="#tab_{$value['cate_id']}" aria-controls="tab_{$value['cate_id']}" role="tab" data-toggle="tab">{$value['cate_title']}</a></li>
HTML;
			}

		}
		return $str_news_cate;
	}

	function news_content_summary($ary_news, $style){
		$arr_date = date_parse($ary_news['news_date']);
		$arr_date['month'] = str_pad($arr_date['month'], 2, '0', STR_PAD_LEFT);
		$arr_date['day'] = str_pad($arr_date['day'], 2, '0', STR_PAD_LEFT);
		$news_content = mb_strimwidth(strip_tags(htmlspecialchars_decode($ary_news['news_content_display'])), 0, 200, '...', 'UTF-8');
		$news_link = 'news-detail/'.$ary_news['news_id'];

		switch ($style) {
			case 'style1':
				$str_news_detail = <<<HTML
										<div class="col-xs-6 col-md-2 clearfix">
											<img src="frontend/img/usr/news/{$ary_news['news_id']}/{$ary_news['news_photo']}" alt="" class="img-responsive">
										</div>
										<div class="news_content">
											<h3><a href="{$news_link}">{$ary_news['news_title_display']}</a></h3><small> {$arr_date['year']}-{$arr_date['month']}-{$arr_date['day']}</small>
											<div class="textedit">
												<p>{$news_content}</p>
											</div>
										<p class="text-right p_more"><a href="{$news_link}">More...</a></p>
										</div>
HTML;
				break;
			default:
				break;
			}
			return $str_news_detail;
	}

	function news_content_summary_by_cat($id, $news_num, $page_style, $style, $btn_end){
		//usr define
		$news_ary = db_news_detail_usr($id, $news_num);
		if (count($news_ary) == 0) {
			return false;
		}
		$tag_id = ($id == '0')?'all':"{$id}";
		$active = ($id == '0')?' active':'';
		switch ($page_style) {
			case 'page_style':
					$str_news = <<<HTML
							<div role="tabpanel" class="tab-pane {$active}" id="tab_{$tag_id}">
								<ul class="news_list">
									<li class="clearfix">
HTML;
				break;
			default:
				break;
		}

		foreach($news_ary as $key => $news_ary_item){
			$str_news .= news_content_summary($news_ary_item, $style);
		}
			$str_news .= <<<HTML
										</li>
									</ul>
HTML;
		if($btn_end) {
			$str_news .= <<<HTML
									<p class="text-center"><a class="btn btn-green" href="news" role="button">閱讀更多...</a></p>
HTML;
		}
			$str_news .= <<<HTML
								</div>
HTML;
//		info_msg(__FILE__, __LINE__, $str_news);
		return $str_news;
	}

	function news_html_ary($news_num, $page_style, $style, $btn_end){

		$news_content = news_content_summary_by_cat(0, $news_num, $page_style, $style, $btn_end);
		$news_content .= news_content_summary_by_cat(1, $news_num, $page_style, $style, $btn_end);
		$news_content .= news_content_summary_by_cat(2, $news_num, $page_style, $style, $btn_end);
		$news_content .= news_content_summary_by_cat(3, $news_num, $page_style, $style, $btn_end);

		$news_ary['content'] = $news_content;
		$news_header = '';
		if (CONF_NEWS_CATEGORY){
			$news_header = news_table_header();
		}
		$news_ary['header'] = $news_header;

		return $news_ary;
	}


