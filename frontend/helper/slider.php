<?php
	//slider
	/*
		slider_array_entry
		ary_slider_entry[id]
		ary_slider_entry[path]
		ary_slider_entry[photoname] 

		slider_array
		ary_slider[0][id]
		ary_slider[0][path]
		ary_slider[0][filename] 
	*/

	function parse_to_slider($ary_tar, $ary_src, $path) {
		$ary_tmp = []; 
		if(count($ary_src) > 0){
			foreach($ary_src as $key => $value){
				if (!array_key_exists("id",$value)) return 1;
				if (!array_key_exists("photoname",$value)) return 3;
				$ary_tmp['id'] = $value['id'];
				$ary_tmp['path'] = $path;
				$ary_tmp['photoname'] = $value['photoname'];	
				array_push($ary_tar, $ary_tmp);
			}
		}
		return $ary_tar;
	}
	
	/*
		slider element
	*/
	function add_slider($ary_slider, $style) {
		
		if(count($ary_slider) <= 0){return -1;}
		$slider_html_str = [];
		switch ($style){
			case 'general':
				foreach($ary_slider as $key => $value){
					$active = (empty($str_slider))? 'active':'';
					$str_slider .= '<div class="item '.$active.'"><img src="'.$value['path'].'/'.$value['id'].'/'.$value['photoname'].'" alt="slider"></div>';
					$str_slider_li .= '<li data-target="#slider_full" data-slide-to="'.$key.'" class= "'.$active.'"></li>';			
				}
				$slider_html_str['div'] = $str_slider;
				$slider_html_str['li'] = $str_slider_li;
				break;

			default:
				break;
		}
		return $slider_html_str;
	}

	/*
		slider container 
	*/
	function slider_html($ary_slider, $container_style, $style) {
		$slide_code = add_slider($ary_slider, $style);
		switch ($container_style){
			case 'container_index':
				$str_slider .= <<<HTML
						<div class="container">
							<div id="slider_full" class="carousel slide" data-ride="carousel">
								<ol class="carousel-indicators">
HTML;
				$str_slider .= $slide_code['li'];
				$str_slider .= <<<HTML
								</ol>
								<div class="carousel-inner" role="listbox">
HTML;
				$str_slider .= $slide_code['div'];
				$str_slider .= <<<HTML
								</div>
								<a class="left carousel-control" href="#slider_full" role="button" data-slide="prev">
									<span class="fa fa-angle-double-left" aria-hidden="true"></span>
									<span class="sr-only">Prev</span>
								</a>
								<a class="right carousel-control" href="#slider_full" role="button" data-slide="next">
									<span class="fa fa-angle-double-right" aria-hidden="true"></span>
									<span class="sr-only">Next</span>
								</a>
							</div>
						</div>
HTML;
				break;
			default:
				break;
		}	
		echo $str_slider;
	}
