<?php
	function feature_product_slider() {
		$arr_product = db_feature_product();
		$str_product = '';
		if(count($arr_product) <= 0) return 0;
		foreach($arr_product as $key => $value){
			$product_img_base_path = PATH_FRONTEND_IMG_USR_SRC."/product";
			$img_path = $product_img_base_path.'/'.$value['product_create_year'].'/'.$value['product_code'].'/'.$value['product_photo'];
			$product_link = './product/'.$value['product_code'];
			$product_name = $value['product_name'];
//	for front end 
			$str_product .= <<<HTML
				<div class="thumbnail np_box">
					<a href="{$product_link}"><img src="{$img_path}" alt=""></a>
					<div class="caption text-center">
						<a href="{$product_link}"><h3>{$product_name}</h3></a>
					</div>
				</div>
HTML;
		}
		echo $str_product;
	}

	function product_sorting_dropdown_list($current_sort) {
		include(PATH_FRONTEND_LANG_SRC."/lang{$_LANG}.php");		//語系檔
		$arr_sort = array(
						'' => $_GET_LANG['sort_new_to_old'],
						'new-asc' => $_GET_LANG['sort_old_to_new'],
						'price' => $_GET_LANG['sort_price_high_to_low'],
						'price-asc' => $_GET_LANG['sort_price_low_to_high'],
						// 'popular' => '依熱門程度'
					);
		$curr_sort = '';
		foreach ($arr_sort as $key => $value) {
			if(empty($curr_sort) && $key == $current_sort) {
				$curr_sort = $value;
			}
		}
		return $curr_sort;
	}

	function product_sidebar_list() {
		global $crud;
		$str_model_cate = $str_model_cate_select = '';
	// get number of product in different category 
		$ary_model_category = db_model_category();
		if(count($ary_model_category) > 0){
			$sql_query_str = "SELECT ";
			$sql_len_str = strlen($sql_query_str);
			foreach ($ary_model_category as $value) {
				if ($sql_len_str != strlen($sql_query_str)) $sql_query_str .= " ,";
				$sql_query_str .= "SUM(CASE WHEN cate_id={$value['cate_id']} THEN 1 ELSE 0 END) as '{$value['cate_id']}'";
				if($value['count_child'] > 0){
					$ary_model_cate_child = db_model_category_child($value['cate_id']);
					foreach ($ary_model_cate_child as $value) {
						if ($sql_len_str != strlen($sql_query_str)) $sql_query_str .= " ,";
						$sql_query_str .= "SUM(CASE WHEN cate_id={$value['cate_id']} THEN 1 ELSE 0 END) as '{$value['cate_id']}'";
					}
				}
			}
			$sql_query_str .= "	FROM `product`
								INNER JOIN `model` ON `product_model` = `model_id`
								INNER JOIN `model_cate` ON `cate_id` = `model_cate`
								WHERE `product_online` = 1";			
			$ary_category_sum = $crud->sql($sql_query_str)[0];

	// perpare the list on sidebar
			foreach ($ary_model_category as $value) {
				if($value['count_child'] > 0){
					$ary_model_cate_child = db_model_category_child($value['cate_id']);
					$cnt_total = 0;
					if(count($ary_model_cate_child) > 0){
						$cnt_total = 0;
						foreach ($ary_model_cate_child as $_value) {
							if ($ary_category_sum[$_value['cate_id']] > 0) {
								$cnt_total += $ary_category_sum[$_value['cate_id']];
								$str_model_cate_child .= <<<HTML
													<li><a href="./products/category.{$_value['cate_id']}" class="btn-filter" data-id="{$_value['cate_id']}">{$_value['cate_title'.$_LANG]} ({$ary_category_sum[$_value['cate_id']]})</a></li>
HTML;
								$str_model_cate_child_select .= <<<HTML
													<option value="{$_value['cate_id']}"> - {$_value['cate_title'.$_LANG]} ({$ary_category_sum[$_value['cate_id']]})</option>
HTML;
							}
						}
					}
					if ($cnt_total > 0) {
						$str_model_cate .= <<<HTML
											<li class="drup-add">
												<a href="./products/category.{$value['cate_id']}" class="btn-filter" data-id="{$value['cate_id']}">{$value['cate_title'.$_LANG]} ({$cnt_total})</a>
												<a class="plus" href="javascript:;">✖</a>
												<ul class="sidebar_drownav">
													{$str_model_cate_child}
												</ul>
											</li>
HTML;
						$str_model_cate_select .= <<<HTML
											<optgroup label="{$value['cate_title'.$_LANG]}">
												<option value="{$value['cate_id']}">{$value['cate_title'.$_LANG]} ({$cnt_total})</option>
												{$str_model_cate_child_select}
											</optgroup>
HTML;
					}
				}
				else{
					if ($ary_category_sum[$value['cate_id']] > 0) {
						$str_model_cate .= <<<HTML
											<li><a href="./products/category.{$value['cate_id']}" class="btn-filter" data-id="{$value['cate_id']}">{$value['cate_title'.$_LANG]} ({$ary_category_sum[$_value['cate_id']]})</a></li>
HTML;
						$str_model_cate_select .= <<<HTML
											<option value="{$value['cate_id']}">{$value['cate_title'.$_LANG]} ({$ary_category_sum[$_value['cate_id']]})</option>
HTML;
					}
				}
			}

		$model_cate_list = array(
						'str_model_cate' => $str_model_cate,
						'str_model_cate_select' => $str_model_cate_select
					);
		}
		return $model_cate_list;
	}



	function product_detail_price($product_price) {
		if($product_price < 1){
			$str_price = <<<HTML
					<span class="main_price">{$_GET_LANG['store_call_us']}</span>
HTML;
		}
		else{
			$_price = number_format($product_price, 0);
			$str_price = <<<HTML
							<span class="main_price">TWD {$_price}</span>
HTML;
		}
		return $str_price;
	}

	function product_detail_album($product) {
		global $crud;
		$ary_product_album_basic = $crud->select('product_album', array('host_id' => $product['product_id']), array('album_sort' => 'ASC'));
		$str_product_album = $str_cover = '';
		$arr_og_images = array(PROTOCOL."://{$_SERVER['HTTP_HOST']}/".BASE."./frontend/img/usr/product/{$product['product_create_year']}/{$product['product_code']}/{$product['product_photo']}");
		if(count($ary_product_album_basic) > 0){
			foreach ($ary_product_album_basic as $key => $value) {
				if(empty($str_cover)) {
					$str_cover = "./frontend/img/usr/product/{$product['product_create_year']}/{$product['product_code']}/{$value['album_photo']}";
				}
				$str_product_album .= <<<HTML
									<li>
										<a href="./frontend/img/usr/product/{$product['product_create_year']}/{$product['product_code']}/{$value['album_photo']}" data-fancybox="images" title="{$product['model_number']}">
											<p class="imgbox" style="background-image: url(./frontend/img/usr/product/{$product['product_create_year']}/{$product['product_code']}/{$value['album_photo']});  background-size: cover;"></p>
										</a>
									</li>
HTML;
				//og:image
				$arr_og_images[] = PROTOCOL."://{$_SERVER['HTTP_HOST']}/".BASE."frontend/img/usr/product/{$product['product_create_year']}/{$product['product_code']}/{$value['album_photo']}";
			}

			$ary_product_album = array(
							'product_cover' => $str_cover,
							'product_album' => $str_product_album,
							'og_images' => $arr_og_images
						);
		}

		return $ary_product_album;
	}

	function product_similar_list($product_list) {
		$str_product_similar = '';
		foreach ($product_list as $key => $value) {
			$_photo = str_replace('.', '_c.', $value['product_photo']);
			$arr_not_in[] = $value['product_id'];
			$_product_price = ($value['product_price'] < 1)? $_GET_LANG['store_call_us']:"{$_currency} ".number_format($value['product_price'], 0);
			$str_product_similar .= <<<HTML
									<li class="clearfix">
										<a href="./product/{$value['product_code']}" style="background-image: url(./frontend/img/usr/product/{$value['product_create_year']}/{$value['product_code']}/{$_photo});  background-size: cover;"></a>
										<ul class="same_list_info">
											<li><a href="./product/{$value['product_code']}">{$value['product_name']}</a></li>
											<li>{$_product_price}</li>
										</ul>
									</li>
HTML;
		}

		return $str_product_similar;
	}
	