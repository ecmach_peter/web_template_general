<?php
	//general
	function debug_message($file, $line, $level, $debug_str) {
		if (CONF_DEBUG_EN != 'Y'){
			return true;
		}
		$debug_lv = 6;
		switch ($level){
			case 'error': $debug_lv = 0; break;
			case 'info': $debug_lv = 1; break;
			case 'test': $debug_lv = 2; break;
		}
		if (CONF_DEBUG_LEVEL > $debug_lv){
			$currentDate = date('Ymd');
		    $debugFile = PATH_LOG_BASE.'/'.$currentDate.'.log';
		    $fp = fopen($debugFile, 'a');
		    $currentDateTime = date('Y-m-d H:i:s');
		    fwrite($fp, $level.' - '.$currentDateTime. ' -> '.$file.'-'.$line.': '.$debug_str."\n");
		    fclose($fp);
		}
	}

	//error - 0
	function error_msg($file, $line, $debug_str) {
		debug_message($file, $line, 'error', $debug_str);
	}

	//info - 1
	function info_msg($file, $line, $debug_str) {
		debug_message($file, $line, 'info', $debug_str);
	}

	//test - 2
	function test_msg($file, $line, $debug_str) {
		debug_message($file, $line, 'test', $debug_str);
	}
