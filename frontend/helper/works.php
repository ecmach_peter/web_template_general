<?php
	//slider
	function get_work_category_ary() {	//過往工作類型
		global $crud;				
		$arr_work = $crud->select(
									'works_type', 
									array('works_type_online' => 1), 
									array('works_type_sort' => 'ASC'), 
									array(
										'works_type_id',
										'works_type_url',
										'works_type_photo',
										'works_type_title' => "works_type_title{$_LANG}",
										'works_type_content' => "works_type_content{$_LANG}",
									)
								);
		return $arr_work;
	}

	function get_work_category_ary_id($id) {	//過往工作類型
		global $crud;				
		$arr_work = $crud->select(
									'works_type', 
									array(
										'works_type_id' => "{$id}",
										'works_type_online' => 1
									), 
									array('works_type_sort' => 'ASC'), 
									array(
										'works_type_id',
										'works_type_url',
										'works_type_photo',
										'works_type_title' => "works_type_title{$_LANG}",
										'works_type_content' => "works_type_content{$_LANG}",
									)
								);
		return $arr_work;
	}

	function validate_url($url){
		$work_category_ary = get_work_category_ary();
		$is_valid = fasle;
		foreach($work_category_ary as $key => $work_category_item){
			if($url == $work_category_item['works_type_url']){
				$is_valid = true;
				break;
			}
		}
		return $is_valid;
	}

	function get_category_id($url) {	//過往工作類型
		$work_category_ary = get_work_category_ary();
		foreach($work_category_ary as $key => $work_category_item){
			if($url == $work_category_item['works_type_url']){
				$id = $work_category_item['works_type_id'];
				break;
			}
		}
		return $id;
	}

	function get_album_ary_by_id($id) {
		global $crud;				
		$ary_album = $crud->select(
									'works_type_album', 
									array('host_id' => "{$id}"), 
									array('album_sort' => 'ASC'), 
									array('album_photo')
								);
		return $ary_album;
	}

	function get_case_slider_html($id, $display) {
		$ary_album = get_album_ary_by_id($id);
		if(count($ary_album) == 0){
			return false;
		}

		foreach($ary_album as $key => $value){
			$active = ($key == '0')? 'active':'';
			$path = PATH_FRONTEND_IMG_USR_SRC;
			$str_label_album .= <<<HTML
					<div class="item {$active}" style="background-image: url('{$path}/works_type/{$id}/{$value['album_photo']}');"></div>
HTML;
			$tmp_label_item .= <<<HTML
					<div data-target="#alexslider" data-slide-to="{$key}" class="thumb" style="background-image: url('{$path}/works_type/{$id}/{$value['album_photo']}');"></div>
HTML;
			//一排4個
			if(($key + 1) % 4 == 0){
				$active_item = (empty($str_label_item))? 'active':'';
				$str_label_item .= <<<HTML
					<div class="item {$active_item} {$key}">
						{$tmp_label_item}
					</div>
HTML;
				$tmp_label_item = '';
			}
		}

		//未滿4個的一組
		if($key % 4 != 0){
			$active_item = ($key < 4)? 'active':'';
			$str_label_item .= <<<HTML
				<div class="item {$active_item}">
					{$tmp_label_item}
				</div>
HTML;
		}

		if($display == 'item'){
			echo $str_label_item;
		} else if($display == 'album'){
			echo $str_label_album;
		}
		
	}


	function get_work_detail_ary_by_id($id) {	//過往工作類型
		global $crud;	
		$arr_case = $crud->select(
								'case', 
								array(
									'works_type_id' => "{$id}",
									'case_online' => 1
								),
								array('case_sort' => 'ASC'),
								array(
									'case_id',
									'case_left_menu',
									'case_photo',
									'case_title' => "case_title{$_LANG}",
									'case_content' => "case_content{$_LANG}"
								)
							);
		return $arr_case;		
	}
	
	function get_work_detail_album_ary_by_id($host_id) {	//過往工作類型
		global $crud;
		$arr_case_album = $crud->select(
								'case_album', 
								array(
									'host_id' => "{$host_id}"
								),
								array(
									'host_id' => 'ASC',
									'album_sort' => 'ASC',
								),
								array(
									'host_id',
									'album_photo'
								)
							);
		return $arr_case_album;
	}

	function get_work_detail_album_html($host_id) {
		$arr_case_album = get_work_detail_album_ary_by_id($host_id);
		if(count($arr_case_album) == 0){
			return false;
		}

		$path = PATH_FRONTEND_IMG_USR_SRC;
		foreach($arr_case_album as $key => $value){
			$thumb = str_replace('.', '_a.', $value['album_photo']);
			$arr_album_data .= <<<HTML
				<a href="{$path}/case/{$host_id}/{$value['album_photo']}" data-type="image" data-fancybox="group0{$host_id}" data-thumb="{$path}/case/{$host_id}/{$thumb}"></a>
HTML;
		}
		echo $arr_album_data;
	}

	function get_work_detail_html($worktype_id) {	//過往工作類型
		include(PATH_FRONTEND_LANG_SRC."/lang{$_LANG}.php");		//語系檔
	//取得此標籤的成功案例
		$arr_case = get_work_detail_ary_by_id($worktype_id);
		if(count($arr_case) == 0){
			return false;
		}
		$str_case = $str_left_menu = '';
		foreach($arr_case as $key => $value){
			$case_content = htmlspecialchars_decode($value['case_content']);
			$str_album = get_work_detail_album_html($value['case_id']);
			$img_path = PATH_FRONTEND_IMG_USR_SRC;

			$str_case .= <<<HTML
						<li id="case0{$key}">
							<div class="thumbnail np_box clearfix">
								<div class="album">
									<a href="{$img_path}/case/{$value['case_id']}/{$value['case_photo']}" data-type="image" data-fancybox="group0{$value['case_id']}">
										<div class="np_box_img" style="background-image: url('{$img_path}/case/{$value['case_id']}/{$value['case_photo']}');"></div>
									</a>
									<div style="display: none;">
										{$str_album}
									</div>
								</div>
								<div class="album_infobox box-close">
									<ul class="album_info">
										<li><span>{$_GET_LANG['case_name']}：</span>{$value['case_title']}</li>
									</ul>
									<div class="textedit case-box box-close">
										{$case_content}
									</div>
									<p class="text-center visible-lg">
										<button class="btn btn-green btn-xs btn_op"></button>
									</p>
								</div>
							</div>
						</li>
HTML;
		}
		echo $str_case;
	}

	function get_work_detail_menu_html($worktype_id) {	//過往工作類型
	//取得此標籤的成功案例
		$arr_case = get_work_detail_ary_by_id($worktype_id);
		if(count($arr_case) == 0){
			return false;
		}
		$str_left_menu = '';
		foreach($arr_case as $key => $value){
			if($value['case_left_menu'] == 1){
				$str_left_menu .= <<<HTML
						<li data-key="{$key}"><a href="#case0{$key}">- {$value['case_title']}</a></li>
HTML;
			}
		}
		echo $str_left_menu;
	}

	function applslider($tag) {	//過往工作類型
	}

	function applslide8r($tag) {	//過往工作類型

		$str_works_type_left = '';
		if(count($arr_label_work) > 0){
			//取出相簿資料
			$sql_all_id = join(',', array_column($arr_label_work, 'works_type_id'));
			$arr_album = $crud->sql("SELECT `host_id`, `album_photo`
									FROM `works_type_album`
									WHERE `host_id` IN ({$sql_all_id})
									ORDER BY `host_id`, `album_sort`");
			$arr_album_data = $arr_album_cover = array();
			if(count($arr_album) > 0){
				foreach($arr_album as $key => $value){
					$thumb = str_replace('.', '_a.', $value['album_photo']);
					if(empty($arr_album_data[$value['host_id']])) 
					$arr_album_data[$value['host_id']] .= <<<HTML
						<a href="./uploadimages/works_type/{$value['host_id']}/{$value['album_photo']}" data-type="image" data-fancybox="group0{$value['host_id']}" data-thumb="./uploadimages/works_type/{$value['host_id']}/{$thumb}"></a>
HTML;
				}
			}
			/*
			20171026 peter remove
			<a href="./uploadimages/works_type/{$value['works_type_id']}/{$value['works_type_photo']}" data-type="image" data-fancybox="group0{$value['works_type_id']}">
			*/
			foreach($arr_label_work as $key => $value){
				$str_works_type_left .= <<<HTML
							<li><a href="./works/{$value['works_type_url']}">{$value['works_type_title']}</a></li>
HTML;
				$works_type_content = nl2br($value['works_type_content']);

				$str_case .= <<<HTML
							<li id="case0{$key}">
								<div class="thumbnail np_box clearfix">
									<div class="album">
										<a href="./works/{$value['works_type_url']}">
											<div class="np_box_img" style="background-image: url('./uploadimages/works_type/{$value['works_type_id']}/{$value['works_type_photo']}');"></div>
										</a>
										<div style="display: none;">
											{$arr_album_data[$value['works_type_id']]}
										</div>
									</div>
									<div class="album_infobox box-close">
										<ul class="album_info">
											<li><span>{$_GET_LANG['category']}：</span>{$value['works_type_title']}</li>
										</ul>
										<div class="textedit case-box box-close">
											{$works_type_content}
										</div>
										<p class="text-center">
											<button class="btn btn-green btn-xs btn_op"></button>
										</p>
									</div>
								</div>
							</li>
HTML;
			}
		}
	}

