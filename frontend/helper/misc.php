<?php
	//slider
	function bilingual_title($style, $lang1, $lang2) {
		switch ($style){
			case '0':
				/* <h2 class="line_title"><?=$lang1;?> &#8260; <large><?=$lang2;?></large></h2> */
				$str_title = '<h2 class="line_title">';
				$str_title .= ($_LANG == '')?$lang1:$lang2;
				$str_title .= '/';
				$str_title .= '<large>';
				$str_title .= ($_LANG == '')?$lang2:$lang1;
				$str_title .= '</large></h2>';
				break;

			case '1':
				/* <h2 class="line_title"><?=$lang1;?> &#8260; <small><?=$lang2;?></small></h2> */
				$str_title = '<h2 class="line_title">';
				$str_title .= ($_LANG == '')?$lang1:$lang2;
				$str_title .= '/';
				$str_title .= '<small>';
				$str_title .= ($_LANG == '')?$lang2:$lang1;
				$str_title .= '</small></h2>';
				break;
		}
		echo $str_title;

	}
