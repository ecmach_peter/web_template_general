(function($) {
    $(document).ready(function() {
        $('ul.dropdown-menu [data-toggle=dropdown]').on('click', function(event) {
            event.preventDefault();
            event.stopPropagation();
            $(this).parent().siblings().removeClass('open');
            $(this).parent().toggleClass('open');
        });
    });
})(jQuery);

$(function() {
    // go top
    $("#gotop").click(function() {
        jQuery("html,body").animate({
            scrollTop: 0
        }, 1000);
    });
    $(window).scroll(function() {
        if ($(this).scrollTop() > 300) {
            $('#gotop').fadeIn("fast");
        } else {
            $('#gotop').stop().fadeOut("fast");
        }
    });

    //產品側欄Slider
    $('#mainNav li').children('a[href*=\\#]:not([href=\\#])').click(function() {
        var target = $(this.hash);
        $('html, body').animate({
            scrollTop: target.offset().top
        }, 1500);
        return false;
    });

    $("li.drup-add .plus").click(function(e) {
        $(this).parent().children('ul').slideToggle('500');
        $(this).toggleClass('clicked');
    });

    $('.btn_logout').on('click', function() {
        swal({
                title: "Are you sure?",
                text: "確定要登出嗎?",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes",
                cancelButtonText: "No",
            },
            function(isConfirm) {
                if (isConfirm) {
                    location.href = './logout';
                }
            });

        return false;
    });

    $('#form_search').on('submit', function() {
        var _keyword = $('#keyword').val();
        if (_keyword != '') location.href = './search/' + _keyword;
        return false;
    });

    //fancybox
    $('[data-type="image"]').fancybox({
        baseTpl: '<div class="fancybox-container" role="dialog" tabindex="-1">' +

            '<div class="fancybox-bg"></div>' +

            '<div class="fancybox-controls">' +
            '<div class="fancybox-infobar">' +
            '<div class="fancybox-infobar__body">' +
            '</div>' +
            '</div>' +

            '<div class="fancybox-buttons">' +
            '<button data-fancybox-close class="fancybox-button fancybox-button--close" title="Close (Esc)"></button>' +
            '</div>' +
            '</div>' +

            '<div class="btn_l">' +
            '<button data-fancybox-previous class="fancybox-button fancybox-button--left" title="Previous"></button>' +
            '</div>' +
            '<div class="fancybox-slider-wrap">' +

            '<div class="fancybox-slider"></div>' +

            '</div>' +
            '<div class="btn_r">' +
            '<button data-fancybox-next class="fancybox-button fancybox-button--right" title="Next"></button>' +
            '</div>' +

            '<div class="fancybox-caption-wrap"><div class="fancybox-caption"></div></div>' +
            '</div>',
        loop: false,
        slideShow: false,
        fullScreen: false,
        thumbs: {
            showOnStart: true
        },
        margin: [44, 0, 90, 0]
    })
});

$(function() {
    $('#filter_btn').on('click', function() {
        $('.filterbox').css('display', 'block');
    })
    $('.filter_close').click(function() {
        $('.filterbox').css('display', 'none');
    })
});

function doajax(ajax) {
    this.async = true;
    this.type = 'text';
    this.debug = false;
    this.reurl = false;

    // var url = (ajax.reurl)? ajax.url:'./'+settings['a']+'-'+ajax.url;

    $.ajax({
        url: ajax.url,
        type: 'post',
        data: ajax.data,
        dataType: ajax.type,
        async: ajax.async,
        beforeSend: function(xhr, opts) {
            if (ajax.beforeSend) ajax.beforeSend(xhr, opts);
        },
        success: function(msg) {
            if (ajax.callback) ajax.callback(msg);
            if (ajax.debug) console.log(msg)
        },
        error: function(xhr, ajaxOptions, thrownError) {
            console.error('err');
            console.error('sts:' + xhr.status);
            console.error('error:' + thrownError);
        }
    });
}