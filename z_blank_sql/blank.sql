-- phpMyAdmin SQL Dump
-- version 4.7.9
-- https://www.phpmyadmin.net/
--
-- 主機: 127.0.0.1
-- 產生時間： 2018 年 11 月 05 日 08:06
-- 伺服器版本: 10.1.31-MariaDB
-- PHP 版本： 7.1.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- 資料庫： `ecmachco_alex`
--

-- --------------------------------------------------------

--
-- 資料表結構 `about`
--

CREATE TABLE `about` (
  `about_id` tinyint(1) NOT NULL,
  `about_index` text NOT NULL,
  `about_index_en` text NOT NULL,
  `about_contact` text NOT NULL,
  `about_contact_en` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 資料表結構 `activity_log`
--

CREATE TABLE `activity_log` (
  `activity_id` int(11) NOT NULL,
  `activity_user` varchar(20) NOT NULL COMMENT '使用者帳號',
  `activity_unit` varchar(50) NOT NULL COMMENT '修改單元',
  `activity_detail` text NOT NULL COMMENT '修改內容',
  `activity_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改時間'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- 資料表的匯出資料 `activity_log`
--

INSERT INTO `activity_log` (`activity_id`, `activity_user`, `activity_unit`, `activity_detail`, `activity_time`) VALUES
(1, 'yaii', '使用者管理', '新增 alex', '2017-10-24 11:45:34'),
(2, 'alex', '最新消息', '新增 asd', '2017-10-24 11:54:19'),
(3, 'alex', '最新消息', '新增 dfgsdf', '2017-10-24 11:58:11'),
(4, 'yaii', '最新消息', '新增 duncan test', '2017-10-24 12:02:33'),
(5, 'alex', '工作類型', '新增 DUNCAN 愛的小手', '2017-10-24 12:14:10'),
(6, 'alex', '工作類型', '修改 NRMM', '2017-10-24 12:15:06'),
(7, 'alex', '工作類型', '修改 QPME', '2017-10-24 12:16:42'),
(8, 'alex', '成功案例', '新增 小手1', '2017-10-24 12:21:26'),
(9, 'yaii', '使用者管理', '修改 yaii', '2017-10-24 12:21:55'),
(10, 'yaii', '關於我們', '修改 &lt;div class=&quot;textedit text-justify&quot;&gt;\r\n&lt;h3 class=&quot;text_title&quot;&gt;.亞力士工程有限公司&lt;/h3&gt;\r\n\r\n&lt;p&gt;本公司專業團隊能為您提供專業解決方案，使您的更加快速、簡單和有效最得環保署的NRMM 核准標籤。&lt;/p&gt;\r\n\r\n&lt;p&gt;我們始終已超越客戶期望，及提供卓越的市場服務為己任。我們提供專業的商務解決方案，在法規日益增多的市場上為客戶領航。&lt;/p&gt;\r\n&lt;/div&gt;\r\n\r\n&lt;div class=&quot;textedit mt30 text-justify&quot;&gt;\r\n&lt;h3 class=&quot;text_title&quot;&gt;我們的願景&lt;/h3&gt;\r\n\r\n&lt;p&gt;成為一個最具競爭力與生產力的服務組織是我們的目標。 持續改善我們在檢驗、查證和驗證的核心能力，以提供最佳的服務， 這是我們之精隨所在。 我們的競爭力與對客戶不斷提供無微不至的服務，將是決定我們客戶心目中首要地位的不二法門。&lt;/p&gt;\r\n&lt;/div&gt;\r\n\r\n&lt;div class=&quot;textedit mt30 text-justify&quot;&gt;\r\n&lt;h3 class=&quot;text_title&quot;&gt;我們的價值&lt;/h3&gt;\r\n\r\n&lt;p&gt;我們追求以熱情、誠信和開創性來表現我們的特質，並持續努力達成我們的願景。 這些價值引導著我們在各個層面的所作所為，是建構我們組織的根本基石。&lt;/p&gt;\r\n&lt;/div&gt;\r\n', '2017-10-24 12:22:07'),
(11, 'yaii', '關於我們', '修改 &lt;div class=&quot;textedit text-justify&quot;&gt;\r\n&lt;h3 class=&quot;text_title&quot;&gt;亞力士工程有限公司&lt;/h3&gt;\r\n\r\n&lt;p&gt;本公司專業團隊能為您提供專業解決方案，使您的更加快速、簡單和有效最得環保署的NRMM 核准標籤。&lt;/p&gt;\r\n\r\n&lt;p&gt;我們始終已超越客戶期望，及提供卓越的市場服務為己任。我們提供專業的商務解決方案，在法規日益增多的市場上為客戶領航。&lt;/p&gt;\r\n&lt;/div&gt;\r\n\r\n&lt;div class=&quot;textedit mt30 text-justify&quot;&gt;\r\n&lt;h3 class=&quot;text_title&quot;&gt;我們的願景&lt;/h3&gt;\r\n\r\n&lt;p&gt;成為一個最具競爭力與生產力的服務組織是我們的目標。 持續改善我們在檢驗、查證和驗證的核心能力，以提供最佳的服務， 這是我們之精隨所在。 我們的競爭力與對客戶不斷提供無微不至的服務，將是決定我們客戶心目中首要地位的不二法門。&lt;/p&gt;\r\n&lt;/div&gt;\r\n\r\n&lt;div class=&quot;textedit mt30 text-justify&quot;&gt;\r\n&lt;h3 class=&quot;text_title&quot;&gt;我們的價值&lt;/h3&gt;\r\n\r\n&lt;p&gt;我們追求以熱情、誠信和開創性來表現我們的特質，並持續努力達成我們的願景。 這些價值引導著我們在各個層面的所作所為，是建構我們組織的根本基石。&lt;/p&gt;\r\n&lt;/div&gt;\r\n', '2017-10-24 12:22:45'),
(12, 'yaii', '使用者管理', '修改 alex', '2017-10-24 12:22:58'),
(13, 'yaii', '使用者管理', '修改 yaii', '2017-10-24 13:07:14'),
(14, 'alex', '工作類型', '新增 機械買賣', '2017-10-24 16:43:19'),
(15, 'alex', '工作類型', '新增 新產品測試', '2017-10-24 16:44:41'),
(16, 'alex', '工作類型', '新增 引入國外產品', '2017-10-24 16:46:15'),
(17, 'alex', '成功案例', '新增 測試乾冰清潔機', '2017-10-24 16:47:45'),
(18, 'alex', '機械資料', '新增 B20', '2017-10-24 16:48:55'),
(19, 'alex', '機械資料', '修改 破碎錘', '2017-10-24 16:50:00'),
(20, 'alex', '機械資料', '修改 拆卸工具', '2017-10-24 16:51:20'),
(21, 'alex', '機械資料', '新增 B30', '2017-10-24 16:54:09'),
(22, 'alex', '機械資料', '修改 B20', '2017-10-24 16:54:55'),
(23, 'alex', '最新消息', '新增 第十四屆中國(北京)國際工程機械、建材機械及礦山機械展覽與技術交流會', '2017-10-24 17:26:49'),
(24, 'alex', '最新消息', '新增 2017年9月14 日 香港重型機械拍賣會', '2017-10-24 17:29:22'),
(25, 'alex', '最新消息', '新增 2017拆除業全球高峰論壇將於倫敦舉行', '2017-10-24 17:30:46'),
(26, 'alex', '最新消息', '新增 2017亞洲國際高空作業機械展覽會', '2017-10-24 17:32:45'),
(27, 'alex', '最新消息', '新增 2017 國際農業機械暨資材展', '2017-10-24 17:34:22'),
(28, 'alex', '工作類型', '新增 顧問工作', '2017-10-24 17:40:13'),
(29, 'alex', '工作類型', '修改 顧問工作', '2017-10-24 17:42:19'),
(30, 'alex', '工作類型', '修改 顧問工作', '2017-10-24 17:43:16'),
(31, 'alex', '參數表及其他文件', '新增 35', '2017-10-24 17:46:41'),
(32, 'alex', '產品目錄', '修改 1', '2017-10-25 03:40:18'),
(33, 'alex', '產品目錄', '新增 1', '2017-10-25 03:42:14'),
(34, 'alex', '產品目錄', '修改 1', '2017-10-25 03:42:40'),
(35, 'alex', '品牌管理', '修改 韓國大模破碎錘', '2017-10-25 03:47:29'),
(36, 'alex', '產品目錄', '新增 1', '2017-10-25 03:54:06'),
(37, 'alex', '品牌管理', '修改 韓國大模工程有有限公司', '2017-10-25 03:55:15'),
(38, 'alex', '品牌管理', '修改 韓國大模工程有有限公司', '2017-10-25 04:07:43'),
(39, 'alex', '最新消息', '新增 Matexpo 2017 in Belgium', '2017-10-25 04:10:35'),
(40, 'alex', '工作類型', '新增 合作伙伴', '2017-10-25 04:15:33'),
(41, 'alex', '品牌管理', '修改 韓國大模工程有有限公司', '2017-10-25 04:21:49'),
(42, 'alex', '品牌管理', '修改 韓國大模工程有有限公司', '2017-10-25 04:23:07'),
(43, 'alex', 'Slider管理', '新增 sliyxaF59f0122feda22.jpg', '2017-10-25 04:25:20'),
(44, 'alex', 'Slider管理', '新增 sliEoFR59f0124f6c5db.jpg', '2017-10-25 04:25:52'),
(45, 'alex', '最新消息', '修改 大模 比利時 Matexpo 2017', '2017-10-25 05:55:28'),
(46, 'alex', '最新消息', '修改 大模 比利時 Matexpo 2017', '2017-10-25 05:58:35'),
(47, 'alex', '工作類型', '修改 引入國外產品', '2017-10-25 06:01:52'),
(48, 'alex', '工作類型', '修改 引入國外產品', '2017-10-25 06:02:08'),
(49, 'alex', '產品資料', '新增 Alicon Breaker B30', '2017-10-25 06:04:05'),
(50, 'alex', '產品目錄', '新增 1', '2017-10-25 08:09:15'),
(51, 'alex', '產品目錄', '修改 1', '2017-10-25 08:40:24'),
(52, 'alex', '產品目錄', '修改 1', '2017-10-25 08:51:18'),
(53, 'alex', '品牌管理', '修改 韓國大模工程有有限公司', '2017-10-25 08:59:17'),
(54, 'alex', '帳單資料', '修改 NmMzNGY359f05139ebfad', '2017-10-25 09:27:21'),
(55, 'alex', '產品目錄', '修改 1', '2017-10-25 10:00:31'),
(56, 'alex', '產品目錄', '修改 1', '2017-10-25 10:00:36'),
(57, 'alex', '產品目錄', '修改 1', '2017-10-25 10:20:58'),
(58, 'alex', '產品目錄', '修改 1', '2017-10-25 10:21:40'),
(59, 'alex', '產品目錄', '新增 1', '2017-10-25 10:22:32'),
(60, 'alex', '產品目錄', '修改 1', '2017-10-25 10:23:46'),
(61, 'alex', '產品目錄', '新增 1', '2017-10-25 10:26:06'),
(62, 'alex', '標籤管理', '新增 NmMzNGY359f05139ebfad', '2017-10-25 10:30:17'),
(63, 'alex', '產品目錄', '新增 1', '2017-10-25 10:36:56'),
(64, 'alex', '產品目錄', '修改 1', '2017-10-25 10:38:00'),
(65, 'alex', '產品目錄', '新增 1', '2017-10-25 10:38:30'),
(66, 'alex', '產品目錄', '新增 1', '2017-10-25 10:38:54'),
(67, 'alex', '產品目錄', '新增 1', '2017-10-25 10:40:12'),
(68, 'alex', '產品目錄', '新增 1', '2017-10-25 10:42:51'),
(69, 'alex', '產品目錄', '修改 1', '2017-10-25 10:44:26'),
(70, 'alex', '產品目錄', '修改 1', '2017-10-25 10:44:49'),
(71, 'alex', '產品目錄', '修改 1', '2017-10-25 10:45:13'),
(72, 'alex', '產品目錄', '修改 1', '2017-10-25 10:45:13'),
(73, 'alex', '產品目錄', '修改 1', '2017-10-25 10:45:25'),
(74, 'alex', '產品目錄', '修改 1', '2017-10-25 10:45:27'),
(75, 'alex', '產品目錄', '修改 1', '2017-10-25 10:45:50'),
(76, 'alex', '產品目錄', '修改 1', '2017-10-25 10:46:48'),
(77, 'alex', '產品目錄', '修改 1', '2017-10-25 10:47:18'),
(78, 'alex', '標籤管理', '新增 NmMzNGY359f05139ebfad', '2017-10-25 10:48:43'),
(79, 'alex', '產品目錄', '修改 1', '2017-10-25 10:49:42'),
(80, 'alex', '產品目錄', '修改 1', '2017-10-25 10:51:06'),
(81, 'alex', '產品目錄', '修改 1', '2017-10-25 10:53:56'),
(82, 'alex', '產品目錄', '修改 1', '2017-10-25 10:54:58'),
(83, 'alex', '產品目錄', '修改 1', '2017-10-25 10:55:31'),
(84, 'alex', '產品目錄', '修改 1', '2017-10-25 10:59:26'),
(85, 'alex', '產品目錄', '修改 1', '2017-10-25 11:01:04'),
(86, 'alex', '產品目錄', '修改 1', '2017-10-25 11:37:05'),
(87, 'alex', '產品目錄', '修改 1', '2017-10-25 11:37:18'),
(88, 'alex', '產品目錄', '修改 1', '2017-10-25 11:38:56'),
(89, 'alex', '產品目錄', '修改 1', '2017-10-25 12:38:10'),
(90, 'alex', '產品目錄', '修改 1', '2017-10-25 12:38:37'),
(91, 'alex', '產品資料', '修改 Alicon Breaker B30', '2017-10-25 12:40:10'),
(92, 'alex', '產品資料', '修改 Alicon Breaker B30', '2017-10-25 12:40:37'),
(93, 'alex', '產品資料', '修改 Alicon Breaker B30', '2017-10-25 12:42:56'),
(94, 'alex', '產品資料', '修改 Alicon Breaker B30', '2017-10-25 12:43:06'),
(95, 'alex', '產品資料', '修改 Alicon Breaker B30', '2017-10-25 12:44:08'),
(96, 'alex', '品牌管理', '修改 韓國大模工程有限公司', '2017-10-25 12:47:58'),
(97, 'alex', '產品資料', '修改 Alicon Breaker B30', '2017-10-25 13:15:33'),
(98, 'alex', '產品資料', '修改 Alicon Breaker B70', '2017-10-25 13:26:52'),
(99, 'alex', '產品資料', '修改 Alicon Breaker B70', '2017-10-25 13:28:18'),
(100, 'alex', '產品資料', '修改 Alicon Breaker B140', '2017-10-25 13:28:31'),
(101, 'alex', '產品資料', '修改 Alicon Breaker B210', '2017-10-25 13:28:48'),
(102, 'alex', '產品資料', '修改 Alicon Breaker B70', '2017-10-25 13:29:54'),
(103, 'alex', '產品資料', '修改 Alicon Breaker B140', '2017-10-25 13:30:07'),
(104, 'alex', '產品資料', '修改 Alicon Breaker B210', '2017-10-25 13:30:21'),
(105, 'alex', '標籤類型', '修改 NRMM', '2017-10-25 13:38:49'),
(106, 'alex', '工作類型', '修改 NRMM', '2017-10-25 13:48:38'),
(107, 'alex', '工作類型', '修改 NRMM', '2017-10-25 13:49:10'),
(108, 'alex', '工作類型', '修改 NRMM', '2017-10-25 13:50:18'),
(109, 'alex', '工作類型', '修改 NRMM', '2017-10-25 13:51:18'),
(110, 'alex', '工作類型', '修改 NRMM', '2017-10-25 13:57:24'),
(111, 'alex', '成功案例', '新增 推土機 申領 NRMM', '2017-10-25 13:59:32'),
(112, 'alex', '成功案例', '修改 推土機 申領 NRMM', '2017-10-25 13:59:48'),
(113, 'alex', '成功案例', '修改 推土機 申領 NRMM', '2017-10-25 14:01:32'),
(114, 'alex', '成功案例', '修改 推土機 申領 NRMM', '2017-10-25 14:02:01'),
(115, 'alex', '成功案例', '修改 推土機 申領 NRMM', '2017-10-25 14:02:20'),
(116, 'alex', '成功案例', '修改 推土機 申領 NRMM', '2017-10-25 14:05:43'),
(117, 'alex', '成功案例', '新增 大型運泥車 申領 NRMM', '2017-10-25 14:08:55'),
(118, 'alex', '成功案例', '修改 大型運泥車 申領 NRMM', '2017-10-25 14:09:37'),
(119, 'alex', '工作類型', '修改 NRMM', '2017-10-25 14:17:14'),
(120, 'alex', '工作類型', '修改 QPME', '2017-10-25 14:18:02'),
(121, 'alex', '工作類型', '修改 QPME', '2017-10-25 14:22:13'),
(122, 'alex', '工作類型', '修改 QPME', '2017-10-25 14:23:26'),
(123, 'alex', '工作類型', '修改 QPME', '2017-10-25 14:23:35'),
(124, 'alex', '工作類型', '修改 合作伙伴', '2017-10-25 14:29:52'),
(125, 'alex', '工作類型', '修改 機械買賣', '2017-10-25 14:31:56'),
(126, 'alex', '工作類型', '修改 新產品測試', '2017-10-25 14:36:31'),
(127, 'alex', '工作類型', '修改 新產品測試', '2017-10-25 14:41:03'),
(128, 'alex', '工作類型', '修改 顧問工作', '2017-10-25 14:43:03'),
(129, 'alex', '工作類型', '修改 顧問工作', '2017-10-25 14:43:57'),
(130, 'alex', '品牌管理', '修改 西班牙Xcentric Ripper', '2017-10-26 03:11:39'),
(131, 'alex', '工作類型', '新增 demo', '2017-10-26 03:26:16'),
(132, 'alex', '品牌管理', '修改 韓國大模工程有限公司', '2017-10-26 06:18:16'),
(133, 'alex', '工作類型', '修改 顧問工作', '2017-10-26 07:49:01'),
(134, 'alex', '工作類型', '修改 顧問工作', '2017-10-26 08:36:23'),
(135, 'alex', '工作類型', '修改 顧問工作', '2017-10-26 08:45:19'),
(136, 'alex', '成功案例', '新增 機械交易平台設計', '2017-10-26 09:01:52'),
(137, 'alex', '成功案例', '修改 機械交易平台設計', '2017-10-26 09:03:20'),
(138, 'alex', '成功案例', '新增 數據分析', '2017-10-26 09:13:37'),
(139, 'alex', '工作類型', '修改 顧問工作', '2017-10-26 09:46:23'),
(140, 'alex', '品牌管理', '修改 Xcentric Ripper', '2017-10-26 10:00:08'),
(141, 'alex', 'Slider管理', '新增 slianAw59f1b28dae1ed.png', '2017-10-26 10:01:51'),
(142, 'alex', 'Slider管理', '新增 sli3zXS59f1b5144661a.jpg', '2017-10-26 10:12:37'),
(143, 'alex', 'Slider管理', '新增 sliDW7m59f1b89a2ecd2.png', '2017-10-26 10:27:39'),
(144, 'alex', 'Slider管理', '新增 slipPX959f1bbedca983.jpg', '2017-10-26 10:41:50'),
(145, 'alex', '工作類型', '修改 新產品測試', '2017-10-26 12:17:04'),
(146, 'alex', '工作類型', '修改 顧問工作', '2017-10-26 12:17:23'),
(147, 'alex', '工作類型', '修改 新產品測試', '2017-10-26 12:17:32'),
(148, 'alex', '工作類型', '修改 引入國外產品', '2017-10-26 12:23:44'),
(149, 'alex', '工作類型', '修改 機械買賣', '2017-10-26 12:47:45'),
(150, 'alex', '工作類型', '修改 機械買賣', '2017-10-26 12:49:17'),
(151, 'alex', '工作類型', '修改 引入國外產品', '2017-10-26 12:50:19'),
(152, 'alex', '工作類型', '修改 機械買賣', '2017-10-26 12:50:56'),
(153, 'alex', '工作類型', '修改 合作伙伴', '2017-10-26 12:55:29'),
(154, 'alex', '成功案例', '新增 韓國貿易會', '2017-10-26 13:16:50'),
(155, 'alex', '成功案例', '修改 韓國貿易會', '2017-10-26 13:40:39'),
(156, 'alex', '成功案例', '新增 2016 上海寶馬展', '2017-10-26 13:44:12'),
(157, 'alex', '產品資料', '修改 Alicon Breaker B30', '2017-10-26 14:10:57'),
(158, 'alex', '產品資料', '修改 Alicon Breaker B70', '2017-10-26 14:15:08'),
(159, 'alex', '產品資料', '修改 Alicon Breaker B140', '2017-10-26 14:15:26'),
(160, 'alex', '產品資料', '修改 Alicon Breaker B210', '2017-10-26 14:15:45'),
(161, 'alex', '產品資料', '修改 Alicon Breaker B70', '2017-10-26 14:16:31'),
(162, 'alex', '產品資料', '修改 Alicon Breaker B140', '2017-10-26 14:16:44'),
(163, 'alex', '產品資料', '修改 Alicon Breaker B210', '2017-10-26 14:16:55'),
(164, 'alex', '關於我們', '修改 &lt;div class=&quot;textedit text-justify&quot;&gt;\r\n&lt;h3 class=&quot;text_title&quot;&gt;亞力士工程有限公司&lt;/h3&gt;\r\n\r\n&lt;p&gt;本公司專業團隊能為您提供專業解決方案，使您的更加快速、簡單和有效最得環保署的NRMM 核准標籤。&lt;/p&gt;\r\n\r\n&lt;p&gt;我們始終已超越客戶期望，及提供卓越的市場服務為己任。我們提供專業的商務解決方案，在法規日益增多的市場上為客戶領航。&lt;/p&gt;\r\n&lt;/div&gt;\r\n\r\n&lt;div class=&quot;textedit mt30 text-justify&quot;&gt;\r\n&lt;h3 class=&quot;text_title&quot;&gt;我們的願景&lt;/h3&gt;\r\n\r\n&lt;p&gt;成為一個最具競爭力與生產力的服務組織是我們的目標。 持續改善我們在檢驗、查證和驗證的核心能力，以提供最佳的服務， 這是我們之精隨所在。 我們的競爭力與對客戶不斷提供無微不至的服務，將是決定我們客戶心目中首要地位的不二法門。&lt;/p&gt;\r\n&lt;/div&gt;\r\n\r\n&lt;div class=&quot;textedit mt30 text-justify&quot;&gt;\r\n&lt;h3 class=&quot;text_title&quot;&gt;我們的價值&lt;/h3&gt;\r\n\r\n&lt;p&gt;我們追求以熱情、誠信和開創性來表現我們的特質，並持續努力達成我們的願景。 這些價值引導著我們在各個層面的所作所為，是建構我們組織的根本基石。&lt;/p&gt;\r\n&lt;/div&gt;\r\n', '2017-10-27 15:09:26'),
(165, 'alex', '關於我們', '修改 &lt;div class=&quot;textedit text-justify&quot;&gt;\r\n&lt;h3 class=&quot;text_title&quot;&gt;亞力士工程有限公司&lt;/h3&gt;\r\n\r\n&lt;p&gt;本公司專業團隊能為您提供專業解決方案，使您的更加快速、簡單和有效最得環保署的NRMM 核准標籤。&lt;/p&gt;\r\n\r\n&lt;p&gt;我們始終已超越客戶期望，及提供卓越的市場服務為己任。我們提供專業的商務解決方案，在法規日益增多的市場上為客戶領航。&lt;/p&gt;\r\n&lt;/div&gt;\r\n\r\n&lt;div class=&quot;textedit mt30 text-justify&quot;&gt;\r\n&lt;h3 class=&quot;text_title&quot;&gt;我們的願景&lt;/h3&gt;\r\n\r\n&lt;p&gt;成為一個最具競爭力與生產力的服務組織是我們的目標。 持續改善我們在檢驗、查證和驗證的核心能力，以提供最佳的服務， 這是我們之精隨所在。 我們的競爭力與對客戶不斷提供無微不至的服務，將是決定我們客戶心目中首要地位的不二法門。&lt;/p&gt;\r\n&lt;/div&gt;\r\n\r\n&lt;div class=&quot;textedit mt30 text-justify&quot;&gt;\r\n&lt;h3 class=&quot;text_title&quot;&gt;我們的價值&lt;/h3&gt;\r\n\r\n&lt;p&gt;我們追求以熱情、誠信和開創性來表現我們的特質，並持續努力達成我們的願景。 這些價值引導著我們在各個層面的所作所為，是建構我們組織的根本基石。&lt;/p&gt;\r\n&lt;/div&gt;\r\n', '2017-10-29 09:13:42'),
(166, 'alex', '關於我們', '修改 &lt;div class=&quot;textedit text-justify&quot;&gt;\r\n&lt;h3 class=&quot;text_title&quot;&gt;亞力士工程有限公司&lt;/h3&gt;\r\n\r\n&lt;p&gt;本公司專業團隊能為您提供專業解決方案，使您的更加快速、簡單和有效最得環保署的NRMM 核准標籤。&lt;/p&gt;\r\n\r\n&lt;p&gt;我們始終已超越客戶期望，及提供卓越的市場服務為己任。我們提供專業的商務解決方案，在法規日益增多的市場上為客戶領航。&lt;/p&gt;\r\n&lt;/div&gt;\r\n\r\n&lt;div class=&quot;textedit mt30 text-justify&quot;&gt;\r\n&lt;h3 class=&quot;text_title&quot;&gt;我們的願景&lt;/h3&gt;\r\n\r\n&lt;p&gt;成為一個最具競爭力與生產力的服務組織是我們的目標。 持續改善我們在檢驗、查證和驗證的核心能力，以提供最佳的服務， 這是我們之精隨所在。 我們的競爭力與對客戶不斷提供無微不至的服務，將是決定我們客戶心目中首要地位的不二法門。&lt;/p&gt;\r\n&lt;/div&gt;\r\n\r\n&lt;div class=&quot;textedit mt30 text-justify&quot;&gt;\r\n&lt;h3 class=&quot;text_title&quot;&gt;我們的價值&lt;/h3&gt;\r\n\r\n&lt;p&gt;我們追求以熱情、誠信和開創性來表現我們的特質，並持續努力達成我們的願景。 這些價值引導著我們在各個層面的所作所為，是建構我們組織的根本基石。&lt;/p&gt;\r\n&lt;/div&gt;\r\n', '2017-10-29 09:13:51'),
(167, 'alex', '品牌管理', '修改 Xcentric Ripper', '2017-10-29 09:33:21'),
(168, 'alex', '品牌管理', '修改 Xcentric Ripper', '2017-10-29 09:37:22'),
(169, 'alex', '品牌管理', '修改 Xcentric Ripper', '2017-10-29 09:38:22'),
(170, 'alex', '品牌管理', '修改 Xcentric Ripper', '2017-10-29 09:39:54'),
(171, 'alex', '最新消息', '新增 搬家了', '2017-10-29 09:43:46'),
(172, 'alex', '最新消息', '修改 Xcentric 搬家了', '2017-10-29 09:44:13'),
(173, 'alex', '產品目錄', '新增 2', '2017-10-29 09:46:49'),
(174, 'alex', '產品目錄', '新增 2', '2017-10-29 09:47:07'),
(175, 'alex', '產品目錄', '新增 2', '2017-10-29 09:47:31'),
(176, 'alex', '產品目錄', '新增 2', '2017-10-29 09:48:22'),
(177, 'alex', '品牌管理', '修改 Xcentric Ripper', '2017-10-29 09:49:50'),
(178, 'alex', '機械資料', '新增 XR20', '2017-10-29 13:04:49'),
(179, 'alex', '產品資料', '新增 Xcentric XR20', '2017-10-29 13:08:08'),
(180, 'alex', '機械資料', '新增 XC20', '2017-10-29 13:11:12'),
(181, 'alex', '產品資料', '新增 Xcentric XC20 碎石機', '2017-10-29 13:13:06'),
(182, 'alex', '機械資料', '修改 XR20', '2017-10-29 13:18:33'),
(183, 'alex', '機械資料', '修改 DMP230CMS', '2017-10-29 13:27:08'),
(184, 'alex', '產品資料', '新增 大模 DMP230CMS 液壓剪', '2017-10-29 13:30:46'),
(185, 'alex', '產品資料', '修改 大模 DMP230CMS 液壓剪', '2017-10-29 13:31:29'),
(186, 'alex', '產品資料', '修改 大模 DMP230CMS 液壓剪', '2017-10-29 13:32:01'),
(187, 'alex', '機械資料', '修改 DMP230CMS', '2017-10-29 13:36:19'),
(188, 'alex', '品牌管理', '修改 韓國大模工程有限公司', '2017-10-30 01:26:55'),
(189, 'alex', '品牌管理', '修改 韓國大模工程有限公司', '2017-10-30 01:27:13'),
(190, 'alex', '品牌管理', '修改 韓國大模工程有限公司', '2017-10-30 01:58:12'),
(191, 'alex', '品牌管理', '修改 韓國大模工程有限公司', '2017-10-30 10:54:45'),
(192, 'alex', '使用者管理', '新增 peter', '2017-11-18 09:51:52'),
(193, 'alex', '參數表及其他文件', '修改 35', '2017-11-18 09:56:21'),
(194, 'alex', '參數表及其他文件', '新增 36', '2017-11-18 11:25:41'),
(195, 'alex', '參數表及其他文件', '修改 35', '2017-11-18 11:25:55'),
(196, 'alex', '參數表及其他文件', '新增 34', '2017-11-18 11:26:24'),
(197, 'alex', '參數表及其他文件', '新增 24', '2017-11-18 11:27:17'),
(198, 'alex', '參數表及其他文件', '新增 25', '2017-11-18 11:27:44'),
(199, 'alex', '參數表及其他文件', '新增 16', '2017-11-18 11:28:04'),
(200, 'alex', '參數表及其他文件', '新增 26', '2017-11-18 11:28:36'),
(201, 'alex', '參數表及其他文件', '新增 27', '2017-11-18 11:29:13'),
(202, 'alex', '參數表及其他文件', '新增 28', '2017-11-18 11:29:39'),
(203, 'alex', '參數表及其他文件', '修改 27', '2017-11-18 11:30:21'),
(204, 'alex', '參數表及其他文件', '新增 17', '2017-11-18 11:31:14'),
(205, 'alex', '參數表及其他文件', '新增 18', '2017-11-18 11:32:23'),
(206, 'peter', '參數表及其他文件', '新增 29', '2017-11-18 12:02:17'),
(207, 'peter', '參數表及其他文件', '新增 30', '2017-11-18 12:03:41'),
(208, 'peter', '參數表及其他文件', '新增 19', '2017-11-18 12:04:09'),
(209, 'peter', '參數表及其他文件', '新增 31', '2017-11-18 12:04:33'),
(210, 'peter', '參數表及其他文件', '新增 20', '2017-11-18 12:05:03'),
(211, 'peter', '參數表及其他文件', '新增 21', '2017-11-18 12:05:34'),
(212, 'peter', '參數表及其他文件', '新增 32', '2017-11-18 12:06:00'),
(213, 'peter', '參數表及其他文件', '新增 22', '2017-11-18 12:06:40'),
(214, 'peter', '參數表及其他文件', '新增 33', '2017-11-18 12:07:21'),
(215, 'peter', '參數表及其他文件', '新增 23', '2017-11-18 12:07:48'),
(216, 'peter', '參數表及其他文件', '新增 41', '2017-11-18 12:08:22'),
(217, 'alex', '產品資料', '新增 Alicon Breaker B06', '2017-12-08 07:54:54'),
(218, 'alex', '產品資料', '新增 Alicon Breaker B10', '2017-12-08 07:56:39'),
(219, 'alex', '產品資料', '新增 Alicon Breaker B20', '2017-12-08 07:57:18'),
(220, 'alex', '產品資料', '新增 Alicon Breaker B40', '2017-12-08 16:33:23'),
(221, 'alex', '產品資料', '新增 Alicon Breaker B60', '2017-12-08 16:34:06'),
(222, 'alex', '產品資料', '新增 Alicon Breaker B90', '2017-12-08 16:34:56'),
(223, 'alex', '產品資料', '新增 Alicon Breaker B180', '2017-12-08 16:35:59'),
(224, 'alex', '產品資料', '新增 Alicon Breaker B230', '2017-12-08 16:36:53'),
(225, 'alex', '產品資料', '新增 Alicon Breaker B250', '2017-12-08 16:37:38'),
(226, 'alex', '產品資料', '新增 Alicon Breaker B300', '2017-12-08 16:38:30'),
(227, 'alex', '產品資料', '新增 Alicon Breaker B360', '2017-12-08 16:39:24'),
(228, 'alex', '產品資料', '新增 Alicon Breaker B450', '2017-12-08 16:40:17'),
(229, 'alex', '產品資料', '新增 Alicon Breaker B650', '2017-12-08 16:41:04'),
(230, 'alex', '產品資料', '新增 Alicon Breaker B800', '2017-12-08 16:42:04'),
(231, 'alex', '產品資料', '新增 Alicon Breaker B1000', '2017-12-08 16:42:52'),
(232, 'alex', '關於我們', '修改 &lt;div class=&quot;textedit text-justify&quot;&gt;\r\n&lt;h3 class=&quot;text_title&quot;&gt;亞力士工程有限公司&lt;/h3&gt;\r\n\r\n&lt;p&gt;本公司專業團隊能為您提供專業解決方案，使您的更加快速、簡單和有效最得環保署的NRMM 核准標籤。&lt;/p&gt;\r\n\r\n&lt;p&gt;我們以超越客戶期望，及提供卓越的市場服務為己任。我們提供專業的商務解決方案，在法規日益增多的市場上為客戶領航。&lt;/p&gt;\r\n&lt;/div&gt;\r\n\r\n&lt;div class=&quot;textedit mt30 text-justify&quot;&gt;\r\n&lt;h3 class=&quot;text_title&quot;&gt;我們的願景&lt;/h3&gt;\r\n\r\n&lt;p&gt;成為一個最具競爭力與生產力的服務組織是我們的目標。 持續改善我們在檢驗、查證和驗證的核心能力，以提供最佳的服務， 這是我們之精髓所在。 我們的競爭力與對客戶不斷提供無微不至的服務，將是決定我們客戶心目中首要地位的不二法門。&lt;/p&gt;\r\n&lt;/div&gt;\r\n\r\n&lt;div class=&quot;textedit mt30 text-justify&quot;&gt;\r\n&lt;h3 class=&quot;text_title&quot;&gt;我們的價值&lt;/h3&gt;\r\n\r\n&lt;p&gt;我們追求以熱情、誠信和開創性來表現我們的特質，並持續努力達成我們的願景。 這些價值引導著我們在各個層面的所作所為，是建構我們組織的根本基石。&lt;/p&gt;\r\n&lt;/div&gt;\r\n', '2018-01-18 01:17:15'),
(233, 'alex', '關於我們', '修改 &lt;div class=&quot;textedit text-justify&quot;&gt;\r\n&lt;h3 class=&quot;text_title&quot;&gt;亞力士工程有限公司&lt;/h3&gt;\r\n\r\n&lt;p&gt;本公司專業團隊能為您提供專業解決方案，使您的更加快速、簡單和有效取得國際建造業資訊以及香港環保署要求的相關標籤。&lt;/p&gt;\r\n\r\n&lt;p&gt;我們以超越客戶期望及提供卓越的市場服務為己任。我們提供專業的商務解決方案，在法規日益增加的市場上為客戶領航。&lt;/p&gt;\r\n&lt;/div&gt;\r\n\r\n&lt;div class=&quot;textedit mt30 text-justify&quot;&gt;\r\n&lt;h3 class=&quot;text_title&quot;&gt;我們的願景&lt;/h3&gt;\r\n\r\n&lt;p&gt;我們的目標是成為一個最具競爭力與生產力的公司。 持續改善我們及提供多完化服務的核心能力。 我們希望為客戶不斷提供無微不至的服務提昇競爭力，並期待可以服務更多的業界同仁, 並肩成長與國際建造業接軌。&lt;/p&gt;\r\n&lt;/div&gt;\r\n\r\n&lt;div class=&quot;textedit mt30 text-justify&quot;&gt;\r\n&lt;h3 class=&quot;text_title&quot;&gt;我們的價值&lt;/h3&gt;\r\n\r\n&lt;p&gt;我們追求以熱情、誠信和創新，並持續努力腳踏實地去達成我們的願景。 這些價值引導著我們在各個層面的一舉一動，是建構我們組織的根本基石。&lt;/p&gt;\r\n&lt;/div&gt;\r\n', '2018-01-18 01:27:45'),
(234, 'alex', '關於我們', '修改 &lt;div class=&quot;textedit text-justify&quot;&gt;\r\n&lt;h3 class=&quot;text_title&quot;&gt;亞力士工程有限公司&lt;/h3&gt;\r\n\r\n&lt;p&gt;本公司專業團隊能為您提供專業解決方案，使您的更加快速、簡單和有效取得國際建造業資訊以及香港環保署要求的相關標籤。&lt;/p&gt;\r\n\r\n&lt;p&gt;我們以超越客戶期望及提供卓越的市場服務為己任。我們竭力為客戶提供專業的商務解決方案，在法規日益增加的市場上為客戶領航。&lt;/p&gt;\r\n&lt;/div&gt;\r\n\r\n&lt;div class=&quot;textedit mt30 text-justify&quot;&gt;\r\n&lt;h3 class=&quot;text_title&quot;&gt;我們的願景&lt;/h3&gt;\r\n\r\n&lt;p&gt;我們的目標是成為一個最具競爭力與生產力的公司。 持續改善我們及提供多完化服務的核心能力。 我們希望為客戶不斷提供無微不至的服務以提昇競爭力，並期待可以服務更多的業界同仁, 並肩成長與國際建造業接軌。&lt;/p&gt;\r\n&lt;/div&gt;\r\n\r\n&lt;div class=&quot;textedit mt30 text-justify&quot;&gt;\r\n&lt;h3 class=&quot;text_title&quot;&gt;我們的價值&lt;/h3&gt;\r\n\r\n&lt;p&gt;我們追求以熱情、誠信和創新，並持續努力腳踏實地去達成我們的願景。 這些價值引導著我們在各個層面的一舉一動，是建構我們業務的根本基石。&lt;/p&gt;\r\n&lt;/div&gt;\r\n', '2018-01-18 01:30:37'),
(235, 'alex', '關於我們', '修改 &lt;div class=&quot;textedit text-justify&quot;&gt;\r\n&lt;h3 class=&quot;text_title&quot;&gt;亞力士工程有限公司&lt;/h3&gt;\r\n\r\n&lt;p&gt;本公司專業團隊能為您提供專業解決方案，使您的更加快速、簡單和有效取得國際建造業資訊以及香港環保署要求的相關標籤。&lt;/p&gt;\r\n\r\n&lt;p&gt;我們以超越客戶期望及提供卓越的市場服務為己任。我們竭力為客戶提供專業的商務解決方案，在法規日益增加的市場上為客戶領航。&lt;/p&gt;\r\n&lt;/div&gt;\r\n\r\n&lt;div class=&quot;textedit mt30 text-justify&quot;&gt;\r\n&lt;h3 class=&quot;text_title&quot;&gt;我們的願景&lt;/h3&gt;\r\n\r\n&lt;p&gt;我們的目標是成為一個最具競爭力與生產力的公司。 持續改善我們及提供多完化服務的核心能力。 我們希望為客戶不斷提供無微不至的服務以提昇競爭力，並期待可以服務更多的業界同仁, 並肩成長與國際建造業接軌。&lt;/p&gt;\r\n&lt;/div&gt;\r\n\r\n&lt;div class=&quot;textedit mt30 text-justify&quot;&gt;\r\n&lt;h3 class=&quot;text_title&quot;&gt;我們的價值&lt;/h3&gt;\r\n\r\n&lt;p&gt;我們追求以熱情、誠信和創新，並持續努力腳踏實地去達成我們的願景。 這些價值引導著我們在各個層面的一舉一動，是建構我們業務的根本基石。&lt;/p&gt;\r\n&lt;/div&gt;\r\n', '2018-01-21 07:01:55'),
(236, 'alex', '關於我們', '修改 &lt;div class=&quot;textedit text-justify&quot;&gt;\r\n&lt;h3 class=&quot;text_title&quot;&gt;亞力士工程有限公司&lt;/h3&gt;\r\n\r\n&lt;p&gt;本公司專業團隊能為您提供專業解決方案，使您的更加快速、簡單和有效取得國際建造業資訊以及香港環保署要求的相關標籤。&lt;/p&gt;\r\n\r\n&lt;p&gt;我們以超越客戶期望及提供卓越的市場服務為己任。我們竭力為客戶提供專業的商務解決方案，在法規日益增加的市場上為客戶領航。&lt;/p&gt;\r\n&lt;/div&gt;\r\n\r\n&lt;div class=&quot;textedit mt30 text-justify&quot;&gt;\r\n&lt;h3 class=&quot;text_title&quot;&gt;我們的願景&lt;/h3&gt;\r\n\r\n&lt;p&gt;我們的目標是成為一個最具競爭力與生產力的公司。 持續改善我們及提供多完化服務的核心能力。 我們希望為客戶不斷提供無微不至的服務以提昇競爭力，並期待可以服務更多的業界同仁, 並肩成長與國際建造業接軌。&lt;/p&gt;\r\n&lt;/div&gt;\r\n\r\n&lt;div class=&quot;textedit mt30 text-justify&quot;&gt;\r\n&lt;h3 class=&quot;text_title&quot;&gt;我們的價值&lt;/h3&gt;\r\n\r\n&lt;p&gt;我們追求以熱情、誠信和創新，並持續努力腳踏實地去達成我們的願景。 這些價值引導著我們在各個層面的一舉一動，是建構我們業務的根本基石。&lt;/p&gt;\r\n&lt;/div&gt;\r\n', '2018-01-21 07:02:36'),
(237, 'alex', '產品資料', '新增 PC2000', '2018-03-19 08:41:33'),
(238, 'alex', 'Slider管理', '新增 ', '2018-06-04 09:56:27');

-- --------------------------------------------------------

--
-- 資料表結構 `brand`
--

CREATE TABLE `brand` (
  `brand_id` int(11) NOT NULL,
  `brand_title` varchar(50) NOT NULL,
  `brand_title_en` varchar(50) NOT NULL,
  `brand_url` varchar(20) NOT NULL,
  `brand_info` text NOT NULL,
  `brand_info_en` text NOT NULL,
  `brand_logo` varchar(50) NOT NULL,
  `brand_website` text NOT NULL,
  `brand_view_cate` varchar(30) NOT NULL,
  `brand_online` tinyint(1) NOT NULL,
  `brand_sort` int(11) NOT NULL,
  `brand_title_short` varchar(50) DEFAULT NULL,
  `brand_listed` tinyint(1) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 資料表結構 `brand_album`
--

CREATE TABLE `brand_album` (
  `album_id` int(11) NOT NULL,
  `host_id` int(11) NOT NULL,
  `album_photo` varchar(50) NOT NULL,
  `album_title` varchar(50) NOT NULL,
  `album_sort` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- --------------------------------------------------------

--
-- 資料表結構 `brand_file`
--

CREATE TABLE `brand_file` (
  `file_id` int(11) NOT NULL,
  `host_id` int(11) NOT NULL,
  `file_file` varchar(50) NOT NULL,
  `file_file_en` varchar(50) NOT NULL,
  `file_title` varchar(50) NOT NULL,
  `file_title_en` varchar(50) NOT NULL,
  `file_online` tinyint(1) NOT NULL,
  `file_sort` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- --------------------------------------------------------

--
-- 資料表結構 `brand_video`
--

CREATE TABLE `brand_video` (
  `video_id` int(11) NOT NULL,
  `host_id` int(11) NOT NULL,
  `video_url` text NOT NULL,
  `video_title` varchar(50) NOT NULL,
  `video_title_en` varchar(50) NOT NULL,
  `video_online` tinyint(1) NOT NULL,
  `video_sort` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- --------------------------------------------------------

--
-- 資料表結構 `case`
--

CREATE TABLE `case` (
  `case_id` int(11) NOT NULL,
  `case_title` varchar(50) NOT NULL,
  `case_title_en` varchar(50) NOT NULL,
  `works_type_id` int(11) NOT NULL,
  `case_content` text NOT NULL,
  `case_content_en` text NOT NULL,
  `case_photo` varchar(50) NOT NULL,
  `case_left_menu` tinyint(1) NOT NULL,
  `case_online` tinyint(1) NOT NULL,
  `case_sort` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- --------------------------------------------------------

--
-- 資料表結構 `case_album`
--

CREATE TABLE `case_album` (
  `album_id` int(11) NOT NULL,
  `host_id` int(11) NOT NULL,
  `album_photo` varchar(50) NOT NULL,
  `album_title` varchar(50) NOT NULL,
  `album_sort` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- --------------------------------------------------------

--
-- 資料表結構 `contact`
--

CREATE TABLE `contact` (
  `contact_id` int(100) NOT NULL,
  `contact_name` varchar(100) NOT NULL,
  `contact_email` varchar(100) NOT NULL,
  `contact_phone` varchar(20) NOT NULL,
  `contact_company` varchar(30) NOT NULL,
  `contact_country` varchar(10) NOT NULL,
  `contact_message` text NOT NULL,
  `contact_time` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 資料表結構 `country_calling_codes`
--

CREATE TABLE `country_calling_codes` (
  `country_id` int(11) NOT NULL,
  `country_name` varchar(50) NOT NULL,
  `country_calling_codes` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- 資料表的匯出資料 `country_calling_codes`
--

INSERT INTO `country_calling_codes` (`country_id`, `country_name`, `country_calling_codes`) VALUES
(1, '台灣Taiwan', 886),
(2, '香港Hong Kong', 852),
(3, '澳門Macao', 853),
(4, '日本Japan', 81),
(5, '南韓Korea South', 82),
(6, '北韓Korea North', 850),
(7, '馬來西亞Malaysia', 60),
(8, '新加坡Singapore', 65),
(9, '汶萊Brunei', 673),
(10, '印尼Indonesia', 62),
(11, '泰國Thailand', 66),
(12, '菲律賓Philippines', 63),
(13, '越南Vietnam', 84),
(14, '柬埔寨Cambodia', 855),
(15, '寮國Laos', 856),
(16, '印度India', 91),
(17, '巴基斯坦Pakistan', 92),
(18, '緬甸Myanmar', 95),
(19, '外蒙古Mongolia', 976),
(20, '尼泊爾Nepal', 977),
(21, '不丹Bhutan', 975),
(22, '馬爾地夫Maldives', 960),
(23, '中國China', 86),
(24, '美國U.S.A.', 1),
(25, '加拿大Canada', 1),
(26, '巴哈馬Bahamas', 1),
(27, '多明尼加Dominican', 1),
(28, '秘魯Peru', 51),
(29, '墨西哥Mexico', 52),
(30, '古巴Cuba', 53),
(31, '阿根廷Argentina', 54),
(32, '巴西Brazil', 55),
(33, '智利Chile', 56),
(34, '哥倫比亞Colombia', 57),
(35, '瓜地馬拉Guatemala', 502),
(36, '宏都拉斯Honduras', 504),
(37, '尼加拉瓜Nicaragua', 505),
(38, '哥斯大黎加Casta Rica', 506),
(39, '巴拿馬Panama', 507),
(40, '摩里西斯Mauritius', 230),
(41, '巴拉圭Paraguay', 595),
(42, '厄瓜多Ecuador', 593),
(43, '巴布亞紐幾內亞Papua New Guinea', 675),
(44, '貝里斯Belize', 501),
(45, '海地Haiti', 509),
(46, '委內瑞拉Venezuela', 58),
(47, '格瑞那達Grenada', 1),
(48, '波多黎各Puerto Rico', 1),
(49, '牙買加Jamaica', 1),
(50, '千里達及托巴哥Trinidad & Tobago', 1),
(51, '希臘Hellenic', 30),
(52, '荷蘭Netherlands', 31),
(53, '比利時Belgium', 32),
(54, '法國France', 33),
(55, '西班牙Spain', 34),
(56, '直布羅陀Gibraltar', 350),
(57, '葡萄牙Portugal', 351),
(58, '盧森堡 Luxembourg', 352),
(59, '愛爾蘭Ireland', 353),
(60, '冰島Iceland', 354),
(61, '芬蘭Finland', 358),
(62, '保加利亞Bulgaria', 359),
(63, '匈牙利Hungary', 36),
(64, '摩納哥Monaco', 377),
(65, '烏克蘭Ukraina', 380),
(66, '南斯拉夫Yugoslavia', 381),
(67, '義大利Italy', 39),
(68, '梵諦岡Vatican', 39),
(69, '羅馬尼亞Romania', 40),
(70, '瑞士Switzerland', 41),
(71, '捷克Czech Rep.', 420),
(72, '奧地利Austria', 43),
(73, '英國U.K.', 44),
(74, '丹麥Denmark', 45),
(75, '瑞典Sweden', 46),
(76, '挪威Norway', 47),
(77, '波蘭Poland', 48),
(78, '德國Germany', 49),
(79, '玻利維亞Bolivia', 591),
(80, '烏拉圭Uruguay', 598),
(81, '俄羅斯Russia', 7),
(82, '愛沙尼亞Estonia', 372),
(83, '斯洛伐克Slovak', 421),
(84, '賽普勒斯Cyprus', 357),
(85, '安道爾Andorra', 376),
(86, '馬爾他Malta', 356),
(87, '澳大利亞Australia', 61),
(88, '紐西蘭New Zealand', 64),
(89, '塞班島Saipan', 670),
(90, '關島Guam', 671),
(91, '所羅門群島Solomon IS.', 677),
(92, '斐濟Fiji', 679),
(93, '帛琉Palau', 680),
(94, '新喀里多尼亞New Caledonia', 687),
(95, '埃及Egypt', 20),
(96, '摩洛哥Morocco', 212),
(97, '尼日共和國Niger', 227),
(98, '奈及利亞Nigeria', 234),
(99, '中非共和國Central African', 236),
(100, '加彭共和國Gabonese Rep.', 241),
(101, '剛果Congo', 242),
(102, '薩伊Zaire', 243),
(103, '衣索匹亞Ethiopia', 251),
(104, '肯亞Kenya', 254),
(105, '烏干達Uganda', 256),
(106, '莫三鼻克Mozambique', 258),
(107, '辛巴威Zimbabwe', 263),
(108, '南非South Africa', 27),
(109, '薩爾瓦多El Salvador', 503),
(110, '利比亞Libya', 218),
(111, '象牙海岸Ivory Coast', 225),
(112, '賴比瑞亞Liberia', 231),
(113, '蘇丹Sudan', 249),
(114, '賴索托Lesotho', 266),
(115, '史瓦濟蘭Swaziland', 268),
(116, '突尼西亞Tunisia', 216),
(117, '賽內加爾Senegal', 221),
(118, '喀麥隆Cameroon', 237),
(119, '坦尚尼亞Tanzania', 255),
(120, '馬達加斯加Madagascar', 261),
(121, '馬拉威Malawi', 265),
(122, '土耳其Turkey', 90),
(123, '約旦Jordan', 962),
(124, '沙烏地阿拉伯Saudi Arabia', 966),
(125, '阿拉伯聯合大公國U.A.E', 971),
(126, '孟加拉Bangladesh', 880),
(127, '巴林Bahrain', 973),
(128, '斯里蘭卡Sri Lanka', 94),
(129, '黎巴嫩Lebanon', 961),
(130, '伊拉克Irag', 964),
(131, '科威特Kuwait', 965),
(132, '以色列Israel', 972),
(133, '卡達Qatar', 974),
(134, '葉門共和國Yemen Rep.', 967),
(135, '阿曼Oman', 968),
(136, '敘利亞Syria', 963),
(137, '伊朗Iran', 98);

-- --------------------------------------------------------

--
-- 資料表結構 `country_code`
--

CREATE TABLE `country_code` (
  `country_id` int(11) NOT NULL,
  `country_name` varchar(100) NOT NULL,
  `country_name_ch` varchar(50) NOT NULL,
  `country_letter2_code` varchar(2) NOT NULL,
  `country_letter3_code` varchar(3) NOT NULL,
  `country_number_code` varchar(3) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- 資料表的匯出資料 `country_code`
--

INSERT INTO `country_code` (`country_id`, `country_name`, `country_name_ch`, `country_letter2_code`, `country_letter3_code`, `country_number_code`) VALUES
(1, 'Afghanistan', '阿富汗', 'AF', 'AFG', '004'),
(2, 'ALA Aland Islands', '阿拉阿蘭群島', 'AX', 'ALA', '248'),
(3, 'Albania', '阿爾巴尼亞', 'AL', 'ALB', '008'),
(4, 'Algeria', '阿爾及利亞', 'DZ', 'DZA', '012'),
(5, 'American Samoa', '美屬薩摩亞', 'AS', 'ASM', '016'),
(6, 'Andorra', '安道爾', 'AD', 'AND', '020'),
(7, 'Angola', '安哥拉', 'AO', 'AGO', '024'),
(8, 'Anguilla', '安圭拉', 'AI', 'AIA', '660'),
(9, 'Antarctica', '南極洲', 'AQ', 'ATA', '010'),
(10, 'Antigua and Barbuda', '安提瓜和巴布達', 'AG', 'ATG', '028'),
(11, 'Argentina', '阿根廷', 'AR', 'ARG', '032'),
(12, 'Armenia', '亞美尼亞', 'AM', 'ARM', '051'),
(13, 'Aruba', '阿魯巴', 'AW', 'ABW', '533'),
(14, 'Australia', '澳大利亞', 'AU', 'AUS', '036'),
(15, 'Austria', '奧地利', 'AT', 'AUT', '040'),
(16, 'Azerbaijan', '阿塞拜疆', 'AZ', 'AZE', '031'),
(17, 'Bahamas', '巴哈馬', 'BS', 'BHS', '044'),
(18, 'Bahrain', '巴林', 'BH', 'BHR', '048'),
(19, 'Bangladesh', '孟加拉國', 'BD', 'BGD', '050'),
(20, 'Barbados', '巴巴多斯', 'BB', 'BRB', '052'),
(21, 'Belarus', '白俄羅斯', 'BY', 'BLR', '112'),
(22, 'Belgium', '比利時', 'BE', 'BEL', '056'),
(23, 'Belize', '伯利茲', 'BZ', 'BLZ', '084'),
(24, 'Benin', '貝寧', 'BJ', 'BEN', '204'),
(25, 'Bermuda', '百慕大', 'BM', 'BMU', '060'),
(26, 'Bhutan', '不丹', 'BT', 'BTN', '064'),
(27, 'Bolivia', '玻利維亞', 'BO', 'BOL', '068'),
(28, 'Bosnia and Herzegovina', '波斯尼亞和黑塞哥維那', 'BA', 'BIH', '070'),
(29, 'Botswana', '博茨瓦納', 'BW', 'BWA', '072'),
(30, 'Bouvet Island', '布韋島', 'BV', 'BVT', '074'),
(31, 'Brazil', '巴西', 'BR', 'BRA', '076'),
(32, 'British Virgin Islands', '英屬維爾京群島', 'VG', 'VGB', '092'),
(33, 'British Indian Ocean Territory', '英屬印度洋領地', 'IO', 'IOT', '086'),
(34, 'Brunei Darussalam', '文萊達魯薩蘭國', 'BN', 'BRN', '096'),
(35, 'Bulgaria', '保加利亞', 'BG', 'BGR', '100'),
(36, 'Burkina Faso', '布基納法索', 'BF', 'BFA', '854'),
(37, 'Burundi', '布隆迪', 'BI', 'BDI', '108'),
(38, 'Cambodia', '柬埔寨', 'KH', 'KHM', '116'),
(39, 'Cameroon', '喀麥隆', 'CM', 'CMR', '120'),
(40, 'Canada', '加拿大', 'CA', 'CAN', '124'),
(41, 'Cape Verde', '維德角', 'CV', 'CPV', '132'),
(42, 'Cayman Islands', '開曼群島', 'KY', 'CYM', '136'),
(43, 'Central African Republic', '中非共和國', 'CF', 'CAF', '140'),
(44, 'Chad', '查德', 'TD', 'TCD', '148'),
(45, 'Chile', '智利', 'CL', 'CHL', '152'),
(46, 'China', '中國', 'CN', 'CHN', '156'),
(47, 'Hong Kong', '香港', 'HK', 'HKG', '344'),
(48, 'Macao, Special Administrative Region of China', '澳門', 'MO', 'MAC', '446'),
(49, 'Christmas Island', '聖誕島', 'CX', 'CXR', '162'),
(50, 'Cocos (Keeling) Islands', '科科斯(基林)群島', 'CC', 'CCK', '166'),
(51, 'Colombia', '哥倫比亞共和國', 'CO', 'COL', '170'),
(52, 'Comoros', '葛摩', 'KM', 'COM', '174'),
(53, 'Congo (Brazzaville)', '布拉薩維爾', 'CG', 'COG', '178'),
(54, 'Democratic Republic of the Congo', '剛果民主共和國', 'CD', 'COD', '180'),
(55, 'Cook Islands', '庫克群島', 'CK', 'COK', '184'),
(56, 'Costa Rica', '哥斯大黎加', 'CR', 'CRI', '188'),
(57, 'Côte d\'Ivoire', '象牙海岸', 'CI', 'CIV', '384'),
(58, 'Croatia', '克羅埃西亞', 'HR', 'HRV', '191'),
(59, 'Cuba', '古巴', 'CU', 'CUB', '192'),
(60, 'Cyprus', '賽普勒斯', 'CY', 'CYP', '196'),
(61, 'Czech Republic', '捷克', 'CZ', 'CZE', '203'),
(62, 'Denmark', '丹麥', 'DK', 'DNK', '208'),
(63, 'Djibouti', '吉布地', 'DJ', 'DJI', '262'),
(64, 'Dominica', '多米尼克', 'DM', 'DMA', '212'),
(65, 'Dominican Republic', '多明尼加', 'DO', 'DOM', '214'),
(66, 'Ecuador', '厄瓜多', 'EC', 'ECU', '218'),
(67, 'Egypt', '埃及', 'EG', 'EGY', '818'),
(68, 'El Salvador', '薩爾瓦多', 'SV', 'SLV', '222'),
(69, 'Equatorial Guinea', '赤道幾內亞', 'GQ', 'GNQ', '226'),
(70, 'Eritrea', '厄利垂亞', 'ER', 'ERI', '232'),
(71, 'Estonia', '愛沙尼亞', 'EE', 'EST', '233'),
(72, 'Ethiopia', '衣索比亞', 'ET', 'ETH', '231'),
(73, 'Falkland Islands (Malvinas)', '福克蘭群島', 'FK', 'FLK', '238'),
(74, 'Faroe Islands', '法羅群島', 'FO', 'FRO', '234'),
(75, 'Fiji', '斐濟', 'FJ', 'FJI', '242'),
(76, 'Finland', '芬蘭', 'FI', 'FIN', '246'),
(77, 'France', '法國', 'FR', 'FRA', '250'),
(78, 'French Guiana', '法屬圭亞那', 'GF', 'GUF', '254'),
(79, 'French Polynesia', '法屬玻里尼西亞', 'PF', 'PYF', '258'),
(80, 'French Southern Territories', '法屬南部和南極領地', 'TF', 'ATF', '260'),
(81, 'Gabon', '加彭', 'GA', 'GAB', '266'),
(82, 'Gambia', '甘比亞', 'GM', 'GMB', '270'),
(83, 'Georgia', '喬治亞', 'GE', 'GEO', '268'),
(84, 'Germany', '德國', 'DE', 'DEU', '276'),
(85, 'Ghana', '迦納', 'GH', 'GHA', '288'),
(86, 'Gibraltar', '直布羅陀', 'GI', 'GIB', '292'),
(87, 'Greece', '希臘', 'GR', 'GRC', '300'),
(88, 'Greenland', '格陵蘭', 'GL', 'GRL', '304'),
(89, 'Grenada', '格瑞那達', 'GD', 'GRD', '308'),
(90, 'Guadeloupe', '瓜德羅普', 'GP', 'GLP', '312'),
(91, 'Guam', '關島', 'GU', 'GUM', '316'),
(92, 'Guatemala', '瓜地馬拉', 'GT', 'GTM', '320'),
(93, 'Guernsey', '根西', 'GG', 'GGY', '831'),
(94, 'Guinea', '幾內亞', 'GN', 'GIN', '324'),
(95, 'Guinea-Bissau', '幾內亞比索', 'GW', 'GNB', '624'),
(96, 'Guyana', '蓋亞那', 'GY', 'GUY', '328'),
(97, 'Haiti', '海地', 'HT', 'HTI', '332'),
(98, 'Heard Island and Mcdonald Islands', '赫德島和麥克唐納群島', 'HM', 'HMD', '334'),
(99, 'Holy See (Vatican City State)', '梵蒂岡', 'VA', 'VAT', '336'),
(100, 'Honduras', '宏都拉斯', 'HN', 'HND', '340'),
(101, 'Hungary', '匈牙利', 'HU', 'HUN', '348'),
(102, 'Iceland', '冰島', 'IS', 'ISL', '352'),
(103, 'India', '印度', 'IN', 'IND', '356'),
(104, 'Indonesia', '印度尼西亞', 'ID', 'IDN', '360'),
(105, 'Islamic Republic of Iran', '伊朗', 'IR', 'IRN', '364'),
(106, 'Iraq', '伊拉克', 'IQ', 'IRQ', '368'),
(107, 'Ireland', '愛爾蘭島', 'IE', 'IRL', '372'),
(108, 'Isle of Man', '曼島', 'IM', 'IMN', '833'),
(109, 'Israel', '以色列', 'IL', 'ISR', '376'),
(110, 'Italy', '義大利', 'IT', 'ITA', '380'),
(111, 'Jamaica', '牙買加', 'JM', 'JAM', '388'),
(112, 'Japan', '日本', 'JP', 'JPN', '392'),
(113, 'Jersey', '澤西', 'JE', 'JEY', '832'),
(114, 'Jordan', '約旦', 'JO', 'JOR', '400'),
(115, 'Kazakhstan', '哈薩克', 'KZ', 'KAZ', '398'),
(116, 'Kenya', '肯亞', 'KE', 'KEN', '404'),
(117, 'Kiribati', '吉里巴斯', 'KI', 'KIR', '296'),
(118, 'North Korea', '北韓', 'KP', 'PRK', '408'),
(119, 'Korea', '南韓', 'KR', 'KOR', '410'),
(120, 'Kuwait', '科威特', 'KW', 'KWT', '414'),
(121, 'Kyrgyzstan', '吉爾吉斯', 'KG', 'KGZ', '417'),
(122, 'Lao PDR', '寮國', 'LA', 'LAO', '418'),
(123, 'Latvia', '拉脫維亞', 'LV', 'LVA', '428'),
(124, 'Lebanon', '黎巴嫩', 'LB', 'LBN', '422'),
(125, 'Lesotho', '賴索托', 'LS', 'LSO', '426'),
(126, 'Liberia', '賴比瑞亞', 'LR', 'LBR', '430'),
(127, 'Libya', '利比亞', 'LY', 'LBY', '434'),
(128, 'Liechtenstein', '列支敦斯登', 'LI', 'LIE', '438'),
(129, 'Lithuania', '立陶宛', 'LT', 'LTU', '440'),
(130, 'Luxembourg', '盧森堡', 'LU', 'LUX', '442'),
(131, 'Republic of Macedonia', '馬其頓共和國', 'MK', 'MKD', '807'),
(132, 'Madagascar', '馬達加斯加', 'MG', 'MDG', '450'),
(133, 'Malawi', '馬拉威', 'MW', 'MWI', '454'),
(134, 'Malaysia', '馬來西亞', 'MY', 'MYS', '458'),
(135, 'Maldives', '馬爾地夫', 'MV', 'MDV', '462'),
(136, 'Mali', '馬利', 'ML', 'MLI', '466'),
(137, 'Malta', '馬爾他', 'MT', 'MLT', '470'),
(138, 'Marshall Islands', '馬紹爾群島', 'MH', 'MHL', '584'),
(139, 'Martinique', '馬丁尼克', 'MQ', 'MTQ', '474'),
(140, 'Mauritania', '茅利塔尼亞', 'MR', 'MRT', '478'),
(141, 'Mauritius', '模里西斯', 'MU', 'MUS', '480'),
(142, 'Mayotte', '馬約特', 'YT', 'MYT', '175'),
(143, 'Mexico', '墨西哥', 'MX', 'MEX', '484'),
(144, 'Federated States of Micronesia', '密克羅尼西亞聯邦', 'FM', 'FSM', '583'),
(145, 'Moldova', '摩爾多瓦', 'MD', 'MDA', '498'),
(146, 'Monaco', '摩納哥', 'MC', 'MCO', '492'),
(147, 'Mongolia', '蒙古國', 'MN', 'MNG', '496'),
(148, 'Montenegro', '蒙特內哥羅', 'ME', 'MNE', '499'),
(149, 'Montserrat', '蒙哲臘', 'MS', 'MSR', '500'),
(150, 'Morocco', '摩洛哥', 'MA', 'MAR', '504'),
(151, 'Mozambique', '莫三比克', 'MZ', 'MOZ', '508'),
(152, 'Myanmar', '緬甸', 'MM', 'MMR', '104'),
(153, 'Namibia', '納米比亞', 'NA', 'NAM', '516'),
(154, 'Nauru', '諾魯', 'NR', 'NRU', '520'),
(155, 'Nepal', '尼泊爾', 'NP', 'NPL', '524'),
(156, 'Netherlands', '荷蘭', 'NL', 'NLD', '528'),
(157, 'Netherlands Antilles', '荷屬安地列斯', 'AN', 'ANT', '530'),
(158, 'New Caledonia', '新喀里多尼亞', 'NC', 'NCL', '540'),
(159, 'New Zealand', '紐西蘭', 'NZ', 'NZL', '554'),
(160, 'Nicaragua', '尼加拉瓜', 'NI', 'NIC', '558'),
(161, 'Niger', '尼日', 'NE', 'NER', '562'),
(162, 'Nigeria', '奈及利亞', 'NG', 'NGA', '566'),
(163, 'Niue', '紐埃', 'NU', 'NIU', '570'),
(164, 'Norfolk Island', '諾福克島', 'NF', 'NFK', '574'),
(165, 'Northern Mariana Islands', '北馬利安納群島', 'MP', 'MNP', '580'),
(166, 'Norway', '挪威', 'NO', 'NOR', '578'),
(167, 'Oman', '阿曼', 'OM', 'OMN', '512'),
(168, 'Pakistan', '巴基斯坦', 'PK', 'PAK', '586'),
(169, 'Palau', '帛琉', 'PW', 'PLW', '585'),
(170, 'Palestinian Territory, Occupied', '巴勒斯坦領土', 'PS', 'PSE', '275'),
(171, 'Panama', '巴拿馬', 'PA', 'PAN', '591'),
(172, 'Papua New Guinea', '巴布亞紐幾內亞', 'PG', 'PNG', '598'),
(173, 'Paraguay', '巴拉圭', 'PY', 'PRY', '600'),
(174, 'Peru', '秘魯', 'PE', 'PER', '604'),
(175, 'Philippines', '菲律賓', 'PH', 'PHL', '608'),
(176, 'Pitcairn', '皮特肯群島', 'PN', 'PCN', '612'),
(177, 'Poland', '波蘭', 'PL', 'POL', '616'),
(178, 'Portugal', '葡萄牙', 'PT', 'PRT', '620'),
(179, 'Puerto Rico', '波多黎各', 'PR', 'PRI', '630'),
(180, 'Qatar', '卡達', 'QA', 'QAT', '634'),
(181, 'Réunion', '留尼旺', 'RE', 'REU', '638'),
(182, 'Romania', '羅馬尼亞', 'RO', 'ROU', '642'),
(183, 'Russian Federation', '俄羅斯', 'RU', 'RUS', '643'),
(184, 'Rwanda', '盧安達', 'RW', 'RWA', '646'),
(185, 'Saint-Barthélemy', '聖巴泰勒米', 'BL', 'BLM', '652'),
(186, 'Saint Helena', '聖赫倫那', 'SH', 'SHN', '654'),
(187, 'Saint Kitts and Nevis', '聖克里斯多福及尼維斯', 'KN', 'KNA', '659'),
(188, 'Saint Lucia', '聖露西亞', 'LC', 'LCA', '662'),
(189, 'Saint-Martin (French part)', '聖馬丁島', 'MF', 'MAF', '663'),
(190, 'Saint Pierre and Miquelon', '聖皮耶與密克隆群島', 'PM', 'SPM', '666'),
(191, 'Saint Vincent and Grenadines', '聖文森及格瑞那丁', 'VC', 'VCT', '670'),
(192, 'Samoa', '薩摩亞', 'WS', 'WSM', '882'),
(193, 'San Marino', '聖馬利諾', 'SM', 'SMR', '674'),
(194, 'Sao Tome and Principe', '聖多美普林西比', 'ST', 'STP', '678'),
(195, 'Saudi Arabia', '沙烏地阿拉伯', 'SA', 'SAU', '682'),
(196, 'Senegal', '塞內加爾', 'SN', 'SEN', '686'),
(197, 'Serbia', '塞爾維亞', 'RS', 'SRB', '688'),
(198, 'Seychelles', '塞席爾', 'SC', 'SYC', '690'),
(199, 'Sierra Leone', '獅子山共和國', 'SL', 'SLE', '694'),
(200, 'Singapore', '新加坡', 'SG', 'SGP', '702'),
(201, 'Slovakia', '斯洛伐克', 'SK', 'SVK', '703'),
(202, 'Slovenia', '斯洛維尼亞', 'SI', 'SVN', '705'),
(203, 'Solomon Islands', '索羅門群島', 'SB', 'SLB', '090'),
(204, 'Somalia', '索馬利亞', 'SO', 'SOM', '706'),
(205, 'South Africa', '南非', 'ZA', 'ZAF', '710'),
(206, 'South Georgia and the South Sandwich Islands', '南喬治亞與南桑威奇', 'GS', 'SGS', '239'),
(207, 'South Sudan', '南蘇丹', 'SS', 'SSD', '728'),
(208, 'Spain', '西班牙', 'ES', 'ESP', '724'),
(209, 'Sri Lanka', '斯里蘭卡', 'LK', 'LKA', '144'),
(210, 'Sudan', '蘇丹共和國', 'SD', 'SDN', '736'),
(211, 'Suriname', '蘇利南', 'SR', 'SUR', '740'),
(212, 'Svalbard and Jan Mayen Islands', '斯瓦巴和揚馬延', 'SJ', 'SJM', '744'),
(213, 'Swaziland', '史瓦濟蘭', 'SZ', 'SWZ', '748'),
(214, 'Sweden', '瑞典', 'SE', 'SWE', '752'),
(215, 'Switzerland', '瑞士', 'CH', 'CHE', '756'),
(216, 'Syrian Arab Republic (Syria)', '敘利亞', 'SY', 'SYR', '760'),
(217, 'Taiwan', '臺灣', 'TW', 'TWN', '158'),
(218, 'Tajikistan', '塔吉克', 'TJ', 'TJK', '762'),
(219, 'United Republic of Tanzania', '坦尚尼亞', 'TZ', 'TZA', '834'),
(220, 'Thailand', '泰國', 'TH', 'THA', '764'),
(221, 'Timor-Leste', '東帝汶', 'TL', 'TLS', '626'),
(222, 'Togo', '多哥', 'TG', 'TGO', '768'),
(223, 'Tokelau', '托克勞', 'TK', 'TKL', '772'),
(224, 'Tonga', '東加', 'TO', 'TON', '776'),
(225, 'Trinidad and Tobago', '千里達及托巴哥', 'TT', 'TTO', '780'),
(226, 'Tunisia', '突尼西亞', 'TN', 'TUN', '788'),
(227, 'Turkey', '土耳其', 'TR', 'TUR', '792'),
(228, 'Turkmenistan', '土庫曼斯坦', 'TM', 'TKM', '795'),
(229, 'Turks and Caicos Islands', '土克凱可群島', 'TC', 'TCA', '796'),
(230, 'Tuvalu', '吐瓦魯', 'TV', 'TUV', '798'),
(231, 'Uganda', '烏干達', 'UG', 'UGA', '800'),
(232, 'Ukraine', '烏克蘭', 'UA', 'UKR', '804'),
(233, 'United Arab Emirates', '阿拉伯聯合大公國', 'AE', 'ARE', '784'),
(234, 'United Kingdom', '英國', 'GB', 'GBR', '826'),
(235, 'United States of America', '美國', 'US', 'USA', '840'),
(236, 'United States Minor Outlying Islands', '美國本土外小島嶼', 'UM', 'UMI', '581'),
(237, 'Uruguay', '烏拉圭', 'UY', 'URY', '858'),
(238, 'Uzbekistan', '烏茲別克', 'UZ', 'UZB', '860'),
(239, 'Vanuatu', '萬那杜', 'VU', 'VUT', '548'),
(240, 'Bolivarian Republic of Venezuela', '委內瑞拉', 'VE', 'VEN', '862'),
(241, 'Viet Nam', '越南', 'VN', 'VNM', '704'),
(242, 'Virgin Islands, US', '美屬維京群島', 'VI', 'VIR', '850'),
(243, 'Wallis and Futuna Islands', '瓦利斯和富圖納', 'WF', 'WLF', '876'),
(244, 'Western Sahara', '西撒哈拉', 'EH', 'ESH', '732'),
(245, 'Yemen', '葉門', 'YE', 'YEM', '887'),
(246, 'Zambia', '尚比亞', 'ZM', 'ZMB', '894'),
(247, 'Zimbabwe', '辛巴威', 'ZW', 'ZWE', '716');

-- --------------------------------------------------------

--
-- 資料表結構 `currency`
--

CREATE TABLE `currency` (
  `currency_id` int(11) NOT NULL,
  `currency_time` datetime NOT NULL,
  `currency_name` char(3) NOT NULL,
  `currency_rate` decimal(11,8) NOT NULL,
  `currency_date` date NOT NULL COMMENT '此匯率是哪天的'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 資料表結構 `download_files`
--

CREATE TABLE `download_files` (
  `file_id` int(11) NOT NULL,
  `model_id` int(11) NOT NULL,
  `file_viewer` text NOT NULL,
  `file_file` varchar(50) NOT NULL,
  `file_file_en` varchar(50) NOT NULL,
  `file_title` varchar(50) NOT NULL,
  `file_title_en` varchar(50) NOT NULL,
  `file_price` int(11) NOT NULL,
  `file_online` tinyint(1) NOT NULL,
  `file_sort` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- --------------------------------------------------------

--
-- 資料表結構 `email_send_temp`
--

CREATE TABLE `email_send_temp` (
  `send_id` int(11) NOT NULL,
  `send_type` varchar(50) COLLATE utf8_unicode_ci NOT NULL COMMENT '1-驗證;2-訂單;3-聯絡我們',
  `send_key` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `send_data` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  `send_hold` tinyint(1) NOT NULL COMMENT '暫時不寄出，如:信用卡付款完成才會寄出'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- 資料表結構 `email_verify`
--

CREATE TABLE `email_verify` (
  `email_verify_id` int(10) UNSIGNED NOT NULL,
  `member_uniqid` varchar(50) DEFAULT NULL,
  `email_verify_created_on` datetime DEFAULT NULL,
  `email_verify_code_hash` varchar(40) DEFAULT NULL COMMENT 'sha1',
  `email_verify_registration` tinyint(1) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 資料表結構 `invoice`
--

CREATE TABLE `invoice` (
  `invoice_id` int(11) NOT NULL,
  `member_uniqid` varchar(50) NOT NULL,
  `invoice_status` tinyint(1) NOT NULL,
  `invoice_code` varchar(50) NOT NULL,
  `invoice_date` date NOT NULL,
  `invoice_file` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- --------------------------------------------------------

--
-- 資料表結構 `label`
--

CREATE TABLE `label` (
  `label_id` int(11) NOT NULL,
  `member_uniqid` varchar(50) NOT NULL,
  `invoice_id` int(11) NOT NULL,
  `model_id` int(11) NOT NULL,
  `label_seria_num` varchar(50) NOT NULL,
  `label_engine_seria_number` varchar(50) NOT NULL,
  `label_ex_factory_Yr` smallint(1) NOT NULL,
  `label_mode` varchar(5) NOT NULL,
  `label_NRMM_code` varchar(50) NOT NULL,
  `label_NRMM_label_code` varchar(50) NOT NULL,
  `label_NRMM_charges` int(11) NOT NULL,
  `label_QPME_label_code` text NOT NULL,
  `label_QPME_expires` date NOT NULL,
  `label_QPME_charges` int(11) NOT NULL,
  `label_NRMM_file` varchar(50) NOT NULL,
  `label_QPME_file` varchar(50) NOT NULL,
  `label_NRMM_photo` varchar(50) NOT NULL,
  `label_QPME_photo` varchar(50) NOT NULL,
  `editor` varchar(20) NOT NULL,
  `edit_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `creator` varchar(20) NOT NULL,
  `create_time` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 資料表結構 `label_info`
--

CREATE TABLE `label_info` (
  `label_info_id` int(11) NOT NULL,
  `label_info_title` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `label_info_title_en` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `works_type_id` int(11) NOT NULL,
  `label_info_url` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `label_info_content` text COLLATE utf8_unicode_ci NOT NULL,
  `label_info_content_en` text COLLATE utf8_unicode_ci NOT NULL,
  `label_info_photo` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `label_info_sort` int(11) NOT NULL,
  `label_info_online` tinyint(1) NOT NULL COMMENT '1-online;0-offline'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=COMPACT;

-- --------------------------------------------------------

--
-- 資料表結構 `login_log`
--

CREATE TABLE `login_log` (
  `log_id` int(11) NOT NULL,
  `l_ip` varchar(20) NOT NULL,
  `l_browser` varchar(200) NOT NULL,
  `l_username` varchar(100) NOT NULL,
  `l_success` tinyint(1) NOT NULL,
  `l_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- 資料表的匯出資料 `login_log`
--

INSERT INTO `login_log` (`log_id`, `l_ip`, `l_browser`, `l_username`, `l_success`, `l_date`) VALUES
(1, '114.33.29.66', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', 'yaii', 1, '2017-10-24 11:45:10'),
(2, '114.24.15.126', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', 'alex', 1, '2017-10-24 12:57:40'),
(3, '114.33.29.66', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', 'yaii', 1, '2017-10-24 13:03:29'),
(4, '114.24.15.126', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', 'alex', 1, '2017-10-24 16:19:43'),
(5, '1.171.154.16', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', 'alex', 1, '2017-10-25 03:32:50'),
(6, '1.171.154.16', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', 'alex', 1, '2017-10-25 05:42:14'),
(7, '114.33.29.66', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', 'alex', 1, '2017-10-25 06:41:43'),
(8, '114.33.29.66', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', 'alex', 1, '2017-10-25 08:05:28'),
(9, '1.171.154.16', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', 'alex', 1, '2017-10-25 08:23:15'),
(10, '114.33.29.66', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', 'alex', 1, '2017-10-25 09:15:39'),
(11, '1.171.154.16', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', 'alex', 1, '2017-10-25 10:20:33'),
(12, '114.33.29.66', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/62.0.3202.62 Safari/537.36', 'alex', 1, '2017-10-25 10:25:31'),
(13, '1.171.154.16', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', 'alex', 1, '2017-10-25 10:36:22'),
(14, '1.171.154.16', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', 'alex', 1, '2017-10-25 12:37:42'),
(15, '114.33.29.66', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/62.0.3202.62 Safari/537.36', 'alex', 1, '2017-10-26 02:44:14'),
(16, '1.171.153.3', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/62.0.3202.62 Safari/537.36', 'alex', 1, '2017-10-26 03:25:09'),
(17, '114.33.29.66', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/62.0.3202.62 Safari/537.36', 'yaii', 0, '2017-10-26 06:17:46'),
(18, '114.33.29.66', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/62.0.3202.62 Safari/537.36', 'alex', 1, '2017-10-26 06:17:51'),
(19, '1.171.153.3', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/62.0.3202.62 Safari/537.36', 'alex', 1, '2017-10-26 07:48:21'),
(20, '1.171.153.3', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/62.0.3202.62 Safari/537.36', 'alex', 1, '2017-10-26 08:35:47'),
(21, '1.171.153.3', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/62.0.3202.62 Safari/537.36', 'alex', 1, '2017-10-26 09:45:59'),
(22, '1.171.153.3', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/62.0.3202.62 Safari/537.36', 'alex', 1, '2017-10-26 11:57:24'),
(23, '61.224.75.51', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/62.0.3202.75 Safari/537.36', 'alex', 1, '2017-10-27 15:08:52'),
(24, '61.230.103.241', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/62.0.3202.75 Safari/537.36', 'alex', 1, '2017-10-29 09:04:20'),
(25, '61.230.103.241', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/62.0.3202.75 Safari/537.36', 'alex', 1, '2017-10-29 13:04:12'),
(26, '49.217.80.178', 'Mozilla/5.0 (iPhone; CPU iPhone OS 11_0_3 like Mac OS X) AppleWebKit/604.1.38 (KHTML, like Gecko) Version/11.0 Mobile/15A432 Safari/604.1', 'alex', 1, '2017-10-30 01:14:31'),
(27, '114.33.29.66', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/62.0.3202.75 Safari/537.36', 'alex', 1, '2017-10-30 01:26:41'),
(28, '36.225.24.155', 'Mozilla/5.0 (Linux; Android 5.0.2; SM-T710 Build/LRX22G) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.98 Safari/537.36', 'alex', 1, '2017-10-30 01:57:30'),
(29, '61.230.103.241', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/62.0.3202.75 Safari/537.36', 'alex', 1, '2017-10-30 10:52:51'),
(30, '1.171.52.233', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/62.0.3202.94 Safari/537.36', 'guest', 0, '2017-11-18 07:37:06'),
(31, '1.171.52.233', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/62.0.3202.94 Safari/537.36', 'guest', 0, '2017-11-18 07:37:15'),
(32, '1.171.52.233', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/62.0.3202.94 Safari/537.36', 'admin', 0, '2017-11-18 07:37:24'),
(33, '1.171.52.233', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/62.0.3202.94 Safari/537.36', '000_Peter', 0, '2017-11-18 07:37:53'),
(34, '1.171.52.233', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/62.0.3202.94 Safari/537.36', '000_Peter', 0, '2017-11-18 07:38:01'),
(35, '1.171.52.233', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/62.0.3202.94 Safari/537.36', 'alex', 1, '2017-11-18 09:50:50'),
(36, '1.171.52.233', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/62.0.3202.94 Safari/537.36', 'peter', 1, '2017-11-18 12:01:32'),
(37, '59.115.25.3', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.84 Safari/537.36', 'alex', 1, '2017-12-08 07:12:17'),
(38, '59.115.25.3', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.84 Safari/537.36', 'alex', 1, '2017-12-08 08:54:49'),
(39, '114.33.29.66', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/62.0.3202.94 Safari/537.36', 'yaii', 1, '2017-12-08 10:38:18'),
(40, '114.33.29.66', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/62.0.3202.94 Safari/537.36', 'yaii', 1, '2017-12-08 10:38:19'),
(41, '59.115.25.3', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.84 Safari/537.36', 'alex', 1, '2017-12-08 14:55:13'),
(42, '59.115.25.3', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.84 Safari/537.36', 'alex', 1, '2017-12-08 16:30:15'),
(43, '114.33.29.66', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36', 'guest', 0, '2018-01-16 02:26:54'),
(44, '114.33.29.66', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36', 'alex', 1, '2018-01-16 02:27:05'),
(45, '223.139.155.20', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36', 'alex', 1, '2018-01-18 01:14:02'),
(46, '122.117.152.58', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36', 'alex', 1, '2018-01-21 06:59:24'),
(47, '42.77.89.166', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.162 Safari/537.36', 'alex', 1, '2018-03-19 08:38:46'),
(48, 'localhost', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'admin', 0, '2018-04-10 05:50:56'),
(49, 'localhost', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'alex', 1, '2018-04-10 05:51:24'),
(50, 'localhost', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.181 Safari/537.36', 'alex', 1, '2018-06-04 09:38:38'),
(51, 'localhost', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.77 Safari/537.36', 'peter', 0, '2018-11-05 06:29:32'),
(52, 'localhost', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.77 Safari/537.36', 'admin', 0, '2018-11-05 06:29:42'),
(53, 'localhost', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.77 Safari/537.36', 'alex', 1, '2018-11-05 06:30:20');

-- --------------------------------------------------------

--
-- 資料表結構 `member`
--

CREATE TABLE `member` (
  `member_id` int(11) NOT NULL,
  `member_uniqid` varchar(50) NOT NULL,
  `member_calling_code` int(3) NOT NULL,
  `member_account` varchar(100) NOT NULL,
  `member_type` tinyint(1) NOT NULL,
  `member_password` varchar(300) NOT NULL,
  `member_name` varchar(30) NOT NULL,
  `member_nickname` varchar(50) NOT NULL,
  `member_direct_line` varchar(30) NOT NULL,
  `member_mobile1` varchar(30) NOT NULL,
  `member_mobile2` varchar(30) NOT NULL,
  `member_mobile_show` varchar(20) NOT NULL,
  `member_email` varchar(100) NOT NULL,
  `member_position` varchar(20) NOT NULL,
  `member_lineID` varchar(50) NOT NULL,
  `member_skypeID` varchar(50) NOT NULL,
  `member_fb_id` varchar(30) NOT NULL,
  `member_sales` int(11) NOT NULL,
  `member_company_name` varchar(100) NOT NULL,
  `member_company_address` varchar(500) NOT NULL,
  `member_company_tel` varchar(30) NOT NULL,
  `member_company_fax` varchar(30) NOT NULL,
  `member_company_email` varchar(50) NOT NULL,
  `member_company_website` varchar(600) NOT NULL,
  `member_have_business_before` tinyint(1) NOT NULL,
  `member_shop` tinyint(1) NOT NULL,
  `member_online` tinyint(1) NOT NULL,
  `member_lastlogin` datetime NOT NULL,
  `member_register` datetime NOT NULL,
  `member_paid` tinyint(1) NOT NULL,
  `member_integral` int(11) NOT NULL COMMENT '會員積分點數',
  `member_wishlist` text NOT NULL,
  `member_get_full_info_point` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 資料表結構 `model`
--

CREATE TABLE `model` (
  `model_id` int(11) NOT NULL,
  `model_number` varchar(20) NOT NULL,
  `model_version` varchar(10) NOT NULL,
  `model_brand` int(11) NOT NULL,
  `model_weight` int(11) NOT NULL,
  `model_cate` int(11) NOT NULL,
  `model_datasheet` varchar(50) NOT NULL,
  `model_online` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- 資料表的匯出資料 `model`
--

INSERT INTO `model` (`model_id`, `model_number`, `model_version`, `model_brand`, `model_weight`, `model_cate`, `model_datasheet`, `model_online`) VALUES
(16, 'B20', '1', 1, 2, 19, '', 1),
(17, 'B30', '1', 1, 3, 19, '', 1),
(18, 'B30 (SSL)', '1', 1, 3, 19, '', 1),
(19, 'B40', '1', 1, 4, 19, '', 1),
(20, 'B50', '1', 1, 5, 19, '', 1),
(21, 'B60', '1', 1, 6, 19, '', 1),
(22, 'B70', '1', 1, 7, 19, '', 1),
(23, 'B90', '1', 1, 9, 19, '', 1),
(24, 'B140', '1', 1, 14, 19, '', 1),
(25, 'B180', '1', 1, 18, 19, '', 1),
(26, 'B210', '1', 1, 21, 19, '', 1),
(27, 'B230', '1', 1, 23, 19, '', 1),
(28, 'B250', '1', 1, 25, 19, '', 1),
(29, 'B300', '1', 1, 30, 19, '', 1),
(30, 'B360', '1', 1, 36, 19, '', 1),
(31, 'B450', '1', 1, 45, 19, '', 1),
(32, 'B650', '1', 1, 65, 19, '', 1),
(33, 'B800', '1', 1, 80, 19, '', 1),
(34, 'B1000', '1', 1, 100, 19, '', 1),
(35, 'B06', '1', 1, 1, 19, '', 1),
(36, 'B10', '1', 1, 1, 19, '', 1),
(37, 'G140', '1', 1, 14, 19, '', 1),
(38, 'G210', '1', 1, 21, 19, '', 1),
(39, 'G300', '1', 1, 30, 19, '', 1),
(40, 'G50', '1', 1, 5, 19, '', 1),
(41, 'DMC10', '1', 1, 0, 17, '', 1),
(42, 'DMC140', '1', 1, 0, 17, '', 1),
(43, 'DMC140B', '1', 1, 0, 17, '', 1),
(44, 'DMC180', '1', 1, 0, 17, '', 1),
(45, 'DMC180B', '1', 1, 0, 17, '', 1),
(46, 'DMC210', '1', 1, 0, 17, '', 1),
(47, 'DMC210B', '1', 1, 0, 17, '', 1),
(48, 'DMC230R', '1', 1, 0, 17, '', 1),
(49, 'DMC230RB', '1', 1, 0, 17, '', 1),
(50, 'DMC30', '1', 1, 0, 17, '', 1),
(51, 'DMC300', '1', 1, 0, 17, '', 1),
(52, 'DMC300B', '1', 1, 0, 17, '', 1),
(53, 'DMC330R', '1', 1, 0, 17, '', 1),
(54, 'DMC330RB', '1', 1, 0, 17, '', 1),
(55, 'DMC50', '1', 1, 0, 17, '', 1),
(56, 'DMP230CMS', '1', 1, 0, 17, 'modvAWv59f5d72bbf326.pdf', 1),
(57, 'DMP230SM', '1', 1, 0, 17, '', 1),
(58, 'DMP250Q - CRUSHER', '1', 1, 0, 17, '', 1),
(59, 'DMP250Q - DEMOLITION', '1', 1, 0, 17, '', 1),
(60, 'DMP250Q - PULVERLIZE', '1', 1, 0, 17, '', 1),
(61, 'DMP250Q - SHEAR', '1', 1, 0, 17, '', 1),
(62, 'DMP330CMS', '1', 1, 0, 17, '', 1),
(63, 'DMP330SM', '1', 1, 0, 17, '', 1),
(64, 'DMS250', '1', 1, 0, 17, '', 1),
(65, 'DMS300', '1', 1, 0, 17, '', 1),
(66, 'DMS330', '1', 1, 0, 17, '', 1),
(67, 'XR20', '1', 2, 20, 32, 'modF4pz59f5d5292515f.pdf', 1),
(68, 'XC20', '1', 2, 20, 32, 'modYSqC59f5d3701c25f.pdf', 1);

-- --------------------------------------------------------

--
-- 資料表結構 `model_cate`
--

CREATE TABLE `model_cate` (
  `cate_id` int(11) NOT NULL,
  `cate_title` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `cate_title_en` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `cate_code` varchar(5) COLLATE utf8_unicode_ci NOT NULL,
  `cate_info` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `cate_photo` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `cate_par_id` int(11) NOT NULL,
  `cate_home` tinyint(1) NOT NULL,
  `cate_sort` int(11) NOT NULL,
  `cate_online` tinyint(1) NOT NULL COMMENT '1-online;0-offline',
  `cate_app` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=COMPACT;

--
-- 資料表的匯出資料 `model_cate`
--

INSERT INTO `model_cate` (`cate_id`, `cate_title`, `cate_title_en`, `cate_code`, `cate_info`, `cate_photo`, `cate_par_id`, `cate_home`, `cate_sort`, `cate_online`, `cate_app`) VALUES
(1, '掘挖機', 'All Excavator', 'EA', '', 'modT1r058d397ce2cde6.png', 0, 1, 1, 1, 1),
(2, '起重機', 'Crane', 'CA', '', 'modNC2t58d39919b1a78.png', 0, 1, 0, 1, 1),
(3, '鑽井機', 'Drilling-rig', 'DA', '', '', 0, 0, 4, 1, 0),
(4, '空氣壓縮機', 'Air-compressor', 'AC', '', '', 3, 0, 0, 1, 0),
(5, '發電機', 'Generator', 'GE', '', 'proOh3S58a15545e117f.png', 0, 1, 5, 1, 1),
(6, '裝載機', 'Loader', 'LA', '', 'modiucF58d3993682e31.png', 0, 1, 6, 1, 1),
(7, '翻斗車', 'Dump-trucks', 'DA', '', 'proCt1n58a155e7187fa.png', 0, 1, 7, 1, 1),
(8, '叉車', 'Forklift', 'FA', '', 'modvyFr58d3996b79b17.png', 0, 1, 8, 1, 1),
(9, '升降台', 'Lifting-platform', 'PA', '', 'mod19Pk58d39a28c5730.png', 0, 1, 9, 1, 1),
(10, '道路工程機', 'Road-works-machine', 'WA', '', 'mod4PO258d3998e8b763.png', 0, 1, 10, 1, 1),
(11, '壓路機', 'Rollers', 'RA', '', 'mod7KA858d3999d70ef7.png', 0, 1, 11, 1, 1),
(12, '零件', 'Spare-parts', 'SA', '', 'modyy3p58d3a3b1e9011.png', 0, 1, 12, 1, 1),
(13, '其他', 'Other', 'OA', '', '', 0, 0, 13, 1, 0),
(14, '履帶式挖掘機', 'Crawler Excavators', 'EC', '', '', 1, 0, 0, 1, 0),
(15, '輪式挖掘機', 'Wheeled Excavators', 'EW', '', '', 1, 0, 1, 1, 0),
(16, '特種掘挖機', 'Special Excavators', 'ES', '', '', 1, 0, 2, 1, 0),
(17, '拆卸工具', 'Demolition tools', 'HD', '', '', 32, 0, 3, 1, 0),
(18, '其他掘挖機', 'Other Excavator', 'EO', '', '', 1, 0, 4, 1, 0),
(19, '破碎錘', 'Hydraulic Breaker', 'HB', '', '', 32, 0, 5, 1, 0),
(20, '輪式起重機', 'Rough Terrain Cranes', 'CR', '', '', 2, 0, 0, 1, 0),
(21, '履帶式起重機', 'Tracked Cranes', 'CT', '', '', 2, 0, 1, 1, 0),
(22, '小型起重機', 'Mini cranes', 'CM', '', '', 2, 0, 2, 1, 0),
(23, '其他起重機', 'Other Cranes', 'CO', '', '', 2, 0, 3, 1, 0),
(24, '滑移裝載機', 'Skid Steer Loaders', 'LS', '', '', 6, 0, 0, 1, 0),
(25, '輪式裝載機', 'Wheel Loaders', 'LW', '', '', 6, 0, 1, 1, 0),
(26, '推土機', 'Crawler Loaders', 'LC', '', '', 6, 0, 2, 1, 0),
(27, '其他裝載機', 'other Loaders', 'LO', '', '', 6, 0, 3, 1, 0),
(28, '曲臂式升降台', 'Articulated boom lifts', 'PA', '', '', 9, 0, 0, 1, 0),
(29, '直臂式升降台', 'Telescopic boom lifts', 'PT', '', '', 9, 0, 1, 1, 0),
(30, '剪叉式升降台', 'Scissor Lifts', 'PS', '', '', 9, 0, 2, 1, 0),
(31, '其他升降台', 'Other lifts and platforms', 'PO', '', '', 9, 0, 3, 1, 0),
(32, '液壓工具', 'Hydradulic Attachment', 'HA', '', 'mod5wbq58f73b0e8148c.png', 0, 0, 2, 1, 1),
(33, '電動工具', 'Electrical Tools', 'MA', '', 'modiTGF58f73a4a2a07a.png', 0, 0, 3, 1, 1);

-- --------------------------------------------------------

--
-- 資料表結構 `news`
--

CREATE TABLE `news` (
  `news_id` int(11) NOT NULL,
  `news_title` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `news_title_en` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `news_cate` int(11) NOT NULL,
  `brand_id` int(11) NOT NULL,
  `news_date` datetime NOT NULL,
  `news_date_end` datetime NOT NULL,
  `news_content` text COLLATE utf8_unicode_ci NOT NULL,
  `news_content_en` text COLLATE utf8_unicode_ci NOT NULL,
  `news_photo` varchar(50) COLLATE utf8_unicode_ci NOT NULL COMMENT '圖片',
  `news_online` tinyint(1) NOT NULL COMMENT '1-online;0-offline',
  `news_sort` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- 資料表結構 `news_cate`
--

CREATE TABLE `news_cate` (
  `cate_id` int(11) NOT NULL,
  `cate_title` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `cate_title_en` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `cate_photo` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `cate_par_id` int(11) NOT NULL,
  `cate_sort` int(11) NOT NULL,
  `cate_online` tinyint(1) NOT NULL COMMENT '1-online;0-offline'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- 資料表的匯出資料 `news_cate`
--

INSERT INTO `news_cate` (`cate_id`, `cate_title`, `cate_title_en`, `cate_photo`, `cate_par_id`, `cate_sort`, `cate_online`) VALUES
(1, '公司', 'Company', '', 0, 0, 1),
(2, '業界', 'Industry', '', 0, 1, 1),
(3, '產品', 'Product', '', 0, 2, 1);

-- --------------------------------------------------------

--
-- 資料表結構 `number_control`
--

CREATE TABLE `number_control` (
  `number_id` int(11) NOT NULL,
  `number_type` varchar(1) COLLATE utf8_unicode_ci NOT NULL,
  `number_cout` int(11) DEFAULT '0',
  `number_key` varchar(20) COLLATE utf8_unicode_ci NOT NULL COMMENT '110225'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- 資料表結構 `product`
--

CREATE TABLE `product` (
  `product_id` int(11) NOT NULL,
  `product_name` varchar(100) NOT NULL,
  `product_name_en` varchar(100) NOT NULL,
  `product_model` int(11) NOT NULL,
  `product_code` varchar(20) NOT NULL,
  `product_ver_code` varchar(1) NOT NULL,
  `product_owner` varchar(50) NOT NULL,
  `product_store` int(11) NOT NULL,
  `product_serial` varchar(50) NOT NULL,
  `product_engine_serial` varchar(50) NOT NULL,
  `product_year` int(5) NOT NULL,
  `product_hours` int(11) NOT NULL,
  `product_cate` int(11) NOT NULL,
  `product_price` int(11) NOT NULL,
  `product_inventory` int(11) UNSIGNED NOT NULL,
  `product_photo` varchar(50) NOT NULL,
  `product_epd_nrmm_label` text NOT NULL,
  `product_country` varchar(3) NOT NULL,
  `product_report` varchar(50) NOT NULL,
  `product_datasheet` varchar(50) NOT NULL,
  `product_remark` text NOT NULL,
  `product_remark_en` text NOT NULL,
  `product_currency` varchar(3) NOT NULL,
  `product_start_price` int(11) UNSIGNED NOT NULL,
  `product_default_price` int(11) UNSIGNED NOT NULL,
  `product_paid` tinyint(1) NOT NULL,
  `product_featured` tinyint(1) NOT NULL,
  `product_new` tinyint(1) NOT NULL,
  `product_online` tinyint(1) NOT NULL,
  `product_sort` int(11) NOT NULL,
  `product_create_year` int(11) NOT NULL COMMENT '商品創建年份',
  `product_callus` tinyint(1) NOT NULL,
  `product_create_date` date NOT NULL,
  `product_expiry_date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 資料表結構 `product_album`
--

CREATE TABLE `product_album` (
  `album_id` int(11) NOT NULL,
  `host_id` int(11) NOT NULL,
  `album_photo` varchar(50) NOT NULL,
  `album_sort` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 資料表結構 `product_price_currency`
--

CREATE TABLE `product_price_currency` (
  `product_price_currency_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `product_currency` char(3) NOT NULL,
  `product_default_price` int(11) NOT NULL,
  `product_start_price` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 資料表結構 `product_selling_price`
--

CREATE TABLE `product_selling_price` (
  `selling_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `selling_price` int(11) UNSIGNED NOT NULL,
  `selling_target` text NOT NULL,
  `selling_online` tinyint(1) NOT NULL,
  `selling_sort` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 資料表結構 `product_selling_price_currency`
--

CREATE TABLE `product_selling_price_currency` (
  `product_price_currency_id` int(11) NOT NULL,
  `selling_id` int(11) NOT NULL,
  `product_currency` char(3) NOT NULL,
  `selling_price` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 資料表結構 `rights`
--

CREATE TABLE `rights` (
  `rights_id` int(11) NOT NULL,
  `system_code` varchar(50) NOT NULL,
  `crud_act` varchar(10) NOT NULL,
  `user_id` int(10) NOT NULL,
  `right_add` tinyint(1) NOT NULL,
  `right_del` tinyint(1) NOT NULL,
  `right_edit` tinyint(1) NOT NULL,
  `right_view` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

--
-- 資料表的匯出資料 `rights`
--

INSERT INTO `rights` (`rights_id`, `system_code`, `crud_act`, `user_id`, `right_add`, `right_del`, `right_edit`, `right_view`) VALUES
(209, 'user', '', 2, 1, 1, 1, 1),
(210, 'account', '', 2, 1, 1, 1, 1),
(211, 'slider', '', 2, 1, 1, 1, 1),
(212, 'contact', '', 2, 1, 1, 1, 1),
(213, 'label_info', '', 2, 1, 1, 1, 1),
(214, 'brand', '', 2, 1, 1, 1, 1),
(215, 'brand_file', '', 2, 1, 1, 1, 1),
(216, 'news', '', 2, 1, 1, 1, 1),
(217, 'product', '', 2, 1, 1, 1, 1),
(218, 'model', '', 2, 1, 1, 1, 1),
(219, 'download_files', '', 2, 1, 1, 1, 1),
(220, 'invoice', '', 2, 1, 1, 1, 1),
(221, 'label', '', 2, 1, 1, 1, 1),
(222, 'member', '', 2, 1, 1, 1, 1),
(223, 'case', '', 2, 1, 1, 1, 1),
(224, 'works_type', '', 2, 1, 1, 1, 1),
(225, 'about', '', 2, 1, 1, 1, 1),
(226, 'user', '', 1, 1, 1, 1, 1),
(227, 'account', '', 1, 1, 1, 1, 1),
(228, 'slider', '', 1, 1, 1, 1, 1),
(229, 'contact', '', 1, 1, 1, 1, 1),
(230, 'label_info', '', 1, 1, 1, 1, 1),
(231, 'brand', '', 1, 1, 1, 1, 1),
(232, 'brand_file', '', 1, 1, 1, 1, 1),
(233, 'news', '', 1, 1, 1, 1, 1),
(234, 'product', '', 1, 1, 1, 1, 1),
(235, 'model', '', 1, 1, 1, 1, 1),
(236, 'download_files', '', 1, 1, 1, 1, 1),
(237, 'invoice', '', 1, 1, 1, 1, 1),
(238, 'label', '', 1, 1, 1, 1, 1),
(239, 'member', '', 1, 1, 1, 1, 1),
(240, 'case', '', 1, 1, 1, 1, 1),
(241, 'works_type', '', 1, 1, 1, 1, 1),
(242, 'about', '', 1, 1, 1, 1, 1),
(243, 'user', '', 3, 1, 1, 1, 1),
(244, 'account', '', 3, 1, 1, 1, 1),
(245, 'slider', '', 3, 1, 1, 1, 1),
(246, 'contact', '', 3, 1, 1, 1, 1),
(247, 'label_info', '', 3, 1, 1, 1, 1),
(248, 'brand', '', 3, 1, 1, 1, 1),
(249, 'brand_file', '', 3, 1, 1, 1, 1),
(250, 'news', '', 3, 1, 1, 1, 1),
(251, 'product', '', 3, 1, 1, 1, 1),
(252, 'model', '', 3, 1, 1, 1, 1),
(253, 'download_files', '', 3, 1, 1, 1, 1),
(254, 'invoice', '', 3, 1, 1, 1, 1),
(255, 'label', '', 3, 1, 1, 1, 1),
(256, 'member', '', 3, 1, 1, 1, 1),
(257, 'case', '', 3, 1, 1, 1, 1),
(258, 'works_type', '', 3, 1, 1, 1, 1),
(259, 'about', '', 3, 1, 1, 1, 1);

-- --------------------------------------------------------

--
-- 資料表結構 `slider`
--

CREATE TABLE `slider` (
  `id` int(11) NOT NULL,
  `photoname` varchar(50) COLLATE utf8_unicode_ci NOT NULL COMMENT 'banner圖片',
  `slider_link` text COLLATE utf8_unicode_ci NOT NULL,
  `slider_online` tinyint(1) NOT NULL COMMENT '1-online;0-offline',
  `slider_sort` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=COMPACT;

--
-- 資料表的匯出資料 `slider`
--

INSERT INTO `slider` (`id`, `photoname`, `slider_link`, `slider_online`, `slider_sort`) VALUES
(13, '', '', 1, 0);

-- --------------------------------------------------------

--
-- 資料表結構 `system_name`
--

CREATE TABLE `system_name` (
  `system_name_id` int(11) NOT NULL,
  `system_unit` varchar(50) NOT NULL,
  `system_code` varchar(50) NOT NULL,
  `system_path` varchar(50) NOT NULL,
  `system_file` varchar(50) NOT NULL,
  `system_table` varchar(50) NOT NULL,
  `system_name` varchar(100) NOT NULL,
  `system_sort` int(11) NOT NULL,
  `system_module` tinyint(1) NOT NULL DEFAULT '1',
  `system_switch` tinyint(1) NOT NULL,
  `system_json` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- 資料表的匯出資料 `system_name`
--

INSERT INTO `system_name` (`system_name_id`, `system_unit`, `system_code`, `system_path`, `system_file`, `system_table`, `system_name`, `system_sort`, `system_module`, `system_switch`, `system_json`) VALUES
(1, 'admin', 'user', '', '', '', '使用者管理', 0, 1, 1, '{\"button\":{\"add\":true,\"del\":true},\"set\":{\"plugins\":{\"form\":true,\"edit\":true,\"control\":true},\"del\":{\"related_data\":[{\"related_table\":\"rights\",\"related_column\":\"user_id\",\"column\":\"u_id\"}]}},\"list\":[{\"column\":\"username\",\"type\":\"text\",\"title\":\"管理員帳戶\",\"issort\":\"a\",\"sort\":true},{\"column\":\"user_online\",\"type\":\"switch\",\"title\":\"狀態\",\"class\":\"col-smedium center\",\"sort\":true},{\"column\":\"course_date\",\"type\":\"edit\",\"title\":\"操作\",\"class\":\"col-lmedium center\"}],\"form\":[{\"column\":\"username\",\"type\":\"text\",\"title\":\"管理員帳戶\",\"size\":5,\"required\":true,\"unique\":true},{\"column\":\"password\",\"type\":\"password\",\"title\":\"密碼\",\"size\":5,\"required\":true,\"info\":\"如您無須修改密碼則無需填寫\"},{\"column\":\"user_online\",\"title\":\"帳戶狀態\",\"type\":\"radio\",\"data\":[{\"title\":\"開啟\",\"value\":\"1\",\"checked\":true},{\"title\":\"關閉\",\"value\":\"0\"}],\"size\":4},{\"column\":\"alias\",\"type\":\"text\",\"title\":\"管理員姓名\",\"size\":5,\"required\":true},{\"type\":\"plugins\",\"_type\":\"right\",\"title\":\"權限設定\",\"size\":8,\"required\":true}]}'),
(2, 'admin', 'account', 'module', 'module_single', 'user', '密碼修改', 0, 0, 1, '{\"set\":{\"single_account\":true},\"form\":[{\"column\":\"username\",\"type\":\"txt\",\"title\":\"管理員帳戶\",\"size\":5},{\"column\":\"password\",\"type\":\"password\",\"title\":\"密碼\",\"size\":5,\"required\":true,\"info\":\"如您無須修改密碼則無需填寫\"}]}'),
(3, 'slider', 'slider', '', '', '', 'Slider管理', 0, 1, 1, '{\"button\":{\"add\":true,\"del\":true,\"sort\":true},\"set\":{\"sort\":{\"photo\":true,\"mode\":\"ASC\"}},\"list\":[{\"column\":\"slider_photo\",\"type\":\"photo\",\"title\":\"Slider圖片\"},{\"column\":\"slider_online\",\"type\":\"switch\",\"title\":\"上架\",\"class\":\"col-smedium center\",\"sort\":true},{\"type\":\"edit\",\"title\":\"操作\",\"class\":\"col-lmedium center\"}],\"form\":[{\"column\":\"slider_online\",\"title\":\"上架狀態\",\"type\":\"radio\",\"data\":[{\"title\":\"上架\",\"value\":\"1\",\"checked\":true},{\"title\":\"下架\",\"value\":\"0\"}],\"size\":4},{\"column\":\"slider_photo\",\"type\":\"photo\",\"title\":\"Slider圖片\",\"size\":5,\"required\":true,\"thumbnail\":\"b,1170,500\",\"need_size\":[1170,500]}]}'),
(4, 'contact', 'contact', '', '', '', '聯絡我們', 0, 1, 1, '{\"button\":{\"del\":true,\"search\":true},\"set\":{\"join\":[{\"table\":\"country_code\",\"primary\":\"country_letter2_code\",\"on_primary\":\"contact_country\"}],\"search\":[\"contact_name\",\"contact_email\",\"contact_phone\"],\"del\":{\"file\":{\"column\":{}}},\"sort\":{\"mode\":\"DESC\"}},\"list\":[{\"column\":\"contact_name\",\"type\":\"text\",\"title\":\"姓名\",\"sort\":true},{\"column\":\"contact_email\",\"type\":\"text\",\"title\":\"信箱\",\"hide\":true,\"sort\":true},{\"column\":\"contact_phone\",\"type\":\"text\",\"title\":\"連絡電話\",\"hide\":true,\"sort\":true},{\"column\":\"contact_company\",\"type\":\"text\",\"title\":\"公司\",\"hide\":true,\"sort\":true},{\"column\":\"country_name_ch\",\"type\":\"text\",\"title\":\"國家\",\"hide\":true,\"sort\":true},{\"column\":\"contact_time\",\"type\":\"text\",\"title\":\"填寫時間\",\"issort\":\"d\",\"sort\":true},{\"type\":\"edit\",\"title\":\"操作\",\"class\":\"col-lmedium center\"},{\"column\":\"contact_message\",\"type\":\"text\",\"title\":\"內容\",\"hide\":\"all\"}],\"form\":[{\"column\":\"contact_time\",\"type\":\"txt\",\"title\":\"填寫時間\",\"size\":5},{\"column\":\"contact_name\",\"type\":\"txt\",\"title\":\"姓名\",\"size\":5},{\"column\":\"contact_phone\",\"type\":\"txt\",\"title\":\"連絡電話\",\"size\":5},{\"column\":\"contact_email\",\"type\":\"txt\",\"title\":\"電子信箱\",\"size\":5},{\"column\":\"contact_company\",\"type\":\"txt\",\"title\":\"公司\",\"size\":5},{\"column\":\"country_name_ch\",\"type\":\"txt\",\"title\":\"國家\",\"size\":5},{\"column\":\"contact_message\",\"type\":\"txt\",\"title\":\"內容\",\"size\":5}]}'),
(5, 'case', 'case', '', '', '', '成功案例', 1, 1, 1, '{\"button\":{\"add\":true,\"del\":true,\"filter_label\":true,\"search\":true},\"set\":{\"search\":[\"case_title\"],\"join\":[{\"table\":\"works_type\",\"primary\":\"works_type_id\",\"on_primary\":\"works_type_id\"}],\"del\":{\"file\":{\"column\":{}},\"related_data\":[{\"related_table\":\"case_album\",\"related_column\":\"host_id\",\"column\":\"case_id\"}],\"check\":[]},\"sort\":{\"mode\":\"ASC\"},\"plugins\":{\"button\":true}},\"list\":[{\"column\":\"case_title\",\"type\":\"text\",\"title\":\"名稱\",\"sort\":true},{\"column\":\"works_type_title\",\"type\":\"text\",\"title\":\"分類\",\"class\":\"col-medium center\",\"sort\":true,\"hide\":\"phone\"},{\"column\":\"case_left_menu\",\"type\":\"switch\",\"title\":\"側欄\",\"class\":\"col-smedium center\",\"sort\":true},{\"column\":\"case_online\",\"type\":\"switch\",\"title\":\"上架\",\"class\":\"col-smedium center\",\"sort\":true},{\"type\":\"edit\",\"title\":\"操作\",\"class\":\"col-lmedium center\"}],\"form\":[{\"column\":\"case_title\",\"type\":\"text\",\"title\":\"名稱\",\"size\":5,\"required\":true,\"language\":true},{\"column\":\"case_online\",\"type\":\"switch\",\"title\":\"上架\",\"size\":5,\"checked\":true},{\"column\":\"case_left_menu\",\"type\":\"switch\",\"title\":\"側欄\",\"size\":5},{\"column\":\"works_type_id\",\"type\":\"select\",\"data\":{\"type\":\"db\",\"table\":\"works_type\",\"value\":\"works_type_id\",\"title\":\"works_type_title\",\"search\":true},\"title\":\"工作類型\",\"size\":5,\"required\":true},{\"column\":\"case_content\",\"type\":\"wysiwyg\",\"title\":\"敘述\",\"size\":5,\"rows\":6,\"required\":true,\"language\":true},{\"column\":\"case_photo\",\"type\":\"photo\",\"title\":\"列表圖片\",\"size\":5,\"thumbnail\":\"b,747,621\",\"need_size\":[747,621],\"required\":true},{\"column\":\"case_album\",\"type\":\"photo\",\"title\":\"相簿\",\"multiple\":true,\"size\":6,\"required\":true,\"limit\":5,\"table\":\"case_album\",\"thumbnail\":\"b,115,90,a|b,945,590\",\"need_size\":[945,590]}]}'),
(6, 'label_info', 'label_info', '', '', '', '標籤類型', 0, 1, 1, '{\"button\":{\"add\":true,\"del\":true,\"search\":true},\"set\":{\"search\":[\"label_info_title\"],\"join\":[{\"table\":\"works_type\",\"primary\":\"works_type_id\",\"on_primary\":\"works_type_id\"}],\"del\":{\"file\":{\"column\":{}},\"related_data\":[],\"check\":[]},\"sort\":{\"mode\":\"ASC\"}},\"list\":[{\"column\":\"label_info_title\",\"type\":\"text\",\"title\":\"名稱\",\"sort\":true},{\"column\":\"label_info_online\",\"type\":\"switch\",\"title\":\"上架\",\"class\":\"col-smedium center\",\"sort\":true},{\"type\":\"edit\",\"title\":\"操作\",\"class\":\"col-lmedium center\"}],\"form\":[{\"column\":\"label_info_title\",\"type\":\"text\",\"title\":\"標籤名稱\",\"size\":5,\"required\":true,\"language\":true},{\"column\":\"label_info_online\",\"type\":\"switch\",\"title\":\"上架\",\"size\":5,\"required\":true},{\"column\":\"label_info_url\",\"type\":\"text\",\"title\":\"網址參數\",\"size\":5,\"unique\":true,\"required\":true},{\"column\":\"works_type_id\",\"type\":\"select\",\"data\":{\"type\":\"db\",\"table\":\"works_type\",\"value\":\"works_type_id\",\"title\":\"works_type_title\",\"search\":true},\"title\":\"成功案例連結\",\"size\":5,\"required\":true},{\"column\":\"label_info_content\",\"type\":\"wysiwyg\",\"title\":\"說明內容\",\"size\":6,\"required\":true,\"language\":true}]}'),
(9, 'brand', 'brand', '', '', '', '品牌管理', 0, 1, 1, '{\"button\":{\"add\":true,\"del\":true,\"search\":true},\"set\":{\"search\":[\"brand_title\"],\"del\":{\"file\":{\"column\":{}},\"related_data\":[{\"related_table\":\"brand_album\",\"related_column\":\"host_id\",\"column\":\"brand_id\"},{\"related_table\":\"brand_file\",\"related_column\":\"host_id\",\"column\":\"brand_id\"},{\"related_table\":\"brand_video\",\"related_column\":\"host_id\",\"column\":\"brand_id\"}],\"check\":[{\"table\":\"model\",\"column\":\"model_brand\",\"column_name\":\"brand_id\",\"msg\":\"上有機械型號使用此品牌，無法刪除\"}]},\"sort\":{\"mode\":\"ASC\"},\"plugins\":{\"form\":true,\"edit\":true,\"control\":true}},\"list\":[{\"column\":\"brand_title\",\"type\":\"text\",\"title\":\"名稱\",\"sort\":true},{\"column\":\"brand_url\",\"type\":\"text\",\"title\":\"網址參數\",\"hide\":true,\"sort\":true},{\"column\":\"brand_online\",\"type\":\"switch\",\"title\":\"上架\",\"class\":\"col-smedium center\",\"sort\":true},{\"type\":\"edit\",\"title\":\"操作\",\"class\":\"col-lmedium center\"}],\"form\":[{\"column\":\"brand_title\",\"type\":\"text\",\"title\":\"品牌名稱\",\"size\":5,\"required\":true,\"language\":true},{\"column\":\"brand_online\",\"type\":\"switch\",\"title\":\"上架\",\"size\":5,\"checked\":true},{\"column\":\"brand_url\",\"type\":\"text\",\"title\":\"網址參數\",\"size\":5,\"unique\":true,\"required\":true},{\"column\":\"brand_website\",\"type\":\"link\",\"title\":\"廠家網站\",\"size\":5},{\"column\":\"brand_view_cate\",\"type\":\"plugins\",\"_type\":\"view_cate\",\"title\":\"顯示分類\",\"size\":7},{\"column\":\"brand_info\",\"type\":\"textarea\",\"title\":\"說明內容\",\"rows\":8,\"size\":6,\"required\":true,\"language\":true},{\"column\":\"brand_logo\",\"type\":\"photo\",\"title\":\"LOGO\",\"size\":6,\"required\":true,\"thumbnail\":\"b,20,20\",\"need_size\":[20,20]},{\"column\":\"brand_album\",\"type\":\"photo\",\"title\":\"Slider\",\"multiple\":true,\"size\":6,\"required\":true,\"limit\":20,\"table\":\"brand_album\",\"thumbnail\":\"b,115,90,a|b,1920,1080\",\"need_size\":[1920,1080]},{\"type\":\"plugins\",\"_type\":\"video\",\"title\":\"影片介紹\",\"size\":7,\"language\":true}]}'),
(10, 'brand', 'brand_file', '', '', '', '產品目錄', 0, 1, 1, '{\"button\":{\"add\":true,\"del\":true,\"filter_brand\":true,\"search\":true},\"set\":{\"search\":[\"file_title\"],\"join\":[{\"table\":\"brand\",\"primary\":\"brand_id\",\"on_primary\":\"host_id\"}],\"del\":{\"file\":{\"column\":{}},\"related_data\":[],\"check\":[]},\"sort\":{\"mode\":\"ASC\"},\"plugins\":{\"form\":true,\"button\":true}},\"list\":[{\"column\":\"file_title\",\"type\":\"text\",\"title\":\"名稱\",\"sort\":true},{\"column\":\"brand_title\",\"type\":\"text\",\"title\":\"品牌\",\"class\":\"col-xlarge center\",\"hide\":true,\"sort\":true},{\"column\":\"file_online\",\"type\":\"switch\",\"title\":\"上架\",\"class\":\"col-smedium center\",\"sort\":true},{\"type\":\"edit\",\"title\":\"操作\",\"class\":\"col-lmedium center\"}],\"form\":[{\"column\":\"file_title\",\"type\":\"text\",\"title\":\"檔案名稱\",\"size\":5,\"required\":true,\"language\":true},{\"column\":\"file_online\",\"type\":\"switch\",\"title\":\"上架\",\"size\":5,\"checked\":true},{\"column\":\"host_id\",\"type\":\"select\",\"data\":{\"type\":\"db\",\"table\":\"brand\",\"value\":\"brand_id\",\"title\":\"brand_title\",\"search\":true},\"title\":\"品牌\",\"size\":5,\"required\":true},{\"column\":\"file_file\",\"type\":\"file\",\"title\":\"中文版檔案\",\"size\":5,\"info\":\"上傳模式：單檔<br>大小限制：5MB\"},{\"column\":\"file_file_en\",\"type\":\"file\",\"title\":\"英文版檔案\",\"size\":5,\"info\":\"上傳模式：單檔<br>大小限制：5MB\"}]}'),
(11, 'news', 'news', '', '', '', '最新消息', 0, 1, 1, '{\"button\":{\"add\":true,\"del\":true,\"categories\":true,\"search\":true},\"set\":{\"search\":[\"news_title\"],\"join\":[{\"table\":\"news_cate\",\"primary\":\"cate_id\",\"on_primary\":\"news_cate\"}],\"extra_column\":{\"brand_title\":\"IFNULL((SELECT `brand_title` FROM `brand` WHERE `brand`.`brand_id` = `news`.`brand_id`), \'其他\')\"},\"del\":{\"file\":{\"column\":{}}},\"sort\":{\"mode\":\"ASC\"},\"categories\":{\"readonly\":false,\"level\":1,\"table\":\"news_cate\",\"form\":[{\"column\":\"cate_title\",\"type\":\"text\",\"title\":\"分類名稱\",\"size\":5,\"language\":true,\"required\":true}]}},\"list\":[{\"column\":\"news_title\",\"type\":\"text\",\"title\":\"標題\",\"sort\":true},{\"column\":\"news_date\",\"type\":\"date\",\"title\":\"上架日期\",\"sort\":true,\"issort\":\"d\",\"hide\":true,\"class\":\"col-medium center\"},{\"column\":\"news_date_end\",\"type\":\"date\",\"title\":\"下架日期\",\"sort\":true,\"hide\":true,\"class\":\"col-lmedium center\"},{\"column\":\"brand_title\",\"type\":\"text\",\"title\":\"品牌\",\"class\":\"col-large center\",\"sort\":true},{\"column\":\"cate_title\",\"type\":\"text\",\"title\":\"分類\",\"class\":\"col-medium center\",\"sort\":true,\"hide\":\"phone\"},{\"column\":\"news_online\",\"type\":\"switch\",\"title\":\"上架\",\"class\":\"col-smedium center\",\"sort\":true},{\"type\":\"edit\",\"title\":\"操作\",\"class\":\"col-lmedium center\"}],\"form\":[{\"column\":\"news_title\",\"type\":\"text\",\"title\":\"標題\",\"size\":5,\"language\":true,\"required\":true,\"info\":\"請至少輸入一個語系\"},{\"column\":\"news_online\",\"title\":\"上架狀態\",\"type\":\"radio\",\"data\":[{\"title\":\"上架\",\"value\":\"1\",\"checked\":true},{\"title\":\"下架\",\"value\":\"0\"}],\"size\":4},{\"column\":\"news_cate\",\"type\":\"cate\",\"title\":\"分類\",\"size\":5,\"required\":true},{\"column\":\"brand_id\",\"type\":\"select\",\"data\":{\"type\":\"db\",\"table\":\"brand\",\"value\":\"brand_id\",\"title\":\"brand_title\",\"extra\":[{\"value\":\"\",\"title\":\"其他\"}],\"search\":true},\"title\":\"品牌\",\"size\":5},{\"column\":\"news_photo\",\"type\":\"photo\",\"title\":\"圖片\",\"size\":5,\"thumbnail\":\"b,450,300\",\"need_size\":[450,300],\"required\":true},{\"column\":\"news_date\",\"type\":\"datetime\",\"default\":\"+0 day\",\"title\":\"上架時間\",\"size\":3,\"required\":true},{\"column\":\"news_date_end\",\"type\":\"datetime\",\"title\":\"下架時間\",\"size\":3},{\"column\":\"news_content\",\"type\":\"wysiwyg\",\"title\":\"內容\",\"size\":8,\"language\":true,\"required\":true,\"info\":\"請至少輸入一個語系\"}]}'),
(13, 'product', 'product', '', '', '', '產品資料', 0, 1, 1, '{\"button\":{\"add\":true,\"del\":true,\"sort\":true,\"date_filter\":{\"title\":\"建立日期區間\",\"column\":\"product_create_date\"},\"search\":true},\"set\":{\"plugins\":{\"page\":true,\"form\":true,\"edit\":true,\"control\":true,\"del\":true,\"extra_path\":true},\"del\":{\"related_data\":[{\"related_table\":\"product_album\",\"related_column\":\"host_id\",\"column\":\"product_id\"},{\"related_table\":\"product_price_currency\",\"related_column\":\"product_id\",\"column\":\"product_id\"},{\"related_table\":\"product_selling_price\",\"related_column\":\"product_id\",\"column\":\"product_id\"}],\"check\":[]},\"search\":[\"product_name\",\"product_code\",\"model_number\"],\"sort\":{\"mode\":\"ASC\"},\"join\":[{\"table\":\"model\",\"primary\":\"model_id\",\"on_primary\":\"product_model\"}],\"concat\":{\"product_code_all\":[\"`product_code`\",\"`product_ver_code`\"]}},\"list\":[{\"column\":\"product_code_all\",\"type\":\"text\",\"title\":\"編號\",\"sort\":true},{\"column\":\"product_create_date\",\"type\":\"text\",\"title\":\"建立時間\",\"sort\":true,\"issort\":\"d\",\"hide\":true},{\"column\":\"product_owner\",\"type\":\"plugins\",\"_type\":\"owner\",\"title\":\"賣家\",\"sort\":true,\"hide\":true},{\"column\":\"model_number\",\"type\":\"text\",\"title\":\"型號\",\"sort\":true,\"hide\":true},{\"column\":\"product_featured\",\"type\":\"switch\",\"title\":\"精選\",\"class\":\"col-smedium center\",\"sort\":true,\"hide\":true},{\"column\":\"product_online\",\"type\":\"switch\",\"title\":\"上架\",\"class\":\"col-smedium center\",\"sort\":true,\"hide\":true},{\"type\":\"edit\",\"title\":\"操作\",\"class\":\"col-lmedium center\"}],\"form\":[{\"column\":\"product_create_year\",\"type\":\"plugins\",\"_type\":\"product_create_year\",\"hidden\":true},{\"column\":\"product_name\",\"type\":\"text\",\"title\":\"產品名稱\",\"required\":true,\"language\":true,\"size\":5},{\"column\":\"product_code\",\"type\":\"plugins\",\"_type\":\"product_code\",\"title\":\"產品編號\",\"size\":3,\"required\":true},{\"column\":\"product_model\",\"type\":\"plugins\",\"_type\":\"product_model\",\"title\":\"型號\",\"size\":5,\"required\":true},{\"column\":\"product_serial\",\"type\":\"text\",\"title\":\"序號\",\"size\":3},{\"column\":\"product_engine_serial\",\"type\":\"text\",\"title\":\"引擎序號\",\"size\":3},{\"column\":\"product_year\",\"type\":\"text\",\"title\":\"年份\",\"size\":3,\"number\":true},{\"column\":\"product_hours\",\"type\":\"text\",\"title\":\"使用時數\",\"size\":3,\"number\":true},{\"column\":\"product_online\",\"title\":\"上架狀態\",\"type\":\"switch\",\"checked\":true,\"size\":4},{\"column\":\"product_report\",\"type\":\"file\",\"title\":\"檢驗報告\",\"size\":5,\"info\":\"上傳模式：單檔<br>大小限制：5MB\"},{\"column\":\"product_country\",\"type\":\"plugins\",\"_type\":\"product_country\",\"title\":\"地點\",\"size\":5,\"required\":true},{\"column\":\"product_remark\",\"type\":\"textarea\",\"title\":\"備註\",\"size\":5,\"rows\":5,\"required\":true,\"language\":true},{\"column\":\"product_photo\",\"type\":\"photo\",\"title\":\"列表圖片\",\"size\":5,\"required\":true,\"water_mark\":true,\"thumbnail\":\"b,395,297|c,80,80,c\",\"need_size\":[395,297]},{\"column\":\"product_currency\",\"type\":\"select\",\"title\":\"幣別\",\"size\":3,\"required\":true,\"data\":{\"type\":\"data\",\"data\":[{\"title\":\"台幣\",\"value\":\"TWD\"},{\"title\":\"港幣\",\"value\":\"HKD\"},{\"title\":\"美金\",\"value\":\"USD\"}]}},{\"column\":\"product_start_price\",\"type\":\"text\",\"title\":\"入手價\",\"size\":3,\"required\":true,\"number\":true},{\"column\":\"product_default_price\",\"type\":\"text\",\"title\":\"預設價格\",\"size\":3,\"required\":true,\"number\":true,\"info\":\"若輸入0將顯示來電洽詢\"},{\"type\":\"plugins\",\"_type\":\"product_price\",\"title\":\"特殊價格\",\"size\":9,\"required\":true,\"info\":\"●請注意，當同一會員設定一個以上特殊價格時將以狀態開啟的第一排序為主<br>●拖曳 <i class=\\\"fa fa-bars\\\"></i> 可進行排序\"},{\"type\":\"plugins\",\"_type\":\"has_edit\"},{\"column\":\"product_album\",\"type\":\"photo\",\"title\":\"產品相簿\",\"size\":7,\"multiple\":true,\"required\":true,\"water_mark\":true,\"table\":\"product_album\",\"thumbnail\":\"b,115,90,a|b,800,800,a\",\"need_size\":[800,800]}]}'),
(14, 'product', 'model', '', '', '', '機械資料', 0, 1, 1, '{\"button\":{\"add\":true,\"del\":true,\"categories\":true,\"search\":true},\"set\":{\"search\":[\"model_number\"],\"plugins\":{\"form\":true,\"edit\":true,\"control\":true,\"extra_path\":true},\"del\":{\"check\":[{\"table\":\"label\",\"column\":\"model_id\",\"column_name\":\"model_id\",\"msg\":\"尚有標籤使用此型號，無法刪除\"},{\"table\":\"download_files\",\"column\":\"model_id\",\"column_name\":\"model_id\",\"msg\":\"尚有參數表及其他文件使用此型號，無法刪除\"}]},\"join\":[{\"table\":\"model_cate\",\"primary\":\"cate_id\",\"on_primary\":\"model_cate\"},{\"table\":\"brand\",\"primary\":\"brand_id\",\"on_primary\":\"model_brand\"}],\"categories\":{\"level\":2,\"table\":\"model_cate\",\"lock_primary\":false,\"form\":[{\"column\":\"cate_title\",\"type\":\"text\",\"title\":\"分類名稱\",\"size\":8,\"required\":true,\"language\":true},{\"column\":\"cate_par_id\",\"type\":\"cate\",\"title\":\"上層分類\",\"size\":8,\"info\":\"如為主分類不需選擇\"},{\"column\":\"cate_code\",\"type\":\"text\",\"title\":\"分類編號\",\"size\":8,\"required\":true,\"unique\":true},{\"column\":\"cate_home\",\"title\":\"首頁顯示\",\"type\":\"radio\",\"primary_only\":true,\"data\":[{\"title\":\"開啟\",\"value\":\"1\"},{\"title\":\"關閉\",\"value\":\"0\",\"checked\":true}],\"size\":8},{\"column\":\"cate_app\",\"title\":\"APP顯示\",\"type\":\"radio\",\"primary_only\":true,\"data\":[{\"title\":\"開啟\",\"value\":\"1\"},{\"title\":\"關閉\",\"value\":\"0\",\"checked\":true}],\"size\":8},{\"column\":\"cate_photo\",\"type\":\"photo\",\"primary_only\":true,\"title\":\"商品圖片\",\"size\":8,\"info\":\"如首頁不顯示可不上傳<br>上傳模式：單檔<br>建議尺寸：790px * 790px<br>大小限制：5MB\",\"thumbnail\":\"b,395,395,s|b,790,790\"}]}},\"list\":[{\"column\":\"model_number\",\"type\":\"text\",\"title\":\"型號\",\"issort\":\"a\",\"sort\":true},{\"column\":\"model_version\",\"type\":\"text\",\"title\":\"版本\",\"hide\":true,\"sort\":true},{\"column\":\"model_weight\",\"type\":\"text\",\"title\":\"噸位\",\"hide\":true,\"sort\":true},{\"column\":\"brand_title\",\"type\":\"text\",\"title\":\"品牌\",\"hide\":true,\"sort\":true},{\"column\":\"cate_title\",\"type\":\"text\",\"title\":\"分類\",\"hide\":true,\"sort\":true},{\"column\":\"model_online\",\"type\":\"switch\",\"title\":\"上架\",\"class\":\"col-smedium center\",\"sort\":true},{\"type\":\"edit\",\"title\":\"操作\",\"class\":\"col-lmedium center\"}],\"form\":[{\"column\":\"model_number\",\"type\":\"text\",\"title\":\"型號\",\"size\":5,\"required\":true},{\"column\":\"model_brand\",\"type\":\"select\",\"data\":{\"type\":\"db\",\"table\":\"brand\",\"value\":\"brand_id\",\"title\":\"brand_title\",\"search\":true},\"title\":\"品牌\",\"size\":5,\"required\":true},{\"column\":\"model_weight\",\"type\":\"text\",\"title\":\"噸位\",\"size\":5,\"number\":true,\"required\":true},{\"column\":\"model_version\",\"type\":\"text\",\"title\":\"版本\",\"size\":5,\"required\":true},{\"column\":\"model_online\",\"title\":\"上架狀態\",\"type\":\"radio\",\"data\":[{\"title\":\"上架\",\"value\":\"1\",\"checked\":true},{\"title\":\"下架\",\"value\":\"0\"}],\"size\":4},{\"column\":\"model_cate\",\"type\":\"cate\",\"title\":\"分類\",\"size\":5,\"required\":true},{\"column\":\"model_datasheet\",\"type\":\"file\",\"title\":\"官方參數表\",\"size\":5,\"info\":\"上傳模式：單檔<br>大小限制：5MB\"}]}'),
(15, 'invoice', 'invoice', '', '', '', '帳單資料', 0, 1, 1, '{\"button\":{\"add\":true,\"del\":true,\"search\":true},\"set\":{\"plugins\":{\"form\":true},\"join\":[{\"table\":\"member\",\"primary\":\"member_uniqid\",\"on_primary\":\"member_uniqid\"}],\"search\":[\"invoice_code\"],\"del\":{\"file\":{\"column\":{}},\"related_data\":[],\"check\":[]},\"sort\":{\"mode\":\"ASC\"}},\"list\":[{\"column\":\"invoice_code\",\"type\":\"text\",\"title\":\"帳單編號\",\"sort\":true},{\"column\":\"member_name\",\"type\":\"text\",\"title\":\"會員\",\"hide\":true,\"sort\":true},{\"column\":\"invoice_date\",\"type\":\"date\",\"title\":\"日期\",\"hide\":true,\"sort\":true},{\"column\":\"invoice_status\",\"type\":\"text\",\"title\":\"帳單狀態\",\"alias\":\"get_invoice_status\",\"hide\":true,\"sort\":true},{\"type\":\"edit\",\"title\":\"操作\",\"class\":\"col-lmedium center\"}],\"form\":[{\"column\":\"invoice_code\",\"type\":\"text\",\"title\":\"帳單編號\",\"size\":5,\"required\":true},{\"column\":\"member_uniqid\",\"type\":\"plugins\",\"_type\":\"member\",\"title\":\"會員\",\"size\":3,\"required\":true},{\"column\":\"invoice_status\",\"title\":\"申請類型\",\"type\":\"radio\",\"data\":[{\"title\":\"申請中\",\"value\":\"1\",\"checked\":true},{\"title\":\"未付\",\"value\":\"2\"},{\"title\":\"已繳清\",\"value\":\"3\"},{\"title\":\"其他\",\"value\":\"4\"}],\"size\":6,\"required\":true},{\"column\":\"invoice_date\",\"type\":\"date\",\"title\":\"帳單日期\",\"size\":3,\"required\":true},{\"column\":\"invoice_file\",\"type\":\"file\",\"title\":\"檔案上傳\",\"size\":5,\"info\":\"上傳模式：單檔<br>大小限制：5MB\",\"required\":true}]}'),
(16, 'label', 'label', '', '', '', '標籤管理', 0, 1, 1, '{\"button\":{\"add\":true,\"del\":true,\"search\":true},\"set\":{\"plugins\":{\"form\":true},\"search\":[\"label_seria_num\"],\"del\":{\"file\":{\"column\":{}},\"related_data\":[],\"check\":[]},\"join\":[{\"table\":\"member\",\"primary\":\"member_uniqid\",\"on_primary\":\"member_uniqid\"}],\"sort\":{\"mode\":\"ASC\"}},\"list\":[{\"column\":\"label_seria_num\",\"type\":\"text\",\"title\":\"序號\",\"sort\":true},{\"column\":\"label_mode\",\"type\":\"text\",\"title\":\"申請類型\",\"hide\":true,\"sort\":true},{\"column\":\"member_name\",\"type\":\"text\",\"title\":\"會員\",\"hide\":true,\"sort\":true},{\"type\":\"edit\",\"title\":\"操作\",\"class\":\"col-lmedium center\"}],\"form\":[{\"column\":\"label_seria_num\",\"type\":\"text\",\"title\":\"序號\",\"size\":5,\"required\":true},{\"column\":\"label_engine_seria_number\",\"type\":\"text\",\"title\":\"引擎序號\",\"size\":5,\"required\":true},{\"column\":\"label_ex_factory_Yr\",\"type\":\"text\",\"title\":\"年分\",\"size\":5},{\"column\":\"member_uniqid\",\"type\":\"plugins\",\"_type\":\"member\",\"title\":\"會員\",\"size\":3,\"required\":true},{\"column\":\"invoice_id\",\"type\":\"select\",\"data\":{\"type\":\"db\",\"table\":\"invoice\",\"value\":\"invoice_id\",\"title\":\"invoice_code\",\"search\":true},\"title\":\"帳單\",\"size\":5,\"required\":true},{\"column\":\"model_id\",\"type\":\"plugins\",\"_type\":\"model\",\"title\":\"型號\",\"size\":3,\"required\":true},{\"column\":\"label_mode\",\"title\":\"申請類型\",\"type\":\"radio\",\"data\":[{\"title\":\"NRMM\",\"value\":\"NRMM\"},{\"title\":\"QPME\",\"value\":\"QPME\"}],\"size\":4,\"required\":true},{\"column\":\"label_NRMM_code\",\"type\":\"text\",\"title\":\"NRMM申請號碼\",\"size\":5},{\"column\":\"label_NRMM_label_code\",\"type\":\"text\",\"title\":\"NRMM標籤號碼\",\"size\":5},{\"column\":\"label_NRMM_charges\",\"type\":\"text\",\"title\":\"NRMM收費\",\"size\":5},{\"column\":\"label_NRMM_file\",\"type\":\"file\",\"title\":\"NRMM檔案上傳\",\"size\":5,\"info\":\"上傳模式：單檔<br>大小限制：5MB\"},{\"column\":\"label_NRMM_photo\",\"type\":\"photo\",\"title\":\"NRMM圖片上傳\",\"size\":5,\"thumbnail\":\"b,1000,1000\",\"need_size\":[\"最大1000\",\"最大1000\"]},{\"column\":\"label_QPME_label_code\",\"type\":\"text\",\"title\":\"QPME標籤號碼\",\"size\":5},{\"column\":\"label_QPME_expires\",\"type\":\"date\",\"title\":\"QPME有效日期\",\"size\":5},{\"column\":\"label_QPME_charges\",\"type\":\"text\",\"title\":\"QPME收費\",\"size\":5},{\"column\":\"label_QPME_file\",\"type\":\"file\",\"title\":\"QPME檔案上傳\",\"size\":5,\"info\":\"上傳模式：單檔<br>大小限制：5MB\"},{\"column\":\"label_QPME_photo\",\"type\":\"photo\",\"title\":\"QPME圖片上傳\",\"size\":5,\"thumbnail\":\"b,1000,1000\",\"need_size\":[\"最大1000\",\"最大1000\"]}]}'),
(17, 'product', 'download_files', '', '', '', '參數表及其他文件', 0, 1, 1, '{\"button\":{\"add\":true,\"del\":true,\"filter_brand\":true,\"search\":true},\"set\":{\"search\":[\"file_title\"],\"join\":[{\"table\":\"model\",\"primary\":\"model_id\",\"on_primary\":\"model_id\"}],\"del\":{\"file\":{\"column\":{}},\"related_data\":[],\"check\":[]},\"sort\":{\"mode\":\"ASC\"},\"plugins\":{\"form\":true},\"concat\":{\"model_number\":[\"`model_number`\",\"\'-\'\",\"`model_version`\"]}},\"list\":[{\"column\":\"model_number\",\"type\":\"text\",\"title\":\"型號\",\"sort\":true},{\"column\":\"file_price\",\"type\":\"text\",\"title\":\"參考價格\",\"sort\":true},{\"column\":\"file_online\",\"type\":\"switch\",\"title\":\"上架\",\"class\":\"col-smedium center\",\"sort\":true},{\"type\":\"edit\",\"title\":\"操作\",\"class\":\"col-lmedium center\"}],\"form\":[{\"column\":\"file_online\",\"type\":\"switch\",\"title\":\"上架\",\"size\":5,\"checked\":true},{\"column\":\"model_id\",\"type\":\"plugins\",\"_type\":\"model\",\"title\":\"型號\",\"size\":3,\"required\":true},{\"column\":\"file_viewer\",\"type\":\"plugins\",\"_type\":\"member\",\"title\":\"可觀看會員\",\"size\":3},{\"column\":\"file_price\",\"type\":\"text\",\"title\":\"參考價格\",\"size\":3,\"required\":true},{\"column\":\"file_file\",\"type\":\"file\",\"title\":\"檔案上傳\",\"size\":5,\"info\":\"上傳模式：單檔<br>大小限制：5MB\",\"required\":true}]}'),
(18, 'member', 'member', '', '', '', '會員管理', 0, 1, 1, '{\"button\":{\"del\":true,\"search\":true},\"set\":{\"search\":[\"member_name\",\"member_nickname\",\"member_mobile1\",\"member_mobile2\",\"member_email\",\"member_lineID\",\"member_skypeID\",\"member_company_name\",\"member_company_address\",\"member_company_tel\",\"member_company_fax\",\"member_company_email\"],\"concat\":{},\"del\":{\"file\":{\"column\":{}},\"related_data\":[],\"check\":[{\"table\":\"invoice\",\"column\":\"member_uniqid\",\"column_name\":\"member_uniqid\",\"msg\":\"尚有此會員的發票，無法刪除\"},{\"table\":\"label\",\"column\":\"member_uniqid\",\"column_name\":\"member_uniqid\",\"msg\":\"尚有此會員的標籤，無法刪除\"}]}},\"list\":[{\"column\":\"member_account\",\"type\":\"text\",\"title\":\"帳號\",\"sort\":true},{\"column\":\"member_company_name\",\"type\":\"text\",\"title\":\"公司名稱\",\"sort\":true,\"hide\":\"phone\"},{\"column\":\"member_name\",\"type\":\"text\",\"title\":\"聯絡人\",\"sort\":true,\"hide\":true},{\"column\":\"member_mobile1\",\"type\":\"text\",\"title\":\"連絡電話1\",\"hide\":true},{\"column\":\"member_shop\",\"type\":\"switch\",\"title\":\"商家\",\"class\":\"col-smedium center\",\"sort\":true,\"hide\":\"phone\"},{\"column\":\"member_have_business_before\",\"type\":\"switch\",\"title\":\"放售\",\"class\":\"col-smedium center\",\"sort\":true,\"hide\":true},{\"column\":\"member_online\",\"type\":\"switch\",\"title\":\"狀態\",\"class\":\"col-smedium center\",\"sort\":true,\"hide\":\"phone\"},{\"column\":\"member_company_tel\",\"type\":\"text\",\"title\":\"公司電話\",\"hide\":\"all\"},{\"column\":\"member_direct_line\",\"type\":\"text\",\"title\":\"分機\",\"hide\":\"all\"},{\"column\":\"member_direct_line\",\"type\":\"text\",\"title\":\"連絡電話\",\"hide\":\"all\"},{\"column\":\"member_mobile2\",\"type\":\"text\",\"title\":\"連絡電話2\",\"hide\":\"all\"},{\"column\":\"member_email\",\"type\":\"text\",\"title\":\"連絡信箱\",\"hide\":\"all\"},{\"column\":\"member_company_email\",\"type\":\"text\",\"title\":\"公司信箱\",\"hide\":\"all\"},{\"column\":\"member_company_fax\",\"type\":\"text\",\"title\":\"公司傳真\",\"hide\":\"all\"},{\"column\":\"member_lineID\",\"type\":\"text\",\"title\":\"LINE ID\",\"hide\":\"all\"},{\"column\":\"member_skypeID\",\"type\":\"text\",\"title\":\"Skype ID\",\"hide\":\"all\"},{\"column\":\"member_company_address\",\"type\":\"text\",\"title\":\"公司地址\",\"hide\":\"all\"},{\"column\":\"member_register\",\"type\":\"date\",\"title\":\"註冊日期\",\"hide\":\"all\"},{\"column\":\"member_lastlogin\",\"type\":\"date\",\"title\":\"最後登入\",\"hide\":\"all\"},{\"type\":\"edit\",\"title\":\"操作\",\"class\":\"col-lmedium center\"}],\"form\":[{\"column\":\"member_account\",\"type\":\"txt\",\"title\":\"帳號\",\"size\":5},{\"column\":\"member_register\",\"type\":\"txt\",\"title\":\"註冊日期\",\"size\":5},{\"column\":\"member_lastlogin\",\"type\":\"txt\",\"title\":\"最後登入時間\",\"size\":5},{\"column\":\"member_online\",\"title\":\"帳號狀態\",\"type\":\"switch\",\"size\":4},{\"column\":\"member_shop\",\"title\":\"商家\",\"type\":\"switch\",\"size\":4},{\"column\":\"member_have_business_before\",\"title\":\"曾經交易\",\"type\":\"switch\",\"size\":4},{\"column\":\"member_name\",\"type\":\"text\",\"title\":\"聯絡人姓名\",\"size\":4},{\"column\":\"member_nickname\",\"type\":\"text\",\"title\":\"聯絡人暱稱\",\"size\":4},{\"column\":\"member_direct_line\",\"type\":\"text\",\"title\":\"聯絡人分機\",\"size\":4},{\"column\":\"member_mobile1\",\"type\":\"text\",\"title\":\"聯絡人電話1\",\"size\":4},{\"column\":\"member_mobile2\",\"type\":\"text\",\"title\":\"聯絡人電話2\",\"size\":4},{\"column\":\"member_mobile_show\",\"type\":\"text\",\"title\":\"展示電話\",\"size\":4},{\"column\":\"member_email\",\"type\":\"text\",\"title\":\"聯絡人信箱\",\"size\":4},{\"column\":\"member_position\",\"type\":\"text\",\"title\":\"聯絡人職稱\",\"size\":4},{\"column\":\"member_lineID\",\"type\":\"text\",\"title\":\"LINE ID\",\"size\":4},{\"column\":\"member_skypeID\",\"type\":\"text\",\"title\":\"Skype ID\",\"size\":4},{\"column\":\"member_fb_id\",\"type\":\"text\",\"title\":\"Facebook ID\",\"size\":4}]}'),
(19, 'case', 'works_type', '', '', '', '工作類型', 0, 1, 1, '{\"button\":{\"add\":true,\"del\":true,\"search\":true},\"set\":{\"search\":[\"works_type_title\"],\"del\":{\"file\":{\"column\":{}},\"related_data\":[{\"related_table\":\"works_type_album\",\"related_column\":\"host_id\",\"column\":\"cate_id\"}],\"check\":[{\"table\":\"case\",\"column\":\"works_type_id\",\"column_name\":\"works_type_id\",\"msg\":\"尚有成功案例使用此標籤，無法刪除\"}]},\"sort\":{\"mode\":\"ASC\"}},\"list\":[{\"column\":\"works_type_title\",\"type\":\"text\",\"title\":\"名稱\",\"sort\":true},{\"column\":\"works_type_online\",\"type\":\"switch\",\"title\":\"上架\",\"class\":\"col-smedium center\",\"sort\":true},{\"type\":\"edit\",\"title\":\"操作\",\"class\":\"col-lmedium center\"}],\"form\":[{\"column\":\"works_type_title\",\"type\":\"text\",\"title\":\"類型名稱\",\"size\":5,\"required\":true,\"language\":true},{\"column\":\"works_type_online\",\"type\":\"switch\",\"title\":\"上架\",\"size\":5,\"required\":true},{\"column\":\"works_type_url\",\"type\":\"text\",\"title\":\"網址參數\",\"size\":5,\"unique\":true,\"required\":true},{\"column\":\"works_type_content\",\"type\":\"textarea\",\"title\":\"說明內容\",\"size\":6,\"rows\":6,\"required\":true,\"language\":true},{\"column\":\"works_type_photo\",\"type\":\"photo\",\"title\":\"列表圖片\",\"size\":5,\"thumbnail\":\"b,747,621\",\"need_size\":[747,621],\"required\":true},{\"column\":\"works_type_album\",\"type\":\"photo\",\"title\":\"Slider\",\"multiple\":true,\"size\":6,\"required\":true,\"limit\":20,\"table\":\"works_type_album\",\"thumbnail\":\"b,115,90,a|b,945,590\",\"need_size\":[945,590]}]}'),
(20, 'about', 'about', 'module', 'module_single', '', '關於我們', 0, 0, 1, '{\"form\":[{\"column\":\"about_index\",\"type\":\"wysiwyg\",\"title\":\"首頁關於我們\",\"size\":7,\"language\":true,\"required\":true},{\"column\":\"about_contact\",\"type\":\"wysiwyg\",\"title\":\"聯絡表單關於我們\",\"size\":7,\"language\":true,\"required\":true}]}');

-- --------------------------------------------------------

--
-- 資料表結構 `system_unit`
--

CREATE TABLE `system_unit` (
  `system_unit_id` int(11) NOT NULL,
  `system_unit_code` varchar(50) NOT NULL,
  `system_unit_name` varchar(50) NOT NULL,
  `system_unit_icon` varchar(50) NOT NULL,
  `system_unit_par_id` int(11) NOT NULL,
  `system_unit_sort` int(11) NOT NULL,
  `system_unit_switch` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- 資料表的匯出資料 `system_unit`
--

INSERT INTO `system_unit` (`system_unit_id`, `system_unit_code`, `system_unit_name`, `system_unit_icon`, `system_unit_par_id`, `system_unit_sort`, `system_unit_switch`) VALUES
(1, 'admin', '後台管理員', 'user-secret', 0, 0, 1),
(2, 'slider', 'Slider管理', 'photo', 0, 1, 1),
(3, 'contact', '聯絡我們', 'envelope-o', 0, 8, 1),
(4, 'label_info', '標籤種類', 'tags', 0, 4, 1),
(6, 'brand', '品牌管理', 'diamond', 0, 3, 1),
(7, 'news', '最新消息', 'newspaper-o', 0, 1, 1),
(8, 'product', '產品管理', 'truck', 0, 2, 1),
(9, 'invoice', '帳單資料', 'credit-card', 0, 6, 1),
(10, 'label', '標籤管理', 'tag', 0, 5, 1),
(11, 'member', '會員管理', 'users', 0, 7, 1),
(12, 'case', '過往工作', 'thumbs-up', 0, 5, 1),
(13, 'about', '關於我們', 'id-card-o', 0, 8, 1);

-- --------------------------------------------------------

--
-- 資料表結構 `user`
--

CREATE TABLE `user` (
  `u_id` int(10) NOT NULL,
  `username` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `alias` varchar(50) NOT NULL,
  `user_online` tinyint(1) NOT NULL,
  `user_lock` tinyint(1) NOT NULL COMMENT '資料鎖定'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

--
-- 資料表的匯出資料 `user`
--

INSERT INTO `user` (`u_id`, `username`, `password`, `alias`, `user_online`, `user_lock`) VALUES
(1, 'yaii', '$2y$12$X.yYM5J.PCBRITybdVuKbegQ5zT3c1MWxOde8JeKx1/Mh51YH8cyO', '雅藝視覺設計', 1, 1),
(2, 'alex', '$2y$12$BJukyuUIjqV5IzVvPcSQMuyxXvguTEq72p/wWD2rDyIq18S5qEyIG', 'Alex', 1, 0),
(3, 'peter', '$2y$12$lInPy9.yJcr9hDm3b7P.ne4NW.BcCHh5ma3bnAfqWS6hCXpLJVw9u', 'Peter', 1, 0);

-- --------------------------------------------------------

--
-- 資料表結構 `works_type`
--

CREATE TABLE `works_type` (
  `works_type_id` int(11) NOT NULL,
  `works_type_title` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `works_type_title_en` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `works_type_url` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `works_type_content` text COLLATE utf8_unicode_ci NOT NULL,
  `works_type_content_en` text COLLATE utf8_unicode_ci NOT NULL,
  `works_type_photo` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `works_type_sort` int(11) NOT NULL,
  `works_type_online` tinyint(1) NOT NULL COMMENT '1-online;0-offline'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=COMPACT;

-- --------------------------------------------------------

--
-- 資料表結構 `works_type_album`
--

CREATE TABLE `works_type_album` (
  `album_id` int(11) NOT NULL,
  `host_id` int(11) NOT NULL,
  `album_photo` varchar(50) NOT NULL,
  `album_title` varchar(50) NOT NULL,
  `album_sort` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

--
-- 已匯出資料表的索引
--

--
-- 資料表索引 `about`
--
ALTER TABLE `about`
  ADD PRIMARY KEY (`about_id`);

--
-- 資料表索引 `activity_log`
--
ALTER TABLE `activity_log`
  ADD PRIMARY KEY (`activity_id`);

--
-- 資料表索引 `brand`
--
ALTER TABLE `brand`
  ADD PRIMARY KEY (`brand_id`);

--
-- 資料表索引 `brand_album`
--
ALTER TABLE `brand_album`
  ADD PRIMARY KEY (`album_id`),
  ADD KEY `host_id` (`host_id`);

--
-- 資料表索引 `brand_file`
--
ALTER TABLE `brand_file`
  ADD PRIMARY KEY (`file_id`),
  ADD KEY `host_id` (`host_id`);

--
-- 資料表索引 `brand_video`
--
ALTER TABLE `brand_video`
  ADD PRIMARY KEY (`video_id`),
  ADD KEY `host_id` (`host_id`);

--
-- 資料表索引 `case`
--
ALTER TABLE `case`
  ADD PRIMARY KEY (`case_id`),
  ADD KEY `label_cate` (`works_type_id`);

--
-- 資料表索引 `case_album`
--
ALTER TABLE `case_album`
  ADD PRIMARY KEY (`album_id`),
  ADD KEY `host_id` (`host_id`);

--
-- 資料表索引 `contact`
--
ALTER TABLE `contact`
  ADD PRIMARY KEY (`contact_id`);

--
-- 資料表索引 `country_calling_codes`
--
ALTER TABLE `country_calling_codes`
  ADD PRIMARY KEY (`country_id`);

--
-- 資料表索引 `country_code`
--
ALTER TABLE `country_code`
  ADD PRIMARY KEY (`country_id`);

--
-- 資料表索引 `currency`
--
ALTER TABLE `currency`
  ADD PRIMARY KEY (`currency_id`);

--
-- 資料表索引 `download_files`
--
ALTER TABLE `download_files`
  ADD PRIMARY KEY (`file_id`),
  ADD KEY `host_id` (`model_id`);

--
-- 資料表索引 `email_send_temp`
--
ALTER TABLE `email_send_temp`
  ADD PRIMARY KEY (`send_id`);

--
-- 資料表索引 `email_verify`
--
ALTER TABLE `email_verify`
  ADD PRIMARY KEY (`email_verify_id`),
  ADD KEY `user_uniqid` (`member_uniqid`),
  ADD KEY `email_verify_code` (`email_verify_code_hash`);

--
-- 資料表索引 `invoice`
--
ALTER TABLE `invoice`
  ADD PRIMARY KEY (`invoice_id`);

--
-- 資料表索引 `label`
--
ALTER TABLE `label`
  ADD PRIMARY KEY (`label_id`),
  ADD KEY `member_uniqid` (`member_uniqid`),
  ADD KEY `model_id` (`model_id`);

--
-- 資料表索引 `label_info`
--
ALTER TABLE `label_info`
  ADD PRIMARY KEY (`label_info_id`),
  ADD KEY `label_info_url` (`label_info_url`);

--
-- 資料表索引 `login_log`
--
ALTER TABLE `login_log`
  ADD PRIMARY KEY (`log_id`);

--
-- 資料表索引 `member`
--
ALTER TABLE `member`
  ADD PRIMARY KEY (`member_id`),
  ADD KEY `member_uniqid` (`member_uniqid`);

--
-- 資料表索引 `model`
--
ALTER TABLE `model`
  ADD PRIMARY KEY (`model_id`);

--
-- 資料表索引 `model_cate`
--
ALTER TABLE `model_cate`
  ADD PRIMARY KEY (`cate_id`),
  ADD KEY `cate_id` (`cate_id`),
  ADD KEY `cate_par_id` (`cate_par_id`);

--
-- 資料表索引 `news`
--
ALTER TABLE `news`
  ADD PRIMARY KEY (`news_id`),
  ADD KEY `news_cate` (`news_cate`),
  ADD KEY `brand_id` (`brand_id`);

--
-- 資料表索引 `news_cate`
--
ALTER TABLE `news_cate`
  ADD PRIMARY KEY (`cate_id`),
  ADD KEY `cate_par_id` (`cate_par_id`);

--
-- 資料表索引 `number_control`
--
ALTER TABLE `number_control`
  ADD PRIMARY KEY (`number_id`);

--
-- 資料表索引 `product`
--
ALTER TABLE `product`
  ADD PRIMARY KEY (`product_id`);

--
-- 資料表索引 `product_album`
--
ALTER TABLE `product_album`
  ADD PRIMARY KEY (`album_id`);

--
-- 資料表索引 `product_price_currency`
--
ALTER TABLE `product_price_currency`
  ADD PRIMARY KEY (`product_price_currency_id`),
  ADD KEY `product_id` (`product_id`);

--
-- 資料表索引 `product_selling_price`
--
ALTER TABLE `product_selling_price`
  ADD PRIMARY KEY (`selling_id`);

--
-- 資料表索引 `product_selling_price_currency`
--
ALTER TABLE `product_selling_price_currency`
  ADD PRIMARY KEY (`product_price_currency_id`),
  ADD KEY `product_id` (`selling_id`);

--
-- 資料表索引 `rights`
--
ALTER TABLE `rights`
  ADD PRIMARY KEY (`rights_id`);

--
-- 資料表索引 `slider`
--
ALTER TABLE `slider`
  ADD PRIMARY KEY (`id`);

--
-- 資料表索引 `system_name`
--
ALTER TABLE `system_name`
  ADD PRIMARY KEY (`system_name_id`),
  ADD KEY `system_unit` (`system_unit`),
  ADD KEY `system_code` (`system_code`);

--
-- 資料表索引 `system_unit`
--
ALTER TABLE `system_unit`
  ADD PRIMARY KEY (`system_unit_id`),
  ADD KEY `system_unit_code` (`system_unit_code`),
  ADD KEY `system_unit_par_id` (`system_unit_par_id`);

--
-- 資料表索引 `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`u_id`);

--
-- 資料表索引 `works_type`
--
ALTER TABLE `works_type`
  ADD PRIMARY KEY (`works_type_id`),
  ADD KEY `label_info_url` (`works_type_url`);

--
-- 資料表索引 `works_type_album`
--
ALTER TABLE `works_type_album`
  ADD PRIMARY KEY (`album_id`),
  ADD KEY `host_id` (`host_id`);

--
-- 在匯出的資料表使用 AUTO_INCREMENT
--

--
-- 使用資料表 AUTO_INCREMENT `about`
--
ALTER TABLE `about`
  MODIFY `about_id` tinyint(1) NOT NULL AUTO_INCREMENT;

--
-- 使用資料表 AUTO_INCREMENT `activity_log`
--
ALTER TABLE `activity_log`
  MODIFY `activity_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=239;

--
-- 使用資料表 AUTO_INCREMENT `brand`
--
ALTER TABLE `brand`
  MODIFY `brand_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- 使用資料表 AUTO_INCREMENT `brand_album`
--
ALTER TABLE `brand_album`
  MODIFY `album_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- 使用資料表 AUTO_INCREMENT `brand_file`
--
ALTER TABLE `brand_file`
  MODIFY `file_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- 使用資料表 AUTO_INCREMENT `brand_video`
--
ALTER TABLE `brand_video`
  MODIFY `video_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- 使用資料表 AUTO_INCREMENT `case`
--
ALTER TABLE `case`
  MODIFY `case_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- 使用資料表 AUTO_INCREMENT `case_album`
--
ALTER TABLE `case_album`
  MODIFY `album_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- 使用資料表 AUTO_INCREMENT `contact`
--
ALTER TABLE `contact`
  MODIFY `contact_id` int(100) NOT NULL AUTO_INCREMENT;

--
-- 使用資料表 AUTO_INCREMENT `country_calling_codes`
--
ALTER TABLE `country_calling_codes`
  MODIFY `country_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=138;

--
-- 使用資料表 AUTO_INCREMENT `country_code`
--
ALTER TABLE `country_code`
  MODIFY `country_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=248;

--
-- 使用資料表 AUTO_INCREMENT `currency`
--
ALTER TABLE `currency`
  MODIFY `currency_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- 使用資料表 AUTO_INCREMENT `download_files`
--
ALTER TABLE `download_files`
  MODIFY `file_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- 使用資料表 AUTO_INCREMENT `email_send_temp`
--
ALTER TABLE `email_send_temp`
  MODIFY `send_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- 使用資料表 AUTO_INCREMENT `email_verify`
--
ALTER TABLE `email_verify`
  MODIFY `email_verify_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- 使用資料表 AUTO_INCREMENT `invoice`
--
ALTER TABLE `invoice`
  MODIFY `invoice_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- 使用資料表 AUTO_INCREMENT `label`
--
ALTER TABLE `label`
  MODIFY `label_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- 使用資料表 AUTO_INCREMENT `label_info`
--
ALTER TABLE `label_info`
  MODIFY `label_info_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- 使用資料表 AUTO_INCREMENT `login_log`
--
ALTER TABLE `login_log`
  MODIFY `log_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=54;

--
-- 使用資料表 AUTO_INCREMENT `member`
--
ALTER TABLE `member`
  MODIFY `member_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=61;

--
-- 使用資料表 AUTO_INCREMENT `model`
--
ALTER TABLE `model`
  MODIFY `model_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=69;

--
-- 使用資料表 AUTO_INCREMENT `model_cate`
--
ALTER TABLE `model_cate`
  MODIFY `cate_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;

--
-- 使用資料表 AUTO_INCREMENT `news`
--
ALTER TABLE `news`
  MODIFY `news_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- 使用資料表 AUTO_INCREMENT `news_cate`
--
ALTER TABLE `news_cate`
  MODIFY `cate_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- 使用資料表 AUTO_INCREMENT `number_control`
--
ALTER TABLE `number_control`
  MODIFY `number_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- 使用資料表 AUTO_INCREMENT `product`
--
ALTER TABLE `product`
  MODIFY `product_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- 使用資料表 AUTO_INCREMENT `product_album`
--
ALTER TABLE `product_album`
  MODIFY `album_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- 使用資料表 AUTO_INCREMENT `product_price_currency`
--
ALTER TABLE `product_price_currency`
  MODIFY `product_price_currency_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- 使用資料表 AUTO_INCREMENT `product_selling_price`
--
ALTER TABLE `product_selling_price`
  MODIFY `selling_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- 使用資料表 AUTO_INCREMENT `product_selling_price_currency`
--
ALTER TABLE `product_selling_price_currency`
  MODIFY `product_price_currency_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- 使用資料表 AUTO_INCREMENT `rights`
--
ALTER TABLE `rights`
  MODIFY `rights_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=260;

--
-- 使用資料表 AUTO_INCREMENT `slider`
--
ALTER TABLE `slider`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- 使用資料表 AUTO_INCREMENT `system_name`
--
ALTER TABLE `system_name`
  MODIFY `system_name_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- 使用資料表 AUTO_INCREMENT `system_unit`
--
ALTER TABLE `system_unit`
  MODIFY `system_unit_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- 使用資料表 AUTO_INCREMENT `user`
--
ALTER TABLE `user`
  MODIFY `u_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- 使用資料表 AUTO_INCREMENT `works_type`
--
ALTER TABLE `works_type`
  MODIFY `works_type_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- 使用資料表 AUTO_INCREMENT `works_type_album`
--
ALTER TABLE `works_type_album`
  MODIFY `album_id` int(11) NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
